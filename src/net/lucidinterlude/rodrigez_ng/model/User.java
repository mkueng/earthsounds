package net.lucidinterlude.rodrigez_ng.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
 
 
@Entity  
public class User extends EntityBase{
	 
	private String root;
	private String endpoint;
	private String firstName;
	private String lastName;
	private String nickName;
	private String email;
	private String gender;
	private String birthdate;
	private String deviceToken;
	private String password;
	private String clearance;
	private String verification;
	
	@OneToOne(fetch = FetchType.LAZY, optional=false, cascade = CascadeType.ALL)
	private Player player;
	 
	public User() {
		super();
		player = new Player();
	}
	
	public String getRoot(){
		return this.root;
	}
	
	public void setRoot(String root){
		this.root = root;
	}
	
	public String getEndpoint(){
		return this.endpoint;
	}
	
	public void setEndpoint(String endPoint){
		this.endpoint = endPoint;
	}
	
	public Player getPlayer(){
		return this.player;
	}
	public void setPlayer(Player player){
		this.player=player;
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	
	public String getEmail() {
		return email;
	}
 
	public void setEmail(String email) {
		this.email = email;
	}
 
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}
	
	public String getDeviceToken() {
		return deviceToken;
	}
	
	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getClearance() {
		return clearance;
	}

	public void setClearance(String clearance) {
		this.clearance = clearance;
	}

	public String getVerification() {
		return verification;
	}

	public void setVerification(String verification) {
		this.verification = verification;
	}
	
	
}
