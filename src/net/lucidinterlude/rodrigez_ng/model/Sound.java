package net.lucidinterlude.rodrigez_ng.model;

import javax.persistence.Entity;

import com.google.appengine.api.datastore.Text;

@Entity
public class Sound extends EntityBase implements Action {
	
	private String root;
	private String endpoint;
	private Text content;
	private String name;
	private Double duration;
	private Double sampleRate;
	private String category;
	private String type;
	
	public Sound(){
		super();
	}

	public String getRoot() {
		return root;
	}

	public void setRoot(String root) {
		this.root = root;
	}

	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	public String getContent() {
		return content.getValue();
	}

	public void setContent(String content) {
		this.content = new Text(content);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getDuration() {
		return duration;
	}

	public void setDuration(Double duration) {
		this.duration = duration;
	}

	public Double getSampleRate() {
		return sampleRate;
	}

	public void setSampleRate(Double sampleRate) {
		this.sampleRate = sampleRate;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
}
