package net.lucidinterlude.rodrigez_ng.model;

import java.util.ArrayList;

public class Locations {

	private ArrayList<Location> locations;
	
	public Locations(){
		
	}

	public ArrayList<Location> getLocations() {
		return locations;
	}

	public void setLocations(ArrayList<Location> locations) {
		this.locations = locations;
	}
	
}
