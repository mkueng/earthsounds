package net.lucidinterlude.rodrigez_ng.model;


import javax.persistence.Entity;
import javax.persistence.Id;


@Entity
public class NickName {

	@Id
	private String nickName;
	private String userId;

	public NickName(String nickName, String userId) {
		this.nickName = nickName;
		this.userId = userId;
	}
	
	public String getNickName(){
		return this.nickName;
	}
	public String getUserId(){
		return this.userId;
	}
	
	
}
