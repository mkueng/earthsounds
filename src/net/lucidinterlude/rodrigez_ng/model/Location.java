package net.lucidinterlude.rodrigez_ng.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.google.appengine.api.datastore.GeoPt;
import com.google.appengine.api.datastore.Key;

@Entity
public class Location {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Key key;
	private String timestamp;
	private GeoPt geoPt;
	private Double speed;
	private Double altitude;
	
	public Location(Double longitude, Double latitude, String timestamp,Double altitude,Double speed) {
		this.timestamp = timestamp;
		this.altitude = altitude;
		this.speed = speed;
		this.geoPt = new GeoPt(latitude.floatValue(),longitude.floatValue());
	}
	
	public String getTimestamp() {
		return timestamp;
	}
	public void setDate(String timestamp) {
		this.timestamp = timestamp;
	}
	public GeoPt getGeoPt() {
		return geoPt;
	}
	public void setGeoPt(GeoPt geoPt) {
		this.geoPt = geoPt;
	}

	public Double getSpeed() {
		return speed;
	}

	public void setSpeed(Double speed) {
		this.speed = speed;
	}

	public Double getAltitude() {
		return altitude;
	}

	public void setAltitude(Double altitude) {
		this.altitude = altitude;
	}
	
	
}
