package net.lucidinterlude.rodrigez_ng.model;

import java.util.Date;
import java.util.LinkedList;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import com.google.appengine.api.datastore.Key;
 

@Entity
public class Player {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Key key;
	
	@OneToOne(fetch = FetchType.EAGER, optional = false, cascade = CascadeType.ALL)
	private LinkedList<Location> locations = new LinkedList<Location>();
	
	private String		timestamp; 
	private Double		longitude;
	private Double 		latitude;
	private Double		altitude;
	private Double		speed;
	private String 		nickName;
	private String 		deviceToken;
	
	public Player() {
		Location location = new Location(0.0,0.0, new Date().toString(),0.0,0.0);
		this.longitude = 0.0;
		this.latitude = 0.0;
		this.altitude = 0.0;
		this.speed = 0.0;
		this.locations.add(location);
	}
	
	public Key getKey(){
		return this.key;
	}
		
	public String getTimestamp() {
		return timestamp;
	}
	
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	
	public Double getAltitude() {
		return altitude;
	}

	public void setAltitude(Double altitude) {
		this.altitude = altitude;
	}

	public Double getSpeed() {
		return speed;
	}

	public void setSpeed(Double speed) {
		this.speed = speed;
	}
	
	public String getNickName() {
		return this.nickName;
	}
	
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}
	
	public String getDeviceToken() {
		return deviceToken;
	}
	
	public void setLocations(LinkedList<Location> locations) {
		this.locations=locations;
	}
	public LinkedList<Location> getLocations(){
		return locations;
	}
	
	public void setLocation(Double longitude, Double latitude, String timestamp,Double altitude,Double speed) {
		Location location = new Location(longitude, latitude,timestamp,altitude,speed);
		if (this.locations.size() > 49 ) {
			this.locations.remove(this.locations.getFirst());
			this.locations.add(location);
		} else {
			this.locations.add(location);
		}
	}	
}
