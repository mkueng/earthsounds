package net.lucidinterlude.rodrigez_ng.model;

import javax.persistence.Entity;

@Entity 
public class UserVerification extends EntityBase {

	private String userId;
	private String verification;
	
	public UserVerification(){
		super();
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getVerification() {
		return verification;
	}

	public void setVerification(String verification) {
		this.verification = verification;
	}
}
