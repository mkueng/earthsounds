package net.lucidinterlude.rodrigez_ng.model;

public interface Action {

	public String getContent();
	public String getId();
	public String getType();
	
}
