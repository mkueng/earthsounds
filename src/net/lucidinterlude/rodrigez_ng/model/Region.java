package net.lucidinterlude.rodrigez_ng.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

import javax.persistence.Entity;

import com.google.appengine.api.datastore.GeoPt;

/**
 * rodrigez/Region
 * @author Marco Kueng
 */

@Entity
public class Region extends EntityBase {
	
	
	private String root;
	private String name;
	private Double mainGrid;
	private ArrayList<GeoPt> geoPoints;
	


	public Region() {
		UUID uuId = UUID.randomUUID();
		this.id = uuId.toString();
	}
	

	
	public String getRoot(){
		return this.root;
	}
	
	public void setRoot(String root){
		this.root = root;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getMainGrid() {
		return mainGrid;
	}

	public void setMainGrid(Double grid) {
		this.mainGrid = grid;
	}	
	
	public ArrayList<GeoPt> getGeoPoints() {
		return geoPoints;
	}

	public void setGeoPoints(ArrayList<GeoPt> geoPoints) {
		this.geoPoints = geoPoints;
	}	
}
