package net.lucidinterlude.rodrigez_ng.model;

/**
 * rodrigez/EntitymanagerFactory 
 * @author Marco Kueng
 */

import javax.persistence.*;

public final class EMF {
	
	private static final EntityManagerFactory emfInstance =
        Persistence.createEntityManagerFactory("transactions-optional");

    private EMF() {}

    public static EntityManagerFactory get() {
        return emfInstance;
    }
}