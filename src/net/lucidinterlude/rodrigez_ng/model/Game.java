package net.lucidinterlude.rodrigez_ng.model;

import com.google.appengine.api.datastore.Text;

public class Game extends EntityBase implements Action{

	private String root;
	private String endpoint;
	private Text content;
	private String name;
	private String type;
	
	public Game(){
		super();
	}
	
	public String getRoot() {
		return root;
	}

	public void setRoot(String root) {
		this.root = root;
	}

	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setContent(String content) {
		this.content = new Text(content);
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String getContent() {
		return content.getValue();
	}
}
