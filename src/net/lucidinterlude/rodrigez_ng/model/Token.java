package net.lucidinterlude.rodrigez_ng.model;

import javax.persistence.Entity;

@Entity
public class Token extends EntityBase{
	
	private String tokenString;
	private String nickName;
	private String clearance;
	
	public Token(){
		super();
	}

	public String getTokenString() {
		return tokenString;
	}

	public void setTokenString(String tokenString) {
		this.tokenString = tokenString;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getClearance() {
		return clearance;
	}

	public void setClearance(String clearance) {
		this.clearance = clearance;
	}
}
