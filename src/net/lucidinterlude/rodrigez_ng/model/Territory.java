package net.lucidinterlude.rodrigez_ng.model;

import java.util.ArrayList;
import javax.persistence.Entity;
import com.google.appengine.api.datastore.GeoPt;
import com.google.appengine.api.datastore.Text;

@Entity
public class Territory extends EntityBaseNoId{
	
	private String region;
	private String root;
	private String endpoint;
	private String name;
	private Double itFactor;
	private String status;
	private String owner;
	private String action;
	private String actionContent;
	private Double spacing;
	private ArrayList<GeoPt> geoPoints;
	
	public Territory() {
		super();
		geoPoints = new ArrayList<GeoPt>();
	}
	
	public Territory(String id) {
		this.id = id;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getRoot(){
		return this.root;
	}
	
	public void setRoot(String root){
		this.root = root;
	}
	
	public String getEndpoint(){
		return this.endpoint;
	}
	
	public void setEndpoint(String endPoint){
		this.endpoint = endPoint;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getItFactor() {
		return itFactor;
	}

	public void setItFactor(Double itFactor) {
		this.itFactor = itFactor;
	}
	
	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
	
	public String getActionContent() {
		return actionContent;
	}

	public void setActionContent(String actionContent) {
		this.actionContent = actionContent;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public Double getSpacing() {
		return spacing;
	}

	public void setSpacing(Double spacing) {
		this.spacing = spacing;
	}

	public ArrayList<GeoPt> getGeoPoints() {
		return geoPoints;
	}

	public void setGeoPoints(ArrayList<GeoPt> geoPoints) {
		this.geoPoints = geoPoints;
	}
}
