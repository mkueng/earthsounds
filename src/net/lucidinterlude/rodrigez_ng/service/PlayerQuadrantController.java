package net.lucidinterlude.rodrigez_ng.service;

import net.lucidinterlude.rodrigez_ng.application.Facade;
import net.lucidinterlude.rodrigez_ng.dto.ResponseDto;

import org.restlet.data.Form;
import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.Representation;
import org.restlet.resource.Delete;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
/**
 * rodrigez/PlayerQuadrantController
 * @author Marco Kueng
 */

public class PlayerQuadrantController extends ServerResource{
	private Gson gson 			= null;
	private Facade facade 		= null;
	private ResponseDto<Object> responseDto	= null;
	
	public PlayerQuadrantController() {
		gson	= new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssz").create();
		facade	= new Facade();
	}
	
	
	@Get("application/json")
	public Representation handleGet() {
		String id = (String) this.getRequestAttributes().get("id");
		Form queryParams = getReference().getQueryAsForm();
		String limit = queryParams.getFirstValue("limit");
		//String distance = queryParams.getFirstValue("distance");
		try {
			responseDto = facade.getPlayerQuadrants(id,limit);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		setStatus(responseDto.getResponse().getStatus());
		return new JsonRepresentation(gson.toJson(responseDto.getResponseRoot()));
	}
	
	@Delete("application/json")
	public Representation handleDelete() {
		String id = (String) this.getRequestAttributes().get("id");
		responseDto = facade.deletePlayerQuadrants(id);
		setStatus(responseDto.getResponse().getStatus());
		return new JsonRepresentation(gson.toJson(responseDto.getResponseRoot()));
	}
	
}
