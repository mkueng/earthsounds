package net.lucidinterlude.rodrigez_ng.service;


import java.util.HashMap;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.lucidinterlude.rodrigez_ng.application.Facade;
import net.lucidinterlude.rodrigez_ng.dto.ResponseDto;

import org.restlet.data.Form;
import org.restlet.data.Status;
import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.Representation;
import org.restlet.resource.Delete;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.Put;
import org.restlet.resource.ServerResource;

import com.google.appengine.api.taskqueue.Queue;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * rodrigez/TerritoryController
 * @author Marco Kueng
 * (c) 2013 lucidinterlude
 */

public class TerritoryController extends ServerResource{
	private Gson gson = null;
	private Facade facade = null;

	final static Logger logger = Logger.getLogger(RegionController.class.getName());

	public TerritoryController() {
		gson	= new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssz").create();
		facade	= new Facade();
	}

//POST
	@Post("application/json")
	public Representation handlePost(JsonRepresentation jsonRepresentation){
		JsonRepresentation jsonRep = null;
	
		try {
			Queue queue = facade.setTerritory(jsonRepresentation.getJsonObject(),getRequest().getResourceRef().toString());
			setStatus(Status.SUCCESS_OK);
			
			jsonRep = new JsonRepresentation(gson.toJson(queue.fetchStatistics()));
		} catch (Exception ex) {
			logger.log(Level.SEVERE,"bad request "+ ex);
			setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
			jsonRep = new JsonRepresentation(gson.toJson("creation failed"));
		}
		return jsonRep;
	}
	
//GET
	@Get("application/json") 
	public Representation handleGet() {
		JsonRepresentation jsonRep = null;
		ResponseDto<Object> responseDto = null;
		
		String id = (String) this.getRequestAttributes().get("id");
		String resource = getRequest().getResourceRef().toString();
		
		//pathParam id
		if (id != null) {
			try {
				responseDto = facade.getTerritory(id,resource);
				setStatus(Status.SUCCESS_OK);
			} catch (Exception ex) {
				logger.log(Level.SEVERE,"bad request "+ ex);
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
			}
		} else {
			Form queryParams 	= getReference().getQueryAsForm();
			try {
				HashMap<String,String> paramMap = new HashMap<String,String>();
				Set<String> paramNames = queryParams.getNames();
				for (String param : paramNames) {
					paramMap.put(param, queryParams.getFirstValue(param));
				}
				// requestParams
				if (!paramMap.isEmpty()) {
					if (paramMap.containsKey("number")) {
						paramMap.remove("number");
						responseDto = facade.getNumberOfTerritories(paramMap,  getRequest().getResourceRef().toString());
						setStatus(Status.SUCCESS_OK);
					}else {
					responseDto = facade.getTerritoriesByParams(paramMap, getRequest().getResourceRef().toString());
					setStatus(Status.SUCCESS_OK);
					}
				}
				else {
					responseDto = facade.getAllTerritories(resource);
					setStatus(Status.SUCCESS_OK);
				}
			} catch (Exception ex) {
				logger.log(Level.SEVERE,"bad request "+ ex);
				System.out.println(ex);
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
			}
		}
		if (responseDto != null) {
			jsonRep = new JsonRepresentation(gson.toJson(responseDto.getResponseRoot()));
		}
		return jsonRep;
	}
	
//PUT
	@Put("application/json")
	public Representation handlePut(JsonRepresentation jsonRepresentation){
		JsonRepresentation jsonRep	= null;
		String id = (String) this.getRequestAttributes().get("id");
		try {
			facade.updateTerritory(id, jsonRepresentation);
			setStatus(Status.SUCCESS_OK);
			jsonRep = new JsonRepresentation(gson.toJson("updated ok"));
		} catch (Exception ex) {
			logger.log(Level.SEVERE,"bad request "+ ex);
			jsonRep=new JsonRepresentation("failed");
			setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
		}
		
		return jsonRep;
	}
	
//DELETE
	@Delete("application/json")
	public Representation handleDelete(JsonRepresentation jsonRepresentation){
		String id = (String) this.getRequestAttributes().get("id");
		Form queryParams 	= getReference().getQueryAsForm();
		JsonRepresentation jsonRep 	= null;
		
		if (id !=null) {
			try {
				facade.deleteTerritory(id);
				setStatus(Status.SUCCESS_OK);
			} catch (Exception e) {
				System.out.println(e);
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
			}
		} else if (queryParams.getFirst("region") !=null){
			try {
				facade.deleteAllTerritoriesFromRegion(queryParams.getFirstValue("region"));
				System.out.println("deleted ok");
				setStatus(Status.SUCCESS_OK);
				jsonRep = new JsonRepresentation(gson.toJson("deleted ok"));
				
			} catch (Exception e) {
				System.out.println(e);
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				jsonRep = new JsonRepresentation(gson.toJson("deletion failed"));
			}
		}
		return jsonRep;
	}
}
