package net.lucidinterlude.rodrigez_ng.service;

import java.util.logging.Logger;

import org.restlet.data.Status;
import org.restlet.engine.header.Header;
import org.restlet.engine.header.HeaderConstants;
import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.Representation;
import org.restlet.resource.Delete;
import org.restlet.resource.Get;
import org.restlet.resource.Options;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;
import org.restlet.util.Series;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import net.lucidinterlude.rodrigez_ng.application.Facade;
import net.lucidinterlude.rodrigez_ng.dto.ResponseDto;

/**
 * rodrigez/GameController
 * @author Marco Kueng
 * (c) 2017 lucidinterlude
 */

public class GameController extends ServerResource{
	private Gson gson 			= null;
	private Facade facade 		= null;
	private ResponseDto<Object> responseDto	= null;
	
	final static Logger logger = Logger.getLogger(GameController.class.getName());

	public GameController(){
		gson	= new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssz").create();
		facade	= new Facade();
	}
	
	
	//OPTIONS
	@SuppressWarnings({ "unchecked" })
	@Options("Application/Json")
	public void generateResponseHeaders() {
		System.out.println("options");
		Series<Header> responseHeaders = ((Series<Header>) getResponse().getAttributes().get(HeaderConstants.ATTRIBUTE_HEADERS));
		if (responseHeaders == null) {
			responseHeaders = new Series<Header>(Header.class);
			getResponse().getAttributes().put(HeaderConstants.ATTRIBUTE_HEADERS,responseHeaders);
		}
		responseHeaders.add("Access-Control-Allow-Methods", "POST, GET, DELETE, OPTIONS");
		responseHeaders.add("Access-Control-Allow-Headers", "content-type");
		responseHeaders.add(new Header("Access-Control-Allow-Origin", "*"));
	}
	
	//POST	
	@Post("application/json")
	public Representation handlePost(JsonRepresentation jsonRepresentation) {
		JsonRepresentation jsonRep = null;
		try {
			responseDto = facade.createGame(jsonRepresentation,getRequest().getResourceRef().toString());
			setStatus(Status.SUCCESS_OK);
			jsonRep = new JsonRepresentation(gson.toJson(responseDto.getResponseRoot()));
		} catch (Exception e) {
			System.out.println(e);
			setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
		}
		return jsonRep;
	}	
	
	//GET
	@Get("application/json")
	public Representation handleGet(){
		String id = (String) this.getRequestAttributes().get("id");
		
		try {
			if (id == null) {
				responseDto = facade.getGames(getRequest().getResourceRef().toString());
			} else {
				responseDto = facade.getGame(id, getRequest().getResourceRef().toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		setStatus(Status.SUCCESS_OK);
		return new JsonRepresentation(gson.toJson(responseDto.getResponseRoot()));

	}
	
	@Delete("application/json")
	public Representation handleDelete() {
		String id = (String) this.getRequestAttributes().get("id");
		responseDto = facade.deleteGame(id);
		setStatus(responseDto.getResponse().getStatus());
		return new JsonRepresentation(gson.toJson(responseDto.getResponseRoot()));
	}
}
