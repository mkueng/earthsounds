package net.lucidinterlude.rodrigez_ng.service;

import net.lucidinterlude.rodrigez.authentication.MailAuthentication;
import net.lucidinterlude.rodrigez_ng.application.Facade;
import net.lucidinterlude.rodrigez_ng.application.MailServlet;
import net.lucidinterlude.rodrigez_ng.dto.ResponseDto;
import net.lucidinterlude.rodrigez_ng.model.User;
import net.lucidinterlude.rodrigez_ng.utility.PasswordAuthentication;

import org.restlet.data.Form;
import org.restlet.data.Parameter;
import org.restlet.data.Status;
import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.Representation;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class UserSignUpController extends ServerResource {
	private Gson gson 						= null;
	private Facade facade 					= null;
	private ResponseDto<Object> responseDto	= null;
	
	public UserSignUpController(){
		gson	= new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssz").create();
		facade	= new Facade();
	}
	
	
	@Get("application/json")
	public Representation handleGet() {
		Form queryParams 	= getReference().getQueryAsForm();
		String verification = "";
		String verificationMessage = "";
		for (Parameter parameter : queryParams) {
		    if (parameter.getName().equals("ver")) {
		    	verification = parameter.getValue();
		    }
		
		};
		if (facade.checkVerificationEntry(verification)) {
			setStatus(Status.SUCCESS_OK);
			verificationMessage = "Your account has been verified!";
		} else {
			setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
			verificationMessage = "Verification failed, please contact admin";
		}
			
		return new JsonRepresentation(gson.toJson(verificationMessage));
	}
	
	
	@Post("application/json")
	public Representation handlePost(JsonRepresentation jsonRepresentation) {
		try {
			responseDto = facade.createUser(jsonRepresentation, getRequest().getResourceRef().toString());
			
			User user = (User) responseDto.getResponseContent("user");
			MailAuthentication mailauth = new MailAuthentication();
			String link = mailauth.createVerificationLink(user.getNickName());
			System.out.println("link: "+link);
			if (facade.createVerificationEntry(user.getId(), link )) {
				MailServlet mailServlet = new MailServlet();
				mailServlet.sendSimpleMail("https://lucid-rodrigez.appspot.com/rodrigez/api/userverification?ver="+link, user.getNickName(), user.getEmail());
			};
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		setStatus(Status.SUCCESS_OK);
		return new JsonRepresentation(gson.toJson(responseDto.getResponseRoot()));
		  
	}
}
