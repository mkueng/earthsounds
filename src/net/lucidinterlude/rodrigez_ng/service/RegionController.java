package net.lucidinterlude.rodrigez_ng.service;

import java.util.HashMap;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.lucidinterlude.rodrigez_ng.application.Facade;
import net.lucidinterlude.rodrigez_ng.dto.ResponseDto;

import org.restlet.data.Form;
import org.restlet.data.Status;
import org.restlet.engine.header.Header;
import org.restlet.engine.header.HeaderConstants;
import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.Representation;
import org.restlet.resource.Delete;
import org.restlet.resource.Get;
import org.restlet.resource.Options;
import org.restlet.resource.Post;
import org.restlet.resource.Put;
import org.restlet.resource.ServerResource;
import org.restlet.util.Series;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * rodrigez/RegionController
 * @author Marco Kueng
 * (c) 2013 lucidinterlude
 */

public class RegionController extends ServerResource{
	private Gson gson 			= null;
	private Facade facade 		= null;

	final static Logger logger = Logger.getLogger(RegionController.class.getName());
	
	public RegionController(){
		gson		= new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssz").create();
		facade	= new Facade();
	}
	
	
//OPTIONS
	@SuppressWarnings({ "unchecked" })
	@Options("Application/Json")
	public void generateResponseHeaders() {
		System.out.println("options");
		Series<Header> responseHeaders = ((Series<Header>) getResponse().getAttributes().get(HeaderConstants.ATTRIBUTE_HEADERS));
		if (responseHeaders == null) {
			responseHeaders = new Series<Header>(Header.class);
			getResponse().getAttributes().put(HeaderConstants.ATTRIBUTE_HEADERS,responseHeaders);
		}
		responseHeaders.add("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
		responseHeaders.add("Access-Control-Allow-Headers", "content-type");
		responseHeaders.add(new Header("Access-Control-Allow-Origin", "*"));
	}

//POST	
	@Post("application/json")
	public Representation handlePost(JsonRepresentation jsonRepresentation) {
		JsonRepresentation jsonRep = null;
		try {
			ResponseDto<Object> responseDto = facade.setRegion(jsonRepresentation,getRequest().getResourceRef().toString());
			setStatus(Status.SUCCESS_OK);
			jsonRep = new JsonRepresentation(gson.toJson(responseDto.getResponseRoot()));
		} catch (Exception e) {
			System.out.println(e);
			setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
		}
		return jsonRep;
	}	
	
//GET
	@Get("application/json")
	public Representation handleGet(){
	
		JsonRepresentation jsonRep 		= null;
		ResponseDto<Object> responseDto = null;
		
		String id = (String) this.getRequestAttributes().get("id");
		String resource = getRequest().getResourceRef().toString();
		Form queryParams = getReference().getQueryAsForm();

		// pathParam id
		if (id !=null) {
			try {
				responseDto = facade.getRegion(id, resource);
				setStatus(Status.SUCCESS_OK);
			} catch (Exception ex) {
				logger.log(Level.SEVERE,"bad request "+ ex);
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
			}
			
		// no pathParam
		} else {
			try {
				HashMap<String,String> paramMap = new HashMap<String,String>();
				Set<String> paramNames = queryParams.getNames();
				for (String param : paramNames) {
					paramMap.put(param, queryParams.getFirstValue(param));
				}
				
				// requestParams
				if (!paramMap.isEmpty()) {	
					responseDto = facade.getRegionByParams(paramMap,getRequest().getResourceRef().toString());
					setStatus(Status.SUCCESS_OK);

				// no requestParams
				} else {
					responseDto = facade.getAllRegions(resource);
					setStatus(Status.SUCCESS_OK);
				}
			} catch(Exception ex) {
				logger.log(Level.SEVERE,"bad request "+ ex);
				System.out.println(ex);
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
			}
		}
		if (responseDto != null) {
			jsonRep = new JsonRepresentation(gson.toJson(responseDto.getResponseRoot()));
		}
		return jsonRep;
	}
	
//UPDATE
	@Put("application/json")
	public Representation handleUpdate(JsonRepresentation jsonRepresentation) {
		JsonRepresentation jsonRep = null;
		ResponseDto<Object> responseDto = null;
		
		String id = (String) this.getRequestAttributes().get("id");
		try {
			responseDto = facade.updateRegion(id, jsonRepresentation, getRequest().getResourceRef().toString());
			jsonRep = new JsonRepresentation(gson.toJson(responseDto.getResponseRoot()));
			setStatus(Status.SUCCESS_OK);
		} catch (Exception ex) {
			logger.log(Level.SEVERE,"bad request "+ ex);
			setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
		}
		return jsonRep;
	}

//DELETE
	@Delete("application/json")
	public Representation handleDelete() {
		JsonRepresentation jsonRep = null;
		String id = (String) this.getRequestAttributes().get("id");
		try {
			facade.deleteRegion(id);
			setStatus(Status.SUCCESS_OK);
			jsonRep = new JsonRepresentation(gson.toJson("deleted ok"));
		} catch (Exception e) {
			System.out.println(e);
			setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
			jsonRep = new JsonRepresentation(gson.toJson("deletion failed"));
		}
		return jsonRep;
	}
}
