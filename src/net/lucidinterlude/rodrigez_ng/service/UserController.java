package net.lucidinterlude.rodrigez_ng.service;

/**
 * rodrigez/UserController
 * @author Marco Kueng
 */

import java.util.HashMap;
import net.lucidinterlude.rodrigez_ng.application.Facade;
import net.lucidinterlude.rodrigez_ng.dto.ResponseDto;

import org.restlet.data.Status;
import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.Representation;
import org.restlet.resource.Delete;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.Put;
import org.restlet.resource.ServerResource;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class UserController extends ServerResource{
	
	private Gson gson = null;
	private Facade facade = null;
	private ResponseDto<Object> responseDto	= null;
	public UserController() {
		gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssz").create();
		facade = new Facade();
		new HashMap<String,Object>();
	}
	
	@Post("application/json")
	public Representation handlePost(JsonRepresentation jsonRepresentation) {
		
			try {
				responseDto = facade.createUser(jsonRepresentation, getRequest().getResourceRef().toString());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		
		setStatus(Status.SUCCESS_OK);
		return new JsonRepresentation(gson.toJson(responseDto.getResponseRoot()));
	}
	
	@Get("application/json")
	public Representation handleGet() {
		if (!getRequest().getClientInfo().getUser().getIdentifier().equals("administrator")) {
			return null;
		} else {
			String id = (String) this.getRequestAttributes().get("id");
			try {	
				if (id == null) {
					responseDto = facade.getAllusers(getRequest().getResourceRef().toString());
				} else {
					responseDto = facade.getUser(id, getRequest().getResourceRef().toString());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			setStatus(Status.SUCCESS_OK);
			return new JsonRepresentation(gson.toJson(responseDto.getResponseRoot()));
		}
	}
	
	@Put("application/json")
	public Representation handleUpdate(JsonRepresentation jsonRepresentation) {
		if (!getRequest().getClientInfo().getUser().getIdentifier().equals("administrator")) {
			return null;
		} else {
			String id = (String) this.getRequestAttributes().get("id");
			responseDto = facade.updateUser(id, jsonRepresentation);
			setStatus(responseDto.getResponse().getStatus());
			return new JsonRepresentation(gson.toJson(responseDto.getResponseRoot()));
		}
	}
	
	@Delete("application/json")
	public Representation handleDelete() {
		if (!getRequest().getClientInfo().getUser().getIdentifier().equals("administrator")) {
			return null;
		} else {
			String id = (String) this.getRequestAttributes().get("id");
			responseDto = facade.deleteUser(id);
			setStatus(responseDto.getResponse().getStatus());
			return new JsonRepresentation(gson.toJson(responseDto.getResponseRoot()));
		}
	}
}
