package net.lucidinterlude.rodrigez_ng.service;

import net.lucidinterlude.rodrigez_ng.application.Facade;
import net.lucidinterlude.rodrigez_ng.dto.ResponseDto;

import org.restlet.Response;
import org.restlet.data.Form;
import org.restlet.data.Status;
import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.Representation;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class TargetController extends ServerResource{
	private Gson gson 			= null;
	private Facade facade 		= null;
	private ResponseDto<Object> responseDto	= null;

	
	public TargetController() {
		gson	= new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssz").create();
		facade	= new Facade();
	}

	@Get("application/json")
	public Representation handleGet() {
		String id = (String) this.getRequestAttributes().get("id");
		Form queryParams = getReference().getQueryAsForm();
		String distance = queryParams.getFirstValue("distance");
		if (distance != null) {
			responseDto = facade.getPlayersWithinDistance(id,distance);
		} else  {
			Response response = new Response(null);
			response.setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
			responseDto=new ResponseDto<Object>();
			responseDto.setResponse(response);
		}
		setStatus(responseDto.getResponse().getStatus());
		return new JsonRepresentation(gson.toJson(responseDto.getResponseRoot()));
	}
}
