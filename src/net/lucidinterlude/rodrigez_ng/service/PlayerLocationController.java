package net.lucidinterlude.rodrigez_ng.service;


import net.lucidinterlude.rodrigez_ng.application.Facade;
import net.lucidinterlude.rodrigez_ng.dto.ResponseDto;

import org.restlet.data.Form;
import org.restlet.data.Status;
import org.restlet.engine.header.Header;
import org.restlet.engine.header.HeaderConstants;
import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.Representation;
import org.restlet.resource.Get;
import org.restlet.resource.Options;
import org.restlet.resource.Put;
import org.restlet.resource.ServerResource;
import org.restlet.util.Series;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
	

/**
 * 
 * @author marcokueng
 *
 */
public class PlayerLocationController extends ServerResource{
	private Gson gson 			= null;
	private Facade facade 		= null;
	private ResponseDto<Object> responseDto	= null;
	
	public PlayerLocationController() {
		gson	= new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssz").create();
		facade	= new Facade();
	}

	
	//OPTIONS
		@SuppressWarnings({ "unchecked" })
		@Options("Application/Json")
		public void generateResponseHeaders() {
			Series<Header> responseHeaders = ((Series<Header>) getResponse().getAttributes().get(HeaderConstants.ATTRIBUTE_HEADERS));
			if (responseHeaders == null) {
				responseHeaders = new Series<Header>(Header.class);
				responseHeaders.add("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
				responseHeaders.add("Access-Control-Allow-Headers", "content-type");
				responseHeaders.add("Access-Control-Allow-Credentials", "*");
				responseHeaders.add("Access-Control-Allow-Origin", "*");
				getResponse().getAttributes().put(HeaderConstants.ATTRIBUTE_HEADERS,responseHeaders);
			}
		
		}
	
	
	@Get("application/json")
	public Representation handleGet() {
		String id = (String) this.getRequestAttributes().get("id");
		Form queryParams = getReference().getQueryAsForm();
		String limit = queryParams.getFirstValue("limit");
		String offset = queryParams.getFirstValue("offset"); 
		try {
			if (limit == null){
				responseDto = facade.getPlayerLocation(id, getRequest().getResourceRef().toString());
			} else {
				responseDto = facade.getPlayerLocationsWithLimit(id, limit, offset, getRequest().getResourceRef().toString());
			}
		} catch (Exception e){
			e.printStackTrace();
		}
		setStatus(Status.SUCCESS_OK);
		return new JsonRepresentation(gson.toJson(responseDto.getResponseRoot()));
	}
	
	@Put("application/json")
	public Representation handleUpdate(JsonRepresentation jsonRepresentation) {
		Series<Header> responseHeaders = ((Series<Header>) getResponse().getAttributes().get(HeaderConstants.ATTRIBUTE_HEADERS));
		if (responseHeaders == null) {
			responseHeaders = new Series<Header>(Header.class);
			
			responseHeaders.add("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
			responseHeaders.add("Access-Control-Allow-Headers", "content-type");
			responseHeaders.add("Access-Control-Allow-Credentials", "*");
			responseHeaders.add(new Header("Access-Control-Allow-Origin", "*"));
			getResponse().getAttributes().put(HeaderConstants.ATTRIBUTE_HEADERS,responseHeaders);
		}
		
		String id = (String) this.getRequestAttributes().get("id");
		try {
			if (!id.equals("userAgentPosition")) {
				responseDto = facade.updatePlayerLocation(id, jsonRepresentation);
			} else {
				responseDto = facade.updateUserAgentLocation(jsonRepresentation);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		setStatus(responseDto.getResponse().getStatus());
		
		return new JsonRepresentation(gson.toJson(responseDto.getResponseRoot()));
	}
	
}
