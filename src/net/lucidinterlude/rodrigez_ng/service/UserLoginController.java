package net.lucidinterlude.rodrigez_ng.service;

import net.lucidinterlude.rodrigez_ng.application.Facade;
import net.lucidinterlude.rodrigez_ng.dto.ResponseDto;
import org.restlet.data.Status;
import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.Representation;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class UserLoginController extends ServerResource {
	private Gson gson 						= null;
	private Facade facade 					= null;
	private ResponseDto<Object> responseDto	= null;
	
	public UserLoginController(){
		gson	= new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssz").create();
		facade	= new Facade();
	}
	
	@Post("application/json")
	public Representation handlePost(JsonRepresentation jsonRepresentation) {
		try {
			
			responseDto = facade.checkLogin(jsonRepresentation,getRequest().getResourceRef().toString());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		setStatus(Status.SUCCESS_OK);
		return new JsonRepresentation(gson.toJson(responseDto.getResponseRoot()));
		  
	}
}
