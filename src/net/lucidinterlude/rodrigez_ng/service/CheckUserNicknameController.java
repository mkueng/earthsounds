package net.lucidinterlude.rodrigez_ng.service;

import net.lucidinterlude.rodrigez_ng.application.Facade;
import net.lucidinterlude.rodrigez_ng.dto.ResponseDto;

import java.net.URI;
import java.util.Map;

import org.restlet.data.Form;
import org.restlet.data.Parameter;
import org.restlet.data.Status;
import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.Representation;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class CheckUserNicknameController extends ServerResource {
	private Gson gson 						= null;
	private Facade facade 					= null;
	private ResponseDto<Object> responseDto	= null;
	
	
	public CheckUserNicknameController() {
		gson	= new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssz").create();
		facade	= new Facade();
	}
	
	@Get("application/json") 
	public Representation handleGet() {
		Form queryParams 	= getReference().getQueryAsForm();
		String nickName = "";
		for (Parameter parameter : queryParams) {
		    if (parameter.getName().equals("nickName")) {
		    	nickName = parameter.getValue();
		    }
		};
		
		//URI decodedNickName = URI.create(nickName);
		//String decodedNickNameString = decodedNickName.getPath();
		try {
			responseDto = facade.checkUserNickName(nickName, getRequest().getResourceRef().toString());
			setStatus(Status.SUCCESS_OK);
			Map fields =  (Map) responseDto.getResponseContent("fields");
			
			if (fields.get("nickName") != null) {
				return new JsonRepresentation(gson.toJson(responseDto.getResponseRoot()));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		setStatus(Status.CLIENT_ERROR_NOT_FOUND);
		return new JsonRepresentation(gson.toJson(""));
		
	}
}
