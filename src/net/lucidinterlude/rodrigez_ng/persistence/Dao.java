package net.lucidinterlude.rodrigez_ng.persistence;

import java.beans.PropertyDescriptor;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.json.JSONObject;

import net.lucidinterlude.rodrigez_ng.model.EMF;
import net.lucidinterlude.rodrigez_ng.model.EntityBaseNoId;


/**
 * rodrigez/Dao
 * @author Marco Kueng
 * (c) 2013 lucidinterlude
 */

public class Dao implements IDao {
	
	

	/**
	 * Returns entity with provided id
	 * 
	 * @param id				the id of the entity to return
	 * @param entityClass	the class of the entities to query
	 * 
	 */
	
	public <T> T getEntityById(String id, Class<T> entityClass) throws Exception {
		
		Logger logger = Logger.getLogger(entityClass.getName());
		
		EntityManager em 		= EMF.get().createEntityManager();
		EntityTransaction et 	= em.getTransaction();
		T entity = null;
		try {
			et.begin();
			entity = em.find(entityClass, id);
			em.flush();
			et.commit();
		} catch (Exception ex){
			logger.log(Level.SEVERE,"Exception:"+ex);
			throw new Exception(ex);
			
		} finally {
			if (et.isActive()) et.commit();
			if (em.isOpen()) em.close();
		}
		return entity;
	}
	
	
	@SuppressWarnings("unchecked")
	public <T> List<T>  getAllEntities(Class<T> entityClass) throws Exception {
		Logger logger = Logger.getLogger(entityClass.getName());
		
		EntityManager em 		= EMF.get().createEntityManager();
		EntityTransaction et 	= em.getTransaction();
		
		List<T> queryResults = new ArrayList<T>();
		try {
			et.begin();
			Query query = em.createQuery("Select t from "+entityClass.getName()+" t");
			em.flush();
			et.commit();
			queryResults =  query.getResultList();
			
		} catch (Exception ex) {
			logger.log(Level.SEVERE,"Exception:"+ex);
			throw new Exception(ex);
		}
		return  queryResults;
	}
	
	
	/**
	 * Returns a List of Entities from a dynamic query based on provided parameters concatenated by AND
	 * 
	 * @param queryMap		a HashMap<String,Object> with key/value of the provided parameter
	 * @param modelClass		the class of the entities to query
	 * @return 						a List of found entities
	 */
	
	public <T> List<T> getEntityByParams(HashMap<String,Object> queryMap, Class<T> modelClass) throws Exception {
		
		Logger logger = Logger.getLogger(modelClass.getName());
		
		EntityManager em 		= EMF.get().createEntityManager();
		EntityTransaction et 	= em.getTransaction();
		List<Predicate> predicates = new ArrayList<Predicate>();
		List<T> allEntities = null;
		
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<T> cq = cb.createQuery(modelClass);
		Root<T> entity = cq.from(modelClass);
		cq.select(entity);
		
		//loop through queryMap building up predicates for query 
		for (Entry<String, Object> entry : queryMap.entrySet()) {
			String key = entry.getKey();
			Object value = entry.getValue();
			 if ((key != null) && (value != null)) {
				 try {
					 predicates.add(cb.equal(entity.get(key), value));
				 }catch (Exception ex) {
					 System.out.println(ex);
					 logger.log(Level.WARNING,"entryMap "+ modelClass.getName()+ " failure");
					 throw new Exception(ex);
				 }
			 }
		}
		cq.where(cb.and(predicates.toArray(new Predicate[predicates.size()])));
		
		//execute query
		try {
			et.begin();
			TypedQuery<T> query = em.createQuery(cq);
			et.commit();
			allEntities = query.getResultList();
		} catch (Exception ex) {
			et.rollback();
			em.close();
			logger.log(Level.WARNING,"getting "+ modelClass.getName()+ " by params failed");
			throw new Exception(ex);
		}finally {
			if (et.isActive()) et.commit();
			if (em.isOpen()) em.close();
		}
		return allEntities;
	}
	
	/**
	 * Updates an entity with the provided values from a JsonObject
	 * 
	 * @param id				id of the entity to update
	 * @param jsonObject	jsonObject with provided values
	 * @param entityClass	the class of the entity
	 * @return					entity with updated values
	 * @throws Exception
	 */
	
	public <T>  T updateEntityWithNoRelation(String id, JSONObject jsonObject, Class<T> entityClass) throws Exception {
		
		Logger logger = Logger.getLogger(entityClass.getName());
		
		EntityManager em 	= EMF.get().createEntityManager();
		EntityTransaction et 	= em.getTransaction();
		String key = null;
		T entity = null;
		try {
			et.begin();
			entity =  em.find(entityClass,id);
			Iterator<?> keys = jsonObject.keys();
			while(keys.hasNext()){
				key = (String) keys.next();
				PropertyDescriptor propDesc = new PropertyDescriptor(key,entity.getClass());
				propDesc.getWriteMethod().invoke(entity, jsonObject.get(key));
				propDesc=null;
			}
			((EntityBaseNoId) entity).setUpdatedAt(new Date());
			em.persist(entity);
			em.flush();
			et.commit();
		} catch (Exception ex) {
			logger.log(Level.WARNING,"updating "+(( EntityBaseNoId ) entity)+" failed");
			throw new Exception(ex);
		} finally {
			if (et.isActive()) et.commit();
			if (em.isOpen()) em.close();
		}
		return entity;
	}
	
	/**
	 * Deletes an entity 
	 * 
	 * @param id				id of the entity to delete
	 * @param entityClass	class of the entity
	 * @throws Exception	
	 */
	
	public <T>void  deleteEntityWithNoRelation(String id, Class<T> entityClass) throws Exception {
		
		Logger logger = Logger.getLogger(entityClass.getName());
		
		EntityManager em 		= EMF.get().createEntityManager();
		EntityTransaction et 	= em.getTransaction();
		 EntityBaseNoId entity = null;
		try {
			entity = (EntityBaseNoId) em.find(entityClass, id);
			if (entity !=null) {
				et.begin();
				em.remove(entity);
				em.flush();
				et.commit();
				em.getEntityManagerFactory().getCache().evictAll();
			}
		} catch (Exception ex) {
			logger.log(Level.WARNING,"deleting "+entity+" failed");
			throw new Exception(ex);
		} finally {
			if (et.isActive()) et.commit();
			if (em.isOpen()) em.close();
		}
	}
}
