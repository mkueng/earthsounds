package net.lucidinterlude.rodrigez_ng.persistence;

/**
 * rodrigez/IDao
 * @author Marco Kueng
 * (c) 2013 lucidinterlude
 */

import java.util.HashMap;
import java.util.List;

import org.json.JSONObject;

public interface IDao {
	public <T> T getEntityById (String id, Class<T> entityClass) throws Exception;
	public <T> List<T> getEntityByParams (HashMap<String,Object> queryMap, Class<T> modelClass) throws Exception;
	public <T> T updateEntityWithNoRelation	(String id, JSONObject jsonObject, Class <T> entityClass) throws Exception;
	public <T> void deleteEntityWithNoRelation(String id, Class<T> entityClass) throws Exception;
}
