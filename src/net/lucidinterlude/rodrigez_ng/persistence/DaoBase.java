package net.lucidinterlude.rodrigez_ng.persistence;

import org.restlet.Response;

import net.lucidinterlude.rodrigez_ng.dto.ResponseDto;


public class DaoBase<T> {
	protected Response			response;
	protected ResponseDto<T>	responseDto;
	
	public DaoBase() {
		response	= new Response(null);
		responseDto = new ResponseDto<T>();
	}
}
