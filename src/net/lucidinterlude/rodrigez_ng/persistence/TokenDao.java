package net.lucidinterlude.rodrigez_ng.persistence;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import org.restlet.data.Status;

import net.lucidinterlude.rodrigez_ng.dto.ResponseDto;
import net.lucidinterlude.rodrigez_ng.model.EMF;
import net.lucidinterlude.rodrigez_ng.model.NickName;
import net.lucidinterlude.rodrigez_ng.model.Player;
import net.lucidinterlude.rodrigez_ng.model.Token;
import net.lucidinterlude.rodrigez_ng.model.User;

public class TokenDao extends DaoBase<Object> {
	final static Logger logger = Logger.getLogger(TokenDao.class.getName());
	
	public TokenDao(){
	}

	public Token persistToken (Token token) throws Exception {
		EntityManager em = EMF.get().createEntityManager();
		EntityTransaction et = em.getTransaction();
		
		try{
			et.begin();
			em.persist(token);
			et.commit();
		} catch (javax.persistence.RollbackException ex) {
			et.rollback();
			em.close();
			System.out.println(ex);
			logger.log(Level.WARNING,"persisting sound failed");
			throw new Exception(ex);
			
		} finally {
			if (et.isActive()) et.commit();
			if (em.isOpen()) em.close();
		}
		
		return token;
	}
	
	public Token readToken(String tokenString) {
		EntityManager em = EMF.get().createEntityManager();
		EntityTransaction et = em.getTransaction();
		Token token = null;
		
		try {
			et.begin();
			Query query = em.createQuery("SELECT b from Token b WHERE b.tokenString = :tokenString").setParameter("tokenString", tokenString);
			et.commit();
			if (query.getResultList().isEmpty()) {
				response.setStatus(Status.CLIENT_ERROR_NOT_FOUND);
			}else {
				response.setStatus(Status.SUCCESS_OK);
				token= (Token) query.getResultList().get(0);
				
			}
			responseDto.setResponse(response);
		} finally{
			if (et.isActive()) {
				et.commit();
			}
			em.close();
		}
		return token;
	}
	
	public ResponseDto<Object> deleteToken(String id) {
		EntityManager em		= EMF.get().createEntityManager();
		EntityTransaction et	= em.getTransaction();
		try {
			et.begin();
			Token token = em.find(Token.class, id);
			em.remove(token);
			em.flush();
			et.commit();
			response.setStatus(Status.SUCCESS_OK, "user deleted");
			responseDto.setResponse(response);
		} catch (EntityNotFoundException ex) {	
			response.setStatus(Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY,"user not found");
			responseDto.setResponse(response);
		} finally {
			if(et.isActive()) {
				et.commit();
			}
			em.close();
		}
	return responseDto;
	
	}
}
