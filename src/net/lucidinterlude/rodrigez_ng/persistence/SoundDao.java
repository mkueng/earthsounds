package net.lucidinterlude.rodrigez_ng.persistence;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import org.restlet.data.Status;

import net.lucidinterlude.rodrigez_ng.dto.ResponseDto;
import net.lucidinterlude.rodrigez_ng.model.EMF;
import net.lucidinterlude.rodrigez_ng.model.Sound;

public class SoundDao extends DaoBase<Object> {

	final static Logger logger = Logger.getLogger(SoundDao.class.getName());
	
	public SoundDao(){		
	}
	
	// create sound
		public Sound createSound(Sound sound) throws Exception {
			EntityManager em = EMF.get().createEntityManager();
			EntityTransaction et = em.getTransaction();
			
			try{
				et.begin();
				em.persist(sound);
				et.commit();
			} catch (javax.persistence.RollbackException ex) {
				et.rollback();
				em.close();
				System.out.println(ex);
				logger.log(Level.WARNING,"persisting sound failed");
				throw new Exception(ex);
			} finally {
				if (et.isActive()) et.commit();
				if (em.isOpen()) em.close();
			}
			return sound;
		}
		
	// get sound
		public Sound getSound(String id) throws Exception {	
			EntityManager em = EMF.get().createEntityManager();
			EntityTransaction et = em.getTransaction();
			Sound sound = null;
			
			try {
				et.begin();
				sound = em.find(Sound.class,id);
				et.commit();
				
			} catch (EntityNotFoundException ex){	
				et.rollback();
				em.close();
				System.out.println(ex);
				logger.log(Level.WARNING,"getting sound failed");
				throw new Exception(ex);
			} 		
			finally {
				if (et.isActive()) et.commit();
				if (em.isOpen()) em.close();
			}
			return sound;
		}
	
	// get all sounds
		@SuppressWarnings("unchecked")
		public List<Sound> getSounds() throws Exception{
			EntityManager em = EMF.get().createEntityManager();
			EntityTransaction et = em.getTransaction();
			List<Sound> sounds = new ArrayList<Sound>();
			
			try {
				et.begin();
				Query query = em.createQuery("SELECT b from Sound b");
				et.commit();
				sounds = query.getResultList();
			} catch (Exception ex) {
				et.rollback();
				em.close();
				System.out.println(ex);
				logger.log(Level.WARNING,"getting all sounds failed");
				throw new Exception(ex);
			}finally {
				if (et.isActive()) et.commit();
				if (em.isOpen()) em.close();
			}
			return sounds;
		}
		
		
	/**
	 * delete sound by id
	 * @param id
	 * @return responseDto
	 */
	public ResponseDto<Object> deleteSound(String id) {
		EntityManager em		= EMF.get().createEntityManager();
		EntityTransaction et	= em.getTransaction();
		try {
			et.begin();
			Sound sound  = em.find(Sound.class, id);
			em.remove(sound);
			em.flush();
			et.commit();
			response.setStatus(Status.SUCCESS_OK, "sound deleted");
			responseDto.setResponse(response);
		} catch (EntityNotFoundException ex) {	
			response.setStatus(Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY,"user not found");
			responseDto.setResponse(response);
		} finally {
			if(et.isActive()) {
				et.commit();
			}
			em.close();
		}
	return responseDto;
	}
}
