package net.lucidinterlude.rodrigez_ng.persistence;



import org.json.JSONException;
import org.json.JSONObject;
import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * rodrigez/EntityCreator
 * @author Marco Kueng
 * (c) 2013 lucidinterlude
 */

public class EntityCreator {

	private PropertyDescriptor propertyDescriptor;
	private final static Logger LOGGER = Logger.getLogger(EntityCreator.class .getName());
	
	public EntityCreator() {
		LOGGER.setLevel(Level.WARNING);
	}
	
	/**
	 * Creates an entity based on the provided JsonObject and the entity class
	 * 
	 * @param jsonObject 
	 * @param entityClass
	 * @return a new entity
	 * @throws IntrospectionException
	 */
	
	
	@SuppressWarnings("unchecked")
	public <T> T createEntity(JSONObject jsonObject, Class<T> entityClass) throws IntrospectionException  {
		Class<?> cls;
		T entity = null;
		String key = null;
		try {
			cls = Class.forName(entityClass.getName());
			Iterator<?> keys = jsonObject.keys();
			entity = (T) cls.newInstance();
			try {
				
				while(keys.hasNext()){
					key = (String)keys.next();
					Object value = jsonObject.get(key);
					if (!key.equals("id") && (!key.equals("createdAt") &!key.equals("key")&!key.equals("geoPoints"))) {
						propertyDescriptor = new PropertyDescriptor(key,cls);
						if (propertyDescriptor.getPropertyType() == Double.class && value instanceof Integer) {
							int 	intValue  = (Integer) value;
							double 	doubleVal = intValue;
							Double  boxDouble = doubleVal;
							value = boxDouble;
						}
						propertyDescriptor.getWriteMethod().invoke(entity, value);
						propertyDescriptor=null;
					}
				};
			} catch (IntrospectionException e) {
				entity=null;
				LOGGER.warning("Introspection failed");
				throw(e);
			
			}
		} catch (InstantiationException e) {
			LOGGER.warning("Instantiation failed");
		} catch (IllegalAccessException e) {
			LOGGER.warning("Illegal Access");
		} catch (ClassNotFoundException e1) {
			LOGGER.warning("Class not found");
		} catch (IllegalArgumentException e) {
			 LOGGER.warning("Illegal Argument");
		} catch (InvocationTargetException e) {
			LOGGER.warning("Invocation Target failed");
			
		} catch (JSONException e) {
			LOGGER.warning("JSON not valid");
		} 	
		return entity;
		
	}
}
