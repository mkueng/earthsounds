package net.lucidinterlude.rodrigez_ng.persistence;

import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import net.lucidinterlude.rodrigez_ng.model.EMF;
import net.lucidinterlude.rodrigez_ng.model.Territory;

public class TerritoryDao extends Dao {
	final static Logger logger = Logger.getLogger(TerritoryDao.class.getName());
	
	public TerritoryDao(){	
	}
	
//set Territory
	public Territory setTerritory(Territory territory) throws Exception{
		EntityManager em 		= EMF.get().createEntityManager();
		EntityTransaction et 		= em.getTransaction();
		try {
			et.begin();
			territory.setCreatedAt(new Date());
			em.persist(territory);
			et.commit();
		}  catch (javax.persistence.RollbackException ex) {
			et.rollback();
			em.close();
			logger.log(Level.WARNING,"persisting territory failed");
			throw new Exception(ex);
		} finally {
			if (et.isActive()) et.commit();
			if (em.isOpen()){ em.close();}
		}
		return territory;
	}

	public void setTerritories(ArrayList<Territory> territories) throws Exception {
		EntityManager em 		= EMF.get().createEntityManager();
		EntityTransaction et 		= em.getTransaction();
	
		try {
			et.begin();
			for (Territory territory : territories) {
				territory.setCreatedAt(new Date());
				
					em.persist(territory);
			
			territory = null;
			}
			et.commit();
		}  catch (Exception ex) {
			et.rollback();
			em.close();
			System.out.println("rollback:"+ex);
			logger.log(Level.WARNING,"persisting territory failed");
			throw new Exception(ex);
		} finally {
			if (et.isActive()) et.commit();
			if (em.isOpen()){ em.close();}
		}
	}
	

	
}
