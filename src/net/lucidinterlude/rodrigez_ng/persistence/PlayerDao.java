package net.lucidinterlude.rodrigez_ng.persistence;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.EntityTransaction;

import org.json.JSONException;
import org.json.JSONObject;
import org.restlet.Response;
import org.restlet.data.Status;

import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;

import net.lucidinterlude.rodrigez_ng.dto.ResponseDto;
import net.lucidinterlude.rodrigez_ng.game.GameHandler;
import net.lucidinterlude.rodrigez_ng.model.EMF;
import net.lucidinterlude.rodrigez_ng.model.Location;
import net.lucidinterlude.rodrigez_ng.model.Player;
import net.lucidinterlude.rodrigez_ng.model.User;


public class PlayerDao  {
	
	public PlayerDao(){
		
	}
	
	/**
	 * get player by id
	 * @param id
	 * @return responseDto
	 */
	public Player getPlayer(String id) {
		Player player = null;
		EntityManager em = EMF.get().createEntityManager();
		EntityTransaction et = em.getTransaction();
	
		Response response = new Response(null);
		final Logger log = Logger.getLogger(GameHandler.class.getName());
		log.info("id"+id);	
		try {
			//et.begin();
			User user= em.find(User.class, id);
			//et.commit();
			log.info("User"+user);
			if (user!=null) {
				player = user.getPlayer();
				em.detach(player);
				
			}  else throw new EntityNotFoundException();
			
		}catch (EntityNotFoundException ex){	
			response.setStatus(Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY);
		
			
			
		} finally {
			if (et.isActive()) {
				et.commit();
			}
			em.close();
		}
		return player;
	}
	
	/**
	 * get player location
	 * @param player
	 * @return responseDto
	 */
	public Location getPlayerLocation(Player player) {	
		Location location = null;
		EntityManager em = EMF.get().createEntityManager();
		EntityTransaction et = em.getTransaction();
		ResponseDto<Object> responseDto = new ResponseDto<Object>();
		Response response = new Response(null);
		
		try {
			et.begin();
			if (player.getLocations().size()>1) {
				
				location = player.getLocations().getLast();
			}
			else {
				
			}
			response.setStatus(Status.SUCCESS_OK);
			responseDto.setResponse(response);
		} catch (EntityNotFoundException ex){	
			response.setStatus(Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY);
			responseDto.setResponse(response);
			responseDto.setResponseContent("player","not found");
		} finally {
			if (et.isActive()) {
				et.commit();
			}
			if (em.isOpen()) {
				em.close();
			}
		}
		return location;
	}
	
	/**
	 * 
	 * @param player
	 * @param limit
	 * @return
	 */
	public ArrayList<Location> getPlayerLocationWithLimit(Player player, String limit, String offset) {
		int length =0;
		int limitInt =0;
		int offsetInt = 0;
		
		Iterator<Location> it;
		ArrayList<Location> locationArray = new ArrayList<Location>();
		EntityManager em = EMF.get().createEntityManager();
		EntityTransaction et = em.getTransaction();
		
		try {
			et.begin();
			length = player.getLocations().size();
			limitInt = Integer.parseInt(limit)+1;
			limitInt = limitInt > length ? length : limitInt;
			List <Location> locations = (player.getLocations().subList(length-limitInt, length));
			locations = Lists.reverse(locations);
			//locations.remove(locations.size()-1);
			it =locations.iterator();
			while (it.hasNext()) {
				Location location  =(Location) it.next();
				locationArray.add(location);	
			}
			
		} catch (EntityNotFoundException ex){	
			
		} finally {
			if (et.isActive()) {
				et.commit();
			}
			if (em.isOpen()) {
				em.close();
			}
		}
		return locationArray;
	}
	 
	/**
	 * update player location
	 * @param id
	 * @param jsonObject
	 * @return responseDto
	 */
	public ResponseDto<Object> updatePlayerLocation(String id,JSONObject jsonObject) {
		EntityManager em = EMF.get().createEntityManager();
		EntityTransaction et = em.getTransaction();
		ResponseDto<Object> responseDto = new ResponseDto<Object>();
		Response response = new Response(null);
		String key = null;
		try {
			et.begin();
			User user = em.find(User.class, id);
			et.commit();
			if (user!=null) {
				Player player = user.getPlayer();
				Iterator<?> keys = jsonObject.keys();
					while(keys.hasNext()){
						key = (String) keys.next();
						Object value = jsonObject.get(key);
						PropertyDescriptor propDesc = new PropertyDescriptor(key,Player.class);
						if (propDesc.getPropertyType() == Double.class && value instanceof Integer) {
							int 	intValue  = (Integer) value;
							double 	doubleVal = intValue;
							Double  boxDouble = doubleVal;
							value = boxDouble;
						}
						propDesc.getWriteMethod().invoke(player, value);
						propDesc=null;
					}
					player.setLocation(player.getLongitude(), player.getLatitude(),player.getTimestamp(),player.getAltitude(),player.getSpeed());
					em.persist(player);
					if (et.isActive()) {
						et.commit(); 
					}
					response.setStatus(Status.SUCCESS_CREATED,"player location updated");
					responseDto.setResponse(response);
					responseDto.setResponseContent("player", player);
			} else throw new EntityNotFoundException();
		} catch (EntityNotFoundException ex){	 
			if (et.isActive()) {
				et.commit();
			}
			response.setStatus(Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY);
			responseDto.setResponse(response);
		} catch (IllegalArgumentException e) {
			if (et.isActive()) {
				et.commit();
			}
			response.setStatus(Status.CLIENT_ERROR_BAD_REQUEST,"illegal argument exception");
			responseDto.setResponse(response);
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			if (et.isActive()) {
				et.commit();
			}
			response.setStatus(Status.CLIENT_ERROR_BAD_REQUEST,"illegal access exception");
			responseDto.setResponse(response);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			if (et.isActive()) {
				et.commit();
			}
			response.setStatus(Status.CLIENT_ERROR_BAD_REQUEST,"invocation exception");
			responseDto.setResponse(response);
			e.printStackTrace();
		} catch (JSONException e) {
			if (et.isActive()) {
				et.commit();
			}
			response.setStatus(Status.CLIENT_ERROR_BAD_REQUEST,"json not valid");
			responseDto.setResponse(response);
			e.printStackTrace();
		} catch (IntrospectionException e) {
			if (et.isActive()) {
				et.commit();
			}
			response.setStatus(Status.CLIENT_ERROR_BAD_REQUEST,"introspection exception");
			responseDto.setResponse(response);
			
		} finally {
			if (et.isActive()) {
				et.commit();
			}
			em.close();
		}
		return responseDto;
	}
}
