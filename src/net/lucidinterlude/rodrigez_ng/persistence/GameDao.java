package net.lucidinterlude.rodrigez_ng.persistence;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import org.restlet.data.Status;

import net.lucidinterlude.rodrigez_ng.dto.ResponseDto;
import net.lucidinterlude.rodrigez_ng.model.EMF;
import net.lucidinterlude.rodrigez_ng.model.Game;

public class GameDao extends DaoBase<Object> {

	final static Logger logger = Logger.getLogger(GameDao.class.getName());
	
	public GameDao(){		
	}
	
	// create Game
		public Game createGame(Game game) throws Exception {
			EntityManager em = EMF.get().createEntityManager();
			EntityTransaction et = em.getTransaction();
			
			try{
				et.begin();
				em.persist(game);
				et.commit();
			} catch (javax.persistence.RollbackException ex) {
				et.rollback();
				em.close();
				System.out.println(ex);
				logger.log(Level.WARNING,"persisting Game failed");
				throw new Exception(ex);
			} finally {
				if (et.isActive()) et.commit();
				if (em.isOpen()) em.close();
			}
			return game;
		}
		
	// get Game
		public Game getGame(String id) throws Exception {	
			EntityManager em = EMF.get().createEntityManager();
			EntityTransaction et = em.getTransaction();
			Game game = null;
			
			try {
				et.begin();
				game = em.find(Game.class,id);
				et.commit();
				
			} catch (EntityNotFoundException ex){	
				et.rollback();
				em.close();
				System.out.println(ex);
				logger.log(Level.WARNING,"getting Game failed");
				throw new Exception(ex);
			} 		
			finally {
				if (et.isActive()) et.commit();
				if (em.isOpen()) em.close();
			}
			return game;
		}
	
	// get all Games
		@SuppressWarnings("unchecked")
		public List<Game> getGames() throws Exception{
			EntityManager em = EMF.get().createEntityManager();
			EntityTransaction et = em.getTransaction();
			List<Game> games = new ArrayList<Game>();
			
			try {
				et.begin();
				Query query = em.createQuery("SELECT b from Game b");
				et.commit();
				games = query.getResultList();
			} catch (Exception ex) {
				et.rollback();
				em.close();
				System.out.println(ex);
				logger.log(Level.WARNING,"getting all Games failed");
				throw new Exception(ex);
			}finally {
				if (et.isActive()) et.commit();
				if (em.isOpen()) em.close();
			}
			return games;
		}
		
		
	/**
	 * delete Game by id
	 * @param id
	 * @return responseDto
	 */
	public ResponseDto<Object> deleteGame(String id) {
		EntityManager em		= EMF.get().createEntityManager();
		EntityTransaction et	= em.getTransaction();
		try {
			et.begin();
			Game game  = em.find(Game.class, id);
			em.remove(game);
			em.flush();
			et.commit();
			response.setStatus(Status.SUCCESS_OK, "Game deleted");
			responseDto.setResponse(response);
		} catch (EntityNotFoundException ex) {	
			response.setStatus(Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY,"user not found");
			responseDto.setResponse(response);
		} finally {
			if(et.isActive()) {
				et.commit();
			}
			em.close();
		}
	return responseDto;
	}
}
