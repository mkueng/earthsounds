package net.lucidinterlude.rodrigez_ng.persistence;

/**
 * rodrigez/UserDao	
 * @author Marco Kueng
 */

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import org.json.JSONException;
import org.json.JSONObject;
import org.restlet.Response;
import org.restlet.data.Status;

import net.lucidinterlude.rodrigez_ng.dto.NicknameDto;
import net.lucidinterlude.rodrigez_ng.dto.ResponseDto;
import net.lucidinterlude.rodrigez_ng.dto.UserDto;
import net.lucidinterlude.rodrigez_ng.model.EMF;
import net.lucidinterlude.rodrigez_ng.model.NickName;
import net.lucidinterlude.rodrigez_ng.model.Player;
import net.lucidinterlude.rodrigez_ng.model.User;
import net.lucidinterlude.rodrigez_ng.model.UserVerification;

public class UserDao extends DaoBase<Object>  {
	
	final Logger logger = Logger.getLogger(UserDao.class.getName());
	
	public UserDao() {
		super();
		
	}
	
	public Boolean createVerificationEntry (UserVerification userVerification) {
		EntityManager em 			= EMF.get().createEntityManager();
		EntityTransaction et 		= em.getTransaction();
		Boolean isSuccessfullCreated = false;
		
		try {
			et.begin();
			em.persist(userVerification);
			et.commit();
		} catch (javax.persistence.RollbackException e) {
			e.printStackTrace();
			et.rollback();
			em.close();
			response.setStatus(Status.SERVER_ERROR_INTERNAL,"verification entry creation failed");
			logger.severe("verification entry creation failed");
			
		} finally {
			if (et.isActive()) et.commit();
			if (em.isOpen()) em.close();
			isSuccessfullCreated = true;
		}
		
		return isSuccessfullCreated;
	}
	
	
	public UserVerification getVerificationEntry(String verification) {
		EntityManager em		= EMF.get().createEntityManager();
		EntityTransaction et	= em.getTransaction();
		UserVerification userVerification = null;
		
		try {
			et.begin();
			Query query = em.createQuery("SELECT b from UserVerification b WHERE b.verification = :verification").setParameter("verification", verification);
			et.commit();
			if (query.getResultList().isEmpty()) {
				response.setStatus(Status.CLIENT_ERROR_NOT_FOUND);
			}else {
				response.setStatus(Status.SUCCESS_OK);
				userVerification= (UserVerification) query.getResultList().get(0);
				
			}
			
		} finally{
			if (et.isActive()) {
				et.commit();
			}
			em.close();
		}
		return userVerification;
	}
	
	public boolean deleteVerificationEntry(String id) {
		EntityManager em	= EMF.get().createEntityManager();
		EntityTransaction et = em.getTransaction();
		try {
			et.begin();
			UserVerification userVerification  = em.find(UserVerification.class, id);
			em.remove(userVerification);
			em.flush();
			et.commit();
			response.setStatus(Status.SUCCESS_OK, "user deleted");
			responseDto.setResponse(response);
		} catch (EntityNotFoundException ex) {
			response.setStatus(Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY,"user not found");
			responseDto.setResponse(response);
			return false;
		} finally {
			if(et.isActive()) {
				et.commit();
			}
			em.close();
		}
	return true;
	}
	
	
	
	
	/**
	 * 
	 * @param user
	 * @return responseDto
	 */
	public User createUser(User user){
		EntityManager em 			= EMF.get().createEntityManager();
		EntityTransaction et 		= em.getTransaction();
		Response nickNameResponse	= null;
		try {
			et.begin();
				nickNameResponse = createNickName(user.getNickName(),user.getId());
				if (nickNameResponse.getStatus().isError()) {
					em.close();
					et.rollback();
					response.setStatus(Status.SERVER_ERROR_INTERNAL,"user creation failed");
					responseDto.setResponse(response);
				} else {
					em.persist(user);
					user.getPlayer().setNickName(user.getNickName());
					user.getPlayer().setDeviceToken(user.getDeviceToken());
					responseDto.setResponseContent("id", user.getId());
					
					response.setStatus(Status.SUCCESS_CREATED,"User created");
					et.commit();
					responseDto.setResponse(response);
					logger.info("user created:"+user.getId()+"/"+user.getNickName());
				}
		} catch (javax.persistence.RollbackException e) {
			e.printStackTrace();
			et.rollback();
			em.close();
			response.setStatus(Status.SERVER_ERROR_INTERNAL,"user creation failed");
			responseDto.setResponse(response);
			logger.warning("user creation failed");
		}
		finally {
			if (et.isActive()) et.commit();
			if (em.isOpen()) em.close();
		}
		return user;
	}
	
	
	/**
	 * get user by id
	 * @param id
	 * @return responseDto
	 * @throws Exception 
	 */
	public User getUser(String id) throws Exception {	
		EntityManager em=null;
		em		= EMF.get().createEntityManager();
		EntityTransaction et	= em.getTransaction();
		User user = null;
		
		logger.info("daoId:"+id);
		try {
			et.begin();
			user = em.find(User.class,id);
			et.commit();
			
		} catch (EntityNotFoundException ex){	
			throw new Exception(ex);
		} 		
		finally {
			if (et.isActive()) {
				et.commit();
			}
			em.close();
		}
		return user;
	}
	
	/**
	 * get all users
	 * @return responseDto
	 * @throws Exception 
	 */
	@SuppressWarnings("unchecked")
	public  List<User> getAllUsers() throws Exception {
		EntityManager em = EMF.get().createEntityManager();
		
		EntityTransaction et = em.getTransaction();
		
		List<User> users=new ArrayList<User>();
		try {
			et.begin();
			Query query = em.createQuery("SELECT b from User b");
			et.commit();
			users = query.getResultList();
		} catch (Exception ex) {
			et.rollback();
			em.close();
			logger.log(Level.WARNING,"getting all users failed");
			throw new Exception(ex);
		}finally {
			if (et.isActive()) et.commit();
			if (em.isOpen()) em.close();
		}
		return users;
	}
	
	public User updateUserVerification (String userId) {
		EntityManager em 		= EMF.get().createEntityManager();
		EntityTransaction et	= em.getTransaction();
		User user = null;
		try {
			et.begin();
			user = em.find(User.class, userId);
			user.setVerification("verificated");
			user.setUpdatedAt(new Date().toString());
			em.persist(user);
			et.commit();
		} catch (EntityNotFoundException ex) {	
			
		} finally {
			if(et.isActive()) {
				et.commit();
			}
			em.close();
		}
		
		return user;
		
	};
	
	
	/**
	 * update user by id
	 * @param id
	 * @param jsonObject
	 * @return responseDto
	 */
	public ResponseDto<Object> updateUser(String id,JSONObject jsonObject) {
		EntityManager em 		= EMF.get().createEntityManager();
		EntityTransaction et	= em.getTransaction();
		String key = null;
		try {
			et.begin();
			User user = em.find(User.class, id);
			Iterator<?> keys = jsonObject.keys();
			while(keys.hasNext()){
				key = (String) keys.next();
				if (key.equals("nickName")) {
					createNickName((String)jsonObject.get(key),id);
				}
				PropertyDescriptor propDesc = new PropertyDescriptor(key,User.class);
				propDesc.getWriteMethod().invoke(user, jsonObject.get(key));
				propDesc=null;
			};	
			user.setUpdatedAt(new Date().toString());
			em.persist(user);
			user.getPlayer().setNickName(user.getNickName());
			user.getPlayer().setDeviceToken(user.getDeviceToken());
			et.commit();
			response.setStatus(Status.SUCCESS_ACCEPTED,"user updated");
			responseDto.setResponse(response);
		} catch (IntrospectionException e) {
			if (et.isActive()) {
				et.commit();
			}
			response.setStatus(Status.CLIENT_ERROR_BAD_REQUEST,"introspection exception");
			responseDto.setResponse(response);
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			if (et.isActive()) {
				et.commit();
			}
			response.setStatus(Status.CLIENT_ERROR_BAD_REQUEST,"illegal argument exception");
			responseDto.setResponse(response);
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			if (et.isActive()) {
				et.commit();
			} 
			response.setStatus(Status.CLIENT_ERROR_BAD_REQUEST,"illegal access exception");
			responseDto.setResponse(response);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			if (et.isActive()) {
				et.commit();
			}
			response.setStatus(Status.CLIENT_ERROR_BAD_REQUEST,"invocation exception");
			responseDto.setResponse(response);
			e.printStackTrace();
		} catch (JSONException e) {
			if (et.isActive()) {
				et.commit();
			}
			response.setStatus(Status.CLIENT_ERROR_BAD_REQUEST,"json not valid");
			responseDto.setResponse(response);
			e.printStackTrace();
		}
		finally {
			if (et.isActive()) {
				et.commit();
			}
			em.close();
		}
		return responseDto;
	}
	
	/**
	 * delete user by id
	 * @param id
	 * @return responseDto
	 */
	public ResponseDto<Object> deleteUser(String id) {
		EntityManager em		= EMF.get().createEntityManager();
		EntityTransaction et	= em.getTransaction();
		try {
			et.begin();
			User user  = em.find(User.class, id);
			Player player = user.getPlayer();
			NickName nickName = em.find(NickName.class, user.getNickName());
			em.remove(nickName);
			user.setPlayer(null);
			em.remove(player);
			em.remove(user);
			em.flush();
			et.commit();
			response.setStatus(Status.SUCCESS_OK, "user deleted");
			responseDto.setResponse(response);
		} catch (EntityNotFoundException ex) {	
			response.setStatus(Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY,"user not found");
			responseDto.setResponse(response);
		} finally {
			if(et.isActive()) {
				et.commit();
			}
			em.close();
		}
	return responseDto;
	}
	
	
	/**
	 * get user by nickName
	 * @param nickName
	 * @return responseDto
	 */
	public ResponseDto<Object> getUserByNickname(String nickName) {
		EntityManager em		= EMF.get().createEntityManager();
		EntityTransaction et	= em.getTransaction();
		try {
			et.begin();
			Query query = em.createQuery("SELECT b from User b WHERE b.nickName = :nickName").setParameter("nickName", nickName);
			et.commit();
			if (query.getResultList().isEmpty()) {
				response.setStatus(Status.CLIENT_ERROR_NOT_FOUND);
			}else {
				response.setStatus(Status.SUCCESS_OK);
				User user= (User) query.getResultList().get(0);
				responseDto.setResponseContent("user", new UserDto(user));
			}
			responseDto.setResponse(response);
		} finally{
			if (et.isActive()) {
				et.commit();
			}
			em.close();
		}
		return responseDto;
	}
	
	/**
	 * check entity by nickName
	 * @param nickNameId
	 * @return
	 */
	public  ResponseDto<Object> checkEntity(String nickNameId) {
		Response response = new Response(null);
		NickName nickName = null;
		EntityManager em	= EMF.get().createEntityManager();
		EntityTransaction et	= em.getTransaction();
		try {
			et.begin();
			nickName = em.find(NickName.class, nickNameId);
			et.commit();
			if (nickName != null) {
				response.setStatus(Status.SUCCESS_OK,nickName.getUserId());
				responseDto.setResponseContent("nickName", new NicknameDto(nickName));
			} else {
				response.setStatus(Status.CLIENT_ERROR_NOT_FOUND);
			}
			responseDto.setResponse(response);
		} finally{
			if (et.isActive()) {
				et.commit();
			}
			em.close();
		}
		return responseDto;	
	}
			
	/**
	 * create nickName
	 * @param nickName
	 * @param userId
	 * @return response
	 */
	private Response createNickName(String nickName,String userId){
		Response response = new Response(null);
		NickName nn = new NickName(nickName, userId);
		EntityManager em	= EMF.get().createEntityManager();
		EntityTransaction et	= em.getTransaction();
		
		try {
			et.begin();
			em.persist(nn);
			et.commit();
			response.setStatus(Status.SUCCESS_CREATED,"NickName created");
		} catch (javax.persistence.RollbackException e) {
			response.setStatus(Status.SERVER_ERROR_INTERNAL,"nickName creation failed");
		} finally {
			if (et.isActive()) {
				et.commit();
			}
			em.close();
		}
		return response;
	}
	
	
}
