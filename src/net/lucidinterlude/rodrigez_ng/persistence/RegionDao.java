package net.lucidinterlude.rodrigez_ng.persistence;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import org.restlet.Response;
import org.restlet.data.Status;

import net.lucidinterlude.rodrigez_ng.dto.ResponseDto;
import net.lucidinterlude.rodrigez_ng.model.EMF;
import net.lucidinterlude.rodrigez_ng.model.NickName;
import net.lucidinterlude.rodrigez_ng.model.Player;
import net.lucidinterlude.rodrigez_ng.model.Region;
import net.lucidinterlude.rodrigez_ng.model.User;


/**
 * rodrigez/RegionDao
 * @author Marco Kueng
 * (c) 2013 lucidinterlude
 */

public class RegionDao extends Dao{
	
	final static Logger logger = Logger.getLogger(RegionDao.class.getName());
	
	public RegionDao(){
	}
	
//setRegion
	public Region setRegion(Region region) throws Exception {
		EntityManager 		em = EMF.get().createEntityManager();
		EntityTransaction	et = em.getTransaction();
		try{
			et.begin();
			em.persist(region);
			et.commit();
		} catch (javax.persistence.RollbackException ex) {
			et.rollback();
			em.close();
			System.out.println(ex);
			logger.log(Level.WARNING,"persisting region failed");
			throw new Exception(ex);
		} finally {
			if (et.isActive()) et.commit();
			if (em.isOpen()) em.close();
		}
		return region;
	}
	
// get all regions
	@SuppressWarnings("unchecked")
	public List<Region> getAllRegions() throws Exception{
		EntityManager 		em = EMF.get().createEntityManager();
		EntityTransaction 	et = em.getTransaction();
		List<Region>  regions=new ArrayList<Region>();
		try {
			et.begin();
			Query query = em.createQuery("SELECT b from Region b");
			et.commit();
			regions = query.getResultList();
		} catch (Exception ex) {
			et.rollback();
			em.close();
			logger.log(Level.WARNING,"getting all regions failed");
			throw new Exception(ex);
		}finally {
			if (et.isActive()) et.commit();
			if (em.isOpen()) em.close();
		}
		return   regions;
	}
	
	/**
	 * delete user by id
	 * @param id
	 * @return responseDto
	 */
	public ResponseDto<Object> deleteRegion(String id) {
		EntityManager em		= EMF.get().createEntityManager();
		EntityTransaction et	= em.getTransaction();
		Response response	= new Response(null);
		ResponseDto responseDto = new ResponseDto();
		
		try {
			et.begin();
			Region region  = em.find(Region.class, id);
			
			em.remove(region);
		
			em.flush();
			et.commit();
			response.setStatus(Status.SUCCESS_OK, "user deleted");
			responseDto.setResponse(response);
		} catch (EntityNotFoundException ex) {	
			response.setStatus(Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY,"user not found");
			responseDto.setResponse(response);
		} finally {
			if(et.isActive()) {
				et.commit();
			}
			em.close();
		}
	return responseDto;
	}
	
}
