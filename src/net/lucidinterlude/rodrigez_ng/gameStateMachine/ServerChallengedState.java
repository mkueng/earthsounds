package net.lucidinterlude.rodrigez_ng.gameStateMachine;

public class ServerChallengedState extends GenericState {
	
	public ServerChallengedState(GameState gameState) {
		super(gameState);
		
		
	}

	@Override
	public void serverAcceptsChallenge() {
		gameState.setState(gameState.getGameAcceptedState());
		
	}
	
	@Override
	public void serverDeclinesChallenge() {
		gameState.setState(gameState.getGameDeclinedState());
	}

	
	public void serverChecksAvailability(){
		
	}
}
