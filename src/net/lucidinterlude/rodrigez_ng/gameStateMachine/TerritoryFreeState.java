package net.lucidinterlude.rodrigez_ng.gameStateMachine;


public class TerritoryFreeState extends GenericState{
	
	public TerritoryFreeState(GameState gameState) {
		super (gameState);
	}
	
	@Override
	public void playerChallengesServer() {
		gameState.setState(gameState.getServerChallengedState());
	}
}
