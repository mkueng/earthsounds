package net.lucidinterlude.rodrigez_ng.gameStateMachine;


public class GameState {

	State territoryFreeState;
	State territoryOccupiedState;
	State serverChallengedState;
	State gameAcceptedState;
	State gameOnState;
	State gameDeclinedState;
	State ownerInfoAvailableState;
	State checkServerAvailabilityState;
	
	State state;
	
	
	public GameState() {
		this.initializeStates();
	}
	
	public GameState(State initialState) {
		this.initializeStates();
		this.state=initialState;
		
		
	}

	
	public void setState(State state) {
		this.state = state; }
	
	public State getState(){
		return this.state;
	}
	
	public void initializeStates() {
		territoryFreeState = new TerritoryFreeState(this);
		territoryOccupiedState = new TerritoryOccupiedState(this);
		serverChallengedState = new ServerChallengedState(this);
		gameAcceptedState = new GameAcceptedState(this);
		gameOnState	= new GameOnState(this);
		gameDeclinedState = new GameDeclinedState(this);
		ownerInfoAvailableState	= new OwnerInfoAvailableState(this);
		checkServerAvailabilityState = new CheckServerAvailabilityState(this);
	}
	
	public void playerChallengesServer() {
		state.playerChallengesServer();
		
	}
	
	public void serverAcceptsChallenge() {
		state.serverAcceptsChallenge();
	}
	
	public void serverDeclinesChallenge() {
		state.serverDeclinesChallenge();
	}
	
	
//getters

	public State getTerritoryFreeState() {
		return territoryFreeState;
	}

	public State getTerritoryOccupiedState() {
		return territoryOccupiedState;
	}

	public State getServerChallengedState() {
		return serverChallengedState;
	}

	public State getGameAcceptedState() {
		return gameAcceptedState;
	}

	public State getGameOnState() {
		return gameOnState;
	}

	public State getGameDeclinedState() {
		return gameDeclinedState;
	}

	public State getOwnerInfoAvailableState() {
		return ownerInfoAvailableState;
	}
	
}
