package net.lucidinterlude.rodrigez_ng.gameStateMachine;

public interface State {
	
	public void playerChallengesServer();
	public void checkOwnerInfo();
	public void ownerIsOnSafeBase();
	public void oponentIsPlayable();
	public void oponentIsInSameTerritory();
	public void playerChallengesOponent();
	public void playerDeclinesChallenge();
	public void oponentDeclinesChallenge();
	public void oponentAcceptsChallenge();
	public void gameIsAvailable();
	public void serverAcceptsChallenge();
	public void serverDeclinesChallenge();
	public void serverChecksAvailability();
	
}
