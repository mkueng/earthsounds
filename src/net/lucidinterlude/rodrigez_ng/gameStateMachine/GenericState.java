package net.lucidinterlude.rodrigez_ng.gameStateMachine;

public class GenericState implements State{

	protected GameState gameState;
	
	public GenericState(GameState gameState) {
		this.gameState = gameState;
	}
	
	@Override
	public void playerChallengesServer() {
	}

	@Override
	public void checkOwnerInfo() {		
	}

	@Override
	public void ownerIsOnSafeBase() {	
	}

	@Override
	public void oponentIsPlayable() {
	}

	@Override
	public void oponentIsInSameTerritory() {	
	}

	@Override
	public void playerChallengesOponent() {		
	}

	@Override
	public void playerDeclinesChallenge() {
	}

	@Override
	public void oponentDeclinesChallenge() {		
	}

	@Override
	public void oponentAcceptsChallenge() {	
	}

	@Override
	public void gameIsAvailable() {	
	}

	@Override
	public void serverAcceptsChallenge() {
	}

	@Override
	public void serverDeclinesChallenge() {
	}

	@Override
	public void serverChecksAvailability() {
		// TODO Auto-generated method stub
		
	}

}
