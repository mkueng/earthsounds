package net.lucidinterlude.rodrigez_ng.gameStateMachine;

public class GameAcceptedState extends GenericState{
	
	public GameAcceptedState(GameState gameState){
		super(gameState);
		
	}

	@Override
	public void gameIsAvailable(){
		gameState.setState(gameState.getGameOnState());
	}
}
