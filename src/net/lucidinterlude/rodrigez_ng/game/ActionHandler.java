package net.lucidinterlude.rodrigez_ng.game;

import java.util.Observable;
import java.util.Observer;

import com.google.appengine.api.search.Document;

import net.lucidinterlude.rodrigez_ng.application.GameBO;
import net.lucidinterlude.rodrigez_ng.application.SoundBO;
import net.lucidinterlude.rodrigez_ng.model.Action;
import net.lucidinterlude.rodrigez_ng.model.Game;
import net.lucidinterlude.rodrigez_ng.model.Sound;

public class ActionHandler implements Observer{

	private SoundBO soundBO;
	private GameBO gameBO;
	private Action action;
	
	public ActionHandler(){
		soundBO = new SoundBO();
		Action action;
	}

	@Override
	public void update(Observable arg0, Object o) {
		Document territoryDoc = (Document)o;
		handleState(territoryDoc);
	}
	
	public Action handleState(Document territoryDoc) {
		String assignedAction = territoryDoc.getOnlyField("action").getText();
		if (assignedAction.equals("Play sound")) {
			Sound sound = null;
			String soundId =  territoryDoc.getOnlyField("actionContent").getText().split("\\.")[1];
			try {
				sound = soundBO.getSound(soundId);
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return sound;
		} else if (assignedAction.equals("Play game")) {
			Game game = null;
			String gameId =  territoryDoc.getOnlyField("actionContent").getText().split("\\.")[1];

			try {
				game = gameBO.getGame(gameId);
			} catch (Exception e){
				
			}
			return game;
		} else {
			return null;
		}
	}
	
}
