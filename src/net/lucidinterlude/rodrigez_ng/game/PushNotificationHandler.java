package net.lucidinterlude.rodrigez_ng.game;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import java.util.HashMap;

import org.apache.commons.codec.binary.Base64;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import net.lucidinterlude.rodrigez_ng.dto.PushNotificationDto;

	
public class PushNotificationHandler {

	private Gson gson	= new GsonBuilder().create();
	public static String APP_KEY = "HwXANmYnT2-CLQBM0if1Fg";
	public static String APP_MASTER_SECRET = "ag7YJoytRAKUGe78hr65iQ";
	String authStringBase64;
	URL url;
	PushNotificationDto pushNotificationDto;
	
	public PushNotificationHandler() {
		String nameAndPassword = APP_KEY+":"+APP_MASTER_SECRET;
		authStringBase64= new String(Base64.encodeBase64(nameAndPassword.getBytes()));
		authStringBase64=authStringBase64.trim();
		try {
			url = new URL("https://go.urbanairship.com/api/push/");
		} catch (MalformedURLException e) {
		}
	}
	
	public Boolean sendPushNotification(String deviceToken, String notification) {
		Boolean isSent = false;
		HttpURLConnection connection;
		HashMap<String,String> aps = new HashMap<String,String>();
		aps.put("alert", notification);
		pushNotificationDto= new PushNotificationDto(deviceToken,aps);
		try {
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setDoOutput(true);
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setRequestProperty("Authorization", "Basic "+authStringBase64);
			String jsonBodyString = gson.toJson(pushNotificationDto);
			OutputStreamWriter osw;
			osw = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
			osw.write(jsonBodyString);
			osw.close();
			int responseCode = connection.getResponseCode();
			if (responseCode > 200) isSent = false; else isSent = true;
		} catch (IOException e) {
			e.printStackTrace();
		}	
		return isSent;
	}
}
