package net.lucidinterlude.rodrigez_ng.game;

import java.util.logging.Logger;

import net.lucidinterlude.rodrigez_ng.dto.PlayerDto;
import net.lucidinterlude.rodrigez_ng.gameStateMachine.GameState;
import net.lucidinterlude.rodrigez_ng.gameStateMachine.TerritoryFreeState;
import net.lucidinterlude.rodrigez_ng.model.Player;

public class GameHandler {
	//logger
	final Logger log = Logger.getLogger(GameHandler.class.getName());
	PushNotificationHandler pushNotificationHandler;
	Player player;
	Player target;
	String targetId;
	String playerDeviceToken;
	String targetDeviceToken;
	String playerNickName;
	String targetNickName;

	public GameHandler(PlayerDto player, PlayerDto target) {
	
	
		GameState gameState = new GameState();
		gameState.setState(new TerritoryFreeState(gameState));
		gameState.playerChallengesServer();
		
		
		/*
		this.targetDeviceToken=target.getDeviceToken();
		this.targetNickName=target.getNickName();
		this.playerDeviceToken=player.getDeviceToken();
		this.playerNickName=player.getNickName();
		pushNotificationHandler = new PushNotificationHandler();
		
		log.setLevel(Level.INFO);
		log.info("TargetNickName"+targetNickName);
		log.info("TargetDeviceToken"+targetDeviceToken);
		
		informPlayerAndTarget();
		*/
	}
	
		
	@SuppressWarnings("unused")
	private void informPlayerAndTarget() {
		
		if (playerDeviceToken != null) {
			String playerNotification = "you entered the territory of "+targetNickName;
			pushNotificationHandler.sendPushNotification(playerDeviceToken, playerNotification);
		}
		if (targetDeviceToken != null) {
			String targetNotification = playerNickName+ " entered your territory, be aware!!!";
			pushNotificationHandler.sendPushNotification(targetDeviceToken, targetNotification);
		}	
	}
}
