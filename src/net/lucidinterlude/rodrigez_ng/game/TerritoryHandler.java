package net.lucidinterlude.rodrigez_ng.game;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Observable;
import java.util.Observer;

import com.google.appengine.api.search.Document;

import net.lucidinterlude.rodrigez_ng.index.GeofenceDocumentManager;
import net.lucidinterlude.rodrigez_ng.index.PlayerDocumentManager;
import net.lucidinterlude.rodrigez_ng.index.TerritoryDocumentManager;
import net.lucidinterlude.rodrigez_ng.model.Player;

public class TerritoryHandler extends Observable implements Observer{
	private TerritoryDocumentManager territoryDocManager;
	
	public TerritoryHandler(PlayerDocumentManager playerDocumentManager, GeofenceDocumentManager geofenceDocumentManager, TerritoryDocumentManager territoryDocManager) {
		this.territoryDocManager = territoryDocManager;
	}

	@Override
	public void update(Observable arg0, Object o) {
		Player player = (Player)o;
		Document doc = this.checkTerritoryStatusRegardingPlayerLocation(player.getLatitude(), player.getLongitude());
		handleState(player, doc);
	}
	
	public Document checkTerritoryState(Player player) {
		Document doc = this.checkTerritoryStatusRegardingPlayerLocation(player.getLatitude(), player.getLongitude());
		return doc;
	}
	
	public Document checkTerritoryState(Double latitude, Double longitude) {
		Document doc = this.checkTerritoryStatusRegardingPlayerLocation(latitude, longitude);
		return doc;
	}
	
	/**
	 * 
	 * @param player
	 * @param territoryDoc
	 */
	private void handleState(Player player, Document territoryDoc) {
		if (territoryDoc !=null) {
			setChanged(); 
			notifyObservers(territoryDoc);
			if (!territoryDoc.getOnlyField("owner").getText().equals(player.getNickName())) {
				System.out.println("NOT YOURS!!!");
			} else {
				System.out.println("YOURS");
			}
		} else {
			System.out.println("THIS IS FREE");
		}
	}
	
	/**
	 * 
	 * @param player
	 * @return
	 */
	private Document checkTerritoryStatusRegardingPlayerLocation(Double latitude, Double longitude) {
		Document doc = null;
		double latSpacing;
		double lngSpacing;
		double modulo = 0.00025;
		
		Double diffLat;
		Double diffLng;
		Double snapLat;
		Double snapLng;
		
		String documentId;
		
		while (modulo <= 0.512) {
			latSpacing = modulo;
			lngSpacing = latSpacing/2*3;
	
			diffLat = latitude-(latitude%latSpacing);
			diffLng = longitude-(longitude%lngSpacing);
			snapLat = (calculateRoundedDouble(diffLat)*100000)/100000;
			snapLng = (calculateRoundedDouble(diffLng)*100000)/100000;
			documentId = snapLat+"_"+snapLng;
			
			try {
				doc = territoryDocManager.getDocumentById(documentId);
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (doc != null) break;
			modulo = modulo*2;
		}
		return doc;
	}
	
	//utility methods
	private Double calculateRoundedDouble(Double fullDouble){
		DecimalFormat df = new DecimalFormat("###.#####");
		df.setRoundingMode(RoundingMode.HALF_DOWN);
		String dfDouble = df.format(fullDouble);
		Double returnDouble = Double.parseDouble(dfDouble);
		
		return returnDouble;
	}
}
