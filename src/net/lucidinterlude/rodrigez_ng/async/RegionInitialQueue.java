package net.lucidinterlude.rodrigez_ng.async;

import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;



public class RegionInitialQueue {

	public Queue putTaskToQueue(JSONObject jsonRepresentation) throws JSONException {
		Iterator<?> keys = jsonRepresentation.keys();
		Queue queue = QueueFactory.getQueue("regionInitial");
		
		JSONArray junkArray = new JSONArray();

		JSONObject jsonObject = new JSONObject();
		JSONObject junkObject = new JSONObject();
		
		/*for (int i = 0; i < territoryArrayLength; i++){
			jsonObject = territoryArray.getJSONObject(i);
			
			junkArray.put(jsonObject);
			
			if (i % 50 == 0) {
				junkObject.put("territories", junkArray);
				queue.add(TaskOptions.Builder.withUrl("/regionInitialWorker").param("jsonRepresentation",junkObject.toString()));
				
				junkArray = new JSONArray();
				junkObject = new JSONObject();
			}
		}*/
		int i = 0;
		while( keys.hasNext() ) {
		    String key = (String)keys.next();
		    if ( jsonRepresentation.get(key) instanceof JSONObject ) {
		    	jsonObject = (JSONObject) jsonRepresentation.get(key);
		    	junkArray.put(jsonObject);
		    	
		    	if (i % 50 == 0) {
					junkObject.put("territories", junkArray);
					queue.add(TaskOptions.Builder.withUrl("/regionInitialWorker").param("jsonRepresentation",junkObject.toString()));
					
					junkArray = new JSONArray();
					junkObject = new JSONObject();
				}
		    }
		}
		
		junkObject.put("territories", junkArray);
		queue.add(TaskOptions.Builder.withUrl("/regionInitialWorker").param("jsonRepresentation",junkObject.toString()));
	return queue;
	}
}
