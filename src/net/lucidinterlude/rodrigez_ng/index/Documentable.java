package net.lucidinterlude.rodrigez_ng.index;

import java.util.ArrayList;

import com.google.appengine.api.search.Cursor;
import com.google.appengine.api.search.Document;
import com.google.appengine.api.search.Index;
import com.google.appengine.api.search.Results;
import com.google.appengine.api.search.ScoredDocument;


/**
 * rodrigez/Documentable
 * @author Marco Kueng
 * (c) 2013 lucidinterlude
 */

public interface Documentable {
	
	/**
	 * 
	 */
	public void createDocument();
	
	/**
	 * 
	 * @param document
	 * @throws Exception
	 */
	public void putDocumentToIndex(Document document) throws Exception;
	
	/**
	 * returns a list of scored documents based on a searchString constructed with the provided queryMap  
	 * 
	 * @param queryMap
	 * @return list of scored documents
	 * @throws Exception
	 */
	public Results<ScoredDocument> getDocumentsByParamMap(Cursor cursor, String queryString) throws Exception;
	
	
	/**
	 * returns Document by provided Id
	 * 
	 * @param id
	 * @return Document
	 * @throws Exception
	 */
	public Document getDocumentById (String documentId) throws Exception;
	
	/**
	 * 
	 * @param documentId
	 * @throws Exception
	 */
	public void deleteDocumentFromIndex (String documentId) throws Exception;
	
	/**
	 * 
	 * @param documentIds
	 * @throws Exception
	 */
	public void deleteDocumentsFromIndex (ArrayList<String> documentIds) throws Exception;
	
	/**
	 * returns the current search index
	 * 
	 * @return
	 */
	public Index getIndex();
}
