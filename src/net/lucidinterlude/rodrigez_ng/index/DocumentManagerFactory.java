package net.lucidinterlude.rodrigez_ng.index;

public class DocumentManagerFactory {
	public static Documentable createDocumentManager(DocumentManagerType docType) {
		switch(docType) {
			case PLAYER 	: { return new PlayerDocumentManager(); }
			case GEOFENCE 	: { return new GeofenceDocumentManager(); }
			case TERRITORY	: { return new TerritoryDocumentManager(); }	
			
			default 		: return null;
		}	
	}
}