package net.lucidinterlude.rodrigez_ng.index;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.NamespaceManager;
import com.google.appengine.api.search.Cursor;
import com.google.appengine.api.search.Document;
import com.google.appengine.api.search.Index;
import com.google.appengine.api.search.IndexSpec;
import com.google.appengine.api.search.PutException;
import com.google.appengine.api.search.Query;
import com.google.appengine.api.search.QueryOptions;
import com.google.appengine.api.search.Results;
import com.google.appengine.api.search.ScoredDocument;
import com.google.appengine.api.search.SearchServiceFactory;

/**
 * rodrigez/DocumentManager
 * @author Marco Kueng
 * (c) 2013 lucidinterlude
 */

public class DocumentManager implements Documentable {
	
	final static Logger logger = Logger.getLogger(DocumentManager.class.getName());
	private static final String index = "lucid-rodrigez-ng-100";
	
	@Override
	public void putDocumentToIndex(Document document) throws Exception {
		try {
			getIndex().put(document);
		}
		catch (PutException e) {
			System.out.println("Fehler beim indexieren");
			throw new Exception(e);
		}
	}

	@Override
	public void deleteDocumentFromIndex(String documentId) throws Exception {
		getIndex().delete(documentId);
		
	}	
	
	@Override
	public void deleteDocumentsFromIndex(ArrayList<String> documentIds) throws Exception {
		getIndex().delete(documentIds);
	}
	

	@Override
	public Document getDocumentById(String documentId) throws Exception {
		return  getIndex().get(documentId);
	}
	
	
	public String createQueryString(HashMap<String,Object> queryMap) {
		String queryStr="";
		//specialcase geolocation
		if (queryMap.containsKey("distance") && queryMap.containsKey("latitude") && queryMap.containsKey("longitude")) {
			queryStr = queryMap.get("region")+" AND "+"(distance(geoPoint, geopoint("+Double.parseDouble((String) queryMap.get("latitude"))+","+Double.parseDouble((String) queryMap.get("longitude"))+"))<"+(String)queryMap.get("distance")+")";

		} else {
			for (Entry<String, Object> entry : queryMap.entrySet()) {
				String key		= entry.getKey();
				Object value	= entry.getValue();
				queryStr = queryStr+(key+":"+value+" AND ");
			}
		
			queryStr = queryStr.substring(0,queryStr.length()-4 )+"   ";
		}
		return queryStr;
	}
	
	@Override
	public Results<ScoredDocument> getDocumentsByParamMap( Cursor cursor, String queryString) throws Exception {
		
		QueryOptions queryOptions = QueryOptions.newBuilder().setLimit(1000).setCursor(cursor).build();		
		Query query = Query.newBuilder().setOptions(queryOptions).build(queryString);
		Results<ScoredDocument> results = null;
		try {
			results = getIndex().search(query);
		}catch (RuntimeException ex) {
			logger.log(Level.WARNING,"getting document by paramMap failed :"+ex);
			throw new RuntimeException(ex);
		}
		return results;
	}

	@Override
	public Index getIndex() {
		NamespaceManager.set(index);
	    IndexSpec indexSpec = IndexSpec.newBuilder().setName(index).build();
	    Index index = SearchServiceFactory.getSearchService().getIndex(indexSpec);
	    return index;
	}

	@Override
	public void createDocument() {		
	}
}
