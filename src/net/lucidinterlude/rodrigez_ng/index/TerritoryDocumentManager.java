package net.lucidinterlude.rodrigez_ng.index;

import com.google.appengine.api.search.Document;
import com.google.appengine.api.search.Field;
import com.google.appengine.api.search.GeoPoint;

/**
 * rodrigez/TerritoryDocumentManager
 * @author Marco Kueng
 * (c) 2013 lucidinterlude
 */
public class TerritoryDocumentManager extends DocumentManager {
	
	/**
	 * 
	 * @param documentId
	 * @param name
	 * @param owner
	 * @param status
	 * @param region
	 * @param itFactor
	 * @param spacing
	 * @param geoPoints
	 * @throws Exception
	 */
	public void createDocument(String documentId, String name, String owner, String status, String region,Double itFactor, Double spacing,String action, String actionContent, String geoPoints) throws Exception {
		
		String[] splitId = documentId.split("_");
		Double latitude = Double.parseDouble(splitId[0]);
		Double longitude = Double.parseDouble(splitId[1]);
		GeoPoint geopoint= createGeoPoint(latitude,longitude);
		
		Document territoryDoc = Document.newBuilder().setId(documentId)
				.addField(Field.newBuilder().setName("name").setText(name))
				.addField(Field.newBuilder().setName("owner").setText(owner))
				.addField(Field.newBuilder().setName("status").setText(status))
				.addField(Field.newBuilder().setName("region").setText(region))
				.addField(Field.newBuilder().setName("itFactor").setNumber(itFactor))
				.addField(Field.newBuilder().setName("spacing").setNumber(spacing))
				.addField(Field.newBuilder().setName("geoPoint").setGeoPoint(geopoint))
				.addField(Field.newBuilder().setName("action").setText(action))
				.addField(Field.newBuilder().setName("actionContent").setText(actionContent))
				.addField(Field.newBuilder().setName("geoPoints").setText(geoPoints))
				.build();
		putDocumentToIndex(territoryDoc);
	}
		
	
	/**
	 * 
	 * @param latitude
	 * @param longitude
	 * @return GeoPoint
	 */
	private GeoPoint createGeoPoint(Double latitude, Double longitude) {
		return new GeoPoint(latitude,longitude);
	}
}
