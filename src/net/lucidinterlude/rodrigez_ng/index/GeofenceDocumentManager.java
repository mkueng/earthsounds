package net.lucidinterlude.rodrigez_ng.index;

import java.util.ArrayList;
import java.util.List;

import com.google.appengine.api.search.Document;
import com.google.appengine.api.search.Field;
import com.google.appengine.api.search.GeoPoint;
import com.google.appengine.api.search.Query;
import com.google.appengine.api.search.QueryOptions;
import com.google.appengine.api.search.Results;
import com.google.appengine.api.search.ScoredDocument;

public class GeofenceDocumentManager extends DocumentManager{

	
	//create GeoFence Document and put it on the index
	/**
	 * 
	 * @param documentId
	 * @param playerId
	 * @param latitude
	 * @param longitude
	 * @param ownerNickName
	 * @param name
	 * @throws Exception
	 */
	public void createGeoFenceDocument(
			String documentId, 
			String playerId,
			Double latitude,
			Double longitude, 
			String ownerNickName,
			String name) throws Exception {
		Document geoFenceDoc = Document.newBuilder().setId(documentId)
				.addField(Field.newBuilder().setName("playerId").setText(playerId))
				.addField(Field.newBuilder().setName("ownerNickName").setText(ownerNickName))
				.addField(Field.newBuilder().setName("name").setText(name))
				.addField(Field.newBuilder().setName("latitude").setNumber(latitude))
				.addField(Field.newBuilder().setName("longitude").setNumber(longitude))
				.addField(Field.newBuilder().setName("geopoint").setGeoPoint(createGeoPoint(latitude,longitude)))
				.build();
	
		putDocumentToIndex(geoFenceDoc);
	}
	
	//delete Documents
	/**
	 * 
	 * @param playerId
	 */
	public void deleteDocuments(String playerId) {
		String queryStr = ("playerId:"+playerId);
		List<String> docIds = new ArrayList<String>();
		Results<ScoredDocument> results = null;
		
		try {
			results = getIndex().search(queryStr);
		}catch (RuntimeException e) {
		}
		for(ScoredDocument document : results) {
			docIds.add(document.getId());
		}
		try {
			getIndex().delete(docIds);
			} catch (RuntimeException e) {
		}
	}
	
	//get Quadrant from GeoPoint
	/**
	 * 
	 * @param latitude
	 * @param longitude
	 * @return results
	 */
	public Results<ScoredDocument> getQuadrant(Double latitude, Double longitude){
		String queryStr = ("latitude:"+latitude+" AND "+"longitude:"+longitude);
		Results<ScoredDocument> results = null;
		try {
			results = getIndex().search(queryStr);
		}
		catch (RuntimeException e) {
		}
		return results;
	}
	
	/**
	 * 
	 * @param playerId
	 * @param resultLimit
	 * @return results
	 */
	public Results<ScoredDocument> getQuadrantsByOwner(String playerId, int resultLimit) {
		QueryOptions queryOptions = QueryOptions.newBuilder().setLimit(resultLimit).build();
		
		String queryStr = ("playerId:"+playerId);
		Query search_query = Query.newBuilder().setOptions(queryOptions).build(queryStr);
		Results<ScoredDocument> results = null;
		try {
			results = getIndex().search(search_query);
		}catch (RuntimeException e) {
		}
		return results;
	}
	
	/**
	 * 
	 * @param latitude
	 * @param longitude
	 * @param distance
	 * @return results
	 */
	public Results<ScoredDocument> getTerritoriesInDistance(Double latitude, Double longitude,String distance) {
		QueryOptions queryOptions = QueryOptions.newBuilder().setLimit(1000).build();
		
		String queryStr = "(distance(geopoint, geopoint("+latitude+","+longitude+"))<"+distance+")";
		Query search_query = Query.newBuilder().setOptions(queryOptions).build(queryStr);
		Results<ScoredDocument> results = null;
		try {
			results = getIndex().search(search_query);
		}
		catch (RuntimeException e) {
		}
		return results;
		
	}
	
	private GeoPoint createGeoPoint(Double latitude, Double longitude) {
		return new GeoPoint(latitude,longitude);
	}	
	
}
