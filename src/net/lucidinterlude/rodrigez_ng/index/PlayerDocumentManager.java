	package net.lucidinterlude.rodrigez_ng.index;

import java.util.logging.Logger;

import net.lucidinterlude.rodrigez_ng.game.GameHandler;
import net.lucidinterlude.rodrigez_ng.model.Player;

import com.google.appengine.api.search.Document;
import com.google.appengine.api.search.Field;
import com.google.appengine.api.search.GeoPoint;
import com.google.appengine.api.search.Index;
import com.google.appengine.api.search.PutException;
import com.google.appengine.api.search.Results;
import com.google.appengine.api.search.ScoredDocument;


/**
 * @author marcokueng
 *
 */
public class PlayerDocumentManager extends DocumentManager{
	final Logger log = Logger.getLogger(GameHandler.class.getName());
	
	public Boolean createDocument(String playerId, Double latitude, Double longitude,String nickName,String deviceToken) throws Exception {
		Document playerDoc = Document.newBuilder().setId(playerId)
				.addField(Field.newBuilder().setName("player_location").setGeoPoint(createGeoPoint(latitude,longitude)))
				.addField(Field.newBuilder().setName("nickName").setText(nickName))
				.addField(Field.newBuilder().setName("deviceToken").setText(deviceToken))
				.build();
		try {
			putDocumentToIndex(playerDoc);
			return true;
		}
		catch (PutException e) {
		}
		return false;
	}
	
	
	public Document getPlayer (String id) {
	 Document doc = null;	
		log.info("documentId"+id);
		Index indexx = getIndex();
		doc =  indexx.get(id);
	return doc;
		
	}
	
	public Results<ScoredDocument> getPlayersInDistance(Player player, String distance) {
		Double latitude = player.getLatitude();
		Double longitude = player.getLongitude();
		String queryStr = "(distance(player_location, geopoint("+latitude+","+longitude+"))<"+distance+")";
		Results<ScoredDocument> results = null;
		
		try {
			results = getIndex().search(queryStr);
		}
		catch (RuntimeException e) {
		}
		return results;
	}
	
	/**
	 * @param latitude
	 * @param longitude
	 * @return GeoPoint
	 */
	
	private GeoPoint createGeoPoint(Double latitude, Double longitude) {
		return new GeoPoint(latitude,longitude);
	}	
}
