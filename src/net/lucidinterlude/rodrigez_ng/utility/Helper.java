package net.lucidinterlude.rodrigez_ng.utility;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public class Helper {

	public static Double calculateSnapDouble(Double fullDouble) {
		DecimalFormat df = new DecimalFormat("###.###");
		df.setRoundingMode(RoundingMode.DOWN);
		String dfDouble = df.format(fullDouble);
		Double returnDouble = Double.parseDouble(dfDouble);
		return returnDouble;
	}
	
	public static Double calculateRoundedDouble(Double fullDouble) {
		DecimalFormat df = new DecimalFormat("###.###");
		df.setRoundingMode(RoundingMode.HALF_DOWN);
		String dfDouble = df.format(fullDouble);
		return Double.parseDouble(dfDouble);
	}
}
