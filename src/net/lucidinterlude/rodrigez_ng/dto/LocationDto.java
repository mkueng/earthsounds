package net.lucidinterlude.rodrigez_ng.dto;

import net.lucidinterlude.rodrigez_ng.model.Location;

public class LocationDto implements IDto{

	
	private Float latitude;
	private Float longitude;
	
	
	public LocationDto (Location location) {
		this.latitude = location.getGeoPt().getLatitude();
		this.longitude = location.getGeoPt().getLongitude();
	}
	
	public LocationDto(){
		
	}

	public Float getLatitude() {
		return latitude;
	}


	public void setLatitude(Float latitude) {
		this.latitude = latitude;
	}


	public Float getLongitude() {
		return longitude;
	}


	public void setLongitude(Float longitude) {
		this.longitude = longitude;
	}


	@Override
	public String getId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setId(String id) {
		// TODO Auto-generated method stub
		
	}

}
