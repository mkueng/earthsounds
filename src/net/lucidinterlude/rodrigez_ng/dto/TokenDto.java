package net.lucidinterlude.rodrigez_ng.dto;

import net.lucidinterlude.rodrigez_ng.model.Token;

public class TokenDto implements IDto {

	
	private String tokenString;
	private String clearance;
	
	
	public TokenDto(Token token) {
		this.tokenString = token.getTokenString();
		this.clearance = token.getClearance();
	}

	public TokenDto(){
		
	}

	public String getTokenString() {
		return tokenString;
	}


	public void setTokenString(String tokenString) {
		this.tokenString = tokenString;
	}

	public String getClearance() {
		return clearance;
	}

	public void setClearance(String clearance) {
		this.clearance = clearance;
	}

	@Override
	public String getId() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void setId(String id) {
		// TODO Auto-generated method stub
		
	}
	
	
	
	
}
