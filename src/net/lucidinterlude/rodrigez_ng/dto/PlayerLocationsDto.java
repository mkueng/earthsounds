package net.lucidinterlude.rodrigez_ng.dto;

import java.util.ArrayList;

import net.lucidinterlude.rodrigez_ng.model.Location;

public class PlayerLocationsDto implements IDto {

	private Float	longitude;
	private Float 	latitude;
	private ArrayList<LocationDto> locations = new ArrayList<LocationDto>();
	
	public PlayerLocationsDto(ArrayList<Location> locations){
		this.latitude = locations.get(0).getGeoPt().getLatitude();
		this.longitude = locations.get(0).getGeoPt().getLongitude();
		
		for (Location location :  locations){
			LocationDto locationDto = new LocationDto();
			locationDto.setLatitude(location.getGeoPt().getLatitude());
			locationDto.setLongitude(location.getGeoPt().getLongitude());
			this.locations.add(locationDto);
		}
	}
	
	
	public Float getLongitude() {
		return longitude;
	}



	public void setLongitude(Float longitude) {
		this.longitude = longitude;
	}



	public Float getLatitude() {
		return latitude;
	}



	public void setLatitude(Float latitude) {
		this.latitude = latitude;
	}



	public ArrayList<LocationDto> getLocations() {
		return locations;
	}
	
	public void setLocations(ArrayList<LocationDto> locations) {
		this.locations = locations;
	}

	@Override
	public String getId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setId(String id) {
		// TODO Auto-generated method stub
		
	}

}
