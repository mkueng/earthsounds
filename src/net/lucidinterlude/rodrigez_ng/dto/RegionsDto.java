package net.lucidinterlude.rodrigez_ng.dto;

import java.util.List;

import net.lucidinterlude.rodrigez_ng.model.Region;

/**
 * rodrigez/RegionDto
 * @author Marco Kueng
 * (c) 2013 lucidinterlude
 */

public class RegionsDto implements IDto{
	private List<Region> regions;

	public RegionsDto(){
	}
	
	public RegionsDto(List<Region> regions) {
		this.setRegions(regions);
	}
	
	public List<Region> getRegions() {
		return regions;
	}

	public void setRegions(List<Region> regions) {
		this.regions = regions;
	}

	@Override
	public String getId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setId(String id) {
		// TODO Auto-generated method stub
		
	}
}