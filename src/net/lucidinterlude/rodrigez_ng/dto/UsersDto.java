package net.lucidinterlude.rodrigez_ng.dto;

import java.util.ArrayList;
import java.util.List;

import net.lucidinterlude.rodrigez_ng.model.User;

/**
 * rodrigez/RegionDto
 * @author Marco Kueng
 * (c) 2013 lucidinterlude
 */

public class UsersDto implements IDto{
	private ArrayList<UserDto> users = new ArrayList<UserDto>();

	public UsersDto(){
	}
	
	public UsersDto(List<User> users) {
	
		for (User user :  users){
			UserDto userDto = new UserDto();
			userDto.setId(user.getId());
			userDto.setRoot(user.getRoot());
			userDto.setNickName(user.getNickName());
			this.users.add(userDto);
		}
	}
	
	public ArrayList<UserDto> getUsers() {
		return users;
	}
	
	public void setUsers(ArrayList<UserDto> usersWithoutPlayer) {
		this.users = usersWithoutPlayer;
	}

	@Override
	public String getId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setId(String id) {
		// TODO Auto-generated method stub
		
	}
}
