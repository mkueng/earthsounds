package net.lucidinterlude.rodrigez_ng.dto;

import java.util.ArrayList;
import java.util.List;

import net.lucidinterlude.rodrigez_ng.model.Sound;

public class SoundsDto implements IDto {

	private ArrayList<SoundDto> sounds = new ArrayList<SoundDto>();
	
	public SoundsDto(){
	}
	
	public SoundsDto(List<Sound> sounds) {
		
		for (Sound sound :  sounds){
			SoundDto soundDto = new SoundDto();
			soundDto.setId(sound.getId());
			soundDto.setRoot(sound.getRoot());
			soundDto.setName(sound.getName());
			soundDto.setDuration(sound.getDuration());
			soundDto.setSampleRate(sound.getSampleRate());
			soundDto.setCategory(sound.getCategory());
			
			this.sounds.add(soundDto);
		}
	}
	
	
	
	public ArrayList<SoundDto> getSounds() {
		return sounds;
	}
	
	public void setSounds(ArrayList<SoundDto> sounds) {
		this.sounds = sounds;
	}
	
	@Override
	public String getId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setId(String id) {
		// TODO Auto-generated method stub
		
	}

}
