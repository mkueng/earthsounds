package net.lucidinterlude.rodrigez_ng.dto;

public class NumberDto implements IDto {

	private Long number;
	
	public NumberDto(){
		
	}
	public NumberDto(Long number){
		this.number = number;
	}
	
	public Long getNumber(){
		return this.number;
	}
	
	public void setNumber(Long number) {
		this.number = number;
	}
	@Override
	public String getId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setId(String id) {
		// TODO Auto-generated method stub
		
	}

}
