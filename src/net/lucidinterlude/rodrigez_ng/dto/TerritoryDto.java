package net.lucidinterlude.rodrigez_ng.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import com.google.appengine.api.datastore.GeoPt;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.search.GeoPoint;

import net.lucidinterlude.rodrigez_ng.utility.Helper;

/**
 * rodrigez/TerritoryDto
 * @author Marco Kueng
 * (c) 2013 lucidinterlude
 */

@SuppressWarnings("unused")
public class TerritoryDto implements IDto {

	private String 	id;
	private String	region;
	private String 	name;
	private Double itFactor;
	private String 	status;
	private String 	owner;
	private Double	spacing;
	private String 	action;
	private String actionContent;
	private Date 	createdAt;
	private Date	updatedAt;
	private ArrayList<GeoPt> geoPoints;

	
	public TerritoryDto() {
		
	}
	
	public TerritoryDto(Double latitude, Double longitude, Double size, String id, String name, String owner, String ownerId) {
		this.id = id;
		this.name = name;
		this.owner = owner;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getItFactor() {
		return itFactor;
	}

	public void setItFactor(Double itFactor) {
		this.itFactor = itFactor;
	}
	
	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
	
	public String getActionContent() {
		return actionContent;
	}

	public void setActionContent(String actionContent) {
		this.actionContent =actionContent;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public Double getSpacing() {
		return spacing;
	}

	public void setSpacing(Double spacing) {
		this.spacing = spacing;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	
	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getGeoPoints() {
		String geoPointsStr = "";
		for (GeoPt s : geoPoints) {
			geoPointsStr = geoPointsStr+s.getLatitude()+"-"+s.getLongitude()+",";
		}
		return geoPointsStr;
	}

	public void setGeoPoints(ArrayList<GeoPt> geoPoints) {
		this.geoPoints=geoPoints;
	}

	public void setGeoPoints(String geoPoints) {
		this.geoPoints = new ArrayList<GeoPt>();
		String[] geoPoint = geoPoints.split(",");
		for(String s : geoPoint) {
			String[] splitString = s.split("/");
			float latitude = Float.parseFloat(splitString[0]);
			float longitude = Float.parseFloat(splitString[1]);
			this.geoPoints.add(new GeoPt(latitude,longitude));
		}
	}
}
