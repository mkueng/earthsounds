package net.lucidinterlude.rodrigez_ng.dto;

import java.util.ArrayList;
import java.util.HashMap;

public class PushNotificationDto {
	private ArrayList<String> device_tokens =new ArrayList<String>();
	@SuppressWarnings("unused")
	private HashMap<String,String> aps = new HashMap<String,String>();


	public PushNotificationDto(String deviceToken, HashMap<String,String> aps) {
		this.device_tokens.add(deviceToken);
		this.aps=aps;
	}
}
