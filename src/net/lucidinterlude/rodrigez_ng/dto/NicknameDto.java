package net.lucidinterlude.rodrigez_ng.dto;

import net.lucidinterlude.rodrigez_ng.model.NickName;

public class NicknameDto implements IDto {

	private String nickName;
	private String userId;
	
	public NicknameDto(){
		
	}
	
	public NicknameDto(NickName nickName) {
		this.nickName = nickName.getNickName();
		this.userId = nickName.getUserId();
	}
	
	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Override
	public String getId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setId(String id) {
		// TODO Auto-generated method stub
		
	}

}
