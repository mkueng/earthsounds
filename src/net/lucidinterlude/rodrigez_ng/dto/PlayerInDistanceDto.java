package net.lucidinterlude.rodrigez_ng.dto;

import com.google.appengine.api.search.GeoPoint;
import com.google.appengine.api.search.ScoredDocument;

@SuppressWarnings("unused")
public class PlayerInDistanceDto {
	private String id;
	private String nickName;
	private GeoPoint location;
	
	
	public PlayerInDistanceDto(ScoredDocument document) {
		this.id = document.getId();
		this.nickName = document.getOnlyField("nickName").getText();
		this.location = document.getOnlyField("player_location").getGeoPoint();
		
	}
}
