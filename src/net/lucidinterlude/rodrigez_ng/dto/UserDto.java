package net.lucidinterlude.rodrigez_ng.dto;

import java.util.Date;

import net.lucidinterlude.rodrigez_ng.model.User;
@SuppressWarnings("unused")
public class UserDto implements IDto{
	private String id;
	private String root;
	private String endpoint;
	private String createdAt;
	private String updatedAt;
	private String deletedAt;
	private String firstName;
	private String lastName;
	private String nickName;
	private String email;
	private String gender;
	private String deviceToken;
	private String birthdate;
	private String password;
	private String clearance;
	private String verification;
	
	public UserDto(User user) {
		this.id 		= user.getId();
		this.root 		= user.getRoot();
		this.endpoint	= user.getEndpoint();
		this.createdAt 	= user.getCreatedAt();
		this.updatedAt 	= user.getUpdatedAt();
		this.deletedAt 	= user.getDeletedAt();
		this.firstName 	= user.getFirstName();
		this.lastName	= user.getLastName();
		this.nickName 	= user.getNickName();
		this.email 		= user.getEmail();
		this.gender		= user.getGender();
		this.birthdate 	= user.getBirthdate();
		this.deviceToken = user.getDeviceToken();
		this.password = user.getPassword();
		this.clearance = user.getClearance();
		this.verification = user.getVerification();
	}
	public UserDto() {	
	}
	
	public String getRoot(){
		return root;
	}
	
	public void setRoot(String root){
		this.root = root;
	}
	
	
	public String getEndpoint() {
		return endpoint;
	}
	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}
	public void setDeletedAt(String deletedAt) {
		this.deletedAt = deletedAt;
	}
	public String getCreatedAt(){
		return this.createdAt;
	}
	
	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}
	
	public String getUpdatedAt(){
		return this.updatedAt;
	}
	
	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}
	
	public String getDeletedAt(){
		return this.deletedAt;
	}
	
	public void setDeleteddAt(String deletedAt){
		this.deletedAt = deletedAt;
	}
	
	public String getFirstName(){
		return this.firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName(){
		return this.lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	
	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getEmail(){
		return this.email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getGender(){
		return this.gender;
	}
	
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public String getBirthdate(){
		return this.birthdate;
	}
	
	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}
	
	
	public String getDeviceToken() {
		return deviceToken;
	}

	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getClearance() {
		return clearance;
	}
	public void setClearance(String clearance) {
		this.clearance = clearance;
	}
	
	public String getVerification() {
		return verification;
	}
	public void setVerification(String verification) {
		this.verification = verification;
	}
	@Override
	public String getId() {
		return this.id;
	}
	@Override
	public void setId(String id) {
		this.id = id;
		
	}
}
