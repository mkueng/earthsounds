package net.lucidinterlude.rodrigez_ng.dto;

import java.util.HashMap;
import java.util.Map;
import org.restlet.Response;

/**
 * rodrigez/UserDao	
 * @author Marco Kueng
 * (c) 2013 lucidinterlude
 */

public class ResponseDto<T> {
	
	private Response response;
	private Map<String, T> responseRoot	= new HashMap<String, T>();
	private Map<String, T> responseContent = new HashMap<String, T>();


	public void setResponseContent(String string, T obj) {
		responseContent.put(string, obj);
	}
	
	public void deleteResponseContent(String string) {
		responseContent.remove(string);
	}
	
	public Object getResponseContent(String key) {
		return this.responseContent.get(key);
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, T> getResponseRoot() {
		//responseRoot.put("response", (T) this.responseContent);
		return this.responseContent;
	}
	
	public Response getResponse() {
		return this.response;
	}
	
	public void setResponse(Response response) {
		this.response = response;
	}	
}
