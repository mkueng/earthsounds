package net.lucidinterlude.rodrigez_ng.dto;

import java.util.ArrayList;

/**
 * rodrigez/RegionDto
 * @author Marco Kueng
 *	(c) 2013 lucidinterlude
 */

public class TerritoriesDto implements IDto{
	private ArrayList<TerritoryDto> territories;

	public TerritoriesDto(){
	}
	
	public TerritoriesDto(ArrayList<TerritoryDto> territories) {
		this.setTerritories(territories);
	}

	public ArrayList<TerritoryDto> getTerritories() {
		return territories;
	}

	public void setTerritories(ArrayList<TerritoryDto> territories) {
		this.territories = territories;
	}

	@Override
	public String getId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setId(String id) {
		// TODO Auto-generated method stub
		
	}
}
