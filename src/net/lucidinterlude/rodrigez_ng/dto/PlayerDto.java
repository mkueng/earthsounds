package net.lucidinterlude.rodrigez_ng.dto;

import net.lucidinterlude.rodrigez_ng.model.Player;

public class PlayerDto implements IDto{

	private Double		longitude;
	private Double 		latitude;
	private Double		altitude;
	private Double		speed;
	private String 		nickName;
	private String 		deviceToken;
	
	public PlayerDto(Player player){
		this.longitude=player.getLongitude();
		this.latitude=player.getLatitude();
		this.altitude=player.getAltitude();
		this.speed=player.getSpeed();
		this.nickName=player.getNickName();
		this.deviceToken=player.getDeviceToken();
	}
	
	public PlayerDto() {
		
	}
	
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getAltitude() {
		return altitude;
	}
	public void setAltitude(Double altitude) {
		this.altitude = altitude;
	}
	public Double getSpeed() {
		return speed;
	}
	public void setSpeed(Double speed) {
		this.speed = speed;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public String getDeviceToken() {
		return deviceToken;
	}
	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}

	@Override
	public String getId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setId(String id) {
		// TODO Auto-generated method stub
		
	}
	
	
	
	
}
