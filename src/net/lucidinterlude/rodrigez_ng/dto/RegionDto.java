package net.lucidinterlude.rodrigez_ng.dto;

/**
 * rodrigez/RegionDto
 * @author Marco Kueng
 * (c) 2013 lucidinterlude
 */

public class RegionDto implements IDto {
	private String id;
	private String createdAt;
	private String updatedAt;
	private String deletedAt;
	private String name;
	private Double mainGrid;
	
	public RegionDto(){
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getMainGrid() {
		return mainGrid;
	}
	public void setMainGrid(Double mainGrid) {
		this.mainGrid = mainGrid;
	}

	@Override
	public String getId() {
		return this.id;
	}
	@Override
	public void setId(String id) {
		this.id = id;
	}
	
	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getDeletedAt() {
		return deletedAt;
	}

	public void setDeletedAt(String deletedAt) {
		this.deletedAt = deletedAt;
	}
}
