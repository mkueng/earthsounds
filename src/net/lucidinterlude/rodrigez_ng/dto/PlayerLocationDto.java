package net.lucidinterlude.rodrigez_ng.dto;

import com.google.appengine.api.datastore.GeoPt;

import net.lucidinterlude.rodrigez_ng.model.Location;

public class PlayerLocationDto implements IDto{
	
	private GeoPt geoPt;
	private Double speed;
	private Double altitude;
	
	public GeoPt getGeoPt() {
		return geoPt;
	}

	public void setGeoPt(GeoPt geoPt) {
		this.geoPt = geoPt;
	}

	public Double getAltitude() {
		return altitude;
	}

	public void setAltitude(Double altitude) {
		this.altitude = altitude;
	}

	public PlayerLocationDto(Location location){
		this.altitude = location.getAltitude();
		this.speed = location.getSpeed();
		this.geoPt = location.getGeoPt();
	}
	
	public PlayerLocationDto(){
		
	}
	
	public Double getSpeed() {
		return speed;
	}

	public void setSpeed(Double speed) {
		this.speed = speed;
	}

	@Override
	public String getId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setId(String id) {
		// TODO Auto-generated method stub
		
	}
}
