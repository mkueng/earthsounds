package net.lucidinterlude.rodrigez_ng.dto;

public class CoordinateDto {

	@SuppressWarnings("unused")
	private Double latitude;
	@SuppressWarnings("unused")
	private Double longitude;
	
	public CoordinateDto(Double latitude,Double longitude) {
		this.latitude = latitude;
		this.longitude = longitude;
	}
	
}
