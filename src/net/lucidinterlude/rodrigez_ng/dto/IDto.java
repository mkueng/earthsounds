package net.lucidinterlude.rodrigez_ng.dto;

/**
 * rodrigez/IDto
 * @author Marco Kueng
 * (c) 2013 lucidinterlude
 */

public interface IDto {
	public String getId();
	public void setId(String id);
}
