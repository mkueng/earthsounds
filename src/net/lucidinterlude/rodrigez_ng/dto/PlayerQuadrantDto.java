package net.lucidinterlude.rodrigez_ng.dto;

import java.util.ArrayList;

import com.google.appengine.api.search.GeoPoint;

import net.lucidinterlude.rodrigez_ng.utility.Helper;

@SuppressWarnings("unused")
public class PlayerQuadrantDto {
	//private GeoPoint nw;
	//private GeoPoint ne;
	//private GeoPoint se;
	//private GeoPoint sw;
	private String id;
	private String name = "xxx";
	private ArrayList<CoordinateDto> coordinates;
	
	
	
	public PlayerQuadrantDto(Double latitude, Double longitude, Double size,String id){
		this.id=id;
		
		coordinates = new ArrayList<CoordinateDto>();
		coordinates.add(new CoordinateDto(latitude,longitude));
		coordinates.add(new CoordinateDto(Helper.calculateRoundedDouble(latitude+size),longitude));
		coordinates.add(new CoordinateDto(Helper.calculateRoundedDouble(latitude+size), Helper.calculateRoundedDouble(longitude+size)));
		coordinates.add(new CoordinateDto(latitude,Helper.calculateRoundedDouble(longitude+size)));
		
		/*this.sw = new GeoPoint(latitude, longitude);
		this.nw = new GeoPoint(Helper.calculateRoundedDouble(latitude+size),longitude);
		this.ne = new GeoPoint(Helper.calculateRoundedDouble(latitude+size), Helper.calculateRoundedDouble(longitude+size));
		this.se = new GeoPoint(latitude,Helper.calculateRoundedDouble(longitude+size));*/
		
		
	}
}
