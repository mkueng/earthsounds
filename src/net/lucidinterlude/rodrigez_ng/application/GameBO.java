package net.lucidinterlude.rodrigez_ng.application;

import java.util.List;

import org.json.JSONObject;
import org.restlet.ext.json.JsonRepresentation;

import net.lucidinterlude.rodrigez_ng.dto.ResponseDto;
import net.lucidinterlude.rodrigez_ng.model.Game;
import net.lucidinterlude.rodrigez_ng.persistence.EntityCreator;
import net.lucidinterlude.rodrigez_ng.persistence.GameDao;

public class GameBO extends BO{
	
	private GameDao gameDao;
	private EntityCreator entityCreator;
	private ResponseDto<Object> responseDto;
	
	public GameBO(){
		entityCreator = new EntityCreator();
		gameDao = new GameDao();
	}
	
	//create game
		public Game createGame(JsonRepresentation jsonRepresentation) throws Exception {
			JSONObject gameJO = new JSONObject();
			Game game =null;
			gameJO = jsonRepresentation.getJsonObject();
			game = new Game();
			game = entityCreator.createEntity(gameJO, Game.class);
			game.setType("game");
			game = gameDao.createGame(game);
		
			return game;
		}

	//get sound
		public Game getGame (String id) throws Exception{
			return gameDao.getGame(id);
		}

	//get sounds
		public List<Game> getGames() throws Exception {
			return gameDao.getGames();
		}
		
	//delete sound
		public ResponseDto<Object> deleteGame(String id){
			responseDto = gameDao.deleteGame(id);
			return responseDto;
		}
}
