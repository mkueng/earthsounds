package net.lucidinterlude.rodrigez_ng.application;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.google.appengine.api.search.Document;
import com.google.appengine.api.search.Field.FieldType;

import net.lucidinterlude.rodrigez_ng.dto.IDto;

/**
 * rodrigez/BO
 * @author Marco Kueng
 * (c) 2013 lucidinterlude
 */

public class BO implements IBO{

	/**
	 * Returns a HashMap<String,Object> which contains the keys and values 
	 * of the provided parameterMap mapped to a model Class
	 * 
	 * @param paramMap 		a HashMap<String,String>
	 * @param modelToMap	the class to map to
	 * @return						a HashMap<String,Object>
	 */
	
	@Override
	public <T> HashMap<String,Object> getEntityByParams(HashMap<String, String> paramMap, Class<T> modelToMap) throws Exception {
		HashMap<String,Object> queryMap = new HashMap<String,Object>();
		Iterator<?> it = paramMap.entrySet().iterator();
		
		while (it.hasNext()) {
			@SuppressWarnings("rawtypes")
			Map.Entry pairs = (Map.Entry)it.next();
			String paramKey = (String) pairs.getKey();
			String paramValue = (String) pairs.getValue();
			try {
				Field field = modelToMap.getDeclaredField(paramKey);
				Type fieldType =field.getType();
			
				if (fieldType.equals(Double.class)) {
					queryMap.put(paramKey, Double.parseDouble(paramValue));
				};
				if (fieldType.equals(Integer.class)) {
					queryMap.put(paramKey, Integer.parseInt(paramValue));
				};
				if (fieldType.equals(String.class)){
					queryMap.put(paramKey, paramValue);
				};
			} catch (NoSuchFieldException ex) {
				queryMap.put(paramKey, paramValue);
			}
	        it.remove(); // avoids a ConcurrentModificationException
		}
		
		return queryMap;
	}
	
	
	/**
	 * Returns an IDto 
	 * 
	 * @param document
	 * @param dto
	 * @return IDto
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws IntrospectionException
	 */
	public IDto mapDocumentToTransferObject(Document document, IDto dto) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException, IntrospectionException{
		Set<String> fieldNames = document.getFieldNames();
		dto.setId(document.getId());
		Class<? extends IDto> klass = dto.getClass();
		
		for (String fieldName : fieldNames) {
			PropertyDescriptor propDesc;
			FieldType fieldType = document.getOnlyField(fieldName).getType();
			
			switch (fieldType) {
				case TEXT 		: {	propDesc = new PropertyDescriptor(fieldName,klass); propDesc.getWriteMethod().invoke(dto, document.getOnlyField(fieldName).getText()); break;}
				case NUMBER 	: { propDesc = new PropertyDescriptor(fieldName,klass); propDesc.getWriteMethod().invoke(dto, document.getOnlyField(fieldName).getNumber());break;}
				case DATE 		: { propDesc = new PropertyDescriptor(fieldName,klass); propDesc.getWriteMethod().invoke(dto, document.getOnlyField(fieldName).getNumber());break;}
			default:
				break;
			}
			propDesc = null;
			
		}
		
		return dto;
	}
	
}
