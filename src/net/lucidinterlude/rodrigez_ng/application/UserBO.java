package net.lucidinterlude.rodrigez_ng.application;

/**
 * rodrigez/UserBO
 * @author Marco Kueng
 */

import java.beans.IntrospectionException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.restlet.Response;
import org.restlet.data.Status;
import org.restlet.ext.json.JsonRepresentation;

import net.lucidinterlude.rodrigez_ng.dto.ResponseDto;
import net.lucidinterlude.rodrigez_ng.dto.UserDto;
import net.lucidinterlude.rodrigez_ng.model.Token;
import net.lucidinterlude.rodrigez_ng.model.User;
import net.lucidinterlude.rodrigez_ng.model.UserVerification;
import net.lucidinterlude.rodrigez_ng.persistence.EntityCreator;
import net.lucidinterlude.rodrigez_ng.persistence.UserDao;
import net.lucidinterlude.rodrigez_ng.utility.PasswordAuthentication;


public class UserBO {
	
	private UserDao userDao;
	private TokenBO tokenBO;
	private EntityCreator entityCreator;
	private ResponseDto<Object> responseDto;
	private Response response;
	
	public UserBO() {
		userDao 		= new UserDao();
		tokenBO			= new TokenBO();
		entityCreator 	= new EntityCreator();
		responseDto 	= null;
		response 		= new Response(null);
	};
	
	public boolean createVerificationEntry (String userId, String verification) {
		UserVerification userVerification = new UserVerification();
		userVerification.setId(userId);
		userVerification.setVerification(verification);
		return userDao.createVerificationEntry(userVerification);
	}
	
	public boolean checkVerificationEntry (String verification) {
		UserVerification userVerification  = null;
		userVerification = userDao.getVerificationEntry(verification);
		User user = null;
		if (userVerification !=null) {
			try {
				user  = userDao.updateUserVerification(userVerification.getId());
				userDao.deleteVerificationEntry(userVerification.getId());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (user != null) return true;
		}
		return false;
	}
	
//create user
	public User createUser(JsonRepresentation jsonRepresentation) {
		JSONObject userJO =new JSONObject();
		User user =null;
		try {
			userJO = jsonRepresentation.getJsonObject();
			user = new User();
			user = entityCreator.createEntity(userJO, User.class);
			user.setRoot("user");
			user.setClearance("user");
			user.setVerification("verificated");
			try {
				user.setPassword(PasswordAuthentication.generateStrongPasswordHash(user.getPassword()));
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvalidKeySpecException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (userDao.checkEntity(user.getNickName()).getResponse().getStatus().isSuccess()) {
				response.setStatus(Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY);
				responseDto = new ResponseDto<Object>();
				responseDto.setResponse(response);
				responseDto.setResponseContent("message","nickname not available");
			} else {
				user = userDao.createUser(user);
			}
			
		} catch (JSONException e) {
			response = new Response(null);
			responseDto = new ResponseDto<Object>();
			response.setStatus(Status.CLIENT_ERROR_BAD_REQUEST,"JSON not valid");
			responseDto.setResponse(response);
			e.printStackTrace();
		} catch (IntrospectionException e) {
			response = new Response(null);
			responseDto = new ResponseDto<Object>();
			response.setStatus(Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY,"Entity not valid");
			responseDto.setResponse(response);
			e.printStackTrace();
		}
		return user;
	}

//update user
	public ResponseDto<Object> updateUser(String id,JsonRepresentation jsonRepresentation) {
		JSONObject userJO =null;
		String nickName = null;
		try {
			userJO = jsonRepresentation.getJsonObject();
			try {
				nickName = userJO.getString("nickName");
			} catch (org.json.JSONException ex) {
				
			}
			if (nickName != null) {
				Response nickNameResponse = userDao.checkEntity(userJO.getString("nickName")).getResponse();
				if (nickNameResponse.getStatus().isSuccess() & !(nickNameResponse.getStatus().getDescription().equals(id))){
					response.setStatus(Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY);
					responseDto = new ResponseDto<Object>();
					responseDto.setResponse(response);
					responseDto.setResponseContent("message","nickname not available");
				} 
				else {responseDto = userDao.updateUser(id,userJO); }
			} 
			else {
				responseDto = userDao.updateUser(id,userJO);
			}
			
			
		} catch (JSONException e) {
			response = new Response(null);
			responseDto = new ResponseDto<Object>();
			response.setStatus(Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY,"JSON not valid");
			responseDto.setResponse(response);
			e.printStackTrace();
		}
		return responseDto;
	}
	
//get user 
	public User getUser(String id) throws Exception {
		return userDao.getUser(id);
	}

//get all users
	public List<User> getAllUsers() throws Exception {
		return userDao.getAllUsers();
	}
	
//delete user
	public ResponseDto<Object> deleteUser(String id){
		responseDto = userDao.deleteUser(id);
		return responseDto;
	}
	
//check nickName
	public ResponseDto<Object> checkNickname(String nickName){
		return userDao.checkEntity(nickName);
	}
	
//check login
	public Token checkLogin(JsonRepresentation jsonRepresentation) {
		JSONObject userJO =null;
		Token token = new Token();
		try {
			userJO = jsonRepresentation.getJsonObject();
			String nickName = userJO.getString("nickName");
			String password = userJO.getString("password");
			String clearance = "";
			
			responseDto=userDao.getUserByNickname(nickName);
			
			if (responseDto.getResponse().getStatus().equals(Status.SUCCESS_OK)) {
				String verification = ((UserDto) responseDto.getResponseContent("user")).getVerification();
				clearance = ((UserDto) responseDto.getResponseContent("user")).getClearance();
				Boolean hashedPassword = PasswordAuthentication.validatePassword(password, ((UserDto) responseDto.getResponseContent("user")).getPassword());
				if (hashedPassword && verification.equals("verificated")) token = tokenBO.persistToken(nickName, password, clearance);

				
			}
			
		} catch (JSONException e) {
			response = new Response(null);
			responseDto = new ResponseDto<Object>();
			response.setStatus(Status.CLIENT_ERROR_BAD_REQUEST,"JSON not valid");
			responseDto.setResponse(response);
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return token;
	
	}
}
