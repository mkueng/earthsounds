package net.lucidinterlude.rodrigez_ng.application;

import java.util.ArrayList;
import java.util.Observable;

import org.json.JSONException;
import org.json.JSONObject;
import org.restlet.Response;
import org.restlet.data.Status;
import org.restlet.ext.json.JsonRepresentation;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.search.Results;
import com.google.appengine.api.search.ScoredDocument;
import com.google.appengine.api.search.Document;

import net.lucidinterlude.rodrigez_ng.dto.PlayerInDistanceDto;
import net.lucidinterlude.rodrigez_ng.dto.PlayerQuadrantDto;
import net.lucidinterlude.rodrigez_ng.dto.ResponseDto;
import net.lucidinterlude.rodrigez_ng.dto.TerritoryDto;
import net.lucidinterlude.rodrigez_ng.game.ActionHandler;
import net.lucidinterlude.rodrigez_ng.game.TerritoryHandler;
import net.lucidinterlude.rodrigez_ng.index.GeofenceDocumentManager;
import net.lucidinterlude.rodrigez_ng.index.PlayerDocumentManager;
import net.lucidinterlude.rodrigez_ng.model.Action;
import net.lucidinterlude.rodrigez_ng.model.Location;
import net.lucidinterlude.rodrigez_ng.model.Player;
import net.lucidinterlude.rodrigez_ng.model.Sound;
import net.lucidinterlude.rodrigez_ng.persistence.PlayerDao;

public class PlayerBO extends Observable{

	private PlayerDao playerDao;
	private ResponseDto<Object> responseDto;
	private Response response;
	private TerritoryHandler territoryHandler;
	private ActionHandler actionHandler;
	
	public PlayerBO() {
		playerDao = new PlayerDao();
		responseDto = null;
		response = new Response(null);
	}
	
	public void setTerritoryHandler(TerritoryHandler territoryHandler) {
		this.territoryHandler = territoryHandler;
	}
	
	public void setActionHandler(ActionHandler actionHandler) {
		this.actionHandler = actionHandler;
	}

	/**
	 * get player
	 * @param id
	 * @return
	 */
	public Player getPlayer(String id) {
		return new PlayerDao().getPlayer(id);
	}
	
	/**
	 * get player location
	 * @param id
	 * @return Location
	 */
	public Location getPlayerLocation(String id) {
		Location location = null;
		Player player = playerDao.getPlayer(id);
		player.getLocations();
		if (player !=null) {
			location =  player.getLocations().getLast();
		}
		return location;
	} 
	
	/**
	 * get player locations with limit
	 * @param playerId
	 * @param limit
	 * @param offset
	 * @return ArrayList<Location>
	 */
	public ArrayList<Location> getPlayerLocationsWithLimit(String playerId, String limit, String offset){
		Player player = playerDao.getPlayer(playerId);
		return playerDao.getPlayerLocationWithLimit(player, limit, offset);
	}
	
	/**
	 * update user agent location
	 * @param playerDocumentManager
	 * @param jsonRepresentation
	 * @return ResponseDto
	 */
	public ResponseDto<Object> updateUserAgentLocation(PlayerDocumentManager playerDocumentManager,JsonRepresentation jsonRepresentation) {
			JSONObject userAgentJO = null;
			responseDto = new ResponseDto<Object>();
			Response response = new Response(null);
			
			try {
				userAgentJO = jsonRepresentation.getJsonObject();
				Document territoryDoc = this.territoryHandler.checkTerritoryState(userAgentJO.getDouble("latitude"), userAgentJO.getDouble("longitude"));
				if (territoryDoc !=null) {
					Action action = actionHandler.handleState(territoryDoc);
					//Sound sound = actionHandler.handleState(territoryDoc);
					if (action != null) {
						responseDto.setResponseContent("actionContent", action.getContent());
						responseDto.setResponseContent("actionId", action.getId() );
					}
				}
			response.setStatus(Status.SUCCESS_CREATED,"userAgentPosition updated");
			responseDto.setResponse(response);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		responseDto.setResponseContent("userAgentPosition", "userAgentPosition updated");
		return responseDto;
	}
	
	/**
	 * update player location
	 * @param playerDocumentManager
	 * @param id
	 * @param jsonRepresentation
	 * @return ResponseDto
	 * @throws Exception
	 */
	public ResponseDto<Object> updatePlayerLocation(PlayerDocumentManager playerDocumentManager, String id,JsonRepresentation jsonRepresentation) throws Exception{
		JSONObject playerJO = null;
		responseDto=null;	
		try {
			playerJO = jsonRepresentation.getJsonObject();
			playerJO.remove("endpoint");
			playerJO.remove("id");
			playerJO.remove("root");
			playerJO.remove("locations");
			
			responseDto = playerDao.updatePlayerLocation(id,playerJO);
			Player player = (Player) responseDto.getResponseContent("player");
			if (player != null) {
				Key key = player.getKey();
				String parentId = key.getParent().getName();
				playerDocumentManager.createDocument(parentId,player.getLatitude(),player.getLongitude(),player.getNickName(),player.getDeviceToken());
				setChanged();
				notifyObservers(player);
				Document territoryDoc = this.territoryHandler.checkTerritoryState(player);
				if (territoryDoc !=null) {
					Action action = actionHandler.handleState(territoryDoc);
					if (action != null) {
						responseDto.setResponseContent("actionContent", action.getContent());
						responseDto.setResponseContent("actionId", action.getId() );
						responseDto.setResponseContent("type", action.getType());
					}
				}
				
				responseDto.setResponseContent("player", "location updated");
			
			}			
		} catch (JSONException e) {
			response = new Response(null);
			responseDto = new ResponseDto<Object>();
			response.setStatus(Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY,"JSON not valid");
			responseDto.setResponse(response);
			e.printStackTrace();
		}
		
		return responseDto;	
	}
	
	/**
	 * get players within distance
	 * @param playerDocumentManager
	 * @param id
	 * @param distance
	 * @return ResponseDto
	 */
	public ResponseDto<Object> getPlayersWithinDistance(PlayerDocumentManager playerDocumentManager,String id, String distance) {
		response = new Response(null);
		responseDto =null;
		Results<ScoredDocument> results = null;
		ArrayList<PlayerInDistanceDto> playerInDistance = new ArrayList<PlayerInDistanceDto>();
		
		Player player = playerDao.getPlayer(id);
		responseDto.setResponseContent("player", null);
		if (player !=null) {
			results = playerDocumentManager.getPlayersInDistance(player, distance);	
			for (ScoredDocument document : results) {
				playerInDistance.add(new PlayerInDistanceDto(document));
			}
			if (playerInDistance.size()>0) {
				playerInDistance.remove(0);
			}
			responseDto.setResponseContent("playersInDistance", playerInDistance);
			response.setStatus(Status.SUCCESS_OK);
			responseDto.setResponse(response);
			playerInDistance=null;
			results=null;
		}else {
	
		}
		return responseDto;
	}
	
	/**
	 * get player quadrants
	 * @param geofenceDocumentManager
	 * @param playerId
	 * @param limit
	 * @return ResponseDto
	 * @throws Exception
	 */
	public ResponseDto<Object> getPlayerQuadrants(GeofenceDocumentManager geofenceDocumentManager, String playerId,String limit) throws Exception{
		
		if (limit == null) limit="1";
		response = new Response(null);
		responseDto = new ResponseDto<Object>();
		
		String nickName = playerDao.getPlayer(playerId).getNickName() ;
		responseDto.deleteResponseContent("nickName");
		responseDto.deleteResponseContent("player");
		Results<ScoredDocument> results = geofenceDocumentManager.getQuadrantsByOwner(playerId,Integer.parseInt(limit));
		ArrayList<PlayerQuadrantDto> playerQuadrantList = new ArrayList<PlayerQuadrantDto>();
		
		
		for (ScoredDocument document : results) {
			String documentId = document.getId();
			//String geopoint = document.getOnlyField("geopoint").getText();
			String playersId = document.getOnlyField("playerId").getText();
			Double latitude = document.getOnlyField("latitude").getNumber();
			Double longitude = document.getOnlyField("longitude").getNumber();
			geofenceDocumentManager.createGeoFenceDocument(documentId,playersId, latitude,longitude,nickName,"xxx");
			//log.info("geopoint"+document.getOnlyField("geopoint"));
			playerQuadrantList.add(new PlayerQuadrantDto(document.getOnlyField("latitude").getNumber(),document.getOnlyField("longitude").getNumber(),0.001,document.getId()));
		}
		response.setStatus(Status.SUCCESS_OK);
		responseDto.setResponse(response);
		responseDto.setResponseContent("id", playerId);
		responseDto.setResponseContent("territories", playerQuadrantList);
		return responseDto;
	}
	
	/**
	 * Delete player quadrants
	 * @param geofenceDocumentManager
	 * @param playerId
	 * @return ResponseDto
	 */
	public ResponseDto<Object> deletePlayerQuadrants(GeofenceDocumentManager geofenceDocumentManager, String playerId) {
		response = new Response(null);
		responseDto = new ResponseDto<Object>();
		geofenceDocumentManager.deleteDocuments(playerId);
		response.setStatus(Status.SUCCESS_OK);
		responseDto.setResponse(response);
		return responseDto;
	}
	
	/**
	 * get territories in distance
	 * @param geofenceDocumentManager
	 * @param latitude
	 * @param longitude
	 * @param distance
	 * @return ResponseDto
	 */
	public ResponseDto<Object> getTerritoriesInDistance(GeofenceDocumentManager geofenceDocumentManager, String latitude,String longitude, String distance) {
		
		response = new Response(null);
		responseDto = new ResponseDto<Object>();
		Results<ScoredDocument> results = geofenceDocumentManager.getTerritoriesInDistance(Double.parseDouble(latitude),Double.parseDouble(longitude),distance);
		ArrayList<TerritoryDto> territoryList = new ArrayList<TerritoryDto>();
		for (ScoredDocument document : results) {
			territoryList.add(new TerritoryDto(document.getOnlyField("latitude").getNumber(),document.getOnlyField("longitude").getNumber(),0.001,document.getId(),document.getOnlyField("name").getText(),document.getOnlyField("ownerNickName").getText(),document.getOnlyField("playerId").getText()));

		}
		response.setStatus(Status.SUCCESS_OK);
		responseDto.setResponse(response);
		
		responseDto.setResponseContent("territories", territoryList);
		return responseDto;
	}

	
	
}
