package net.lucidinterlude.rodrigez_ng.application;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import net.lucidinterlude.rodrigez_ng.model.Token;
import net.lucidinterlude.rodrigez_ng.persistence.TokenDao;
import security.TokenHandler;

public class TokenBO extends BO{

	private TokenDao tokenDao;
	private TokenHandler tokenHandler;
	private Token token;
	
	
	public TokenBO(){
		tokenDao = new TokenDao();
		tokenHandler = new TokenHandler();
		token = new Token();
	}

	public Token persistToken (String nickName, String password, String clearance) throws Exception {
		token.setTokenString(this.createToken(nickName, password));
		token.setNickName(nickName);
		token.setClearance(clearance);
		tokenDao.persistToken(token);
		return token;
	}
	
	public Token checkToken (String tokenString) throws ParseException{
		Token token = tokenDao.readToken(tokenString);
		DateFormat format = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy", Locale.ENGLISH);
		Date tokenDate = format.parse(token.getCreatedAt());
		Date nowDate = new Date();
		long timeDiff = nowDate.getTime()-tokenDate.getTime();
		
		if (token != null && token.getTokenString().equals(tokenString) && timeDiff < 18000000) {
			return token;
		} else {
			tokenDao.deleteToken(token.getId());
			return token;
		}
	}
	
	private String createToken(String nickName, String password) {
		return tokenHandler.createToken(nickName, password);
	}
}
