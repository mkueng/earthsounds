package net.lucidinterlude.rodrigez_ng.application;

/**
 * rodrigez/IBO
 * @author Marco Kueng
 * (c) 2013 lucidinterlude
 */

import java.util.HashMap;

public interface IBO {
	public <T> HashMap<String,Object> getEntityByParams(HashMap<String,String> paramMap, Class<T> classToMap) throws Exception;
}
