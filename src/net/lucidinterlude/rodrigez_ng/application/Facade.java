package net.lucidinterlude.rodrigez_ng.application;

import java.text.ParseException;
import java.util.HashMap;
import org.json.JSONObject;
import org.restlet.ext.json.JsonRepresentation;

import com.google.appengine.api.taskqueue.Queue;


import net.lucidinterlude.rodrigez_ng.async.RegionInitialQueue;
import net.lucidinterlude.rodrigez_ng.dto.GameDto;
import net.lucidinterlude.rodrigez_ng.dto.NicknameDto;
import net.lucidinterlude.rodrigez_ng.dto.NumberDto;
import net.lucidinterlude.rodrigez_ng.dto.PlayerDto;
import net.lucidinterlude.rodrigez_ng.dto.PlayerLocationDto;
import net.lucidinterlude.rodrigez_ng.dto.PlayerLocationsDto;
import net.lucidinterlude.rodrigez_ng.dto.RegionDto;
import net.lucidinterlude.rodrigez_ng.dto.RegionsDto;
import net.lucidinterlude.rodrigez_ng.dto.ResponseDto;
import net.lucidinterlude.rodrigez_ng.dto.SoundDto;
import net.lucidinterlude.rodrigez_ng.dto.SoundsDto;
import net.lucidinterlude.rodrigez_ng.dto.TerritoriesDto;
import net.lucidinterlude.rodrigez_ng.dto.TerritoryDto;
import net.lucidinterlude.rodrigez_ng.dto.TokenDto;
import net.lucidinterlude.rodrigez_ng.dto.UserDto;
import net.lucidinterlude.rodrigez_ng.dto.UsersDto;
import net.lucidinterlude.rodrigez_ng.game.ActionHandler;
import net.lucidinterlude.rodrigez_ng.game.TerritoryHandler;
import net.lucidinterlude.rodrigez_ng.index.DocumentManagerFactory;
import net.lucidinterlude.rodrigez_ng.index.DocumentManagerType;
import net.lucidinterlude.rodrigez_ng.index.GeofenceDocumentManager;
import net.lucidinterlude.rodrigez_ng.index.PlayerDocumentManager;
import net.lucidinterlude.rodrigez_ng.index.TerritoryDocumentManager;
import net.lucidinterlude.rodrigez_ng.model.Game;
import net.lucidinterlude.rodrigez_ng.model.NickName;
import net.lucidinterlude.rodrigez_ng.model.Player;
import net.lucidinterlude.rodrigez_ng.model.Region;
import net.lucidinterlude.rodrigez_ng.model.Sound;
import net.lucidinterlude.rodrigez_ng.model.User;
import net.lucidinterlude.rodrigez_ng.model.Territory;
import net.lucidinterlude.rodrigez_ng.model.Token;

/**
 * rodrigez/Facade
 * @author Marco Kueng
 * (c) 2017 lucidinterlude
 */

public class Facade {
	
	private UserBO userBO;
	private PlayerBO playerBO;
	private TerritoryBO territoryBO;	
	private PlayerDocumentManager playerDocumentManager;
	private GeofenceDocumentManager geofenceDocumentManager;
	private TerritoryDocumentManager territoryDocumentManager;
	private RegionInitialQueue regionInitialQueue;
	
	
	public Facade() {
	}
	
//user
	
	/**
	 * create user
	 * @param jsonRepresentation
	 * @param request
	 * @return ResponseDto
	 * @throws Exception
	 */
	public ResponseDto<Object> createUser(JsonRepresentation jsonRepresentation, String request) throws Exception {
		userBO = new UserBO();
		TOAssembler<User> toAssembler = new TOAssembler<User>();
		User user = userBO.createUser(jsonRepresentation);
		
		ResponseDto<Object> responseDto =  toAssembler.assembleResponseTO(new UserDto(),user, request);
		responseDto.setResponseContent("user", user);
		return responseDto;
	}
	
	/**
	 * get user
	 * @param id
	 * @param request
	 * @return ResponseDto
	 * @throws Exception
	 */
	public ResponseDto<Object> getUser(String id, String request) throws Exception {
		UserBO userBO = new UserBO();
		TOAssembler<User> toAssembler = new TOAssembler<User>();
		return toAssembler.assembleResponseTO(new UserDto(),userBO.getUser(id), request);	
	}
	
	/**
	 * get all users
	 * @param request
	 * @return ResponseDto
	 * @throws Exception
	 */
	public ResponseDto<Object> getAllusers(String request) throws Exception {
		UsersDto usersDto = new UsersDto(new UserBO().getAllUsers());
		TOAssembler<UsersDto> toAssembler = new TOAssembler<UsersDto>();
		return toAssembler.assembleResponseTO(usersDto, usersDto, request);
	}
	
	/**
	 * update user
	 * @param id
	 * @param jsonRepresentation
	 * @return ResponseDto
	 */
	public ResponseDto<Object> updateUser(String id, JsonRepresentation jsonRepresentation) {
		userBO = new UserBO();
		return userBO.updateUser(id,jsonRepresentation);
	}
	
	/**
	 * delete user
	 * @param id
	 * @return  ResponseDto
	 */
	public ResponseDto<Object> deleteUser(String id) {
		userBO = new UserBO();
		return userBO.deleteUser(id);
	}
	
	/**
	 * check user nickname
	 * @param nickName
	 * @param request
	 * @return ResponseDto
	 * @throws Exception
	 */
	public ResponseDto<Object> checkUserNickName(String nickName,String request) throws Exception {
		UserBO userBO = new UserBO();
		TOAssembler<NickName> toAssembler = new TOAssembler<NickName>();
		return toAssembler.assembleResponseTO(new NicknameDto(),userBO.checkNickname(nickName).getResponseContent("nickName"), request);	
	}
	
	/**
	 * create verification entry
	 * @param userId
	 * @param verification
	 * @return
	 */
	public boolean createVerificationEntry(String userId, String verification) {
		userBO = new UserBO();
		return userBO.createVerificationEntry(userId, verification);
	}
	
	/**
	 * check verification entry
	 * @param verification
	 * @return boolean
	 */
	public boolean checkVerificationEntry(String verification) {
		userBO = new UserBO();
		return userBO.checkVerificationEntry(verification);
	}
	
	/**
	 * check login
	 * @param jsonRepresentation
	 * @param request
	 * @return ResponseDto
	 * @throws Exception
	 */
	public ResponseDto<Object> checkLogin(JsonRepresentation jsonRepresentation, String request) throws Exception {
		userBO = new UserBO();
		TOAssembler<Token> toAssembler = new TOAssembler<Token>();
		return toAssembler.assembleResponseTO(new TokenDto(),userBO.checkLogin(jsonRepresentation),request);
	}
	
//player
	
	/**
	 * get player
	 * @param id
	 * @param request
	 * @return ResponseDto
	 * @throws Exception
	 */
	public ResponseDto<Object> getPlayer(String id,String request) throws Exception {
		playerBO = new PlayerBO();
		TOAssembler<Player> toAssembler = new TOAssembler<Player>();
		return toAssembler.assembleResponseTO(new PlayerDto(),playerBO.getPlayer(id), request);	
	}
	
//player location
	
	/**
	 * get player location
	 * @param id
	 * @param request
	 * @return ResponseDto
	 * @throws Exception
	 */
	public ResponseDto<Object> getPlayerLocation(String id,  String request) throws Exception {
		playerBO = new PlayerBO();
		TOAssembler<Player> toAssembler = new TOAssembler<Player>();
		return toAssembler.assembleResponseTO(new PlayerLocationDto(),playerBO.getPlayerLocation(id), request);	
	}
	
	/**
	 * get player locations with limit
	 * @param id
	 * @param limit
	 * @param offset
	 * @param request
	 * @return ResponseDto
	 * @throws Exception
	 */
	public ResponseDto<Object> getPlayerLocationsWithLimit(String id, String limit, String offset, String request) throws Exception {
		PlayerLocationsDto playerlocationsDto = new PlayerLocationsDto(new PlayerBO().getPlayerLocationsWithLimit(id, limit, offset));
		TOAssembler<PlayerLocationsDto> toAssembler = new TOAssembler<PlayerLocationsDto>();
		return toAssembler.assembleResponseTO(playerlocationsDto, playerlocationsDto, request);	
	}
	
	/**
	 * update user agent
	 * @param jsonRepresentation
	 * @return ResponseDto
	 * @throws Exception
	 */
	public ResponseDto<Object> updateUserAgentLocation(JsonRepresentation jsonRepresentation) throws Exception {
		playerBO = new PlayerBO();
		ActionHandler actionHandler = new ActionHandler();
		
		playerDocumentManager = (PlayerDocumentManager)DocumentManagerFactory.createDocumentManager(DocumentManagerType.PLAYER);
		geofenceDocumentManager = (GeofenceDocumentManager)DocumentManagerFactory.createDocumentManager(DocumentManagerType.GEOFENCE);
		territoryDocumentManager = (TerritoryDocumentManager)DocumentManagerFactory.createDocumentManager(DocumentManagerType.TERRITORY);
		TerritoryHandler territoryHandler = new TerritoryHandler(playerDocumentManager, geofenceDocumentManager, territoryDocumentManager);
		
		playerBO.setTerritoryHandler(territoryHandler);
		playerBO.setActionHandler(actionHandler);
		//playerBO.addObserver(territoryHandler);
		//territoryHandler.addObserver(actionHandler);
		
		return playerBO.updateUserAgentLocation(playerDocumentManager,jsonRepresentation);
	}
	
	/**
	 * update player location
	 * @param id
	 * @param jsonRepresentation
	 * @return ResponseDto
	 * @throws Exception
	 */
	public ResponseDto<Object> updatePlayerLocation(String id, JsonRepresentation jsonRepresentation) throws Exception {
		playerBO = new PlayerBO();
		ActionHandler actionHandler = new ActionHandler();
		
		playerDocumentManager = (PlayerDocumentManager)DocumentManagerFactory.createDocumentManager(DocumentManagerType.PLAYER);
		geofenceDocumentManager = (GeofenceDocumentManager)DocumentManagerFactory.createDocumentManager(DocumentManagerType.GEOFENCE);
		territoryDocumentManager = (TerritoryDocumentManager)DocumentManagerFactory.createDocumentManager(DocumentManagerType.TERRITORY);
		TerritoryHandler territoryHandler = new TerritoryHandler(playerDocumentManager, geofenceDocumentManager, territoryDocumentManager);
		
		playerBO.setTerritoryHandler(territoryHandler);
		playerBO.setActionHandler(actionHandler);
		//playerBO.addObserver(territoryHandler);
		//territoryHandler.addObserver(actionHandler);
		
		return playerBO.updatePlayerLocation(playerDocumentManager,id,jsonRepresentation);
	}

//target
	
	/**
	 * get players within distance
	 * @param id
	 * @param distance
	 * @return ResponseDto
	 */
	public ResponseDto<Object> getPlayersWithinDistance(String id, String distance) {
		playerBO = new PlayerBO();
		playerDocumentManager = (PlayerDocumentManager)DocumentManagerFactory.createDocumentManager(DocumentManagerType.PLAYER);
		return playerBO.getPlayersWithinDistance(playerDocumentManager,id, distance);
	}
	
//quadrants
	
	/**
	 * get player quadrants
	 * @param id
	 * @param limit
	 * @return ResponseDto
	 * @throws Exception
	 */
	public ResponseDto<Object> getPlayerQuadrants(String id,String limit) throws Exception {
		playerBO						= new PlayerBO();
		geofenceDocumentManager 		= (GeofenceDocumentManager)DocumentManagerFactory.createDocumentManager(DocumentManagerType.GEOFENCE);
		return playerBO.getPlayerQuadrants(geofenceDocumentManager,id,limit);
	}
	
	/**
	 * delete player quadrants
	 * @param id
	 * @return ResponseDto
	 */
	public ResponseDto<Object> deletePlayerQuadrants(String id) {
		playerBO						= new PlayerBO();
		geofenceDocumentManager 		= (GeofenceDocumentManager)DocumentManagerFactory.createDocumentManager(DocumentManagerType.GEOFENCE);
		return playerBO.deletePlayerQuadrants(geofenceDocumentManager, id);
	}
	
//territories
	
	/**
	 * get territories in distance
	 * @param latitude
	 * @param longitude
	 * @param distance
	 * @return ResponseDto
	 */
	public ResponseDto<Object> getTerritoriesInDistance(String latitude,String longitude, String distance) {
		playerBO						= new PlayerBO();
		geofenceDocumentManager 		= (GeofenceDocumentManager)DocumentManagerFactory.createDocumentManager(DocumentManagerType.GEOFENCE);
		return playerBO.getTerritoriesInDistance(geofenceDocumentManager,latitude,longitude,distance);
	}
	
//territory
	
	/**
	 * set territory
	 * @param jsonRepresentation
	 * @param request
	 * @return Queue
	 * @throws Exception
	 */
	public Queue setTerritory(JSONObject jsonRepresentation, String request) throws Exception {
		regionInitialQueue = new RegionInitialQueue();
		Queue queue = regionInitialQueue.putTaskToQueue(jsonRepresentation);
		return queue;		
	}
	
	/**
	 * get territory 
	 * @param id
	 * @param request
	 * @return ResponseDto
	 * @throws Exception
	 */
	public ResponseDto<Object> getTerritory(String id, String request) throws Exception {
		territoryBO = new TerritoryBO();
		TOAssembler<Territory> toAssembler = new TOAssembler<Territory>();
		return toAssembler.assembleResponseTO(new TerritoryDto(), territoryBO.getTerritoryFromDataStore(id),request);
	}
	
	/**
	 * get all territories
	 * @param request
	 * @return ResponseDto
	 * @throws Exception
	 */
	public ResponseDto<Object> getAllTerritories(String request) throws Exception {
		territoryBO = new TerritoryBO();
		TerritoriesDto territoriesDto = new TerritoriesDto(new TerritoryBO().getAllTerritories());
		TOAssembler<TerritoriesDto> toAssembler = new TOAssembler<TerritoriesDto>();
		return toAssembler.assembleResponseTO(new TerritoriesDto(),territoriesDto,request);
	}
	
	/**
	 * get number of territories
	 * @param paramMap
	 * @param request
	 * @return ResponseDto
	 * @throws Exception
	 */
	public ResponseDto<Object> getNumberOfTerritories(HashMap<String,String> paramMap, String request) throws Exception {
		NumberDto numberDto = new NumberDto(new TerritoryBO().getNumberOfTerritories(paramMap));
		TOAssembler<NumberDto> toAssembler = new TOAssembler<NumberDto>();
		return toAssembler.assembleResponseTO(new NumberDto(), numberDto, request);
	}
	
	/**
	 * get territory by params
	 * @param paramMap
	 * @param request
	 * @return ResponseDto
	 * @throws Exception
	 */
	public ResponseDto<Object> getTerritoriesByParams (HashMap<String,String> paramMap, String request) throws Exception {
		TerritoriesDto territoriesDto = new TerritoriesDto(new TerritoryBO().getTerritoriesByParams(paramMap));
		TOAssembler<TerritoriesDto> toAssembler = new TOAssembler<TerritoriesDto>();
		return toAssembler.assembleResponseTO(new TerritoriesDto(),territoriesDto,request);
	}
	
	/**
	 * update territory
	 * @param id
	 * @param jsonRep
	 * @throws Exception
	 */
	public void updateTerritory(String id, JsonRepresentation jsonRep) throws Exception {
		territoryBO = new TerritoryBO();
		territoryBO.updateTerritory(id, jsonRep);
	}
	
	/**
	 * deltete territory
	 * @param id
	 * @throws Exception
	 */
	public void deleteTerritory(String id) throws Exception {
		territoryBO = new TerritoryBO();
		territoryBO.deleteTerritory(id);
	}
	
	/**
	 * deltete all territories from region
	 * @param region
	 * @throws Exception
	 */
	public void deleteAllTerritoriesFromRegion (String region) throws Exception {
		HashMap<String, String> paramMap = new HashMap<String, String>();
		paramMap.put("region", region);
		new TerritoryBO().deleteAllTerritoriesFromRegion(paramMap);
	}
	
	/**
	 * delete territory
	 * @param jsonRep
	 * @throws Exception
	 */
	public void deleteTerritory(JsonRepresentation jsonRep) throws Exception {
		//territoryBO = new TerritoryBO();
		//territoryBO.deleteTerritory(jsonRep);
	}
	
//region
	
	/**
	 * set region
	 * @param jsonRepresentation
	 * @param request
	 * @return ResponseDto
	 * @throws Exception
	 */
	public ResponseDto<Object> setRegion (JsonRepresentation jsonRepresentation, String request) throws Exception {
		RegionBO regionBO= new RegionBO();
		TOAssembler<Region> toAssembler = new TOAssembler<Region>();
		return toAssembler.assembleResponseTO(new RegionDto(), regionBO.setRegion(jsonRepresentation), request);
	}
	
	/**
	 * get region
	 * @param id
	 * @param request
	 * @return ResponseDto
	 * @throws Exception
	 */
	public ResponseDto<Object> getRegion (String id, String request) throws Exception {
		RegionBO regionBO = new RegionBO();
		TOAssembler<Region> toAssembler = new TOAssembler<Region>();
		return toAssembler.assembleResponseTO(new RegionDto(),regionBO.getRegion(id), request);
	}
	
	/**
	 * get region by params
	 * @param paramMap
	 * @param request
	 * @return ResponseDto
	 * @throws Exception
	 */
	public ResponseDto<Object> getRegionByParams (HashMap<String,String> paramMap, String request) throws Exception {
		RegionsDto regionsDto = new RegionsDto(new RegionBO().getRegionByParams(paramMap));
		TOAssembler<RegionsDto> toAssembler = new TOAssembler<RegionsDto>();
		return toAssembler.assembleResponseTO(new RegionsDto(),regionsDto,request);
	}
	
	/**
	 * get all regions
	 * @param request
	 * @return ResponseDto
	 * @throws Exception
	 */
	public ResponseDto<Object> getAllRegions(String request) throws Exception {
		RegionsDto regionsDto = new RegionsDto(new RegionBO().getAllRegions());
		TOAssembler<RegionsDto> toAssembler = new TOAssembler<RegionsDto>();
		return toAssembler.assembleResponseTO(regionsDto, regionsDto,request);
	}
	
	/**
	 * update region
	 * @param id
	 * @param jsonRep
	 * @param request
	 * @return ResponseDto
	 * @throws Exception
	 */
	public ResponseDto<Object> updateRegion(String id,JsonRepresentation jsonRep, String request) throws Exception {
		RegionBO regionBO = new RegionBO();
		TOAssembler<Region> toAssembler = new TOAssembler<Region>();
		return toAssembler.assembleResponseTO(new RegionDto(), regionBO.updateRegion(id, jsonRep), request);
	}
	
	/**
	 * delete region
	 * @param id
	 * @throws Exception
	 */
	public void deleteRegion(String id) throws Exception{
		RegionBO regionBO = new RegionBO();
		regionBO.deleteRegion(id);
	}
	
// sound
	
	/**
	 * create sound
	 * @param jsonRepresentation
	 * @param request
	 * @return ResponseDto
	 * @throws Exception
	 */
	public ResponseDto<Object> createSound (JsonRepresentation jsonRepresentation, String request) throws Exception {
		SoundBO soundBO= new SoundBO();
		TOAssembler<Sound> toAssembler = new TOAssembler<Sound>();
		return toAssembler.assembleResponseTO(new SoundDto(), soundBO.createSound(jsonRepresentation), request);
	}
	
	/**
	 * get sound
	 * @param id
	 * @param request
	 * @return ResponseDto
	 * @throws Exception
	 */
	public ResponseDto<Object> getSound(String id, String request) throws Exception {
		SoundBO soundBO = new SoundBO();
		TOAssembler<Sound> toAssembler = new TOAssembler<Sound>();
		return toAssembler.assembleResponseTO(new SoundDto(),soundBO.getSound(id), request);	
	}
	
	/**
	 * get sounds
	 * @param request
	 * @return ResponseDto
	 * @throws Exception
	 */
	public ResponseDto<Object> getSounds(String request) throws Exception {
		SoundsDto soundsDto = new SoundsDto(new SoundBO().getSounds());
		TOAssembler<SoundsDto> toAssembler = new TOAssembler<SoundsDto>();
		return toAssembler.assembleResponseTO(soundsDto, soundsDto, request);
	}
	
	/**
	 * delete sound
	 * @param id
	 * @return ResponseDto
	 */
	public ResponseDto<Object> deleteSound(String id) {
		SoundBO soundBO = new SoundBO();
		return soundBO.deleteSound(id);
	}
	
// token
	
	/**
	 * check token
	 * @param tokenString
	 * @return Token
	 * @throws ParseException
	 */
	public Token checkToken (String tokenString) throws ParseException{
		TokenBO tokenBO = new TokenBO();
		return tokenBO.checkToken(tokenString);
	}

// game

	public ResponseDto<Object> createGame (JsonRepresentation jsonRepresentation, String request) throws Exception {
		GameBO gameBO= new GameBO();
		TOAssembler<Game> toAssembler = new TOAssembler<Game>();
		return toAssembler.assembleResponseTO(new GameDto(), gameBO.createGame(jsonRepresentation), request);
	}
	
	public ResponseDto<Object> getGame(String id, String request) throws Exception {
		GameBO gameBO= new GameBO();
		TOAssembler<Game> toAssembler = new TOAssembler<Game>();
		return toAssembler.assembleResponseTO(new GameDto(),gameBO.getGame(id), request);	
	}
	
	public ResponseDto<Object> getGames(String request) throws Exception {
		SoundsDto soundsDto = new SoundsDto(new SoundBO().getSounds());
		TOAssembler<SoundsDto> toAssembler = new TOAssembler<SoundsDto>();
		return toAssembler.assembleResponseTO(soundsDto, soundsDto, request);
	}
	
	public ResponseDto<Object> deleteGame(String id) {
		GameBO gameBO = new GameBO();
		return gameBO.deleteGame(id);
	}
	
}
