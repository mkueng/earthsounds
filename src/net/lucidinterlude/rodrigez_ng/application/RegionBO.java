package net.lucidinterlude.rodrigez_ng.application;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.lucidinterlude.rodrigez_ng.dto.RegionDto;
import net.lucidinterlude.rodrigez_ng.model.Region;
import net.lucidinterlude.rodrigez_ng.persistence.EntityCreator;
import net.lucidinterlude.rodrigez_ng.persistence.RegionDao;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.restlet.ext.json.JsonRepresentation;

import com.google.appengine.api.datastore.GeoPt;

/**
 * rodrigez/RegionBO
 * @author Marco Kueng
 * (c) 2013 lucidinterlude
 */

public class RegionBO extends BO {
	private RegionDao regionDao;
	private EntityCreator entityCreator;
	
	public RegionBO(){
		regionDao = new RegionDao();
		entityCreator = new EntityCreator();
	}
	
	
	//set region
	
	/**
	 * 
	 * @param jsonRep
	 * @return
	 * @throws Exception
	 */
	public Region setRegion(JsonRepresentation jsonRep) throws Exception {
		JSONObject regionJO = new JSONObject();
		regionJO =jsonRep.getJsonObject();
		Region region = null;
		HashMap<String,String> paramMap = new HashMap<String,String>();
		paramMap.put("name", regionJO.getString("name"));
		List<Region> regions = getRegionByParams(paramMap);
		if(regions.size()==0) {
			ArrayList<GeoPt> geoPoints = createBoundary(regionJO);
			regionJO.remove("boundary");
			region = entityCreator.createEntity(regionJO, Region.class);
			region.setGeoPoints(geoPoints);
			regionDao.setRegion(region);
		} else {
			throw new Exception("Region exists");
		}
		
		return region;
	}
	
	
	//get region
	
	/**
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public Region getRegion(String id) throws Exception {
		return regionDao.getEntityById(id, Region.class);
	}
	
	
	//get region by params
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	public List<Region> getRegionByParams(HashMap<String,String> paramMap) throws Exception {
		HashMap<String,Object> queryMap;
		queryMap = getEntityByParams(paramMap,RegionDto.class);
		return regionDao.getEntityByParams(queryMap, Region.class);
	}
	
	
	//get all regions
	
	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<Region> getAllRegions() throws Exception {
		return regionDao.getAllRegions();
	}
	
	
	//update region
	
	/**
	 * 
	 * @param id
	 * @param jsonRep
	 * @return
	 * @throws Exception
	 */
	public Region updateRegion(String id, JsonRepresentation jsonRep) throws Exception {
		JSONObject regionJO = null;
		regionJO = jsonRep.getJsonObject();
		Region region =(Region)  regionDao.updateEntityWithNoRelation(id, regionJO, Region.class );
		return region;
	}

	
	//delete region
	
	/**
	 * 
	 * @param id
	 * @throws Exception
	 */
	public void deleteRegion(String id) throws Exception {
		regionDao.deleteRegion(id);
	}
	
	private ArrayList<GeoPt> createBoundary (JSONObject regionJo) {
		ArrayList<GeoPt> geoPoints = new ArrayList<GeoPt>();
		JSONArray boundaryJA;
		try {
			boundaryJA = regionJo.getJSONArray("geoPoints");
			for(int i = 0; i < boundaryJA.length(); i++) {
				JSONObject geoPointJO = boundaryJA.getJSONObject(i);
				
				double latitudeD=  geoPointJO.getDouble("lat");
				double longitudeD =  geoPointJO.getDouble("lng");
				float latitude =(float) latitudeD;
				float longitude= (float) longitudeD;
				GeoPt geoPt = new GeoPt(latitude,longitude);
				geoPoints.add(geoPt);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return geoPoints;
	}
	
}
