package net.lucidinterlude.rodrigez_ng.application;

import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import net.lucidinterlude.rodrigez_ng.dto.TerritoryDto;
import net.lucidinterlude.rodrigez_ng.index.DocumentManagerFactory;
import net.lucidinterlude.rodrigez_ng.index.DocumentManagerType;
import net.lucidinterlude.rodrigez_ng.index.TerritoryDocumentManager;
import net.lucidinterlude.rodrigez_ng.model.EMF;
import net.lucidinterlude.rodrigez_ng.model.Territory;
import net.lucidinterlude.rodrigez_ng.persistence.EntityCreator;
import net.lucidinterlude.rodrigez_ng.persistence.TerritoryDao;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.restlet.ext.json.JsonRepresentation;

import com.google.appengine.api.datastore.GeoPt;
import com.google.appengine.api.search.Cursor;
import com.google.appengine.api.search.Document;
import com.google.appengine.api.search.Results;
import com.google.appengine.api.search.ScoredDocument;


/**
 * rodrigez/TerritoryBO
 * @author Marco Kueng
 * (c) 2013 lucidinterlude
 */

/**
 * 
 * @author kuema
 *
 */
public class TerritoryBO extends BO {
	private TerritoryDao territoryDao;
	private EntityCreator entityCreator;
	private TerritoryDocumentManager documentManager;
	private DomainModelMapper domainModelMapper;
	
	final static Logger logger = Logger.getLogger(TerritoryBO.class.getName());
	
	/**
	 * constructor
	 */
	public TerritoryBO(){
		territoryDao = new TerritoryDao();
		entityCreator = new EntityCreator();
		documentManager = (TerritoryDocumentManager) DocumentManagerFactory.createDocumentManager(DocumentManagerType.TERRITORY);
		domainModelMapper = new DomainModelMapper();
	}
	
	
	/**
	 * persist the provided jsonRepresentation of a territoryObject in the datastore and puts a representation to the index
	 * 	
	 * @param jsonRep
	 * @return Territory
	 * @throws Exception
	 */
	public void setTerritory(JsonRepresentation jsonRep) throws Exception {
		JSONObject territoryJO = new JSONObject();
		territoryJO	= jsonRep.getJsonObject();
		try {
			//array of territories?
			JSONArray getArray = territoryJO.getJSONArray("territories");
				for(int i = 0; i < getArray.length(); i++) {
					territoryJO	= getArray.getJSONObject(i);
					createTerritory(territoryJO);
					territoryJO=null;
				} 
		//single territory
		} catch (JSONException ex) {
			 createTerritory(territoryJO);
		}
	}
	
	
	/**
	 * updates the provided jsonRepresentation of a territoryObject in the datastore and puts a representation to the index
	 * 	
	 * @param jsonRep
	 * @return Territory
	 * @throws JSONException 
	 * @throws IOException 
	 * @throws Exception
	 */
	public void updateTerritory(String id, JsonRepresentation jsonRepresentation) throws JSONException, IOException {
		
		JSONObject territoryJO = new JSONObject(jsonRepresentation.getText());
		Territory territory;
		EntityManager em = EMF.get().createEntityManager();
		EntityTransaction et = em.getTransaction();
		try {	
			et.begin();
			territory = territoryDao.getEntityById(id, Territory.class);
			String key = null;
			Iterator<?> keys = territoryJO.keys();
			while(keys.hasNext()){
				key = (String) keys.next();
				Object value = territoryJO.get(key);
				if (!key.equals("id") && !key.equals("geoPoints")) {
					PropertyDescriptor propDesc = new PropertyDescriptor(key,Territory.class);
					if (propDesc.getPropertyType() == Double.class && value instanceof Integer) {
						int 	intValue  = (Integer) value;
						double 	doubleVal = intValue;
						Double  boxDouble = doubleVal;
						value = boxDouble;
					}
					propDesc.getWriteMethod().invoke(territory, value);
					propDesc=null;
				}
			}
			territory.setUpdatedAt(new Date());
			//territory.setGeoPoints(createGeoPoints(territoryJO));
			em.merge(territory);
			et.commit();
			createDocument(territory);	
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			if (et.isActive()) et.commit();
			if (em.isOpen()){ em.close();}
		}
	}
	
	
	/**
	 * 
	 * @param id
	 * @param jsonRep
	 * @return Territory
	 * @throws Exception
	 */
	public void  setOrUpdateTerritories(String jsonRep) throws Exception {
		
		JSONObject territoryJO = new JSONObject(jsonRep);
		EntityManager em = EMF.get().createEntityManager();
		EntityTransaction et = em.getTransaction();
		ArrayList<Territory> territories = new ArrayList<Territory>();
		try {
			//array of territories?
			JSONArray territoryArray = territoryJO.getJSONArray("territories");
			int length = territoryArray.length();
			String id ="";
			String newId ="";
			for(int index = 0; index < length; index++) {
				territoryJO = territoryArray.getJSONObject(index);
				et.begin();
				id="";
				id = territoryJO.getString("id");
				Territory territory = territoryDao.getEntityById(id, Territory.class);
				if (territory == null) {
					Territory newTerritory = entityCreator.createEntity(territoryJO, Territory.class);
					newId ="";
					newId = territoryJO.getString("id");
					newTerritory.setId(newId);
					newTerritory.setGeoPoints(createGeoPoints(territoryJO));
					territoryDao.setTerritory(newTerritory);
					et.commit();
					territories.add(newTerritory);
					territory = null;
				} else {
					Iterator<?> keys = territoryJO.keys();
					String key = null;
					while(keys.hasNext()){
						key = (String) keys.next();
						Object value = territoryJO.get(key);
						if (!key.equals("id") && !key.equals("geoPoints")) {
							PropertyDescriptor propDesc = new PropertyDescriptor(key,Territory.class);
							if (propDesc.getPropertyType() == Double.class && value instanceof Integer) {
								int 	intValue  = (Integer) value;
								double 	doubleVal = intValue;
								Double  boxDouble = doubleVal;
								value = boxDouble;
							}
							propDesc.getWriteMethod().invoke(territory, value);
							propDesc=null;
						}
					}
					territory.setUpdatedAt(new Date());
					territory.setGeoPoints(createGeoPoints(territoryJO));
					em.merge(territory);
					et.commit();
					territories.add(territory);
					territory = null;
				}
			}	 
		//else single territory
		} catch (Exception ex) {
			System.out.println(ex);
			throw new Exception(ex);
		
		} finally {
			if (et.isActive()) et.commit();
			if (em.isOpen()){ em.close();}
		}
		for (Territory territory : territories) {
			createDocument(territory);	
		}
	}
	
	
	/**
	 * gets the territory from datastore by id
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public Territory getTerritoryFromDataStore(String id) throws Exception {
		return territoryDao.getEntityById(id, Territory.class);
	}
	

	/**
	 * get territory from index by id
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public TerritoryDto getTerritoryFromIndex(String id) throws Exception {
		TerritoryDto territoryDto = new TerritoryDto();
		Document document= documentManager.getDocumentById(id);
		territoryDto = (TerritoryDto) mapDocumentToTransferObject(document,territoryDto);
		return territoryDto;
	}
	

	/**
	 * get all territories from datastore
	 * 
	 * @return
	 * @throws Exception
	 */
	public ArrayList<TerritoryDto> getAllTerritories() throws Exception {
		List<Territory> territories =  territoryDao.getAllEntities(Territory.class);
		ArrayList<TerritoryDto> territoryDtos = new ArrayList<TerritoryDto>();
		for (Territory territory : territories) {
			TerritoryDto territoryDto = new TerritoryDto();
			territoryDto = (TerritoryDto)domainModelMapper.map(territory, territoryDto);
			territoryDtos.add(territoryDto);
			territoryDto = null;
		}
		return territoryDtos;
	}

	
	/**
	 * get number of territories from index by parameter map
	 * 
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	public  Long getNumberOfTerritories(HashMap<String,String> paramMap) throws Exception {
		Long numberOfResultsFound;
		
		HashMap<String,Object> queryMap;
		//map to entity model
		queryMap = getEntityByParams(paramMap,TerritoryDto.class);
		 String queryString = documentManager.createQueryString(queryMap);
		// get numberOfResultsFound

		Cursor cursor = Cursor.newBuilder().build();
		Results<ScoredDocument> results = documentManager.getDocumentsByParamMap(cursor, queryString);
		numberOfResultsFound = results.getNumberFound();
		logger.log(Level.INFO,"number of territories found:"+numberOfResultsFound);
		System.out.println("number of territories found:"+numberOfResultsFound);
		return numberOfResultsFound;
	}
	

	/**
	 * get territories from index by paramter map
	 * 
	 * @param paramMap
	 * @return ArrayList<TerritoryDto>
	 * @throws Exception
	 */
	public  ArrayList<TerritoryDto> getTerritoriesByParams(HashMap<String,String> paramMap) throws Exception {
		HashMap<String,Object> queryMap;
		
		//map to entity model
		queryMap = getEntityByParams(paramMap,TerritoryDto.class);
		String queryString = documentManager.createQueryString(queryMap);
		 
		return mapDocumentsToDtos(queryString);
	}
	

	private ArrayList<TerritoryDto> mapDocumentsToDtos(String queryString) throws Exception{
		int numberRetrieved = 0;
		Cursor cursor = Cursor.newBuilder().build();
		Results<ScoredDocument> results;
		
		ArrayList<TerritoryDto> territoryList = new ArrayList<TerritoryDto>(5000);
		
		do {
			results = documentManager.getDocumentsByParamMap(cursor,queryString);
			numberRetrieved = results.getNumberReturned();
			
			cursor = results.getCursor();
			//map results to TransferObject
			if (numberRetrieved > 0) {
	            // process the matched docs
	            for (ScoredDocument document : results) {
	            	TerritoryDto territoryDto = new TerritoryDto();
	            	territoryDto.setId(document.getId());
	            	territoryDto.setName(document.getOnlyField("name").getText());
	            	territoryDto.setItFactor(document.getOnlyField("itFactor").getNumber());
	            	territoryDto.setOwner(document.getOnlyField("owner").getText());
	            	territoryDto.setSpacing(document.getOnlyField("spacing").getNumber());
	            	territoryDto.setRegion(document.getOnlyField("region").getText());
	            	territoryDto.setStatus(document.getOnlyField("status").getText());
	            	territoryDto.setAction(document.getOnlyField("action").getText());
	            	territoryDto.setActionContent(document.getOnlyField("actionContent").getText());
	            	territoryDto.setGeoPoints(document.getOnlyField("geoPoints").getText());
	            	
					territoryList.add(territoryDto);
				}
	        }
		
		} while (cursor != null);
		return territoryList;
	}
	
	
	
	/**
	 * delete territory in datastore and index by id
	 * 
	 * @param id
	 * @throws Exception
	 */
	public void deleteTerritory(String id) throws Exception {
		try {
			territoryDao.deleteEntityWithNoRelation(id, Territory.class);
			deleteDocument(id);
		} catch (Exception ex) {
			logger.log(Level.SEVERE,"deleting territory with id: "+id+" failed");
			throw new Exception(ex);
		}
	}

	/**
	 * delete territories from datastore and index by jsonRepresentation
	 * 
	 * @param jsonRep
	 * @throws Exception
	 */
	public void deleteTerritory(JsonRepresentation jsonRep) throws Exception{
		JSONObject territoryJO	= new JSONObject();
		territoryJO	= jsonRep.getJsonObject();
		ArrayList<String> territoryIds	= new ArrayList<String>();
		EntityManager em = EMF.get().createEntityManager();
		EntityTransaction et = em.getTransaction();
	
		try {
			JSONArray territoryArray = territoryJO.getJSONArray("territoryIds");
			int length = territoryArray.length();
			String id ="";
			Territory territory  = null;
			for(int index = 0; index < length; index++) {
				et.begin();
				territoryJO = territoryArray.getJSONObject(index);
				id = territoryJO.getString("id");
				territory = em.find(Territory.class, id);
				em.remove(territory);
				et.commit();
				territoryIds.add(id);
				territory=null;
			}
			for (String element : territoryIds ) {
				deleteDocument(element);
			}
		} catch (JSONException ex) {
			System.out.println(ex);
		} finally {
			if (et.isActive()) et.commit();
			if (em.isOpen()){ em.close();}
		}
	}
	
	
	/**
	 * delete all territories from region
	 * @param paramMap
	 * @throws Exception 
	 */
	public void deleteAllTerritoriesFromRegion(HashMap<String, String> paramMap) throws Exception{
		ArrayList<TerritoryDto> territories = getTerritoriesByParams(paramMap);
		ArrayList<String> territoryIds	= new ArrayList<String>();
		String id ="";
		
		for (TerritoryDto territoryDto : territories){
			id = territoryDto.getId();
			//territoryDao.deleteEntityWithNoRelation(id, Territory.class);
			territoryIds.add(id);
		}
		for (String element : territoryIds ) {
			deleteDocument(element);
		}
	}
	
	/**
	 * create document
	 * 
	 * @param territory
	 * @throws Exception
	 */
	private void createDocument(Territory territory) throws Exception {
		String documentId = territory.getId();
		String name 	= territory.getName();
		String owner	= territory.getOwner();
		String status 	= territory.getStatus();
		String region 	= territory.getRegion();
		Double itFactor = territory.getItFactor();
		Double spacing = territory.getSpacing();
		String action = territory.getAction();
		String actionContent = territory.getActionContent();
		String geoPoints ="";
		for (int i=0; i< territory.getGeoPoints().size();i++) {
			GeoPt geoPt = territory.getGeoPoints().get(i);
			String lat = geoPt.getLatitude()+"";
			String lng = geoPt.getLongitude()+"";
			geoPoints = geoPoints+ lat+"/"+lng+",";
		}
		
		documentManager.createDocument(
				documentId, 
				name,  
				owner, 
				status, 
				region, 
				itFactor, 
				spacing,
				action,
				actionContent,
				geoPoints);
	}
	
	/**
	 * delete document
	 * 
	 * @param documentId
	 * @throws Exception
	 */
	private void deleteDocument(String documentId) throws Exception{
		documentManager.deleteDocumentFromIndex(documentId);
	}
	

	/**
	 * create territory
	 * 
	 * @param territoryJO
	 * @throws Exception
	 */
	private void createTerritory(JSONObject territoryJO) throws Exception{
		Territory territory 			= entityCreator.createEntity(territoryJO, Territory.class);
		String id = territoryJO.getString("id");
		territory.setId(id);	
		
		try {
			//check if territory with same id exists
			if (getTerritoryFromDataStore(id) == null) {
				territory.setGeoPoints(createGeoPoints(territoryJO)); 
				territoryDao.setTerritory(territory);
				createDocument(territory);	
			} else {
				logger.log(Level.WARNING,"territory exists");
				throw new Exception("Entity exists");
			}
		} catch (Exception ex ) {
			logger.log(Level.SEVERE,"persisting territory with id:"+territoryJO.getString("id")+" failed");
			 throw new Exception(ex);
		}
	}
	
	/**
	 * create geoPoints
	 * 
	 * @param territoryJo
	 * @return
	 */
	private ArrayList<GeoPt> createGeoPoints(JSONObject territoryJo) {
		ArrayList<GeoPt> geoPoints = new ArrayList<GeoPt>();
		JSONArray geoPointsJA;
		try {
			geoPointsJA = territoryJo.getJSONArray("geoPoints");
			for(int i = 0; i < geoPointsJA.length(); i++) {
				JSONObject geoPointJO = geoPointsJA.getJSONObject(i);
				
				double latitudeD=  geoPointJO.getDouble("lat");
				double longitudeD =  geoPointJO.getDouble("lng");
				float latitude =(float) latitudeD;
				float longitude= (float) longitudeD;
				GeoPt geoPt = new GeoPt(latitude,longitude);
				geoPoints.add(geoPt);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return geoPoints;
	}
}
