package net.lucidinterlude.rodrigez_ng.application;

import java.text.ParseException;

import org.restlet.Context;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.Restlet;
import org.restlet.engine.header.Header;
import org.restlet.routing.Filter;
import org.restlet.security.User;
import org.restlet.util.Series;

import net.lucidinterlude.rodrigez_ng.model.Token;

public class GlobalFilter extends Filter {
	
	public static String clearance = "";
	
	private Facade facade;
    public GlobalFilter(Context context, Restlet next) {
    	super(context, next);
    	facade = new Facade();
    }
   
    @Override
    protected void afterHandle(Request request, Response response) {
    }

    @Override
    @SuppressWarnings("unchecked")
    protected int beforeHandle(Request request, Response response) {
    	request.getAttributes().get("org.restlet.http.headers");
        String lastSegment = request.getResourceRef().getLastSegment();
		Series<Header> series = (Series<Header>)request.getAttributes().get("org.restlet.http.headers");
        if (lastSegment.equals("login") || lastSegment.equals("signup") || lastSegment.equals("userverification")  || lastSegment.equals("nickname")) {
    		return 0;
    	}
    	else {
    		try {
    			Token token = facade.checkToken(series.getFirstValue("token"));
				if (series.getFirstValue("token")!= null && token != null) {
					User user = new User();
					user.setIdentifier(token.getClearance());
					clearance = token.getClearance();
					request.getClientInfo().setUser(user);
					return 0;
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			};
    		return 1;
    	}
    }
}
