package net.lucidinterlude.rodrigez_ng.application;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

@SuppressWarnings("serial")
public class MailServlet {



public void sendSimpleMail(String verificationLink, String userName, String emailAdresse) {
 // [START simple_example]
 Properties props = new Properties();
 Session session = Session.getDefaultInstance(props, null);

 try {
   Message msg = new MimeMessage(session);
   msg.setFrom(new InternetAddress("admin@lucid-rodrigez.appspotmail.com", "lucid-rodrigez.appspotmail.com Admin"));
   msg.addRecipient(Message.RecipientType.TO,
                    new InternetAddress(emailAdresse, "Mr. "+userName));
   msg.setSubject("Your lucid-rodrigez account has been activated");
   msg.setText("Please verify your email with this link: "+verificationLink);
   Transport.send(msg);
 } catch (AddressException e) {
   // ...
 } catch (MessagingException e) {
   // ...
 } catch (UnsupportedEncodingException e) {
   // ...
 }
 // [END simple_example]
}

private void sendMultipartMail() {
 Properties props = new Properties();
 Session session = Session.getDefaultInstance(props, null);

 String msgBody = "...";

 try {
   Message msg = new MimeMessage(session);
   msg.setFrom(new InternetAddress("admin@example.com", "Example.com Admin"));
   msg.addRecipient(Message.RecipientType.TO,
                    new InternetAddress("user@example.com", "Mr. User"));
   msg.setSubject("Your Example.com account has been activated");
   msg.setText(msgBody);

   // [START multipart_example]
   String htmlBody = "";          // ...
   byte[] attachmentData = null;  // ...
   Multipart mp = new MimeMultipart();

   MimeBodyPart htmlPart = new MimeBodyPart();
   htmlPart.setContent(htmlBody, "text/html");
   mp.addBodyPart(htmlPart);

   MimeBodyPart attachment = new MimeBodyPart();
   InputStream attachmentDataStream = new ByteArrayInputStream(attachmentData);
   attachment.setFileName("manual.pdf");
   attachment.setContent(attachmentDataStream, "application/pdf");
   mp.addBodyPart(attachment);

   msg.setContent(mp);
   // [END multipart_example]

   Transport.send(msg);

 } catch (AddressException e) {
   // ...
 } catch (MessagingException e) {
   // ...
 } catch (UnsupportedEncodingException e) {
   // ...
 }
}
}
