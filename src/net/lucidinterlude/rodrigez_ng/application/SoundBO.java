package net.lucidinterlude.rodrigez_ng.application;

import java.util.List;

import org.json.JSONObject;
import org.restlet.ext.json.JsonRepresentation;

import net.lucidinterlude.rodrigez_ng.dto.ResponseDto;
import net.lucidinterlude.rodrigez_ng.model.Sound;
import net.lucidinterlude.rodrigez_ng.persistence.EntityCreator;
import net.lucidinterlude.rodrigez_ng.persistence.SoundDao;

public class SoundBO extends BO{
	
	private SoundDao soundDao;
	private EntityCreator entityCreator;
	private ResponseDto<Object> responseDto;
	public SoundBO(){
		soundDao = new SoundDao();
		entityCreator = new EntityCreator();
	}
	
	
//create sound
	public Sound createSound(JsonRepresentation jsonRepresentation) throws Exception {
		JSONObject soundJO = new JSONObject();
		Sound sound =null;
		
		soundJO = jsonRepresentation.getJsonObject();
		sound = new Sound();
		sound = entityCreator.createEntity(soundJO, Sound.class);
		sound.setType("sound");
		sound = soundDao.createSound(sound);
	
		return sound;
	}

//get sound
	public Sound getSound (String id) throws Exception{
		return soundDao.getSound(id);
	}

//get sounds
	public List<Sound> getSounds() throws Exception {
		return soundDao.getSounds();
	}
	
//delete sound
	public ResponseDto<Object> deleteSound(String id){
		responseDto = soundDao.deleteSound(id);
		return responseDto;
	}
}
