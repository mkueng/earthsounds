package net.lucidinterlude.rodrigez_ng.application;

import org.restlet.Application;
import org.restlet.Restlet;
import org.restlet.routing.Router;

import net.lucidinterlude.rodrigez_ng.service.CheckUserNicknameController;
import net.lucidinterlude.rodrigez_ng.service.GeospatialController;
import net.lucidinterlude.rodrigez_ng.service.PlayerController;
import net.lucidinterlude.rodrigez_ng.service.PlayerLocationController;
import net.lucidinterlude.rodrigez_ng.service.PlayerQuadrantController;
import net.lucidinterlude.rodrigez_ng.service.RegionController;
import net.lucidinterlude.rodrigez_ng.service.SoundController;
import net.lucidinterlude.rodrigez_ng.service.TargetController;
import net.lucidinterlude.rodrigez_ng.service.TerritoryController;
import net.lucidinterlude.rodrigez_ng.service.UserController;
import net.lucidinterlude.rodrigez_ng.service.UserLoginController;
import net.lucidinterlude.rodrigez_ng.service.UserSignUpController;

public class Main extends Application{
		
		@Override
	    public Restlet createInboundRoot() {
	    	Router router = new Router(getContext());
	    	
	    	router.attach("/rodrigez/api/user",UserController.class);
	    	router.attach("/rodrigez/api/login",UserLoginController.class);
	    	router.attach("/rodrigez/api/signup",UserSignUpController.class);
	        router.attach("/rodrigez/api/user/{id}",UserController.class);
	        router.attach("/rodrigez/api/nickname",CheckUserNicknameController.class);
	        router.attach("/rodrigez/api/userverification",UserSignUpController.class);
	        
	        router.attach("/rodrigez/api/player/{id}",PlayerController.class);
	        router.attach("/rodrigez/api/player/{id}/quadrant",PlayerQuadrantController.class); 
	        router.attach("/rodrigez/api/player/{id}/location",PlayerLocationController.class);
	        router.attach("/rodrigez/api/player/{id}/target",TargetController.class); 
	        
	        router.attach("/rodrigez/api/geospatial/boundingbox",GeospatialController.class); 
	        router.attach("/rodrigez/api/territory/{id}",TerritoryController.class);
	        router.attach("/rodrigez/api/territory",TerritoryController.class);
	        router.attach("/rodrigez/api/region",RegionController.class);
	        router.attach("/rodrigez/api/region/{id}",RegionController.class);
	        
	        router.attach("/rodrigez/api/sound",SoundController.class);
	        router.attach("/rodrigez/api/sound/{id}",SoundController.class);
	        return new GlobalFilter(getContext(),router);   
	    }
}