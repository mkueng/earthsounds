package net.lucidinterlude.rodrigez_ng.application;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.lucidinterlude.rodrigez_ng.dto.IDto;

/**
 * rodrigez/DomainModelMapper
 * @author Marco Kueng
 * (c) 2013 lucidinterlude
 */

public class DomainModelMapper {
	
	private final static Logger logger = Logger.getLogger(DomainModelMapper.class.getName());
	private PropertyDescriptor propertyDescriptor;
	
	/**
	 * Maps a provided entity to a provided domainModel
	 * 
	 * @param entity
	 * @param domainModel
	 * @return domainModel
	 * @throws Exception
	 */
	public IDto map(Object entity, IDto domainModel) throws Exception {
		if (entity != null) {
		for (Field field : domainModel.getClass().getDeclaredFields()) {
			field.setAccessible(true);
			try {
				propertyDescriptor = new PropertyDescriptor(field.getName(),entity.getClass());
				Object value = propertyDescriptor.getReadMethod().invoke(entity, (Object[])null);
				field.set(domainModel, value);
			} catch (IntrospectionException e) {
				logger.log(Level.SEVERE,"introspection failed");
				e.printStackTrace();
				throw new Exception(e);
			} catch (IllegalArgumentException e) {
				logger.log(Level.SEVERE,"argument exception");
				e.printStackTrace();
				throw new Exception(e);
			} catch (IllegalAccessException e) {
				logger.log(Level.SEVERE,"access exception");
				e.printStackTrace();
				throw new Exception(e);
			} catch (InvocationTargetException e) {
				logger.log(Level.SEVERE,"invocation exception");
				e.printStackTrace();
				throw new Exception(e);
			}
		}
		}
	return domainModel;
	}
}
