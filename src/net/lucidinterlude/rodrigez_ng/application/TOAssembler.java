package net.lucidinterlude.rodrigez_ng.application;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import net.lucidinterlude.rodrigez_ng.dto.IDto;
import net.lucidinterlude.rodrigez_ng.dto.ResponseDto;
import net.lucidinterlude.rodrigez_ng.model.EntityBase;
 
/**
 * rodrigez/TerritoryController
 * @author Marco Kueng
 * (c) 2013 lucidinterlude
 * */

public class TOAssembler<T> {
	
	public TOAssembler(){
	}
	
	public ResponseDto<Object> assembleResponseTO(IDto domainDto,EntityBase entity, String request) throws Exception{
		ResponseDto<Object> responseDto = assemble(domainDto,entity,request);
		responseDto.setResponseContent("id", entity.getId());
	
		return responseDto;
	}
	
	public ResponseDto<Object> assembleResponseTO(IDto domainDto,Object object, String request) throws Exception{
		ResponseDto<Object> responseDto = assemble(domainDto,object,request);
		return responseDto;
	}
	
	/**
	 * Assembles a new ResponseDto from provided domainDto, entity/object and request parameters
	 * 
	 * @param domainDto
	 * @param object
	 * @param request
	 * @return responseDto
	 * @throws Exception
	 */
	private ResponseDto<Object> assemble(IDto domainDto,Object object, String request) throws Exception{
		ResponseDto<Object> responseDto = new ResponseDto<Object>();
		DomainModelMapper	dmm = new DomainModelMapper();
		Map<String, Object> link = new HashMap<String,Object>();
		Map<String, Object> fields = new HashMap<String,Object>();
		
		domainDto = dmm.map(object, domainDto);
		
		link.put("rel", "self");
		link.put("href", request);
		for (Field field : domainDto.getClass().getDeclaredFields()) {
			field.setAccessible(true); 
		    Object value = field.get(domainDto); 
		    fields.put(field.getName(), value);
		}
		
		responseDto.setResponseContent("link", link);
		responseDto.setResponseContent("fields", fields);
		return responseDto;
	}
	
}
