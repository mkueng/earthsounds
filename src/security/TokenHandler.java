package security;

import java.security.MessageDigest;
import java.util.Date;

import javax.xml.bind.DatatypeConverter;

public class TokenHandler {

	
	public TokenHandler(){
		
	}
	
	public String createToken(String userName, String password) {
		return getSHA256Hash(userName+password+new Date());
	}
	
	
	public Boolean validateToken(String token){
		return false;
	}
	/**
	* Returns a hexadecimal encoded SHA-256 hash for the input String.
	* @param data
	* @return
	*/
    private String getSHA256Hash(String data) {
        String result = null;
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(data.getBytes("UTF-8"));
            return bytesToBase64(hash); // make it printable
        }catch(Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    private String  bytesToBase64(byte[] hash) {
    	return DatatypeConverter.printBase64Binary(hash);
    }

	
}
