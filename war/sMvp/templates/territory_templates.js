/**
 * Created by kuema on 12.03.15.
 */



Templates.infoWindow=[
    "<div id ='infoWindowContainer'>",
        "<%=html%>",
    "</div>"
].join("\n");


Templates.newTerritoryinfoWindow=[
    "<%var content =null%>",
    "<%var toggle = true%>", 
    "<div id ='newTerritoryinfo'>",
    	"<%$.each(namespace.domainModel.territory, function(key, val){%>",
    		
	    	"<%content = displayContent[key]%>",
	        "<% if (this.displayable){%>",
	        	"<%toggle =!toggle%>",
	        	"<%if (toggle) {%>",
					"<div class='infoRowOdd' >",
				"<%} else {%>",
					"<div class='infoRowEven' >",
				"<%}%>",
	        	
	        	
	                "<div class='left territoryInfoLabel'><%=key%>: </div>",
	                "<div class='right'> <%=content%></div>",
	                "<div class='clearFix'></div>",
	            "</div>",
	        "<%}%>",
	        "<%})%>",
	        "</div>",
       
    
    "<%content = null%>"
].join("\n");


Templates.territoryEditWindow = [
    "<div id ='territoryEditWindow'>",
        "<%var content = null%>",
        "<%var id = displayContent.id%>",
        "<%var toggle = true%>", 

        "<div class='clearFix'></div>",
        "<%$.each(namespace.domainModel.territory, function(key, val){%>",
            "<%content = displayContent[key]%>",
            "<% if (this.displayable){%>",
	            "<%toggle =!toggle%>",
	        	"<%if (toggle) {%>",
					"<div class='editRowOdd' >",
				"<%} else {%>",
					"<div class='editRowEven' >",
				"<%}%>",
                "<div class='left editTerritoryLabel'><%=key%>: </div>",
                "<% if (this.mutable) {%>",
                    "<%if (this.type == 'text') {%>",
                        "<input type='text' id='territoryEdit_<%=key%>' class='infoBoxTextInput' data-property='<%=key%>' data-id='<%=id%>' value='<%=content%>' />",
                        "<input type='checkbox' id='territoryEditCopy_<%=key%>' data-property='<%=key%>' class='right editBoxCheckbox'/>",
                    "<%} else if (this.type =='select') {%>",
                    	
	                        "<select id='territoryEdit_<%=key%>' class='editBoxSelect' data-id='<%=id%>' data-property='<%=key%>'  >",
	                            "<%var selected = ''%>",
	                            "<option value=''>",
	                            "<% for (var i=0, len = this.default.length; i < len; i++) { %>",
	                            	"<%console.log('default: ',this.default[i])%>",
	                                "<%if (this.data == this.default[i] || this.default[i] == content){ %>",
	                                    "<option value='<%=this.default[i].split('.')[0]%>' selected>",
	                                "<%} else {%>",
	                                    "<option value='<%=this.default[i].split('.')[0]%>'>",	
	                                "<%}%>",
	                                    "<%=this.default[i].split('.')[0]%>",
	                                    "</option>",
	                            "<%}%>",
	                        "</select>",
	                 
                        "<input type='checkbox' id='territoryEditCopy_<%=key%>' data-property='<%=key%>' class='right editBoxCheckbox'/>",
                    "<%} else if (this.type == 'slider') {%>",
                        "<label id='itFactorValue' class='left infoBoxSlider'><%=content%></label>",
                        "<input type='checkbox' class='right editBoxCheckbox' id='territoryEditCopy_<%=key%>' data-property='<%=key%>' />",
                        "<input type='range' data-id='<%=id%>' id='territoryEdit_<%=key%>' class='left infoBoxSlider' data-property='<%=key%>' value='<%=content%>' min='<%=this.min%>' max='<%=this.max%>' />",

                    "<%}%>",
                    
                "<%} else {%>",
                    "<div class='left infoLeft'><%=this.data%></div>",
                    "<div class='left infoBoxContent'><%=content%></div>",
                "<%}%>",
                
                "<div class='clearFix'></div>",
                
            "<%}%>",
            
        "<%})%>",
        "</div>",
    "<br/>",
    "<div class='clearFix'></div>",
    "<input type='button' id='territoryEditDelete' data-id='<%=id%>' class='right editTerritoryLabel' value='delete'/>",
    "<input type='button' id='territoryEditDivide' data-id='<%=id%>' class='right editTerritoryLabel' value='divide'/>",
    "<div class='clearFix'></div>",

    "</div>"
].join("\n");