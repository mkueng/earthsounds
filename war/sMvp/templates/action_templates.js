/**
 * 
 */

Templates.allAboutSoundActionViewTemplate = [
    "<div id='soundActionHeaderContainer' class='headerContainer' data-container='headerContainer'>",
    	"<div id='allAboutSoundAction' class='containerTitle left'>SoundAction</div>",
    	"<div id='soundActionSlider' class='right slider'>",
    		"<select class='right' name='flip-mini' id='showHideSoundAction' data-role='slider' data-mini='true'>",
    			"<option value='hide'>hide</option>",
    			"<option  selected='selected' value='show'>show</option>",
    		"</select>",
    	"</div>",
    	"<div class='clearFix'></div>",
    "</div>",
   
    "<div id='soundActionCreateContainer' class='createEntityContainer' data-container='soundActionCreateContainer' style='display:none'></div>",
    "<div id='soundActionOptionsContainer' class='optionsContainer' data-container='soundActionOptionsContainer'>",
    	"<div id='soundActionSearchContainer' class='searchContainer left' data-container='soundActionSearchContainer'>",
    		"<input id='soundActionFilter' type='text' data-type='search' placeholder='search soundAction'/>",
    	"</div>",
    	"<div id='soundActionControlContainer' class='right controlContainer' data-container='soundActionControlContainer'></div>",
    	"<div class='clearFix'></div>",
   
			"<select id='soundActionCategoryFilter' class='infoBoxSelect right' >",
				"<%$.each(namespace.domainModel.soundAction.category.selection, function(key,value){%>",
					"<%if (getCategoryFilter() != value){%>",
						"<option value='<%=value%>'>",
					"<%} else {%>",
						"<option value='<%=value%>' selected='true'>",
					"<%}%>",
					"<%=value%>",
					"</option>",
				"<%})%>",
			"</select>",
		
    	
    	"<div class='clearFix'></div>",
    	
    "</div>",
    "<div class='clearFix'></div>",
  
	
    
    "<div id='soundActionListContainer' class='listContainer' data-container='soundActionListContainer'>",
	"<ul id='soundActionList' class='list' data-event='click' data-id='allAboutSoundActionView' data-filter='true' data-input='#soundActionFilter' data-role='listview' data-theme='a'>",
	
		"<%$.each(getData().getCollection(),function(key,value){%>",
			
			"<%if (value.id != 'template' && (getCategoryFilter() == this.category || getCategoryFilter() =='' )) {%>",
				"<li id='<%=value.name%>_li' class='listItem' data-icon='false'>",
					
					"<label id='showSoundAction_<%=key%>' class='label listItem'><%=value.name%>",
						"<%var actions = getActions()%>",
						"<%for (action in actions){%>",
							"<input type='button' id='<%=actions[action]%>soundAction_<%=key%>' data-icon='<%=actions[action]%>'/>",
						"<%}%>",
							
						"<input type='button' id='playSoundAction_<%=key%>' class='right openclose' data-icon='audio'/>",
						"<div class='clearFix'></div>",
						"<img class='left' id='soundAnimation_<%=key%>' src='../resources/animated-sound.gif' alt='Smiley face' height='22' width='22' style='display:none'>",
						"<div id='category_<%=key%>' class='left'> category: <%=value.category%></div>",
						"<div id='duration_<%=key%>' class='label right'> duration:<%=value.duration%></div>",
					"</label>",
					
				"</li>",
				"<div id='soundActionInfoContainer_<%=value.id%>' data-container='soundActionInfoContainer_<%=value.id%>'></div>",
				"<div id='soundActionEditContainer_<%=value.id%>' data-container='soundActionEditContainer_<%=value.id%>'></div>",
				"<div id='soundActionCreateContainer_<%=value.id%>' data-container='soundActionCreateContainer_<%=value.id%>'></div>",
			"<%}%>",
		"<%})%>",
	"</ul>",
"</div>"
    
	
].join("\n");

Templates.soundActionControl = [
	"<input type='button' data-event='click' data-id='soundActionControlView' data-icon='plus' title='create sound' class='right'></div>"
].join("\n");

Templates.soundActionInfoViewTemplate = [
  "<%var toggle = true%>", 
  "<div id='soundActionInfo' class='entityInfo'>",
	"<%for (property in getData().getObjectRepresentation()){%>",
		"<%if (property !='content') {%>",
			"<%toggle =!toggle%>",
	    	"<%if (toggle) {%>",
				"<div class='infoStripesOdd' >",
			"<%} else {%>",
				"<div class='infoStripesEven' >",
			"<%}%>",
				"<div class='left infoLeft'><%=property%> :</div>",
				"<div class='right infoRight'><%=getData()['get'+property.ucfirst()]()%></div>",
				"<div class='clearFix'></div>",
			"</div>",
		"<%}%>",
	"<%}%>",
"</div>"
  ].join("\n");
	
	
	
Templates.soundActionCreateViewTemplate = [
	"<%var toggle = true%>", 
	"<div data-id='soundActionCreateView' id='soundActionCreate' class='editEntity' data-event='click'>",
		"<div class='subContainerTitel'>create soundAction</div>",
		"<form id='newSoundActionForm' >",
			"<%$.each(namespace.domainModel.soundAction, function(key,value){%>",
				"<%if (this.displayable && this.mutable) {%>",
					"<%toggle =!toggle%>",
	            	
					"<div class='left infoLeft'><%=key%>:</div>",
					"<%if (namespace.domainModel.soundAction[key].type == 'text' || namespace.domainModel.soundAction[key].type =='email' || namespace.domainModel.soundAction[key].type =='date') {%>",
						"<input type='<%=namespace.domainModel.soundAction[key].type%>' minlength='<%=namespace.domainModel.soundAction[key].minlength%>' id='newSoundAction_<%=key%>' class='right' />",
					"<%}%>",
					"<%if (this.type == 'select') {%>",
						"<select id='newSoundAction_<%=key%>' class='infoBoxSelect right'  name='<%=key%>' >",
					 		"<%$.each(this.selection, function(key, value){%>",
					 			"<option value='<%=value%>'>",
					 				"<%=value%>",
					 			"</option>",
					 		"<%})%>",
					 	"</select>",
					"<%}%>",
					"<div class='clearFix'></div>",
				"<%}%>",
			"<%})%>",
			
		    "<input type='file' id='soundActionFile'> name='file' />",
			"<div class='clearFix'></div>",
		"</form>",
		"<div id='createSoundActionCancelSubmit' class='right'>",
			"<input type='button' id='cancelNewSoundAction' data-icon='delete' />",
			"<input type='button' id='submitNewSoundAction' data-icon='action' />",
		"</div>",
		"<div class='clearFix'></div>",
	"</div>"
 ].join("\n");