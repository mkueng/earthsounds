var Templates = Templates || {};
 
Templates.allAboutRegionViewTemplate = [
    "<div id='regionHeaderContainer' class='headerContainer' data-container='headerContainer'>",
    	"<div id='allAboutRegion' class='containerTitle left'>Region</div>",
    	"<div id='regionSlider' class='right slider'>",
    		"<select class='right' name='flip-mini' id='showHideRegion' data-role='slider' data-mini='true'>",
    			"<option value='hide'>hide</option>",
    			"<option selected='selected' value='show'>show</option>",
    		"</select>",
    	"</div>",
    	"<div class='clearFix'></div>",
    "</div>",
   
    "<div id='regionCreateContainer' class='createEntityContainer' data-container='regionCreateContainer' style='display:none'></div>",
    "<div id='regionOptionsContainer' class='optionsContainer' data-container='regionOptionsContainer'>",
    	"<div id='regionSearchContainer' class='searchContainer left' data-container='regionSearchContainer'>",
    		"<input id='regionFilter' type='text' data-type='search' placeholder='search region'/>",
    	"</div>",
    	"<div id='regionControlContainer' class='right controlContainer' data-container='regionControlContainer'></div>",
    	"<div class='clearFix'></div>",
    "</div>",
    "<div id = 'regionListContainer' class='listContainer' data-container='regionListContainer'>",
    	"<ul id='regionList' class='list' data-event='click' data-id='allAboutRegionView' data-filter='true' data-input='#regionFilter' data-role='listview' data-theme='a'>",
    		"<%$.each(getData().getCollection(),function(key,value){%>",
    			"<%if (value.id != 'template') {%>",
					"<li id='<%=value.name%>_li' class='listItem' data-icon='false'>",
						"<label id='showRegion_<%=key%>' class='label'><%=value.name%>",
							"<input type='button' id='actionToggleRegion_<%=key%>' class='right openclose' data-icon='carat-r'/>",
							"<div class='clearFix'></div>",
							"<div id='actionToggleButton_<%=key%>' class='right actionbutton' style='display:none'>",
								"<%var actions = getActions()%>",
								"<%for (action in actions){%>",
									"<input type='button' id='<%=actions[action]%>region_<%=value.id%>' data-icon='<%=actions[action]%>'/>",
								"<%}%>",
							"</div>",
						"</label>",
					"</li>",
					"<div id='regionInfoContainer_<%=value.id%>' data-container='regionInfoContainer_<%=value.id%>'></div>",
					"<div id='regionEditContainer_<%=value.id%>' data-container='regionEditContainer_<%=value.id%>'></div>",
				"<%}%>",
			"<%})%>",
		"</ul>",
	"</div>"
	
].join("\n");


Templates.regionInfoViewTemplate = [
    "<%var toggle = true%>", 
	"<div id='regionInfo' class='entityInfo'>",
		"<%for (property in getData().getObjectRepresentation()){%>",
			"<%if (namespace.domainModel.region[property].displayable) {%>",
				"<%toggle =!toggle%>",
				"<%if (toggle) {%>",
					"<div class='infoStripesOdd' >",
				"<%} else {%>",
					"<div class='infoStripesEven' >",
				"<%}%>",
						"<div class='left infoLeft'><%=property%> :</div>",
						"<%try{%>",
							"<div class='right infoRight'><%=getData()['get'+property.ucfirst()]()%></div>",
						"<%}catch(e){console.log(e)}%>",
						"<div class='clearFix'></div>",
					"</div>",
			"<%}%>",
		"<%}%>",
		"<div class='infoStripesEven'>",
			"<div class='left infoLeft'>number of territories:</div>",
			"<div id='numberofter' class='right infoRight'></div>",
		"</div>",
	"</div>"
].join("\n");

Templates.regionEditViewTemplate=[
	"<%var toggle = true%>", 
    "<div id='editRegion' class='editEntity' data-id='regionEditView' data-event='click'>",
    	"<form id='editRegionForm' class='entityInfo' >",
    		"<%for (property in getData().getObjectRepresentation()){%>",
    			"<%if (namespace.domainModel.region[property].displayable) {%>",
    				"<%toggle =!toggle%>",
    	    		"<%if (toggle) {%>",
						"<div class='infoStripesOdd' >",
					"<%} else {%>",
						"<div class='infoStripesEven' >",
					"<%}%>",
					"<div class='left infoLeft'><%=property%>:</div>",
					"<div class='right infoRight'><%=getData()['get'+property.ucfirst()]()%></div>",
					"<div class='clearFix'></div>",
	            "<%}%>",
	       "<%}%>",
       "</form>",
     "<div id='editRegionCancelSubmit' class='right cancelSubmit'>",
			"<input type='button' id='cancelEditRegion' data-icon='delete' />",
			
     "</div>",
     "<div class='clearFix'></div>",
 "</div>"                               
].join("\n"); 


Templates.regionControl=[
	"<input type='button' data-event='click' data-id='regionControlView' data-icon='plus' title='create region' class='right'></div>"
].join("\n");

Templates.regionCreateViewTemplate = [
	"<%var toggle = true%>", 
	"<div id='regionCreate' data-id='regionCreateView' class='editEntity' data-event='click'>",
		"<div class='subContainerTitel'>create region</div>",
		
        "<form id='newRegionForm'>",
        	"<%$.each(namespace.domainModel.region, function(key,value){%>",
        		"<%if (this.displayable && this.mutable) {%>",
					"<%toggle =!toggle%>",
					"<div class='left infoLeft'><%=key%>:</div>",
						"<%if (namespace.domainModel.region[key].type == 'text' || namespace.domainModel.region[key].type =='email' || namespace.domainModel.region[key].type =='date') {%>",
			 				"<input type='<%=namespace.domainModel.region[key].type%>' minlength='<%=namespace.domainModel.region[key].minlength%>' id='newUser_<%=key%>' class='right' />",
			 			"<%}%>",
			 			"<%if (this.type == 'select') {%>",
			 				"<select id='newRegion_<%=key%>' class='infoBoxSelect right'  name='<%=key%>' >",
			 					"<%$.each(this.selection, function(key, value){%>",
			 						"<option value='<%=value%>'>",
			 							"<%=value%>",
			 						"</option>",
			 					"<%})%>",
			 				"</select>",
			 			"<%}%>",
			 			 "<%if (this.type=='radiobuttons') {%>",
	                        "<fieldset data-role='controlgroup' id='new_Region_<%=key%>' class='right' data-mini='true' mandatory='<%=this.mandatory%>'>",
	                           
	                             "<%var value = this.value%>",
	                             "<%var placeholder = this.placeholder%>",
	                             "<%var checked =''%>",
	                           "<%$.each(this.selection, function(){%>",
	                                "<%if (value== this) {checked = 'checked'} else {checked = ''}%>",
	                                    "<input type='radio' name='radio-choice-<%=key%>' id='<%=this%>_<%=key%>' value='<%=this%>' <%=checked%>>",
	                                    "<label for='<%=this%>_<%=key%>'><%=this%></label>",    
	                            "<%})%>",
	                        "</fieldset>",
	                   "<%}%>",
			 		"<div class='clearFix'></div>",
			 	"<%}%>",
			 "<%})%>",
			
			
        "</form>",
        
        "<div id='createRegionTools' class='left'>",
            "<input id='removeTerritoryOn' type='button' class='left' data-icon='minus'>",
            "<input id='gridOn' type='button' class='left' data-icon='grid'>",
        "</div>",
        
        "<div class='clearFix'></div>",
		"<div id='createRegionCancelSubmit' class='right'>",
			"<input type='button' id='cancelNewRegion' data-icon='delete' />",
			"<input type='button' id='submitNewRegion' data-icon='action' />",
		"</div>",
		
		"<div id='createRegionPath' class='left'>",
			"<input id='undoPath' type='button' class='left' data-icon='back'>",
            "<input id='redoPath' type='button' class='left' data-icon='forward'>",
            "<input id='clearPath' type='button' class='left' data-icon='delete'>",
			"<input id='renderPath' type='button' class='left' data-icon='check'>",
		"</div>",
	"</div>"                               
].join("\n");

