/**
 * 
 */

$(document).ready(function(){
	
	$.support.cors = true;
	
	rodrigez = {}; //namespace rodrigez
	namespace = rodrigez;
	namespace.baseUrl = "http://localhost:8888/rodrigez/api/";
	
	SMVP.setDataGateway(new SMVP.DataGateway(new SMVP.AjaxHandler()));
	SMVP.setNameSpace(namespace);
	
	var isNickNameValid = false;
	
	userSignUpModel = new SMVP.Model({
		"id" : "",
		"root" : "signup",
		"endpoint" : "",
		"firstName" : "",
		"lastName" : "",
		"nickName" : "",
		"gender" : "",
		"email" : "",
		"birthdate" : "",
		"password" : "",
		"clearance" : ""
	})
	
	nickNameModel =  new SMVP.Model({
		"id" : "",
		"root" : "nickname",
		"endpoint" : "",
		"nickName" : "",
		"userId" : ""
	})
	
	$( "#txt-nick-name" ).keyup(function() {
			if (this.value !="" && this.value.length > 2) {
				nickNameModel.setEndpoint("?nickName="+this.value);
			 	nickNameModel.fetch(function(response){
			 		
			 		if (response.getNickName() != "") {
			 			$('#txt-nick-name').parent().css('border-color','red');
			 			nickNameModel.setNickName("");
			 			isNickNameValid = false;
			 		} else {
			 			$('#txt-nick-name').parent().css('border-color','white');
			 			nickNameModel.setNickName("");
			 			isNickNameValid = true;
			 		}
			 	})
			}
		});
	
	$('#btn-submit').click(function(){
		if (validateInputs() == true && isNickNameValid){
			$.mobile.loading('show');
			userSignUpModel.post(function(response){
				$.mobile.loading('hide');
				$("#dlg-sign-up-sent").popup("open");
			});
		} else {
			setTimeout(function(){
				$("#dlg-invalid-form").popup("open");
			},50);
			
		}
	})
	
	$('#confirmemail').click(function(){
		window.location="sign-in.html";
	})
	
	var validateInputs = function(){
		var isValid = true;
		var firstName = $('#txt-first-name').val();
		if (firstName.length < 2) {
			isValid = false  
		} else {
			userSignUpModel.setFirstName(firstName);
		}
		var lastName = $('#txt-last-name').val();
		if (lastName.length < 2) {
			isValid = false  
		} else {
			userSignUpModel.setLastName(lastName);
		}
		
		var nickName = $('#txt-nick-name').val();
		if (nickName.length < 4) {
			isValid = false  
		} else {
			userSignUpModel.setNickName(nickName);
		}
		
		var email = $('#txt-email').val();
		if (email.length < 4) {
			isValid = false  
		} else {
			userSignUpModel.setEmail(email);
		}
		
		var password = $('#txt-password').val();
		if (password.length < 4) {
			isValid = false  
		} else {
			userSignUpModel.setPassword(password);
		}
		
		return isValid;
	}
})