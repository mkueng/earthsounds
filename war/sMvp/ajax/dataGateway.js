var DataGateway = (function(){
	
	var DataGateway =  function(ajaxHandler){
		
		this.ajaxHandler = ajaxHandler;
		
	} 
	
	DataGateway.prototype = {
		fetchCollection : function(collection){
			return {};
		},
		
		postCollection : function(collection){
			
		},
		
		postModel : function(model){
			this.ajaxHandler.post( model.getUrl(),model.getJsonRepresentation(),function(){});
		}
	}
	
	return DataGateway;
})();