var MockDataGateway = (function(){
	
	var MockDataGateway =  function(){
		this.createResponseObject = function(responseCode,data){
			return  {
				"status" : responseCode,
				"data" : data
			};
		};
	} 
	
	MockDataGateway.prototype = {

        /**
         *
         * @param collection
         * @returns {{}}
         */
		fetchCollection : function(collection, callback){
			var resource = "apis_"+collection.getUrlRoot();
            var entities = {};
            for (var entity in rodrigez[resource]){
            	entities[entity] = {};
            	$.each(rodrigez[resource][entity], function(key,value){
            		entities[entity][key] = value;
            	});
            }
            
            typeof callback !='undefined' ? callback(this.createResponseObject(200,entities)) : false;
		},

        /**
         *
         * @param model
         * @returns {*}
         */
		fetchModel : function(model, callback){
			typeof model.id == 'undefined' ? model.id = "template" : false;
			typeof callback !='undefined'
				? callback(this.createResponseObject(200,rodrigez["apis_"+model.root][model.id])) 
				: false;
		},

        /**
         *
         * @param model
         * @returns {*}
         */
        postModel : function(model, callback){
        	model.id = SMVP.Util.uuid();
        	rodrigez["apis_"+model.root][model.id]=model;
        	typeof callback != 'undefined'
            	? callback(this.createResponseObject(201,model))
            	: false;
            
        },

        /**
         *
         * @param model
         * @returns {*}
         */
        updateModel : function(model, callback){
        	rodrigez["apis_"+model.root][model.id]=model;
			typeof callback !='undefined' 
				? callback(this.createResponseObject(200,model))
				: false;
        },

        
        /**
         * @param model
         * @param callback
         */
        deleteModel: function(model, callback){
            delete  rodrigez["apis_"+model.root][model.id];
            typeof callback != 'undefined'
            	? callback(this.createResponseObject(200,"deleted"))
            	: false;
           
        },

        /**
         *
         * @param model
         * @param collection
         * @returns {*}
         */
        postRegion : function(model, collection){
            if (collection[model.getName()] == null){
                model.setGettersSetters({
                    id:model.getName().substring(0, 3).toLowerCase(),
                    created : "21.03.1971",
                    lastUpdate : "21.03.1971",
                    creator : "mac",
                    numberOfTerritories : 0,
                    url :"region/"+model.getName(),
                    mutable:"name"

                })
                rodrigez["apis_region_"+model.getId()]= model.getObjectRepresentation();
                rodrigez["apis_region"][model.getId()]=model.getObjectRepresentation();
                return {
                    httpStatus: 201,
                    body : {
                        status: "created",
                        id:model.getId(),
                        url:"region/"+model.getId()
                    }
                };
            } else {
                return {
                    httpStatus:403,
                    body: {
                        status: "creation failed",
                        name:model.getId()
                    }
                }
            }
        },

        /**
         *
         * @param model
         * @param collection
         * @returns {{httpStatus: number, body: {status: string, id: *, url: string}}}
         */
        updateRegion : function(model, collection){
            if (collection[model.getId()] != null){
                model.setGettersSetters({
                    lastUpdate : "08.02.2015"
                })

                rodrigez["apis_region"][model.getId()]= model.getObjectRepresentation();
                //rodrigez["apis_region"][model.getId()]= model.getObjectRepresentation();
                return {
                    httpStatus: 200,
                    body : {
                        status: "updated",
                        id:model.getId(),
                        url:"region/"+model.getId()
                    }
                };
            } else {
                return {
                    httpStatus:403,
                    body: {
                        status : "update failed",
                        id : model.getId(),
                        url:"region/"+model.getId()
                    }
                }
            }
        },

        postPlayer : function(model, collection){

            console.log("post player:", model);
            if (collection[model.getName()] == null){
                model.setGettersSetters({
                    id:model.getName().substring(0, 3).toLowerCase(),
                    created : "21.03.1971",
                    lastUpdate : "21.03.1971",
                    url :"player/"+model.getName().substring(0, 3).toLowerCase(),
                    mutable: ["name", "nickName", "email"]

                })
                rodrigez["apis_player_"+model.getId()]= model.getObjectRepresentation();
                rodrigez["apis_player"][model.getId()]=model.getObjectRepresentation();
                return {
                    httpStatus: 201,
                    body : {
                        status: "created",
                        id:model.getId(),
                        url:"player/"+model.getId()
                    }
                };
            } else {
                return {
                    httpStatus:403,
                    body: {
                        status: "creation failed",
                        name:model.getId()
                    }
                }
            }
        },

        /**
         *
         * @param model
         * @param collection
         */
        updatePlayer : function(model, collection){
            if (collection[model.getId()] != null){
                model.setGettersSetters({
                    lastUpdate : "08.02.2015"
                })

                rodrigez["apis_player"][model.getId()]= model.getObjectRepresentation();
                //rodrigez["apis_region"][model.getId()]= model.getObjectRepresentation();
                return {
                    httpStatus: 200,
                    body : {
                        status: "updated",
                        id:model.getId(),
                        url:"player/"+model.getId()
                    }
                };
            } else {
                return {
                    httpStatus:403,
                    body: {
                        status : "update failed",
                        id : model.getId(),
                        url:"player/"+model.getId()
                    }
                }
            }
        }
	};
	
	return MockDataGateway;
})();