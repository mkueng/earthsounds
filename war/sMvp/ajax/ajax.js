var rodrigez = rodrigez || {}

//GET
var getRequest = function(requestUrl, callback) {
	$.mobile.showPageLoadingMsg();
	$.ajax({
		type	 : "GET",
		dataType : "json",
		contentType : 'application/json',
		url 	 : requestUrl,
		
		success	 :	function(data) {
						$.mobile.hidePageLoadingMsg();
						data.success=true;
						callback(data);
						
					},
		error	 :	function(data) {
						$.mobile.hidePageLoadingMsg();
						data.success=false;
						callback(data);
					},
		complete :  function (data) {
						
					}
	});
}

//POSTJson
var postRequestJson = function(requestUrl, data, callback){

	$.ajax({
		type : "POST",
		dataType : "json",
		contentType : 'application/json',
		data : data,
		url : requestUrl,
		success : function(data) {
						callback();
		},
		error : function(data){
		},
		complete : function(data) {
			$.mobile.hidePageLoadingMsg();
		}
	})
}

//POST
var postRequest = function(requestUrl, data, callback, callbackInfo){
	var jsonData = JSON.stringify(data);
	
	$.mobile.loadingMessageTheme = 'a';
	$.mobile.loadingMessage = "writing data to remote server...";
	$.mobile.loadingMessageTextVisible = true;
	$.mobile.showPageLoadingMsg();
	$.ajax({
		type : "POST",
		dataType : "json",
		contentType : 'application/json',
		data : jsonData,
		url : requestUrl,
		success : function(data) {
			
		},
		error : function(data){
			
		},
		complete : function(data) {
			$.mobile.hidePageLoadingMsg();
		}
	})
}

//PUT
var putRequest = function(requestUrl,data,callback,callbackInfo) {
	$.mobile.loadingMessageTextVisible = true;
	$.mobile.loadingMessageTheme = 'a';
	$.mobile.loadingMessage = "updating data on remote server...";
	$.mobile.showPageLoadingMsg();
	$.ajax({
		type : "PUT",
		dataType : "json",
		contentType : 'application/json',
		data : data,
		url : requestUrl,
		success : function(data) {
			console.log("getBack_putData:",data);
			if (callback !=null) {
				callbackInfo.status=true;
				callback(callbackInfo);
			}
		},
		error : function(data){
			if (callback !=null) {
				callbackInfo.status=false;
				callback(callbackInfo);
			}
		},
		complete : function(data) {
			$.mobile.hidePageLoadingMsg();
		}
	})
}

//DELETE
var deleteRequest = function(requestUrl,data,callback,callbackInfo) {
	$.mobile.loadingMessageTextVisible = true;
	$.mobile.loadingMessageTheme = 'a';
	$.mobile.loadingMessage = "deleting data on remote server...";
	$.mobile.showPageLoadingMsg();
	$.ajax({
		type : "DELETE",
		dataType : "json",
		contentType : 'application/json',
		data:data,
		url : requestUrl,
		success : function(data) {
			if (callback !=null) {
				callbackInfo.status=true;
				callback(callbackInfo);
			}
		},
		error : function(data){
			if (callback !=null) {
				callbackInfo.status=false;
				callback(callbackInfo);
			}
		},
		complete : function(data) {
			$.mobile.hidePageLoadingMsg();
		}
	})
}

//post tranche
rodrigez.postTranche = function(counter, territoryArray) {
	var length = territoryArray.length;
	var progress =100/length*counter;
	var terArr = [];
		if (counter < length-50) {
			var lengthCounter = counter +50;
			for (var i = counter; i < lengthCounter; i++) {
				terArr.push(territoryArray[i]);
			}
			var jsonData = "{\"territories\""+":"+JSON.stringify(terArr)+"}";
			postRequestJson(rodrigez.baseUrl+'territory', jsonData,rodrigez.postTranche,counter+50);
		} else {
			var lengthCounter = length;
			for (var i = counter; i < lengthCounter; i++) {
				terArr.push(territoryArray[i]);
				
			}
			var jsonData = "{\"territories\""+":"+JSON.stringify(terArr)+"}";
			postRequestJson(rodrigez.baseUrl+'territory', jsonData,rodrigez.endTranche,null);
		}
}

//end tranche
rodrigez.endTranche = function (){
    rodrigez.region.boundary = [];
    for (var i=0,len = rodrigez.boundaryPath.length;i<len;i++){
        rodrigez.region.boundary.push({lat:rodrigez.boundaryPath[i].lat(), lng:rodrigez.boundaryPath[i].lng()});
    }
	
	postRequest(rodrigez.baseUrl+'region',rodrigez.region,null);
	$.publish("clearCurrentBoundaryPath", {});
	
	$("#popupBasic").text("region '"+rodrigez.region.name+"' successfully created");
	$('#popupBasic').popup("open");
}

//get tranche
rodrigez.getTranche = function(numberOfResultsFound,providedData) {
	console.log("PROVIDED:",providedData);
	console.log("numberOfResultsFound:",numberOfResultsFound);
	getRequest(rodrigez.baseUrl+'territory'+"?"+"region="+providedData.id+"&offset="+offset,rodrigez.facade.setPolygonesFromData, rodrigez.facade.setPolygonesFromDataFailed	);
}


