var AjaxHandler = (function(){
		
	/**
	 * @constructor
	 */
	function AjaxHandler(baseUrl){
		
		this.baseUrl=baseUrl;
		this.executeRequest = function(type,endpoint,callback,jsonRep){
			
			
			$.ajax({
				type 		: type,
				dataType 	: "json",
				contentType : 'application/json',
				data 		: jsonRep,
				url 		: this.baseUrl+endpoint,
				success 	: function(data) {
					callback(data);
				},
				error 		: function(xhr){
			        alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
				},
				complete : function(data) {	
				}
			});
		};
	}
	
	AjaxHandler.prototype.post = function(endpoint,jsonRep,callback){
		this.executeRequest("POST", endpoint,callback,jsonRep);
	};
	
	AjaxHandler.prototype.get = function(url,callback){
		this.executeRequest("GET", url,callback);
	};
	AjaxHandler.prototype.put = function(url,data,callback){
		this.executeRequest("PUT", url,callback,data);
	};
	AjaxHandler.prototype.destroy = function(url,callback){
		this.executeRequest("DELETE", url, callback);
	};
	
	return AjaxHandler;
})();