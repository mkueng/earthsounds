/**
 * 
 */

var Config = (function(){
	
	rodrigez = {}; //namespace rodrigez
	namespace = rodrigez;
	namespace.domainModel = null;
	
	$.support.cors = true;
	namespace.baseUrl = "http://localhost:8888/rodrigez/api/"
	
	namespace.BOUNDARY_COLOR	= "#ffffff";	
	namespace.QUADRANT_COLOR	= "#111111";
	namespace.LANGUAGE		= "en";
	

	namespace.TERRITORY_TYPE_COLOR = {
			BLOCKED	 	: "#ff0000",
			SAVEBASE	: "#00ff00",
			FREE		: "#ffffff",
			OCCUPIED	: "#ffff00",
			HOMEBASE 	: "#0000ff"
	}
	
	//mobile 
	if ( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		namespace.mobile = true;
		namespace.clkEvent = "dblclick";
	} else {
		namespace.mobile = false;
		namespace.clkEvent ="click";
	}
	
	//publish/subscribe > IE8
	var o = $( {} ); 
	$.subscribe 	= o.on.bind(o); 
	$.unsubscribe 	= o.off.bind(o);
	$.publish 		= o.trigger.bind(o);
	
	SMVP.setDataGateway(new SMVP.DataGateway(new SMVP.AjaxHandler()));
	SMVP.setNameSpace(namespace);
	
	$(window).resize(function() {
		$(document).trigger("windowResize", {});
	});
	
	
	
})()  