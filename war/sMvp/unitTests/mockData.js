
namespace.apis_territory = {
	"template" : {
		id:"template",
		name:"",
		owner:"",
		status:"",
		itFactor:"",
		region:"",
		spacing:"",
		geoPoints:""
	}
};

namespace.apis_region = {
    "template" : {
    	id:"template",
        name:"",
        outline : [],
        spacing : "",
        created : "",
        lastUpdate : "",
        creator : "",
        numberOfTerritories : 0,
        mainGrid: [0.0016,0.0032,0.0064,0.0128,0.0256],
        root: "region",
        mutable: ["name"],
        mandatory : ["name"]
    },
    
    "zur" : {
    	id: "zur",
    	name: "zurich",
    	created: "",
		creator: "",
		lastUpdate: "",
		mainGrid: [0.0016,0.0032,0.0064,0.0128,0.0256],
		mandatory: ['name'],
		mutable: ["name"],
		numberOfTerritories: 0,
		outline: [
		  {"lat": 47.1336, "lng": 6.988799999999969},
		  {"lat" : 47.0336, "lng": 6.998399999999947},
		  {"lat" : 47.0336, "lng": 6.998399999999947},
		  {"lat" : 47.04, "lng" : 7.008000000000038},
		  {"lat" : 47.04, "lng" : 7.008000000000038},
		  {"lat" : 47.0464, "lng" : 7.008000000000038},
		  {"lat" : 47.0464, "lng" : 7.008000000000038},
		  {"lat" : 47.0464, "lng" : 6.998399999999947},
		  {"lat" : 47.0464, "lng" : 6.998399999999947},
		  {"lat" : 47.04, "lng" : 6.988799999999969},
		  {"lat" : 47.04, "lng" : 6.988799999999969},
		  {"lat" :	47.1336, "lng" : 6.988799999999969}
		],
		root: "region",
		spacing: "0.0256"
    }
};


namespace.apis_player = {
    "template" : {
    	id : "template",
        name:"",
        nickName:"",
        position: {},
        email:"",
        created:"",
        updated:"",
        deleted:"",
        password:"",
        root:"player",
        mutable: ["name", "nickName", "email", "password"],
        mandatory : ["name", "nickName", "email", "password"]
    },

	"a367dg27dihsa" : {
		id:"a367dg27dihsa",
		name:"Max Mustermann",
        nickName:"max",
        position:{lat:47.4632, lng: 8.3064},
        email:"max@bla.com",
        created:"21.03.1989",
        updated:"",
        deleted:"",
        password:"a2ää$$sdhiaosd",
		root:"player",
        mutable: ["name", "nickName", "email", "password"]
	},
	"9audsha912hej" : {
		id:"9audsha912hej",
        name:"Heinz BlBla",
        nickName:"bla",
        position:{lat:47.7632, lng: 8.7064},
        email:"hein@bla.com",
        created:"23.12.2000",
        updated:"",
        deleted:"",
        password:"a2ää$$sdhiaosd",
        root:"player",
        mutable: ["name", "nickName", "email", "password"]
	},
	"8ashdu28hd8dhsu" : {
		id:"8ashdu28hd8dhsu",
        name:"Bianca Bianci",
        nickName:"Bibi",
        position:{lat:47.3632, lng: 8.1064},
        email:"bibi@bla.com",
        created:"07.10.1974",
        updated:"",
        deleted:"",
        password:"a2ää$$sdhiaosd",
        root:"player",
        mutable: ["name", "nickName", "email", "password"]
	},
	"8a9djasd89893" : {
		id:"8a9djasd89893",
        name:"Peter Putler",
        nickName:"putin",
        position:{lat:47.4832, lng: 8.3964},
        email:"putin@bla.com",
        created:"01.01.1999",
        updated:"",
        deleted:"",
        password:"a2ää$$sdhiaosd",
        root:"player",
        mutable: ["name", "nickName", "email", "password"]
	},
	"a367dsadg27dihsa" : {
		id:"a367dsadg27dihsa",
        name:"Mini Driver",
        nickName:"mini",
        position:{lat:47.0, lng: 8.0064},
        email:"mini@bla.com",
        created:"21.03.1989",
        updated:"",
        deleted:"",
        password:"a2ää$$sdhiaosd",
        root:"player",
        mutable: ["name", "nickName", "email", "password"]
	},
	"9auddsha912hej" : {
		id:"9auddsha912hej",
        name:"Jessica Simpson",
        nickName:"jessy",
        position:{lat:47.01, lng: 8.1064},
        email:"jessi@bla.com",
        created:"23.12.2000",
        updated:"",
        deleted:"",
        password:"a2ää$$sdhiaosd",
        root:"player",
        mutable: ["name", "nickName", "email", "password"]
	},
	"8ashduas28hd8dhsu" : {
		id:"8ashduas28hd8dhsu",
        name:"Pan Tau",
        nickName:"pan",
        position:{lat:46.4632, lng: 8.3064},
        email:"pan@bla.com",
        created:"07.10.1974",
        updated:"",
        deleted:"",
        password:"a2ää$$sdhiaosd",
        root:"player",
        mutable: ["name", "nickName", "email", "password"]
	},
	"8a9ddas89893" : {
		id:"8a9ddas89893",
        name:"Peter Vestera",
        nickName:"peter",
        position:{lat:46.6632, lng: 8.0064},
        email:"peter@bla.com",
        created:"01.01.1999",
        updated:"",
        deleted:"",
        password:"a2ää$$sdhiaosd",
        root:"player",
        mutable: ["name", "nickName", "email", "password"]
	},
    "a367dg27dihwsa" : {
        id:"a367dg27dihwsa",
        name:"Helge Schneider",
        nickName:"helge",
        position:{lat:43.4632, lng: 5.3064},
        email:"max@bla.com",
        created:"21.03.1989",
        updated:"",
        deleted:"",
        password:"a2ää$$sdhiaosd",
        root:"player",
        mutable: ["name", "nickName", "email", "password"]
    },
    "9audsha91w2hej" : {
        id:"9audsha91w2hej",
        name:"Marius Mal",
        nickName:"mal",
        position:{lat:41.7632, lng: 9.7064},
        email:"hein@bla.com",
        created:"23.12.2000",
        updated:"",
        deleted:"",
        password:"a2ää$$sdhiaosd",
        root:"player",
        mutable: ["name", "nickName", "email", "password"]
    },
    "8aashdu28hd8dhsu" : {
        id:"8aashdu28hd8dhsu",
        name:"Pippi Langstrumpf",
        nickName:"pipi",
        position:{lat:42.3632, lng: 7.1064},
        email:"bibi@bla.com",
        created:"07.10.1974",
        updated:"",
        deleted:"",
        password:"a2ää$$sdhiaosd",
        root:"player",
        mutable: ["name", "nickName", "email", "password"]
    },
    "8a9djasd8r9893" : {
        id:"8a9djasd8r9893",
        name:"Yves Robert",
        nickName:"yves",
        position:{lat:42.4832, lng: 7.3964},
        email:"putin@bla.com",
        created:"01.01.1999",
        password:"a2ää$$sdhiaosd",
        root:"player",
        mutable: ["name", "nickName", "email", "password"]
    },
    "a367dsadg27qdihsa" : {
        id:"a367dsadg27qdihsa",
        name:"Micky Maus",
        nickName:"Micky",
        position:{lat:41.0, lng: 8.1064},
        email:"mini@bla.com",
        created:"21.03.1989",
        updated:"",
        deleted:"",
        password:"a2ää$$sdhiaosd",
        root:"player",
        mutable: ["name", "nickName", "email", "password"]
    },
    "9auddsha912chej" : {
        id:"9auddsha912chej",
        name:"Darth Vader",
        nickName:"darth",
        position:{lat:41.01, lng: 8.1864},
        email:"jessi@bla.com",
        created:"23.12.2000",
        updated:"",
        deleted:"",
        password:"a2ää$$sdhiaosd",
        root:"player",
        mutable: ["name", "nickName", "email", "password"]
    },
    "8ashduas28hd8ddhsu" : {
        id:"8ashduas28hd8ddhsu",
        name:"Globi",
        nickName:"globi",
        position:{lat:41.4632, lng: 10.3064},
        email:"pan@bla.com",
        created:"07.10.1974",
        updated:"",
        deleted:"",
        password:"a2ää$$sdhiaosd",
        root:"player",
        mutable: ["name", "nickName", "email", "password"]
    },
    "8a9ddas89w893" : {
        id:"8a9ddas89w893",
        name:"Berni Eigenheer",
        nickName:"berni",
        position:{lat:44.6632, lng: 6.0064},
        email:"peter@bla.com",
        created:"01.01.1999",
        updated:"",
        deleted:"",
        password:"a2ää$$sdhiaosd",
        root:"player",
        mutable: ["name", "nickName", "email", "password"]
    }
};


