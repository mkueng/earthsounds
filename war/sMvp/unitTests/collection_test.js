Collection_test = TestCase("Collection_test");

//setUp
	Collection_test.prototype.setUp = function(){
		cut = new Collection({
			name:"region_model_collection",
			isUnitTest:true,
			data:null,
			model:rodrigez.region_model,
			dataGateway: new MockDataGateway(),
			url: "region"
		});
	}

//tearDown
	Collection_test.prototype.tearDown = function(){
		cut = null;
	}

//tests
	Collection_test.prototype.test_mapModels_should_return_collection_of_models = function(){
		
		var expected = {};
		var actual = cut.read();
		assertEquals(typeof(expected), typeof(actual));
	}

	Collection_test.prototype.test_equal_mapModels_should_return_value = function(){
		
		var expected = "New York";
		var actual = cut.read();
		assertEquals(expected, actual["new"].getName());
	}
