/**
 * Created by marcokueng on 09/02/15.
 */

MockDataGateway_test = TestCase("MockDataGateway_test");

//setUp
    MockDataGateway_test.prototype.setUp = function(){
        collection = new Collection({
            url : "region",
            data: null
        });
        model = new Model({
            url :"region",
            id : "zur"
        })

        model2 = new Model({
            url : "region",
            name : "BananaRepublic"
        })

        cut = new MockDataGateway();
    }

//tearDown
    MockDataGateway_test.prototype.tearDown = function(){
        cut = null;
    }


//tests

    MockDataGateway_test.prototype.test_readCollection_should_return_object = function(){
        var expected = {};
        var actual = cut.readCollection(collection);
        assertEquals(typeof(expected), typeof(actual));

    }

    MockDataGateway_test.prototype.test_fetchModel_should_return_object = function(){
        var expected = {};
        var actual = cut.fetchModel(model);
        assertEquals(typeof(expected), typeof(actual));

    }

    MockDataGateway_test.prototype.test_postModel_should_return_status = function(){
        var expected = "ban";
        var result = cut.postModel(model2);
        assertEquals(expected, result.body.id);

    }

    MockDataGateway_test.prototype.test_updateModel_should_return_status = function(){
        var expected = "Zurich City";
        var actualModel = new Model(cut.fetchModel(model));
        actualModel.setName("Zurich City");
        cut.updateModel(actualModel);
        var result = cut.fetchModel(model);
        assertEquals(expected, result.name);
    }