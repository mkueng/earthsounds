/**
 * 
 */

$(document).ready(function(){
	
	$.support.cors = true;
	
	rodrigez = {}; //namespace rodrigez
	namespace = rodrigez;
	namespace.baseUrl = "http://localhost:8888/rodrigez/api/";
	
	SMVP.setDataGateway(new SMVP.DataGateway(new SMVP.AjaxHandler()));
	SMVP.setNameSpace(namespace);
	
	userSignInModel = new SMVP.Model({	
		"id" : "",
		"nickName" : "",
		"password" : "",
		"tokenString" :"",
		"root" : "login"
	})
	
	$('#btn-submit').click(function(){
		$.mobile.loading('show');
		var nickName = $('#txt-nickName').val();
		var password = $('#txt-password').val();
		userSignInModel.setNickName(nickName);
		userSignInModel.setPassword(password);
		
		userSignInModel.post(function(response){
			
			document.cookie = "tokenString="+response.getTokenString();
			document.cookie = "nickName="+response.getNickName();
			if ( typeof response.getClearance === 'function') {
				console.log("document.cookie nickName: ", response.getNickName());
				document.cookie = "clearance="+response.getClearance();
			}
				
			if (response.getTokenString() != "") {
				//$("#dlg-valid-credentials").popup("open");
				setTimeout(function(){
					$.mobile.loading('hide');
					window.location="main.html";
				},3000);
				
			} else {
				$.mobile.loading('hide');
				$("#dlg-invalid-credentials").popup("open");
			}
		});
	})
})