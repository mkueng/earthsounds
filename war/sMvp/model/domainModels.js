/**
 * rodrigez
 * domain models
 * mk 15/16 
 */


namespace.userCollection = new SMVP.Collection("user");
namespace.regionCollection = new SMVP.Collection("region");
namespace.soundCollection = new SMVP.Collection("sound");
namespace.actionCollection = new SMVP.Collection("action");

var agentLocationImage = {
	    url: "../resources/location_resized.gif",
	    size: new google.maps.Size(60, 60),
	    origin: new google.maps.Point(0, 0),
	    anchor: new google.maps.Point(0,0)
	  };

var soundActionImage = {
	 url: "../resources/icons/buttonPlay.svg",
	    size: new google.maps.Size(20, 20),
	    origin: new google.maps.Point(0, 0),
	    anchor: new google.maps.Point(-5,25)
};

var soundAction = new SMVP.Model({
	id : "soundAction",
	name : "soundAction",
	sounds : null
})

namespace.actionCollection.addModel(soundAction);

//rodrigez.territoryModelCollection = new SMVP.Collection(rodrigez.territoryModel);


namespace.user = new SMVP.Model({
	root : null,
	firstName : null,
	lastName : null,
	nickName : null,
	email : null,
	gender : null,
	birthdate : null,
	deviceToken : null
})

namespace.territory = new SMVP.Model({
    id: null,
    name: null,
    owner : null,
    status : null,
    itFactor : null,
    region : null,
    spacing : null,
    geoPoints : null,
    action : null,
    urlRoot : "/territory"
});

namespace.territories = new SMVP.Model({
    newRegion : null,
    existingRegions : {}
});

namespace.polyline = new SMVP.Model({
    id:"polyline",
    boundary : {
        polylines : [],
        path: [],
        props : {
            type : "boundary",
            path : [],
            strokeColor : "#FF0000",
            strokeOpacity : 1,
            strokeWeight : 1
        }
    }
});

namespace.polygon = new SMVP.Model({
    id:"polygon",
    statusColors  : {
        FREE : "#ffffff",
        HOMEBASE : "#0000ff",
        SAVEBASE : "#00ff00",
        OCCUPIED : "#ffff00",
        BLOCKED : "#ff0000"
    },

    newRegion : {
        mouseInFillColor : "#faa",
        polygons : {}
    },
    
    existingRegions : {
    	mouseInFillColor : "#faa",
    	polygons : {}
	},
	regionOutline : {
		mouseInFillColor : "#88dcff",
		mouseOutFillColor :"#bccbff",
		regionSelectedFillColor : "#883387"
	}
});

namespace.marker = new SMVP.Model({
    id : "marker",
    
    boundaryMarkers : {
        count : 0,
        undoRedo: 0,
        data : {}
    },
    playerMarkers : {
        count : 0,
        cluster : null,
        data : {}
    },
    currentAgentLocationMarkers : {
    	 count : 0,
         cluster : null,
         data : {}
    },
    
    territorySoundActionMarkers : {
    	count : 0,
        cluster : null,
        data : {}
    },

    playerMarker : {
        id : null,
        type : 'player',
        draggable : true,
        position : null,
        title : "",
        isCluster : true,
        icon : {
            path: google.maps.SymbolPath.BACKWARD_OPEN_ARROW,
            scale: 7,
            strokeWeight: 3,
            strokeColor: '#ff0000'
        }
    },

    boundaryMarker : {
        id : null,
        type : 'boundary',
        position : null,
        isCluster : false,
        icon : {
            path: google.maps.SymbolPath.CIRCLE,
            scale: 2.5,
            strokeWeight:2,
            strokeColor: "#FF9999"
        }
    },
    
    currentAgentLocationMarker : {
    	id : null,
    	position: null,
    	isCluster : false,
    	map: null,
    	optimized: false,
    	labelClass: "markerLabels",
    	icon : {
            path: google.maps.SymbolPath.BACKWARD_OPEN_ARROW,
            scale: 7,
            strokeWeight: 3,
            strokeColor: '#00cc00'
        }
    },
    
    territorySoundActionMarker : {
    	id : null,
    	position: null,
    	isCluster : true,
    	map: null,
    	optimized: true,
    	labelClass: "markerLabels",
    	icon :  soundActionImage,
    	zIndex : -1
    }
});


