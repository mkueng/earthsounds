/**
 * view models
 * rodrigez
 * mk 15/16 
 */

//map
	namespace.mapViewModel = new SMVP.Model({
		id:"mapView",
		data : null,
		template : null,
		container : null,
		currentValue:null
	});

//statusInfo
	
	namespace.statusInfoViewModel = new SMVP.Model({
		id:"statusInfoView",
		data : null,
		template : null,
		container : null
	})
	
	
//region
	namespace.allAboutRegionViewModel = new SMVP.Model({
		id : "allAboutRegionView",
		data : rodrigez.regionCollection,
		template : Templates.allAboutRegionViewTemplate,
		container : "allAboutRegionContainer",
		actions : ["minus", "edit", "info"],
		subTriads : ["regionControlView"],
		currentValue : ""
	});
		namespace.regionControlViewModel = new SMVP.Model({
			id : "regionControlView",
			data : null,
			template : Templates.regionControl,
			container : "regionControlContainer"
		});

	namespace.regionCreateViewModel = new SMVP.Model({
		id : "regionCreateView",
		data : null,
	    template : Templates.regionCreateViewTemplate,
	    container : "regionCreateContainer"
	});

	namespace.regionEditViewModel = new SMVP.Model({
		id : "regionEditView",
		data : null,
		template : Templates.regionEditViewTemplate,
		container : "regionEditContainer"
	});

	namespace.regionInfoViewModel = new SMVP.Model({
		id : "regionInfoView",
		data : null,
		template : Templates.regionInfoViewTemplate,
		container : "regionInfoContainer"
	});

//territory
	namespace.territoryShowModel = new SMVP.Model({
		id : "territoryShowView",
		data : null,
		template : null,
		container : null
	});
	
	
	
//user
	namespace.allAboutUserViewModel = new SMVP.Model({
		id : "allAboutUserView",
		data : rodrigez.userCollection,
		template : Templates.allAboutUserViewTemplate,
		container : "allAboutUserContainer",
		actions : ["location","home","minus", "edit", "info"],
		subTriads : ["userControlView"],
		currentValue : ""
	});

		namespace.userControlViewModel = new SMVP.Model({
			id : "userControlView",
			data : null,
			template : Templates.userControl,
			container : "userControlContainer"
		});

	namespace.userCreateViewModel = new SMVP.Model({
		id : "userCreateView",
	    data : null,
	    template : Templates.userCreateViewTemplate,
	    container : "userCreateContainer"
	 
	});

	namespace.userEditViewModel = new SMVP.Model({
		id : "userEditView",
		data : null,
		template : Templates.userEditViewTemplate,
		container : "userEditContainer"
	});

	namespace.userInfoViewModel = new SMVP.Model({
		id : "userInfoView",
		data : null,
		template : Templates.userInfoViewTemplate,
		container : "userInfoContainer"
	});
	
	namespace.userLocationViewModel = new SMVP.Model({
		id : "userLocationView",
		data : null,
		template : Templates.userLocationViewTemplate,
		container : "userLocationContainer"
	});
	
//player
	namespace.allAboutPlayerViewModel = new SMVP.Model({
		id : "allAboutPlayerView",
		data : null,
		template : Templates.allAboutPlayerViewTemplate,
		container : "",
		actions : [],
		subTriads : [],
		currentValue : ""
	});

	

	namespace.territoryInfoViewModel = new SMVP.Model({
	    id:"territoryInfoViewModel",
	    data : null,
	    template : Templates.newTerritoryinfoWindow,
	    container : null
	});

	namespace.territoryEditViewModel = new SMVP.Model({
	    id:"territoryEditViewModel",
	    data : null,
	    template: Templates.territoryEditWindow,
	    container : null
	});

//polygon
	
	namespace.polygonViewModel = new SMVP.Model({
		id:"polygonViewModel",
		data:null,
		template:null,
		container:null
	});
	
//infoBox
	namespace.infoBoxViewModel = new SMVP.Model({
		id: "infoBoxViewModel",
		
		infoboxes : {
			regionLabelInfoBox: {
				position: null,
				content : null,
				boxClass: 'regionMapInfoBoxfalse',
				enableEventPropagation:false,
				closeBoxURL: "",
				disableAutoPan: true,
				zIndex : 0,
				instance : true
			}
		}
	})
	
	
//actionHandler
	
	namespace.allAboutSoundActionViewModel = new SMVP.Model({
		id : "allAboutSoundActionView",
		data : namespace.soundCollection,
		categoryFilter : "",
		template : Templates.allAboutSoundActionViewTemplate,
		container : "allAboutSoundActionContainer",
		actions : ["minus", "info"],
		subTriads : ['soundActionControlView'],
		currentValue : ""
	})
	
	namespace.soundActionControlViewModel = new SMVP.Model({
		id : "soundActionControlView",
		data : null,
		template : Templates.soundActionControl,
		container : "soundActionControlContainer"
	});
	
	namespace.soundActionInfoViewModel = new SMVP.Model({
		id : "soundActionInfoView",
		data : null,
		template : Templates.soundActionInfoViewTemplate,
		container : "soundActionInfoContainer"
	});
	
	namespace.soundActionCreateViewModel = new SMVP.Model({
		id : "soundActionCreateView",
	    data : null,
	    template : Templates.soundActionCreateViewTemplate,
	    container : "soundActionCreateContainer"
	 
	});
	
	