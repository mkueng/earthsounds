/**
 * 
 */

var Utilities = (function(){
	var self = this;
	
	return {
		
		isValid : function(input) {
			if ($(input).is('select') != true) {
				return this["is"+input.type](input);
			} else return true;
		},
		
		isemail : function(input){
			var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			return regex.test($(input).val());
		},
		
		isdate : function(input){
			return true;
		},
		
		istext : function(input){
			return $(input).val().length > ($(input).attr('minlength'));
		},
		
		isnumber : function(input) {
			return $(input).val().length > ($(input).attr('minlength'));
		},
		
		isradio : function(input){
			return true;
		},

		isfile : function(input){
			return true;
		}
	}
})()