namespace.appStart = function(){

	//map
	namespace.mapPresenter = new MapPresenter();
	
	//app
	namespace.appPresenter = new AppPresenter();
	
    //static
	namespace.markerPresenter = new MarkerPresenter();
	namespace.polylinePresenter = new PolylinePresenter();
    namespace.territoryInfoPresenter = new TerritoryInfoPresenter();
   
    namespace.polygonPresenter = new PolygonPresenter();
	namespace.territoryPresenter = new TerritoryPresenter();
	namespace.regionExistPresenter = new RegionExistPresenter();
	namespace.statusInfoPresenter = new StatusInfoPresenter();
	namespace.infoBoxPresenter = new InfoBoxPresenter();
	namespace.actionPresenter = new ActionPresenter();
	
    
	namespace.soundCollection.fetch(function(){
		 namespace.territoryEditPresenter = new TerritoryEditPresenter();
		 namespace.soundActionInfoPresenter = new SoundActionInfoPresenter();
		 namespace.soundActionCreatePresenter = new SoundActionCreatePresenter();
		 namespace.soundActionPresenter = new SoundActionPresenter();
	});
	
    //player
	namespace.userCollection.fetch(function(response){
		
		namespace.userInfoPresenter = new UserInfoPresenter();
		
		namespace.userEditPresenter = new UserEditPresenter();
		namespace.userCreatePresenter = new UserCreatePresenter();
		namespace.userLocationPresenter = new UserLocationPresenter();
		namespace.userPresenter = new UserPresenter();
		namespace.playerPresenter = new PlayerPresenter();
		
		//region
		namespace.regionCollection.fetch(function(){
			namespace.regionInfoPresenter = new RegionInfoPresenter();
			namespace.regionEditPresenter = new RegionEditPresenter();
			namespace.regionCreatePresenter = new RegionCreatePresenter();
			namespace.regionPresenter = new RegionPresenter();
			
			 
		});
	});
 
	new KeyInputHandler();
	
	//territory
	
	$('#toggleSideBar').click(function(){
		$('#navi_container').fadeToggle ("fast", function(){
		})
	});
};