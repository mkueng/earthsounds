/**
 * InfoBoxPresenter
 */

var InfoBoxPresenter = (function(){
	
	InfoBoxPresenter.prototype = new SMVP.Presenter(namespace.infoBoxViewModel);
	var infoBoxes = {};
	var boxOptions = {};
	
	/**
	 * @constructor
	 */
	function InfoBoxPresenter(){
		var self = this;
	
		$.each(this.getModel().getInfoboxes(), function(key, options){
			infoBoxes[key] = new InfoBox();
			infoBoxes[key].setOptions(options);
			infoBoxes[key].instance = options.instance;
		})
	};
	
	InfoBoxPresenter.prototype.renderInfobox = function(type, position, content){
		var infoBoxInstance = infoBoxes[type].instance ?  new InfoBox() : infoBoxes[type];
		boxOptions = this.getModel().getInfoboxes()[type];
		boxOptions.position = position;
		boxOptions.content = content;
		infoBoxInstance.setOptions(boxOptions);
		infoBoxInstance.open(namespace.mapPresenter.getMap());
		return infoBoxInstance;
	};
	
	return InfoBoxPresenter;
})();