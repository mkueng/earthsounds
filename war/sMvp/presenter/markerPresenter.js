/**
 * MarkerPresenter
 * (c) kuema 2017
 * 
 * @binding createMarker
 * @binding updateMarker
 * @binding deleteMarker
 * @binding deleteMarkers
 * @binding undoPath
 * @binding redoPath
 * @binding clearPath
 * @binding hideMarkers
 * @binding showMarkers
 * 
 * @listener google.maps.event marker dragend
 * @listener google.maps.event marker drag
 *
 * @trigger markerUpdateEvent
 * @trigger markerCreated
 * @trigger markersDeleted
 * @trigger markerDeleted
 * @trigger markersHide
 * @trigger markersShow
 */

var MarkerPresenter = (function(){

    MarkerPresenter.prototype = new SMVP.Presenter(namespace.marker);
    
    var map=null;

    /**
     *
     * @constructor
     */
    function MarkerPresenter(){
        var self = this;
        
        map = namespace.mapPresenter.getMap();
        
        this.mouseoverSoundActionMarker = {
        	url:"../../resources/icons/buttonPlay.svg",
        	size: new google.maps.Size(20, 20),
        	origin: new google.maps.Point(0, 0),
        	anchor: new google.maps.Point(-5,25)
        	
        }
        
        this.soundActionMarker = {
        	url: "../../resources/icons/buttonPlay.svg",
        	size: new google.maps.Size(20, 20),
        	origin: new google.maps.Point(0, 0),
        	anchor: new google.maps.Point(-5,25)
        };
        
        //config marker clusterer
        var markerClustererStyle =   {
        		styles: [{
        			 textColor: 'black',
        	    	    url: '../../resources/icons/videoplaybutton.png',
        	    	    height: 50,
        	    	    width: 50,
        	    	    opacity : 0.1
        		}],
        	
    	    gridSize:100,
    	    maxZoom:15
        }
        
        
        self.getModel().getPlayerMarkers().cluster = new MarkerClusterer(
          map, 
          [], { gridSize: 70, 
        		maxZoom: 15, 
        		imagePath: 'https://cdn.rawgit.com/googlemaps/js-marker-clusterer/gh-pages/images/m' 
        });
        
        //config marker clusterer
        self.getModel().getTerritorySoundActionMarkers().cluster = new MarkerClusterer(
          map, 
          [], markerClustererStyle
        );
        
        //config infoBox
        self.infoBox = new InfoBox({
            boxClass : "markerInfoBox",
            enableEventPropagation:false,
            closeBoxURL: "https://www.google.com/intl/en_us/mapfiles/close.gif",
            disableAutoPan: true
        });

        // application events
        
        /**
         * bind createMarker
         */
        $(document).bind("createMarker", function(o,data){
            self.createMarker(data);
        });
        
        /**
         * bind updateMarker
         */
        $(document).bind("updateMarker", function(o,data){
        	self.updateMarker(data);
        });
        
        /**
         * bind deleteMarker
         */
        $(document).bind("deleteMarker", function(o,data){
            self.deleteMarker(data);
        });
        
        /**
         * bind deleteMarkers
         */
        $(document).bind("deleteMarkers", function(o,data){
            self.deleteMarkers(data);
        });
        
        /**
         * bind undoPath
         */
        $(document).bind("undoPath", function(o,data){
            self.undoMarker(data);
        });
        
        /**
         * bind redoPath
         */
        $(document).bind("redoPath", function(o,data){
            self.redoMarker(data);
        });
        
        /**
         * bind clearPath
         */
        $(document).bind("clearPath", function(o,data){
            self.deleteMarkers(data);
        });
        
        /**
         * bind hideMarkers
         */
        $(document).bind("hideMarkers", function(o,data){
            self.hideMarkers(data);
        });
        
        /**
         * bind showMarkers
         */
        $(document).bind("showMarkers", function(o,data){
            self.showMarkers(data);
        });
    }

    // public API

    /**
     * create marker
     * @param data
     * @trigger markerCreated
     */
    MarkerPresenter.prototype.createMarker = function(data) {
        var self = this;
        var model = this.getModel();
        var markerModel = model["get"+data.type.ucfirst()+"Marker"]();
        var markerModelClone = null;
        var markers = model["get"+data.type.ucfirst()+"Markers"]();
        

        switch (data.type) {
            case "player" : {
                markerModel.id = data.id;
                markerModel.nickName = data.model.getNickName();
                markerModel.position = new google.maps.LatLng(data.model.getLatitude(), data.model.getLongitude());
                markerModel.title = data.model.getNickName();
                markers.data[markerModel.id]=new google.maps.Marker(markerModel);
                markers.cluster.addMarker(markers.data[markerModel.id]);
                
                google.maps.event.addListener(markers.data[data.id], 'dragend', function(evt){
                	markers.data[data.id].setTitle(markers.data[data.id].nickName);
                	$(document).trigger("markerUpdateEvent", {id:data.id, lat:evt.latLng.lat().toFixed(5), lng:evt.latLng.lng().toFixed(5) });
                });

                google.maps.event.addListener(markers.data[data.id], 'drag', function(evt){
                    self.infoBox.setOptions({position:new google.maps.LatLng(evt.latLng.lat(), evt.latLng.lng()+0.1), content:"lat: "+evt.latLng.lat().toFixed(4)+" / lng: "+evt.latLng.lng().toFixed(4)});
                    self.infoBox.open(this.map);
                    self.infoBox.zIndex_ = 0;
                    self.infoBox.type = "markerInfo";
                    
                });
                break;
            }

            case "boundary" : {
                markerModel.id = "boundaryMarker_"+(++model.getBoundaryMarkers().count);
                markerModel.position = data.latLng;
                markers.data[markerModel.id]=new google.maps.Marker(markerModel);
                model.getBoundaryMarkers().undoRedo = model.getBoundaryMarkers().count;
                markers.data[markerModel.id].setMap(map);
                break;
            }
            
            case "currentAgentLocation" : {
            	markerModel.id = "currentLocationMarker";
                markerModel.position = new google.maps.LatLng(data.latLng.latitude, data.latLng.longitude);
                
            	if (typeof markers.data[markerModel.id] == 'undefined') {
	            	//markerModel.position = data.latLng;
	            	markers.data[markerModel.id] = new google.maps.Marker(markerModel);
	            	
	            	markers.data[markerModel.id].setMap(map)
            	} else {
            		
            		markers.data[markerModel.id].setPosition(markerModel.position);
            		
            	}
                break;
            }
            
            case "territorySoundAction" : {
                markerModel.id = data.id;
                markerModel.actionId = data.actionContent;
                markerModel.title = data.actionContent.split('.')[0];
                markerModel.position = new google.maps.LatLng(data.latLng.latitude, data.latLng.longitude);
               	markers.data[markerModel.id]=new google.maps.Marker(markerModel);
            	markers.data[markerModel.id].setOptions({'opacity': 0.5});
                markers.cluster.addMarker(markers.data[markerModel.id]);
                markerModelClone = $.extend({}, markerModel);
                
                google.maps.event.addListener(markers.data[markerModel.id], 'click', function(evt){
                	$(document).trigger("soundActionClicked", {actionId:markerModelClone.actionId});
                });
                
                google.maps.event.addListener(markers.data[markerModel.id], 'mouseover', function(evt){
                	markers.data[markerModelClone.id].setIcon(self.mouseoverSoundActionMarker);
                	markers.data[markerModelClone.id].setOptions({'opacity': 1})
                });
                
                google.maps.event.addListener(markers.data[markerModel.id], 'mouseout', function(evt){
                	markers.data[markerModelClone.id].setIcon(self.soundActionMarker)
                	markers.data[markerModelClone.id].setOptions({'opacity': 0.5})
                });
                
                break;
            }
        }

        $(document).trigger("markerCreated",  markers.data[markerModel.id]);
    };
    
    /**
     * update marker
     * @param data
     */
    MarkerPresenter.prototype.updateMarker = function(data){
    	var model = this.getModel();
        var markerModel = model["get"+data.type.ucfirst()+"Marker"]();
        var markers = model["get"+data.type.ucfirst()+"Markers"]();
		
        switch (data.type) {
			case "player" :	
			{
				markerModel = markers.data[data.id];
				markerModel.setPosition(new google.maps.LatLng(data.model.getLatitude(), data.model.getLongitude()));
	            break;
	       }
		}
    };
    
    /**
     * delete marker
     * @param data
     */
    MarkerPresenter.prototype.deleteMarker = function(data){
        var markers = null;

        switch (data.type){
        	case "player" : {
                markers = this.getModel().getPlayerMarkers();
                markers.cluster.removeMarker(markers.data[data.id]);
                delete markers.data[data.id];
                break;
            }
            case "boundary" : {
                markers = this.getModel().getBoundaryMarkers();
                markers.data[data.id].setMap(null);
                delete markers.data[data.id];
                break;
            }
            
            case "currentAgentLocation" : {
            	markers = this.getModel().getCurrentAgentLocationMarkers();
            	markers.data[data.id].setMap(null);
            	delete markers.data[data.id];
            	break;
            }
            
            case "territorySoundAction" : {
                markers = this.getModel().getTerritorySoundActionMarkers();
                markers.cluster.removeMarker(markers.data[data.id]);
                delete markers.data[data.id];
                break;
            }
        }
    };

    /**
     * delete markers
     * @param type
     */
    MarkerPresenter.prototype.deleteMarkers = function(type) {
        var markers = null;

        switch (type){
            case "player" : {
                break;
            }
            case "boundary" : {
                markers = this.getModel().getBoundaryMarkers();
                if (markers.count > 0) {
                    $.each(markers.data, function () {
                        this.setMap(null);
                    });
                    $(document).trigger("markersDeleted", markers.data);
                    markers.data = {};
                    markers.count = 0;
                    markers.undoRedo = 0;
                }
                break;
            }
        }
    };

    /**
     * undo marker
     * @param data
     */
    MarkerPresenter.prototype.undoMarker = function(data){
        var markers = null;

        switch (data){
            case "boundary" : {
                markers = this.getModel().getBoundaryMarkers();
                if (markers.undoRedo > 0) {
                    var id = "boundaryMarker_"+(markers.undoRedo);
                    --markers.undoRedo;
                    markers.data[id].setMap(null);
                    $(document).trigger("markerDeleted",markers.data[id]);
                    break;
                }
            }
        }
    };

    /**
     * redo marker
     * @param data
     */
    MarkerPresenter.prototype.redoMarker = function(data){
        var markers = null;

        switch (data){
            case "boundary" : {
                markers = this.getModel().getBoundaryMarkers();
                if (markers.undoRedo < markers.count) {
                    var id = "boundaryMarker_" + (++markers.undoRedo);
                    markers.data[id].setMap(map);
                    $(document).trigger("markerCreated", markers.data[id]);
                    break;
                }
            }
        }
    };

    /**
     * hide markers
     * @param data
     */
    MarkerPresenter.prototype.hideMarkers = function(data){
        var model = this.getModel();
        
        switch (data) {
            case "player" : {
                model.getPlayerMarkers().cluster.clearMarkers();
                break;
            }
            case "boundary" : {
                $(document).trigger("markersHide", markers);
                break;
            }
        }
    };

    /**
     * show markers
     * @param data
     */
    MarkerPresenter.prototype.showMarkers = function(data){
        var model = this.getModel();

        switch (data) {
            case "player" : {
                var markers = model.getPlayerMarkers();
                $.each(markers.data, function() {
                   markers.cluster.addMarker(this);
                });

                break;
            }
            case "boundary" : {
                if (!$.isEmptyObject(markers)) {
                    $(document).trigger("markersShow", markers);
                }
                break;
            }

        }
    };

    return MarkerPresenter;
})();