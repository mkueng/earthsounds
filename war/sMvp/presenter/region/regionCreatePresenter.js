/**
 * RegionCreatePresenter
 * (c) kuema 2017
 * 
 * @binding createRegion_control_event_propagate
 * @binding polygonMouseOverNewRegionEvent
 * @binding polygonMouseOutNewRegionEvent
 * @binding polygonClickNewRegionEvent
 * @binding mapClicked
 * 
 * @trigger createMarker
 * @trigger deleteMarkers
 * @trigger clearPath
 * @trigger undoPath
 * @trigger redoPath
 * @trigger cancelNewRegion
 * 
 */
var RegionCreatePresenter = (function(){

    RegionCreatePresenter.prototype = new SMVP.Presenter(rodrigez.regionCreateViewModel);

    var spacing = null;
    var territoryInfoPresenter = null;
    var territoryEditPresenter = null;
    var outlinePolygon = null;
    
    namespace.territories.setNewRegion(new SMVP.Collection('territory'));

    /**
     * @constructor
     */
    function RegionCreatePresenter(){
        var self = this;
        this.mandatory = [];
        territoryInfoPresenter = new TerritoryInfoPresenter();
        territoryEditPresenter = new TerritoryEditPresenter();

        // >>> application events

            /**
            * bind createRegion_control_event_propagate
            */
            $(document).bind("createRegion_control_event_propagate", function(){
            	var newModel = {};
            	$.each(namespace.domainModel.region, function(key,val){
            		newModel[key] = val.value;
            		if (this.mandatory === true) {
            			self.mandatory.push(key);
            		}
            	});
            	
                self.getModel().setData(new SMVP.Model(newModel));
                
                /**
                 * render view
                 */
                self.renderView(self, function(){
                	$('#gridOn').addClass('active');
                	$('#submitNewRegion').addClass('error');
                	$('#newRegionForm :input').each(function() {
               		 if ($(this).is('select') !== true) $(this).addClass('missingValue');
        	        });
                	
                	self.showView();
                	
                	$('#newRegionForm :input').keyup(function(event) {
                    	validateFormInput(this);
                    });
                    
                    $('#newRegionForm :input').change(function(event) {
                    	validateFormInput(this);
                    });
                });

                
                /**
                 * validate form input
                 * @param event
                 */
                var validateFormInput = function(event){
                	var changeStack = {};
                    var currentVal = $(event).val();
                    if(Utilities.isValid(event) === true) {
                        $(event).removeClass('missingValue');
                        $('#submitNewRegion').removeClass('error');
                    } else if (Utilities.isValid(event) === false){
                        $(event).addClass('missingValue');
                        $('#submitNewRegion').addClass('error');
                    }
                    currentVal = !isNaN(parseFloat(currentVal)) ? parseFloat(currentVal) : currentVal;
                    self.getModel().getData()["set"+event.id.split('_')[1].ucfirst()](currentVal,"noTrigger");
                };
               
                /**
                 * bind polygonMouseOverNewRegionEvent
                 */
                $(document).bind("polygonMouseOverNewRegionEvent", function(o,polygon){
                    territoryInfoPresenter.renderView(namespace.territories.getNewRegion().getCollection()[polygon.id].geoPoints[0], namespace.territories.getNewRegion().getCollection()[polygon.id]);
                });

                /**
                 * bind polygonMouseOutNewRegionEvent
                 */
                $(document).bind("polygonMouseOutNewRegionEvent", function(o,polygon){
                    territoryInfoPresenter.destroyView();
                });

                /**
                 * bind polygonClickNewRegionEvent
                 */
                $(document).bind("polygonClickNewRegionEvent", function(o,polygon){
                    handlePolygonClickEvent(polygon);
                });

                /**
                 * bind mapClicked
                 */
                $(document).bind("mapClicked", function(o,event){
                    handleMapClickEvent(event);
                });

            });

        // private functions

        /**
         * @hint handle map click event
         * @param event
         * @return void
         */
        var handleMapClickEvent = function(event){
        	//if grid mode active create marker
            if ($('#gridOn').hasClass('active')) {
                $(document).trigger("createMarker", {type: 'boundary', latLng: event.latLng});
            } else
            //create single territory
            if (!$('#removeTerritoryOn').hasClass('active')) {
                spacing = spacing === null ? Number($('input[name=radio-choice-mainGrid]:checked', '#newRegionForm').val()) : spacing;
                var newTerritory = rodrigez.territoryPresenter.createTerritory("", event.latLng, spacing);
                namespace.polygonPresenter.renderPolygon(namespace.polygonPresenter.createPolygon(newTerritory));
                territoryEditPresenter.destroyView();
            }
        };

        /**
         * @hint handle polygon click event
         * @param polygon
         * @return void
         */
        var handlePolygonClickEvent = function(polygon){
        	//delete territory
            if ($('#removeTerritoryOn').hasClass('active')) {
                namespace.polygonPresenter.removePolygon(polygon.id,"newRegion");
                delete namespace.territories.getNewRegion().getCollection()[polygon.id];
                namespace.territories.getNewRegion().removeModel[polygon.id];
               
                territoryInfoPresenter.destroyView();
                territoryEditPresenter.destroyView();
            //invoke territory edit
            } else {
                territoryEditPresenter.renderView(namespace.territories.getNewRegion().getModels()[polygon.id].getGeoPoints()[0], namespace.territories.getNewRegion().getCollection()[polygon.id]);
            }
        };
    }

    // public API
    
    /**
     * @hint createRegion event
     * @param event
     * @return void
     */
    RegionCreatePresenter.prototype.regionCreateView_event = function(event){
        $('[id^=newRegion_]').removeClass('missingValue');
        //try {
        	this["" + event.element.target.id](event); //invoke method according to event
        //} catch(e){console.log(e)}
    };

    /**
     * @hint grid button on / off
     * @param event
     * @return void
     */
    RegionCreatePresenter.prototype.gridOn = function(event){
        $('#gridOn').toggleClass('active');
    };

    /**
     * @hint removeTerritory button on / off
     * @param event
     * @return void
     */
    RegionCreatePresenter.prototype.removeTerritoryOn = function(event){
        $('#removeTerritoryOn').toggleClass('active');
    };
    
    /**
     * @hint render path 
     * @return void
     */
    RegionCreatePresenter.prototype.renderPath = function(event){
    	spacing = spacing === null ? Number($('input[name=radio-choice-mainGrid]:checked', '#newRegionForm').val()) : spacing;
    	var polygons = null;
        var outline = namespace.polylinePresenter.getPath("boundary");
        var newTerritories = namespace.territoryPresenter.calculateTerritoryGrid("", spacing, outline);
        
        $.each(newTerritories, function(){
        	var territoryObject = {};
            
            $.each(this.getObjectRepresentation(), function(key, value){
            	territoryObject[key]= value;
            });
            var territoryDomainModel = new  SMVP.Model(territoryObject);
            namespace.territories.getNewRegion().addModel(territoryDomainModel);
        });
    
        namespace.polygonPresenter.removePolygons("newRegion");
        polygons = namespace.polygonPresenter.createPolygonsFromTerritories(namespace.territories.getNewRegion().getCollection(), "newRegion");
        namespace.polygonPresenter.renderPolygons(polygons);
        $(document).trigger("deleteMarkers", "boundary");
    };
    
    /**
     * @hint clear path
     * @return void
     */
    RegionCreatePresenter.prototype.clearPath = function(event){
        $(document).trigger("clearPath", "boundary");
    };
    
    /**
     * @hint undo path
     * @param event
     * @return void
     */
    RegionCreatePresenter.prototype.undoPath = function(event){
        $(document).trigger("undoPath","boundary");
    };

    /**
     * @hint redo Path
     * @param event
     * @return void
     */
    RegionCreatePresenter.prototype.redoPath = function(event){
        $(document).trigger("redoPath","boundary");
    };
    
    /**
     * @hint submit new Region
     * @return void
     */
    RegionCreatePresenter.prototype.submitNewRegion = function() {
        if (!$('#submitNewRegion').hasClass('error')) {
        	
	        var self = this;
	        var outline = [];
	        var outlineConvert = [];
	        var geopoints = [];
	        var regionName = null;
	        
	        // remove borderline
	        if (outlinePolygon !== null) {
	        	rodrigez.polygonPresenter.remove(outlinePolygon);
	        }
	       
	        // create outline
	        outlinePolygon = rodrigez.polygonPresenter.createPolygonFromGeoPoints(this.createConvexHull());
	        $.each(outlinePolygon.getPath().getArray(), function(){
	        	outline.push({lat:this.lat(),lng:this.lng()});
	        	outlineConvert.push({latitude:this.lat(),longitude:this.lng()});
	        });
	        // render outline
	        rodrigez.polygonPresenter.renderPolygon(outlinePolygon);
	        // render infoBox 
	        namespace.infoBoxPresenter.renderInfobox(
					"regionLabelInfoBox", new google.maps.LatLng (outline[0]),
					'<h2>' +self.getModel().getData().getName()+'</h2>'
			)
	        namespace.polygonPresenter.removePolygons('newRegion');
	        namespace.regionPresenter.addOutline( self.getModel().getData().getName(), outlinePolygon);
	        
	        //update model data
	        self.getModel().getData().setGeoPoints(outline);
	        regionName = self.getModel().getData().getName();
	        
	        self.getModel().getData().post(function(response){
	        	namespace.regionCollection.getCollection()[self.getModel().getData().getId()].geoPoints= outlineConvert;
	        	if (response != false) {
	        		$.each(namespace.territories.getNewRegion().getCollection(), function(key, value){
	    	        	this.region=regionName;
	    	        });
	        		
	        		namespace.territories.getNewRegion().post(function(model,response){
	        			outlinePolygon = null;
		       	        spacing = null;
		       	       
		       	        self.hideView();
		       	        self.destroyView();
		       	        namespace.territories.setNewRegion(new SMVP.Collection('territory'));
	        		});
	        	}
	        });
	    }
    };
    
    /**
     * @hint create convex hull
     * @returns convexHullPolygonPoints
     */
    RegionCreatePresenter.prototype.createConvexHull = function(){
    	   var geopoints = [];
    	   var convexHullPolygonPoints = [];
    	   var convexHullPoints = null;
    	   
    	   $.each(namespace.territories.getNewRegion().getModels(), function(){
	        	$.each(this.getGeoPoints(), function(){
	        		geopoints.push([this.lat(), this.lng()]);
	        	});
	        });
    	   
    	   convexHullPoints = rodrigez.territoryPresenter.getConvexHull(geopoints);
	        
    	   $.each(convexHullPoints, function(){
	        	$.each(this, function(){
	        		convexHullPolygonPoints.push(new google.maps.LatLng(this[0], this[1]));
	            });
	        });
    	   
	        return convexHullPolygonPoints;
    	
    }
    /**
     * @hint cancel new Region
     */
    RegionCreatePresenter.prototype.cancelNewRegion = function() {
        namespace.territories.setNewRegion(new SMVP.Collection('territory'));
        spacing = null;
        $(document).trigger("clearPath", "boundary");
        $(document).trigger("cancelNewRegion");
        $(document).trigger("deleteMarkers","boundary");
        $(document).off("mapClicked");
        territoryInfoPresenter.destroyView();
        territoryEditPresenter.destroyView();
        this.hideView();
        this.destroyView();
    };
    
    // <<<

    return RegionCreatePresenter;
})();