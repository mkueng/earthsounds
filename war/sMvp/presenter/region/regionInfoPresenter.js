/**
 * RegionInfoPresenter
 * (c) kuema 2017
 * 
 * @binding regionFilterFocus
 * @binding region_edit_event_propagate
 * @binding region_info_event_propagate
 */
var RegionInfoPresenter = (function(){

    RegionInfoPresenter.prototype = new SMVP.Presenter(namespace.regionInfoViewModel);

    
    /**
     * @constructor
     */
    function RegionInfoPresenter(){
    	var self=this;
    	var viewOffset = 28;

        this.numberOfTerritoriesModel = new SMVP.Model(namespace.domainModel.regionTerritoryNumber);
    	/**
    	 * bind regionFilterFocus
    	 */
    	$(document).bind("regionFilterFocus", function(o,data){

            $('[id^=inforegion_]').removeClass('active');
    		self.destroyView();
    	});

    	/**
    	 * @bind region_edit_event_propagate
    	 */
        $(document).bind("region_edit_event_propagate", function(o,data){
            self.destroyView();
        });


        /**
         * @bind region_info_event_propagate
         */
    	$(document).bind("region_info_event_propagate", function(o,data){
            $('[id^=info_]').removeClass('active');
            $('#'+data.event.element.target.id).addClass('active');
            data.model.fetch(function(response){
            	self.getModel().setData(data.model);
            	self.destroyView();
            	self.getView().setContainer("regionInfoContainer_"+data.model.getId());
            	self.renderView(self);
            	self.animateView(data.event, $('#regionListContainer'), viewOffset);
            });
            // fetch number of territories
            self.numberOfTerritoriesModel.setEndpoint("?region="+data.model.getName()+"&number");
            self.numberOfTerritoriesModel.fetch(function(){
            	$('#numberofter').text(self.numberOfTerritoriesModel.getNumber());
            });
    	});
    }

    return RegionInfoPresenter;
})();