/**
 * RegionEditPresenter
 * (c) kuema 2017
 * 
 * @binding regionFilterFocus
 * @binding region_info_event_propagate
 * @binding region_edit_event_propagate
 * 
 * @trigger cancelEditExistingRegion
 */
var RegionEditPresenter = (function(){

    RegionEditPresenter.prototype = new SMVP.Presenter(namespace.regionEditViewModel);

    /**
     * @constructor
     */
    function RegionEditPresenter(){
        var self=this;
        var viewOffset = 28;
        var _regionId = null;
        var spacing = null;

        /**
         * get _regionId
         * @returns _regionId
         */
        this.getRegionId = function(){
            return _regionId;
        };
        
        /**
         * set _regionId
         * @param regionId
         */
        this.setRegionId = function(regionId){
            _regionId=regionId;
        };

        /**
         * bind regionFilterFocus
         */
        $(document).bind("regionFilterFocus", function(o,event){
			$('[id^=edit]').removeClass('active');
			$(document).off("mapClicked");
			$(document).trigger("cancelEditExistingRegion", self.getModel().getData());
    		self.destroyView();
    	});

        /**
         * bind region_info_event_propagate
         */
        $(document).bind("region_info_event_propagate", function(o,data){
        	$(document).off("mapClicked");
        	$(document).trigger("cancelEditExistingRegion", self.getModel().getData());
            self.destroyView();
        });

        /**
         * bind region_edit_event_propagate
         */
        $(document).bind("region_edit_event_propagate", function(o,data){
            
        	var id= data.event.element.target.id;
            var regionId = id.split('_')[1];

            /**
             * bind mapClicked
             */
             $(document).bind("mapClicked", function(o,event){
             	handleMapClickEvent(event,self);
             });
            
            self.setRegionId(regionId);
            $('[id^=edit_]').removeClass('active');
            $('#'+id).addClass('active');

            data.model.fetch(function(response){
            	self.getModel().setData(response);
            	self.destroyView();
     	    	self.getView().setContainer("regionEditContainer_"+data.model.getId());
     	    	self.renderView(self,function(){
     	    		self.animateView(data.event, $('#regionListContainer'), viewOffset);
     	    		validateFormInput();
     	    	});
            });
        });
        
        /**
         * @hint handle map click event
         * @param event
         * @return void
         */
        var handleMapClickEvent = function(event, self){
        	var data = self.getModel().getData();
            var newTerritory = rodrigez.territoryPresenter.createTerritory(data.getName(), event.latLng, data.getMainGrid());
            newTerritory.root = 'territory';
            var territoryCollection = new SMVP.Collection('territory').addModel(new SMVP.Model(newTerritory)).post(function(response){
                namespace.polygonPresenter.renderPolygons(namespace.polygonPresenter.createPolygonsFromTerritories(territoryCollection.getCollection(),"existingRegion",data.getName()));
            });
        };

        /**
         * validate form input
         */
        var validateFormInput = function(){
            var changeStack = {};
            $('#editRegionForm :input').keyup(function(event) {
                var currentVal = $(this).val();
                var actualVal = self.getModel().getData()["get" + event.target.id.split('_')[1].ucfirst()]();

                if (currentVal !== "" && currentVal != actualVal) {
                    changeStack[this.id] = 'changed';
                    $(this).removeClass('missingValue');
                    $('#submitEditRegion').removeClass('error');
                } else if (currentVal == actualVal) {
                    delete changeStack[this.id];
                    $('#submitEditRegion').removeClass('error');
                } else if (currentVal === "") {
                    $(this).addClass('missingValue');
                    $('#submitEditRegion').addClass('error');
                    delete changeStack[this.id];
                }

                if (!$.isEmptyObject(changeStack) && !$('#submitEditRegion').hasClass('error')) {
                    $('#submitEditRegion').addClass('changed');
                } else {
                    $('#submitEditRegion').removeClass('changed');
                }
            });

            $('#editRegionForm :input').change(function(event) {
                var currentVal = $(this).val();
                self.getModel().getData()["set"+event.target.id.split('_')[1].ucfirst()](currentVal,"noTrigger");
            });
        };
    }

    // public API
    
    /**
     * region edit view event
     * @param event
     */
    RegionEditPresenter.prototype.regionEditView_event = function(event){
    	event.element.stopPropagation();
        //try {
            this["" + event.element.target.id](event);
        //} catch(e){}
    };


    /**
     * submit edit region
     */
    RegionEditPresenter.prototype.submitEditRegion = function(){
        if ( $('#submitEditRegion').hasClass('changed')) {
        	var model = this.getModel().getData().update(function(model){
        		$(document).off("mapClicked");
        		self.destroyView();
           });
        }
    };

    /**
     * cancel edit region
     */
    RegionEditPresenter.prototype.cancelEditRegion = function(){
    	$('[id^=edit]').removeClass('active');
        $('#action_'+this.getRegionId()).removeClass('changed');
    	$(document).trigger("cancelEditExistingRegion", this.getModel().getData());
        $(document).off("mapClicked");
        this.destroyView();
    };
    
    return RegionEditPresenter;
})();