/**
 * RegionPresenter
 * (c) kuema 2017
 * 
 * @binding collectionChanged_region
 * @binding regionPolygonClicked
 * 
 * @trigger activeRegionAdded
 * @trigger activeRegionRemoved
 * @trigger regionFilterFocus
 * @trigger activeRegionsChanged
 * @trigger createRegion_control_event_propagate
 * 
 */
var RegionPresenter = (function(){

    RegionPresenter.prototype = new SMVP.Presenter(namespace.allAboutRegionViewModel);
    
    var activeShownRegions = {};
    var regionOutlines = {};
    var infoBoxes = {};
    var infoBoxClass = "regionMapInfoBox";
    var checkBox = null;
    
    var addActiveShownRegion = function(region){
    	$(document).trigger("activeRegionAdded", region.name);
    	activeShownRegions[region.id] = region;
    };
    
    var removeActiveShownRegion = function(regionId){
    	$(document).trigger("activeRegionRemoved", activeShownRegions[regionId].name);
    	delete activeShownRegions[regionId];
    };
    
	/**
	 * @constructor
	 */
    function RegionPresenter(){
        var self=this;

        this.getActiveShownRegions = function(){
        	return activeShownRegions;
        };
        
        this.getInfoBoxes = function(){
        	return infoBoxes;
        }
        
        /**
         * add outline
         * @param region
         * @param outline
         */
        this.addOutline = function(region, outline){
        	regionOutlines[region]=outline;
        }
        
        /**
         * bind region collection change event
         * 
         */
        
		$(document).bind("collectionChanged_region", function(){
			self.destroyView();
        	self.renderView(self,function(){
                self.setBindings();
                //self.showOutlines();
           });
        });

		/**
		 * bind standard dialog button event
		 */
        $('#standardDialogButtonYes').bind('click',function(event){
            if ($(this).attr('modelType') == 'region') {
            	self[$(this).attr('modelAction')+'region'](  $(this).attr('modelId'));
            }
        });

       
        /**
         * set bindings
         */
        this.setBindings = function(){
            var showHide = $('#showHideRegion');
            var regionFilter =  $('#regionFilter');

            $(showHide).off("click");
            $(regionFilter).off("click");

            $(showHide).bind('change',function(event){
            	$("#regionListContainer").toggle();
                $("#regionOptionsContainer").toggle();
                $("#regionCreateContainer").hide();
            	$("#allAboutRegionContainer").toggleClass("allAboutRegionContainer");
                
            });
            
            $(regionFilter).on("focus", function(){
                $(document).trigger("regionFilterFocus");
            });
            
            $(document).bind('regionPolygonClicked', function(o,polygon){
            	// toggle regiom shown checkbox
            	checkBox = $('#checkbox_'+polygon.id);
            	checkBox.prop('checked', !checkBox.prop('checked')).checkboxradio('refresh');
            	
            	//infoBoxes[polygon.region].setOptions({boxClass : "regionMapInfoBox"+$(checkBox).prop('checked')});
            })
        };

        /**
         * render view
         */ 
        self.renderView(self,function(){
            self.setBindings();
            self.showOutlines();
            if (namespace.clearance != "administrator") {
            	$.each(namespace.regionCollection.getCollection(), function(){
            		addActiveShownRegion(this);
            	});
            	$(document).trigger("activeRegionsChanged", activeShownRegions);
            }
            $('#showHideRegion').change();
        });

    }

   // public API

   /**
     * region control (create)
     * @param event
     */
    RegionPresenter.prototype.regionControlView_event = function(event){
        $(document).trigger("createRegion_control_event_propagate", event);
    };

    /**
     * all about region (info, edit)
     * @param event
     */
    RegionPresenter.prototype.allAboutRegionView_event = function(event){
        if (event.element.target.firstChild === null) {
            $('[id^=editregion_]').removeClass('active');
            $('[id^=inforegion_]').removeClass('active');
            event.element.preventDefault();
            //invoke method according to event
            this[""+event.element.target.id.split("_")[0]](event);
             
        } else {
       		if (typeof event.element.target.firstChild.id != 'undefined' && event.element.target.firstChild.id.split('_')[0] == 'actionToggleRegion'){
       			event.element.preventDefault();
        	} else if (event.element.target.id.split('_')[1] !== undefined) {
        		event.element.preventDefault();
            	$('#'+event.element.target.id).parent().toggleClass('actionToggle');
            	this.toggleActiveRegion(event);
        	}
        }
    };
    
    /**
     * toggle region view
     * @param event
     */
	RegionPresenter.prototype.actionToggleRegion = function(event){
		
		$('#'+event.element.target.id).parent().toggleClass('ui-icon-carat-r ui-icon-carat-d');
		$('#actionToggleButton_'+event.element.target.id.split('_')[1]).toggle();
		$(document).trigger("regionFilterFocus");
	};


	/**
	 * show region outline
	 * @param region
	 */
	RegionPresenter.prototype.showOutline = function(region){
		namespace.polygonPresenter.renderPolygon(regionOutlines[region]);
	};
	
	/**
	 * show region outlines
	 * @param region
	 */
	RegionPresenter.prototype.showOutlines = function(region){
		$.each(namespace.regionCollection.getCollection(), function(){
			var mapsGeoPoints = [];
			if (this.id != 'template'){
				$.each(this.geoPoints, function(){
					mapsGeoPoints.push({lat:this.latitude, lng:this.longitude});
				});
				
				var regionOutline = namespace.polygonPresenter.createPolygonFromGeoPoints(mapsGeoPoints,this);
				regionOutlines[this.name] = regionOutline;
				namespace.polygonPresenter.renderPolygon(regionOutline);
			}
			
			infoBoxes[this.name] = namespace.infoBoxPresenter.renderInfobox(
					"regionLabelInfoBox", new google.maps.LatLng (mapsGeoPoints[0]),
					'<div class="regionMapLabel">' +this.name+'</div>'
			)
		});
	};
	
	/**
	 * remove region outlines
	 * @param region
	 */
	RegionPresenter.prototype.removeOutlines = function(region){
		namespace.polygonPresenter.remove(regionOutlines[region]);
	};
	
	/**
	 * toggle active region
	 * @param event
	 */
	RegionPresenter.prototype.toggleActiveRegion = function(event){
		var regionId =  event.element.target.id.split("_")[1];
		var regionContent = namespace.regionCollection.getCollection()[regionId];
		
		regionId in activeShownRegions
			? removeActiveShownRegion(regionId)
			: addActiveShownRegion(regionContent);
		
		namespace.mapPresenter.setMapCenterPosition(regionContent.geoPoints[0].latitude,regionContent.geoPoints[0].longitude);
		$(document).trigger("activeRegionsChanged", activeShownRegions);

	};
    /**
     * region info show
     * @param event
     */
    RegionPresenter.prototype.inforegion = function(event){
        this.triggerPropagate(event, "info");
    };

    /**
     * region edit show
     * @param event
     */
    RegionPresenter.prototype.editregion = function(event){
        this.triggerPropagate(event, "edit");
    };

    /**
     * region delete request
     * @param event
     */
    RegionPresenter.prototype.minusregion = function(event){
		event.element.stopPropagation();
        var id = event.element.target.id.split("_")[1];
        var name = this.getModel().getData().readModel(id).getName();
        var dialogButton =  $('#standardDialogButtonYes');
        $('#standardDialogContentText').html('sure to delete region: '+name+'?');

        $(dialogButton).attr('modelId', id);
        $(dialogButton).attr('modelType', "region");
        $(dialogButton).attr('modelAction', "delete");
        $('#standardDialogLink').click();
    };

    /**
     * region delete
     * @param modelId
     */
    RegionPresenter.prototype.deleteregion = function(modelId){
       var model = this.getModel().getData().readModel(modelId);
       var regionName = model.getName();
       var deleteTerritoriesModel = new SMVP.Model(namespace.domainModel.territoryDelete);
       deleteTerritoriesModel.setEndpoint("?region="+model.getName());
       model.destroy();
       deleteTerritoriesModel.destroy();
       this.removeOutlines(regionName);
       $(document).trigger("regionDeleted", regionName);
       delete regionOutlines[regionName];
       infoBoxes[regionName].close();
    };

 
    /**
     * trigger propagate
     * @param event
     * @param type
     */
    RegionPresenter.prototype.triggerPropagate = function(event,type){
        var id = event.element.target.id.split("_")[1];
        event.element.stopPropagation();
        $(document).trigger("region_"+type+"_event_propagate",{
            event:event,
            model:this.getModel().getData().readModel(id)
        });
    };

    return RegionPresenter;
})();