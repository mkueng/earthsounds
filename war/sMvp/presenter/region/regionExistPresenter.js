/**
 * RegionExistPresenter
 * (c) kuema 2017
 * 
 * @binding activeRegionsChanged
 * @binding activeRegionRemoved
 * @binding activeRegionAdded
 * @binding mapZoomChanged
 * @binding mapDragend
 * @binding territoryEditBoxClose
 * @binding polygonClickExistingRegionEvent
 * @binding polygonMouseOverExistingRegionEvent
 * @binding polygonMouseOutExistingRegionEvent
 * @binding cancelEditExistingRegion
 * 
 * @trigger outlineShown
 */

var RegionExistPresenter = (function(){

    RegionExistPresenter.prototype = new SMVP.Presenter(namespace.territoryShowModel);

    var _activeRegions = [];
    var _distance = null;
    
    /**
     * get _activeRegions
     * @returns _activeRegions
     */
    var getActiveRegions = function(){
    	return _activeRegions;
    };
    
    
    /**
     * @constructor
     */
    function RegionExistPresenter(){
    	var self = this;
    	this.polygons = null;
    	this.dataModel = new SMVP.Model(namespace.domainModel.territoryShow);
    	
        this.map = namespace.mapPresenter.getMap();
        
       
        
        /**
         * bind activeRegionsChanged
         */
        $(document).bind("activeRegionsChanged", function(o,data){
        	_activeRegions = [];
        	$.each(data, function(key, val){
        		_activeRegions.push(val.name);
        	});
        });
        
        /**
         * bind activeRegionRemoved
         */
        $(document).bind("activeRegionRemoved", function(o,name){
        	self.removeTerritories(null,name);
        	self.showOutline(name);
        });
        
        /**
         * bind regionDeleted
         */
        $(document).bind("regionDeleted", function(o,name){
        	var index = _activeRegions.indexOf("name");
        	_activeRegions.splice(index,1);
        });

        /**
         * bind activeRegionAdded
         */
        $(document).bind("activeRegionAdded", function(o,name){
        });
        
        /**
         * bind mapZoomChanged
         */
        $(document).bind("mapZoomChanged", function(o,data){
        	mapPropertyChangedPropagate(data);
        });
        
        /**
         * bind mapDragend
         */
        $(document).bind("mapDragend", function(o,data){
        	mapPropertyChangedPropagate(data);
        });
        
        /**
         * bind territoryEditBoxClose
         */
        $(document).bind("territoryEditBoxClose", function(o,data){      	
        });
        
        
        /**
         * bind polygonClickExistingRegionEvent
         */
         $(document).bind("polygonClickExistingRegionEvent", function(o,polygon){
        	handlePolygonClickEvent(polygon);
         });
     

         /**
         * bind polygonMouseOverExistingRegionEvent
         */
         $(document).bind("polygonMouseOverExistingRegionEvent", function(o,polygon){
            var geoPoints = rodrigez.territories.getExistingRegions()[polygon.region][polygon.id].geoPoints[0];
            namespace.territoryInfoPresenter.renderView(geoPoints, rodrigez.territories.getExistingRegions()[polygon.region][polygon.id]);
         });

	      /**
	      * bind polygonMouseOutExistingRegionEvent
	      */
	      $(document).bind("polygonMouseOutExistingRegionEvent", function(o,polygon){
	        namespace.territoryInfoPresenter.destroyView();
	      });
	      
	      
	      /**
	       * bind region_edit_event_propagate
	       */
	      $(document).bind("region_edit_event_propagate", function(o,data){
	    	  self.removeOutline(data.model.getName());
	    	  self.fetchAllTerritories(data.model.getName());
	      });
	      
	      /**
           * bind mapClicked
           */
          $(document).bind("mapClicked", function(o,event){
        	 if ( $("[id^=editregion]").hasClass('active')) {
        		  handleMapClickEvent(event); 
        	  };
          });
          
          /**
           * bind cancelEditExistingRegion
           */
          $(document).bind("cancelEditExistingRegion", function(o,regionModel){
        	  if (typeof regionModel !== 'undefined') {
        		  self.removeTerritories(regionModel, regionModel.getName());
        		  self.showOutline(regionModel.getName())
        	  }
          })
          
          /**
           * @hint handle map click event
           * @param event
           * @return void
           */
          var handleMapClickEvent = function(event){
          	//if grid mode active create marker
              if ($('#gridOn').hasClass('active')) {
                  $(document).trigger("createMarker", {type: 'boundary', latLng: event.latLng});
              } else
              //create single territory
              if (!$('#removeTerritoryOn').hasClass('active')) {
                  spacing = spacing === null ? Number($('input[name=radio-choice-mainGrid]:checked', '#newRegionForm').val()) : spacing;
                  var newTerritory = rodrigez.territoryPresenter.createTerritory("", event.latLng, spacing);
                  namespace.polygonPresenter.renderPolygon(namespace.polygonPresenter.createPolygon(newTerritory));
                  territoryEditPresenter.destroyView();
              }
          };
          
        /**
         * mapPropertyChangedPropagate
         * @param data
         */
        var mapPropertyChangedPropagate = function(data){
        
        	if (data.zoomLevel > 11) {
                $.each(_activeRegions, function(){
                	self.removeOutline(this);
                    self.fetchTerritories(data, this);
                });
        	} else {
        		$.each(_activeRegions, function(){
        			self.removeTerritories(data, this);
        			self.showOutline(this);
        		});
        	}
        };
        
        /**
         * @hint handle polygon click event
         * @param polygon
         * @return void
         */
        var handlePolygonClickEvent = function(polygon){
        	
        	//delete territory
            if ($('#removeTerritoryOn').hasClass('active')) {
                namespace.polygonPresenter.removePolygon(polygon.id,"existingRegion");
                delete rodrigez.territories.getExistingRegion()[polygon.id];
                territories.removeModel[polygon.id];
               
                territoryInfoPresenter.destroyView();
                territoryEditPresenter.destroyView();
            //invoke territory edit
            } else {
            	var geoPoints = rodrigez.territories.getExistingRegions()[polygon.region][polygon.id].geoPoints[0];
                namespace.territoryEditPresenter.renderView(geoPoints, rodrigez.territories.getExistingRegions()[polygon.region][polygon.id]);
            }
        };
    }
    
    // public API
    
    /**
     * fetch all territories
     * @param region
     */
    RegionExistPresenter.prototype.fetchAllTerritories = function(region){
    	var self = this;
    	this.dataModel.setEndpoint("?region="+region);
    	this.dataModel.fetch(function(){
    		namespace.polygonPresenter.removePolygons("existingRegion",region);
    		namespace.territories.getExistingRegions()[region] = {};
    		self.polygons = namespace.polygonPresenter.createPolygonsFromTerritories(self.dataModel.getTerritories(),"existingRegion",region);
    		namespace.polygonPresenter.renderPolygons(self.polygons);
    	});
    }
    
    /**
     * fetch territories
     * @param data
     * @param region
     */
    RegionExistPresenter.prototype.fetchTerritories = function(data, region){
    	var self = this;
    	_distance = google.maps.geometry.spherical.computeDistanceBetween (data.boundaries.getNorthEast(), data.boundaries.getSouthWest());
    	_distance = _distance / 1;
   
    	this.dataModel.setDistance(_distance) ;
    	this.dataModel.setMapCenter(data.center);
    	this.dataModel.setEndpoint("?region="+region+"&latitude="+data.center.lat()+"&longitude="+data.center.lng()+"&distance="+_distance);
    	
    	this.dataModel.fetch(function(){
    		namespace.polygonPresenter.removePolygons("existingRegion",region);
    		namespace.territories.getExistingRegions()[region] = {};
    		self.polygons = namespace.polygonPresenter.createPolygonsFromTerritories(self.dataModel.getTerritories(),"existingRegion",region);
    		namespace.polygonPresenter.renderPolygons(self.polygons);
    	});
    };
    
    /**
     * remove territories
     * @param data
     * @param region
     */
    RegionExistPresenter.prototype.removeTerritories = function(data, region){
    	namespace.polygonPresenter.removePolygons("existingRegion",region);
    };

     /**
     * remove outline
     * @param region
     */
    RegionExistPresenter.prototype.removeOutline = function(region){
    	namespace.regionPresenter.removeOutlines(region);
    };
    
    /**
     * show outline
     * @param region
     */
    RegionExistPresenter.prototype.showOutline = function(region){
    	namespace.regionPresenter.showOutline(region);
    	$(document).trigger("outlineShown", region);
    };
    
    return RegionExistPresenter;
})();