/**
 * SoundActionInfoPresenter
 * (c) kuema 2017
 * 
 * @binding soundActionFilterFocus
 * @binding soundAction_edit_event_propagate
 * @binding soundAction_location_event_propagate
 * @binding soundAction_info_event_propagate
 * 
 */
var SoundActionInfoPresenter = (function(){

    SoundActionInfoPresenter.prototype = new SMVP.Presenter(namespace.soundActionInfoViewModel);

    function SoundActionInfoPresenter(){
    	var self=this;
        var viewOffset = 30;
    	
    	/**
    	 * bind userFilterFocus
    	 */
    	$(document).bind("soundActionFilterFocus", function(o,event){
			$('[id^=info]').removeClass('active');
			$('[id^=infosound]').removeClass('open');
            $('[id^=infosound]').parent().parent().removeClass('actionToggle');

			
    		self.destroyView();
    	});

    	/**
    	 * bind user_edit_event_propagate
    	 */
        $(document).bind("soundAction_edit_event_propagate", function(o,data){
            self.destroyView();
        });

        /**
         * bind user_location_event_propagate
         */
        $(document).bind("soundAction_location_event_propagate", function(o,data){
            self.destroyView();
        });
        
        /**
         * bind user_info_event_propagate
         */
    	$(document).bind("soundAction_info_event_propagate", function(o,data){
    		if ($('#'+data.event.element.target.id).hasClass('open')) {
    			$('#'+data.event.element.target.id).removeClass('open');

    			$(document).trigger("soundActionFilterFocus");
    		} else {
	            $('[id^=infosound]').removeClass('open');
	            $('[id^=infosound]').removeClass('active');
	            $('[id^=infosound]').parent().parent().removeClass('actionToggle');

	            $('#'+data.event.element.target.id).addClass('active');
	           // data.model.fetch(function(){
	            	self.getModel().setData(data.model);
	            	self.destroyView();
	    	        self.getView().setContainer("soundActionInfoContainer_"+data.model.getId());
	    	        self.renderView(self);
	    	        self.animateView(data.event, $('#soundActionListContainer'), viewOffset);
	    	        $('#'+data.event.element.target.id).addClass('open');

	            //});
    		}
    	        
	        // $(document).trigger("setMapCenter", self.getModel().getData().getPosition())
    	});
    }
    
    return SoundActionInfoPresenter;
})();