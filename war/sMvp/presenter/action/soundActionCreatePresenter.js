/**
 * SoundActionCreatePresenter
 * (c) kuema 2017
 * 
 * @binding createUser_control_event_propagate
 * 
 */
var SoundActionCreatePresenter = (function(){

    SoundActionCreatePresenter.prototype = new SMVP.Presenter(namespace.soundActionCreateViewModel);

    /**
     * constructor
     */
    function SoundActionCreatePresenter(){
    	var self=this;
    	this.mandatory = [];
    	var audioContext = window.AudioContext || window.webkitAudioContext;
    	var context = new audioContext();
    	var gainNode = context.createGain();
		var audioBuffer = null;
    
    	/**
    	 * bind createUser_control_event_propagate
    	 */
        $(document).bind("createSoundAction_control_event_propagate", function(){
        	var newModel = {};
        	$.each(namespace.domainModel.soundAction, function(key,val){
        		newModel[key] = val.value;
        		if (this.mandatory === true) self.mandatory.push(key);
        	});
        	
            self.getModel().setData(new SMVP.Model(newModel));
        	
            self.renderView(self, function(){
            	$('#newSoundActionForm :input').each(function() {
            		 if ($(this).is('select') !== true) $(this).addClass('missingValue');
     	        });
            	self.showView();
    		   
        		$('#newSoundActionForm :input').keyup(function(event) {
                	validateFormInput(event);
                });
                
                $('#newSoundActionForm :input').change(function(event) {
                	validateFormInput(event);
                });
        	});
        });
        
        /**
         * validate form input
         */
        var validateFormInput = function(event){
            var changeStack = {};
            var currentVal = $(event.target).val();
              
            if(Utilities.isValid(event.target) === true) {
                changeStack[event.target.id] = 'changed';
                $(event.target).removeClass('missingValue');
                $('#submitNewSoundAction').removeClass('error');
            
            } else if (Utilities.isValid(event.target) === false){
                $(event).addClass('missingValue');
                $('#submitNewSoundAction').addClass('error');
                delete changeStack[event.id];
            }
            if(event.target.id == 'soundActionFile') {
                var files = event.target.files;
                for (var i = 0, f; f = files[i]; i++) {
                   // Only process audio files.
                   if (!f.type.match('audio.*')) {
                        continue;
                   }
                   var sd = document.getElementById("soundActionFile");
                   var reader = new FileReader();
                   reader.onload= (function(theFile){
                	   return function(e) {
                		   var bytes = new Uint8Array(e.target.result);
                           var len = e.target.result.byteLength;
                           var binary = "";
                           for (var i = 0; i < len; i++) {
                        	   binary += String.fromCharCode(bytes[i]);
                           }
							context.decodeAudioData(e.target.result, function (buffer) {
								
										audioBuffer = buffer;
										
										source = context.createBufferSource();
										source.buffer = audioBuffer;
										source.loop = false;
										source.connect(gainNode);
										gainNode.connect(context.destination);
										gainNode.gain.value = namespace.soundActionPresenter.getGain();
										source.start();
										var roundedDuration = Math.round( buffer.duration * 10 ) / 10;
										self.getModel().getData().setDuration(roundedDuration);
										self.getModel().getData().setSampleRate(buffer.sampleRate);
									});


                          
	                       self.getModel().getData().setContent(window.btoa(binary));
                        };
                       
                    })(f);
                    reader.readAsArrayBuffer(f);
                }

            } else {
                self.getModel().getData()["set"+event.target.id.split('_')[1].ucfirst()](currentVal,"noTrigger");
            }
        };
    }
    
    // public API
    
    /**
     * userCreateView_event
     * @param event
     */
    SoundActionCreatePresenter.prototype.soundActionCreateView_event = function(event){
        try {
            this["" + event.element.target.id](event);
        } catch(e){}
    };

    /**
     * cancel new user creation
     */
    SoundActionCreatePresenter.prototype.cancelNewSoundAction = function() {
        this.destroyView();
        this.hideView();
    };

    /**
     * submit new user
     */
    SoundActionCreatePresenter.prototype.submitNewSoundAction = function() {
        var self = this;
        if (!$('#newSoundActionForm :input').hasClass('missingValue')) {
        	self.getModel().getData().post();
        } 
    };

    return SoundActionCreatePresenter;
})();