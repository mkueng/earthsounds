/**
 * 
 */
/**
 * SoundActionPresenter
 * (c) kuema 2017
 * 
 * @binding userFilterFocus
 * @binding user_edit_event_propagate
 * @binding user_location_event_propagate
 * @binding user_info_event_propagate
 * 
 */
var SoundActionPresenter = (function(){

    SoundActionPresenter.prototype = new SMVP.Presenter(namespace.allAboutSoundActionViewModel);
    
    function SoundActionPresenter(){
    	
    	this.soundQueue = {};
    	var self=this;	
        window.AudioContext = window.AudioContext || window.webkitAudioContext;
        var context  = new window.AudioContext();
    	
    	var gainNode = context.createGain();
		var audioBuffer = null;
		var base64String = null;
		
		this.gain = 0.5;
		this.sound = new SMVP.Model(namespace.domainModel.sound);

		/**
		 * read base64 String into buffer
		 * @param base64String
		 * @returns buffer
		 */
    	this.base64ToBuffer = function(base64String) {
    		var binary = window.atob(base64String);
		    var buffer = new ArrayBuffer(binary.length);
		    var bytes = new Uint8Array(buffer);
		    for (var i = 0; i < buffer.byteLength; i++) {
		        bytes[i] = binary.charCodeAt(i) & 0xFF;
		    }
		    return buffer;
    	}
    	
    	/**
    	 * init sound 
    	 * @param base64String
    	 * @param event
    	 */
    	this.initSound = function(base64String, event, actionId){
			var audioFromString = this.base64ToBuffer(base64String);
			context.decodeAudioData(audioFromString, function (buffer) {
				audioBuffer = buffer;
				source = context.createBufferSource();
				source.buffer = audioBuffer;
				source.connect(gainNode);
				gainNode.connect(context.destination);
				gainNode.gain.value = self.gain;
				source.loop = false;
				source.onended = function(){
					delete self.soundQueue[actionId];
					if (typeof event != 'undefined') {
						$('#'+event.element.target.id).removeClass('active');
						$('#soundAnimation_'+event.element.target.id.split('_')[1]).fadeOut();
					}
				}
				if (window.webkitAudioContext) {
					source.start(0)
				} else {
					source.start(0);
				};
			});
		}
    	
    	this.setGain = function(gain){
    		self.gain = gain;
    	}
    	
    	this.getGain = function(){
    		return self.gain;
    	}
    	
    	
    	/**
    	 * bind territory action
    	 *  
    	 */
    	$(document).bind('soundAction', function(o, data){
    		if (typeof self.soundQueue[data.actionId] == 'undefined'){
    			self.soundQueue[data.actionId] = true;
    			self.initSound(data.actionContent,undefined, data.actionId);	
    		}
    	}) 
    	
    	
    	/**
    	 * bind soundActionClicked action
    	 *  
    	 */
    	$(document).bind('soundActionClicked', function(o, data){
    		self.playSound(data.actionId.split('.')[1]);
    		/*if (typeof self.soundQueue[data.actionId] == 'undefined'){
    			self.soundQueue[data.actionId] = true;
    			self.initSound(data.actionContent,undefined, data.actionId);	
    		}*/
    	})
    	
    	/**
    	 * bind collectionChanged_sound
    	 */
        $(document).bind("collectionChanged_sound", function(){
        	self.renderView(self,function(){
                self.setBindings();
           });
        });
    	
    	
    	
        /**
         * bind standardDialogButtonYes click
         */
        $('#standardDialogButtonYes').bind('click',function(event){
            $(this).attr('modelType') == 'soundAction'
                ? self[$(this).attr('modelAction')+'soundAction'](  $(this).attr('modelId'))
                : false;
        });

        /**
         * set bindings
         */
        this.setBindings = function(){
            var showHide = $('#showHideSoundAction');
            var userFilter =  $('#soundActionFilter');

            $(showHide).off("click");
            $(userFilter).off("click");

            $(showHide).bind('change',function(event){
                $("#allAboutSoundActionContainer").toggleClass("allAboutSoundActionContainer");
                $("#soundActionListContainer").toggle();
                $("#soundActionOptionsContainer").toggle();
                $("#soundActionCreateContainer").hide();

            });

            $(soundActionFilter).on("focus", function(){
                $(document).trigger("soundActionFilterFocus");
            });
            
            $('#soundActionGain').on('change', function(){
            	self.setGain($(this).val()/100);
            })
            
            $('#soundActionCategoryFilter').on('change', function(event){
            	console.log("filter change", this.value);
            	self.getModel().setCategoryFilter(this.value);
            	namespace.domainModel.territory.actionContent['default']="bla";
            	self.renderView(self,function(){
                    self.setBindings();
               });
            })
        };
        
    	  /**
         * render view
         */
        self.renderView(self,function(){
           self.setBindings();
           $('#showHideSoundAction').trigger('change');
           
        });
    }
    
    
 // public API

    /**
     * soundAction control (create)
     * @param event
     */
    SoundActionPresenter.prototype.soundActionControlView_event = function(event){
    	console.log("event: ", event);
    	$(document).trigger("createSoundAction_control_event_propagate", event);
    };
    
    /**
     * soundActionView events
     * @param event
     */
    SoundActionPresenter.prototype.allAboutSoundActionView_event = function(event){
    	
    	if (event.element.target.firstChild === null ) {
		    $('[id^=editsoundAction_]').removeClass('active');
		    $('[id^=infosoundAction_]').removeClass('active');
		    event.element.preventDefault();
        	//invoke method according to event
            this[""+event.element.target.id.split("_")[0]](event);
           
        } else {
        	if (typeof event.element.target.firstChild.id != 'undefined' && event.element.target.firstChild.id.split('_')[0] == 'actionToggleSoundAction'){
        		event.element.preventDefault();
        	} else if (event.element.target.id.split('_')[1] !== undefined) {
        		event.element.preventDefault();
        		this.showSoundAction(event);
	        }
        }
    };
    
    /**
     * soundAction info
     * @param event
     */
    SoundActionPresenter.prototype.infosoundAction = function(event){
        this.triggerPropagate(event, "info");
    };
    
    /**
     * soundAction info
     * @param event
     */
    SoundActionPresenter.prototype.playSoundAction = function(event){
    	var self = this;
    	this.sound.setId(event.element.target.id.split("_")[1])
    	this.sound.fetch(function(){
    		self.initSound(self.sound.getContent(), event);
    		$('#'+event.element.target.id).addClass('active');
    		$('#soundAnimation_'+event.element.target.id.split('_')[1]).fadeIn();
    	})
    };
    
    SoundActionPresenter.prototype.playSound = function(id){
    	var self = this;
    	this.sound.setId(id);
    	this.sound.fetch(function(){
    		self.initSound(self.sound.getContent(), undefined, id);

    	})
    };
    
    
    /**
     * soundAction delete request
     * @param event
     */
    SoundActionPresenter.prototype.minussoundAction = function(event){
        event.element.stopPropagation();
        var id = event.element.target.id.split("_")[1];
        var name = this.getModel().getData().readModel(id).getName();
        var dialogButton =  $('#standardDialogButtonYes');
        $('#standardDialogContentText').html('sure to delete sound: '+name+'?');

        $(dialogButton).attr('modelId', id);
        $(dialogButton).attr('modelType', "soundAction");
        $(dialogButton).attr('modelAction', "delete");
        $('#standardDialogLink').click();
    };
    
    /**
     * soundAction delete
     * @param modelId
     */
    SoundActionPresenter.prototype.deletesoundAction = function(modelId){
    	this.getModel().getData().readModel(modelId).destroy();
    };
    
    /**
     * trigger propagate
     * @param event
     * @param type
     */
    SoundActionPresenter.prototype.triggerPropagate = function(event,type){
        var id = event.element.target.id.split("_")[1];
        event.element.stopPropagation();
        $(document).trigger("soundAction_"+type+"_event_propagate",{
            event:event,
            model:this.getModel().getData().readModel(id)
        });
    };
    
    return SoundActionPresenter;
})();