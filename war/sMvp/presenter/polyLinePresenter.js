/**
 * PolylinePresenter 
 * (c) kuema 2017
 * 
 * @binding markerCreated
 * @binding markerDeleted
 * @binding markersDeleted
 * 
 */
var PolylinePresenter = (function(){

    PolylinePresenter.prototype = new SMVP.Presenter(namespace.polyline);
    
    var map=null;

    /**
     * @constructor
     */
    function PolylinePresenter(){
        var self = this;
        map = namespace.mapPresenter.getMap();

        /**
         * bind markerCreated
         */
        $(document).bind("markerCreated", function(o,marker){
            var model = null;
            switch (marker.type){
                case "boundary" :
                {
                    model = self.getModel().getBoundary();
                    model.path.push(marker.position);

                    if ( model.path.length > 1) {
                        model.props.path.length = 0;
                        model.props.path.push(model.path[model.path.length - 2]);
                        model.props.path.push(model.path[model.path.length - 1]);
                        self.setPolyline({props: model.props});
                    }
                    break;
                }
            }
        });

        /**
         * bind markerDeleted
         */
        $(document).bind("markerDeleted", function(o,marker){
            var model = null;
            switch (marker.type){
                case "boundary" :
                {
                    model = self.getModel().getBoundary();
                    if (model.path.length > 1) {
                        model.path.pop();
                        model.polylines[model.polylines.length-1].setMap(null);
                        model.polylines.pop();
                    }
                    break;
                }
            }
        });

        /**
         * bind markersDeleted
         */
        $(document).bind("markersDeleted", function(o,markers){
            var model = null;
            var type = markers[Object.keys(markers)[0]].type;

            switch (type){
                case "boundary" :
                {
                    model = self.getModel().getBoundary();
                    console.log("boundary :", self.getModel().getBoundary());
                    if (model.path.length >1) {
                        for (var i= 0,len=model.polylines.length; i < len; i++){
                            model.polylines[i].setMap(null);
                        }
                        model.polylines.length=0;
                        model.path.length = 0;
                    }
                    break;
                }
            }
        });
    }

    //public API
    
    /**
     * get path
     * @param type
     * @returns path
     */
    PolylinePresenter.prototype.getPath = function(type){
        switch (type) {
            case "boundary" :
            {
                return this.getModel().getBoundary().path;
            }
        }
    };

    /**
     * set polyline on map
     * @param data
     */
    PolylinePresenter.prototype.setPolyline = function(data){
        var model = null;

        switch (data.props.type){
            case "boundary" :
            {
                model = this.getModel().getBoundary();
                model.polylines.push(this.createPolyline(data.props));
                model.polylines[model.polylines.length-1].setMap(map);
            }
        }
    };

    /**
     * create polyline
     * @param properties
     * @returns google.maps.Polyline
     */
    PolylinePresenter.prototype.createPolyline = function(props){
        return new google.maps.Polyline(props);
    };

    return PolylinePresenter;
})();