/**
 * StatusInfoPresenter
 * (c) kuema 2017
 * 
 * @binding dataGatewayError
 * @binding dataGatewaySuccess
 */

var StatusInfoPresenter = (function(){
	
	StatusInfoPresenter.prototype = new SMVP.Presenter(namespace.statusInfoViewModel);
	
	var infoQueue = [];
	var info = null;
	var showInfo = true;
	var backgroundColor  = '';

	/**
	 * @constructor
	 */
	function StatusInfoPresenter(){
		var self = this;
		
		$(document).bind("dataGatewayInfo", function(o,message){
			infoQueue.push(message);
			if (showInfo) {
				self.showInfos();
				showInfo = false;
			}
		})
	}
	
	StatusInfoPresenter.prototype.showInfos = function(){
		if (infoQueue.length > 0) {2
			this.displayInfo(infoQueue.pop());
		} else {
			showInfo = true;
		}
	}
	
	StatusInfoPresenter.prototype.displayInfo = function(message){
		var self = this;
		backgroundColor = message.status == 'success' ? 'rgba(0,200,0,.8)' : 'rgba(200,0,0,.8)';
		$('#statusInfo').css('background-color', backgroundColor );
		$('#statusInfoText').text(message.text);
		$('#statusInfo').fadeIn(300, function(){
			setTimeout(function(){
				$('#statusInfo').fadeOut(300,function(){
					self.showInfos();
				});
			}, 3000);
		});
	}
	
	return StatusInfoPresenter;
})();