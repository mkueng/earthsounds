/**
 * TerritoryInfoPresenter
 * (c) kuema 2017
 * 
 */
var TerritoryInfoPresenter = (function(){

    TerritoryInfoPresenter.prototype = new SMVP.Presenter(namespace.territoryInfoViewModel);

    /**
     * constructor
     */
    function TerritoryInfoPresenter(){
    	var self = this;
        this.template = this.getView().getTemplate();

        this.infoBox = new InfoBox({
            boxClass : "",
            enableEventPropagation:false,
            closeBoxURL: "https://www.google.com/intl/en_us/mapfiles/close.gif",
            disableAutoPan: true
        });
        
        $(document).bind("outlineShown", function(o,region){
        	self.infoBox.close();
        })
        
        google.maps.event.addListener(this.infoBox, 'domready', function (e) {
            $(document).trigger('territoryInfoBoxReady', this);
        });

        this.map = namespace.mapPresenter.getMap();
    }

    // public API
    
    /**
     * destroy view
     */
    TerritoryInfoPresenter.prototype.destroyView = function() {
        this.infoBox.close();
    };

    /**
     * render view
     */
    TerritoryInfoPresenter.prototype.renderView = function(position, data){
    	if (typeof position.latitude != 'undefined') {
    	    position = new google.maps.LatLng(position.latitude, position.longitude);
    	}
        var displayContent = jQuery.extend({}, data);
        var infoLabelOptions={
            position : position,
            content	: _.template(this.template,{displayContent:displayContent})
        };
        this.infoBox.setOptions(infoLabelOptions);
        this.infoBox.open(this.map);
        this.infoBox.zIndex_ = 0;
        this.infoBox.type = "territoryInfo";
    };

    return TerritoryInfoPresenter;
})();

