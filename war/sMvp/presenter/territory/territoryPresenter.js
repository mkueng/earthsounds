/**
 * TerritoryPresenter
 * (c) kuema 2017
 * 
 * @binding createTerritory
 * @binding divideTerritory
 * 
 */
var TerritoryPresenter = (function(){

	/**
	 * constructor
	 */
    function TerritoryPresenter(){
        var self=this;
     
        /**
         * bind createTerritory
         */
        $(document).bind("createTerritory", function(o,data){
            self.createTerritory(data);
        });

        /**
         * bind divideTerritory
         */
        $(document).bind("divideTerritory", function(o,territory){
        });
    }

    // public API
    
    /**
     * divide territory
     * @param territory
     * @oaran region
     * @returns newTerritories
     */
    TerritoryPresenter.prototype.divideTerritory = function(territory, region){
        var newLatSpacing = territory.spacing / 2;
        var newLngSpacing =((newLatSpacing/2*3)*100000)/100000;
        var startLatLng = null;
        
        if (region == "newRegion" ) {
            startLatLng = territory.geoPoints[0];
        } else {
            if (typeof territory.geoPoints[0].latitude != 'undefined') {
                startLatLng = new google.maps.LatLng (territory.geoPoints[0].latitude, territory.geoPoints[0].longitude);
            } else {
                startLatLng = territory.geoPoints[0];
            }
        }
      
        var newTerritories = {};
        var newTerritory = null;
        var spacing = territory.spacing / 2;
        var latLngs = [];


        latLngs.push(startLatLng);
        latLngs.push(new google.maps.LatLng(startLatLng.lat()+newLatSpacing, startLatLng.lng()));
        latLngs.push(new google.maps.LatLng(startLatLng.lat()+newLatSpacing, startLatLng.lng()+newLngSpacing));
        latLngs.push(new google.maps.LatLng(startLatLng.lat(), startLatLng.lng()+newLngSpacing));
        
        for (var i = 0, len = latLngs.length; i < len; i++){

            newTerritory = this.createTerritory(territory.region,latLngs[i],spacing,"isDivide", territory);
         
            territory.region === "" 
                ? namespace.territories.getNewRegion().addModel(new SMVP.Model(newTerritory))
                : namespace.territories.getExistingRegions()[territory.region][territory.id]=newTerritory;
            newTerritories[newTerritory.id] = newTerritory;
        }
        
        return newTerritories;
    };

    /**
     * create territory
     * @param region
     * @param latLng
     * @param spacing
     * @param isDivide
     * @returns {Model}
     */
    TerritoryPresenter.prototype.createTerritory = function(region, latLng, spacing, isDivide, cloneValues) {
    
    	var properties = {};
    	if (typeof cloneValues == 'undefined') {
    		properties.name= "";
    		properties.owner = "";
    		properties.status = "FREE";
    		properties.itFactor = 1;
    		properties.action = "";
    		properties.actionContent = "";
    	} else {
    		properties.name = cloneValues.name;
    		properties.owner = cloneValues.owner;
    		properties.status = cloneValues.status;
    		properties.itFactor = cloneValues.itFactor;
    		properties.action = cloneValues.action;
    		properties.actionContent = cloneValues.actionContent;
    	}
    	
        var lat = latLng.lat();
        var lng = latLng.lng();
        var latSpacing = parseFloat(spacing);
        var lngSpacing = ((latSpacing/2*3)*100000)/100000;

        if (!isDivide) {
            lat = lat-(lat%latSpacing);
            lng = lng-(lng%lngSpacing);
        }

        var endLat = Math.round((lat +latSpacing-0.00000001)*10000000)/10000000;
        var endLng = Math.round((lng+ lngSpacing-0.00000001)*10000000)/10000000;
        lng=Math.round(lng*100000)/100000;
        lat=Math.round(lat*100000)/100000;

        var newTerritoryModel = {};
        $.each(namespace.domainModel.territory, function(key, val){
        	newTerritoryModel[key] = null;
        });
        var newModel = {};

        newModel.id = (lat+"_"+lng);
        newModel.name = properties.name;
        newModel.owner = properties.owner;
        newModel.status = properties.status;
        newModel.itFactor = properties.itFactor;
        newModel.region = region;
        newModel.spacing = spacing;
        newModel.action = properties.action;
        newModel.actionContent = properties.actionContent;
        newModel.geoPoints = [
            new google.maps.LatLng(lat, lng),
            new google.maps.LatLng(endLat, lng),
            new google.maps.LatLng(endLat, endLng),
            new google.maps.LatLng(lat, endLng)
        ];
        
        region === ""
            ?  namespace.territories.getNewRegion().addModel(new SMVP.Model(newModel))
            : namespace.territories.getExistingRegions()[region][newModel.id]= newModel;
        
        return newModel;
    };

    /**
     * calculate territory grid
     * @param region
     * @param spacing
     * @param boundaryPath
     * @returns {{}}
     */
    TerritoryPresenter.prototype.calculateTerritoryGrid = function(region, spacing, boundaryPath) {

        var newTerritories = {};
        var latSpacing= parseFloat(spacing);
        var lngSpacing = ((latSpacing/2*3)*100000)/100000;
        var boundaryPolygon = new google.maps.Polygon({
            paths:boundaryPath
        });
        var isWithinBoundaryPolygon = false;

        // getting highest and lowest values from boundary path
        var highLowVal = this.getHighestAndLowestLatLngFromPointSet(boundaryPath);

        // snap to modulo for fixed origin points (long wider than lat for apprx. squares)
        var difLat =  highLowVal.minLat-(highLowVal.minLat%latSpacing);
        var difLng =  highLowVal.minLng-(highLowVal.minLng%lngSpacing);

        // all 4 region-points from boundary-path rounded to 3 decimals
        highLowVal.minLat = Math.round(difLat*100000)/100000;
        highLowVal.minLng = Math.round(difLng*100000)/100000;
        highLowVal.maxLat = Math.round(highLowVal.maxLat*100000)/100000;
        highLowVal.maxLng = Math.round(highLowVal.maxLng*100000)/100000;

        var lat = highLowVal.minLat;
        var lng = highLowVal.minLng;
        var newModel = null;
        var newTerritoryModel = null;

        while (lng < highLowVal.maxLng) {
            while (lat < highLowVal.maxLat) {
                isWithinBoundaryPolygon = boundaryPolygon.containsLatLng(lat,lng);
                if (isWithinBoundaryPolygon) {
                    var endLat = Math.round((lat +latSpacing-0.00000001)*10000000)/10000000;
                    var endLng = Math.round((lng+ lngSpacing-0.00000001)*10000000)/10000000;
                    lng=Math.round(lng*100000)/100000;
                    lat=Math.round(lat*100000)/100000;

                    newTerritoryModel = {};
                    $.each(namespace.domainModel.territory, function(key, val){
                    	newTerritoryModel[key] = null;
                    });
                    newModel = new SMVP.Model(newTerritoryModel);
                    
                    newModel.setId(lat+"_"+lng);
                    newModel.setRoot("territory");
                    newModel.setEndpoint("");
                    newModel.setName("");
                    newModel.setOwner("");
                    newModel.setStatus("FREE");
                    newModel.setItFactor(1);
                    newModel.setAction("");
                    newModel.setActionContent("");
                    newModel.setRegion("");
                    newModel.setSpacing(spacing);
                    newModel.setGeoPoints([
                        new google.maps.LatLng(lat, lng),
                        new google.maps.LatLng(endLat, lng),
                        new google.maps.LatLng(endLat, endLng),
                        new google.maps.LatLng(lat, endLng)
                    ]);

                    newTerritories[newModel.getId()] = newModel;
                }
                lat+=latSpacing;
                lat=Math.round(lat*100000)/100000;
            }
            lat=highLowVal.minLat;
            lng+=lngSpacing;
            lng=Math.round(lng*100000)/100000;
        }
       
        return newTerritories;
    };
    
    /**
     * getDistant
     */
    TerritoryPresenter.prototype.getDistant = function(cpt, bl) {
        var Vy = bl[1][0] - bl[0][0];
        var Vx = bl[0][1] - bl[1][1];
        return (Vx * (cpt[0] - bl[0][0]) + Vy * (cpt[1] -bl[0][1]));
    };
    	
    /**
     * findMostDistantPointFromBaseLine
     * @param baseline: array
     * @param points: array
     */
    TerritoryPresenter.prototype.findMostDistantPointFromBaseLine = function(baseLine, points) {
        var maxD = 0;
        var maxPt = [];
        var newPoints = [];
        for (var idx in points) {
            var pt = points[idx];
            var d = this.getDistant(pt, baseLine);
            
            if ( d > 0) {
                newPoints.push(pt);
            } else {
                continue;
            }
            
            if ( d > maxD ) {
                maxD = d;
                maxPt = pt;
            }
        }
        return {'maxPoint':maxPt, 'newPoints':newPoints};
    };
    
    /**
     * @param baseline:array
     * @param points:array
     */
    TerritoryPresenter.prototype.buildConvexHull = function (baseLine, points) {
    	var allBaseLines = [];
        allBaseLines.push(baseLine);
        var convexHullBaseLines = new Array();
        var t = this.findMostDistantPointFromBaseLine(baseLine, points);
        if (t.maxPoint.length) { // if there is still a point "outside" the base line
            convexHullBaseLines = 
                convexHullBaseLines.concat( 
                    this.buildConvexHull( [baseLine[0],t.maxPoint], t.newPoints) 
                );
            convexHullBaseLines = 
                convexHullBaseLines.concat( 
                    this.buildConvexHull( [t.maxPoint,baseLine[1]], t.newPoints) 
                );
            return convexHullBaseLines;
        } else {  // if there is no more point "outside" the base line, the current base line is part of the convex hull
            return [baseLine];
        }    
    };
    
    /**
     * @param points:array
     * returns convex hull:array
     */
    TerritoryPresenter.prototype.getConvexHull = function(points) {
        //find first baseline
        var maxX, minX;
        var maxPt, minPt;
        for (var idx in points) {
            var pt = points[idx];
            if (pt[0] > maxX || !maxX) {
                maxPt = pt;
                maxX = pt[0];
            }
            if (pt[0] < minX || !minX) {
                minPt = pt;
                minX = pt[0];
            }
        }
        var ch = [].concat(this.buildConvexHull([minPt, maxPt], points),
                           this.buildConvexHull([maxPt, minPt], points));
        return ch;
    };
    
    /**
     * @param pointSet:array
     * @returns pointSet:object
     */
    TerritoryPresenter.prototype.getHighestAndLowestLatLngFromPointSet= function(pointSet){
    	var minLat = pointSet[0].lat();
        var maxLat = pointSet[0].lat();
        var minLng = pointSet[0].lng();
        var maxLng = pointSet[0].lng();
    	
    	$.each(pointSet, function(){
    		this.lat() > maxLat ? maxLat = this.lat() : false;
            this.lat() < minLat ? minLat = this.lat() : false;
            this.lng() > maxLng ? maxLng = this.lng() : false;
            this.lng() < minLng ? minLng = this.lng() : false;
    	});
    	
    	return {"minLat":minLat, "minLng":minLng, "maxLat":maxLat, "maxLng":maxLng};
    };
    
    return TerritoryPresenter;
})();