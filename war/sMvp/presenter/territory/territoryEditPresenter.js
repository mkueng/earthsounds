/**
 * TerritoryEditPresenter
 * (c) kuema 2017
 * 
 * @listener google.maps.event infoBox domready
 * @listener google.maps.event infoBox closeclick
 * 
 * @trigger territoryEditBoxReady
 * @trigger territoryEditBoxClose
 * @trigger territoryStatusChange
 */

var TerritoryEditPresenter = (function(){

    TerritoryEditPresenter.prototype = new SMVP.Presenter(namespace.territoryEditViewModel);
    
    var editCopy = {};

    /**
     * @constructor
     */
    function TerritoryEditPresenter(){
        var self = this;
        var newModel = {};
        var userNicknames = [];
        var sounds = [];
        this.map = namespace.mapPresenter.getMap();
        this.template = this.getView().getTemplate();

        
    	$.each(namespace.domainModel.territory, function(key,val){
    		newModel[key] = val.value;
    	});
        
        this.dataModel = new SMVP.Model(newModel);
        
        this.infoBox = new InfoBox({
            boxClass : "",
            enableEventPropagation : false,
            closeBoxMargin: "10px 10px 2px 2px",
            closeBoxURL: "../css/images/icons-svg/delete-white.svg",
            disableAutoPan: true
        });

        this.map = namespace.mapPresenter.getMap();
        
        $(document).bind("outlineShown", function(o,region){
        	self.infoBox.close();
        })
        
        $(document).bind("collectionChanged_sound", function(){
        	sounds = [];
        	 $.each(namespace.soundCollection.getCollection(), function(){
             	sounds.push(this.name+"."+this.id+"."+this.category);
             })
             
             namespace.domainModel.territory.actionContent["default"]["playsound"]= sounds;
        })
        
        /*
         * infoBox ready
         */
        google.maps.event.addListener(this.infoBox, 'domready', function (e) {
            $(document).trigger('territoryEditBoxReady', this);
            handleEditBoxReadyEvent(this);
        });
        
        /**
         * infoBox close
         */
        google.maps.event.addListener(this.infoBox, 'closeclick', function (e) {
        	$(document).unbind('keyEvent_d');
        	$(document).trigger('territoryEditBoxClose', this);
        });
    
        /**
         * set default user nicknames 
         */
        $.each(namespace.userCollection.getCollection(), function(){
        	userNicknames.push(this.nickName);
        });
        
      
        $.each(namespace.soundCollection.getCollection(), function(){
        	sounds.push(this.name+"."+this.id+"."+this.category);
        
        })
        
        namespace.domainModel.territory.actionContent["default"]["playsound"]= sounds;
        namespace.domainModel.territory.owner['default'] = userNicknames;
        
      
        
        /**
         * @hint bind edit box change events
         * @param element
         * @param infoBox
         */
        var bindChangeEvents = function(element, infoBox){
        	
        	 $('#'+"territoryEdit_"+element).unbind().bind('change',function(e) {
        		 // trigger copy change
        		 $('#'+"territoryEditCopy_"+element).trigger('change');
        		 // set itFactor slider value
                 if ($(this).attr('id') =='territoryEdit_itFactor') {
                     $('#itFactorValue').text($(this).val());
                 }
                 
                 //action
                 if ($(this).attr('id') =='territoryEdit_action') {
                	 if ($(this).val() == "Play sound") {
                		
                		 namespace.domainModel.territory.actionContent['default'] = sounds;
                		 var $el = $('#territoryEdit_actionContent')
                		 $el.empty();
                		 $el.append($("<option></option>").attr("value", " ").text(" "));
                		 $.each(sounds, function() {
                			  $el.append($("<option></option>")
                			     .attr("value", this).text(this));
                			});
                	 }
                 }
                 
                 if ($(this).attr('id') =='territoryEdit_actionContent') {
                	 
                 
                 }
                 
                 if (infoBox.region === "") {
                	 namespace.territories.getNewRegion().getCollection()[$(this).attr('data-id')][$(this).attr('data-property')]= $(this).val();
                 } else {
                     var value = $(this).val();
                     if ($(this).attr('data-property') == 'itFactor') value = parseInt(value);
                	 namespace.territories.getExistingRegions()[infoBox.region][$(this).attr('data-id')][$(this).attr('data-property')]= $(this).val();
                	 self.dataModel['set'+($(this).attr('data-property')).ucfirst()](value);
                	 
                	 // update territory
                	 self.dataModel.update(function(){
                	 });
                 }
                	
                 // status change
                 if ($(this).attr('id') =='territoryEdit_status') {
                     $(document).trigger('territoryStatusChange',  namespace.territories.getNewRegion()[$(this).attr('data-id')]);
                     
                     if (infoBox.region === "") {
	                     namespace['polygonPresenter'].setProperties(
	                         $(this).attr('data-id'),"newRegion",
	                         {status:namespace.territories.getNewRegion().getCollection()[$(this).attr('data-id')].status}
	                     );
                     } else {
                    	 namespace['polygonPresenter'].setProperties(
    	                   $(this).attr('data-id'),"existingRegion",
    	                       { region : infoBox.region,
    	                         status : namespace.territories.getExistingRegions()[infoBox.region][$(this).attr('data-id')].status
    	                       }
    	                 );
                     }
                 }
                 
            
                
             });
        };
        
        /**
         * bind divide button event
         * @param infoBox
         */
        var bindDivideEvent = function(infoBox){
        	
            $('#territoryEditDivide').unbind().bind('click', function(event){
            	var territory = null;
                event.stopPropagation();
                
                if (infoBox.region === "") {
                    territory = namespace.territories.getNewRegion().getCollection()[$(this).attr('data-id')];
                } else {
                    territory = namespace.territories.getExistingRegions()[infoBox.region][$(this).attr('data-id')];
                }
                if (territory.spacing > 0.00025) {
                    // new region
                	if (infoBox.region === "") {
                		// remove territory polygon
                        namespace.polygonPresenter.removePolygon($(this).attr('data-id'), "newRegion");
                        
                        // delete territory
                        delete namespace.territories.getNewRegion().getCollection()[$(this).attr('data-id')];
                        
                        //create new territories
                        var newTerritories = namespace.territoryPresenter.divideTerritory(territory,"newRegion");
                        namespace.polygonPresenter.renderPolygons(namespace.polygonPresenter.createPolygonsFromTerritories(newTerritories, "newRegion"));
                        self.destroyView();
                    } else {
                    //existing region
                    	// remove territory polygon
                        namespace.polygonPresenter.removePolygon($(this).attr('data-id'), "existingRegion", infoBox.region);
                        // delete territory
                        delete namespace.territories.getExistingRegions()[infoBox.region][$(this).attr('data-id')];
                        self.dataModel.destroy(function(){
                            // create new territories
                        	var newTerritories = namespace.territoryPresenter.divideTerritory(territory,"existingRegion");
                            var newTerritoriesCollection = new SMVP.Collection('territory');
                            newTerritoriesCollection.setCollection(newTerritories);
                           
                            //post collection of new territories
                            newTerritoriesCollection.post(function(){
                            });
                            
                            //render new territory polygons
                            namespace.polygonPresenter.renderPolygons(namespace.polygonPresenter.createPolygonsFromTerritories(newTerritories, "existingRegion"));
                            self.destroyView();
                        });
                    }
                }
            });
        };
        
        /**
         * bind delete button event
         * @param infoBox
         */
        var bindDeleteEvent = function(infoBox){
        	 $('#territoryEditDelete').unbind().bind('click', function(event){
             	var territory = null;
                 event.stopPropagation();
                 
                 if (infoBox.region === "") {
                     territory = namespace.territories.getNewRegion().getCollection()[$(this).attr('data-id')];
                 } else {
                     territory = namespace.territories.getExistingRegions()[infoBox.region][$(this).attr('data-id')];
                 }
                 if (infoBox.region === "") {
             		// remove territory polygon
                     namespace.polygonPresenter.removePolygon($(this).attr('data-id'), "newRegion");
                     
                     // delete territory
                     delete namespace.territories.getNewRegion().getCollection()[$(this).attr('data-id')];
                     
                 } else {
                 //existing region
                 	// remove territory polygon
                     namespace.polygonPresenter.removePolygon($(this).attr('data-id'), "existingRegion", infoBox.region);
                     // delete territory
                     delete namespace.territories.getExistingRegions()[infoBox.region][$(this).attr('data-id')];
                     self.dataModel.destroy(function(){
                     });
                 }
                 self.destroyView();
             });
        }
        
        /**
         *
         * @hint handle edit box ready event
         * @param event
         */
        var handleEditBoxReadyEvent = function(event){
        	
        	self.dataModel.setId(self.infoBox.territoryId);
        	bindDivideEvent(self.infoBox);
        	bindDeleteEvent(self.infoBox);
        	
        	$(document).unbind('keyEvent_d').bind('keyEvent_d', function(){
        		 $('#territoryEditDivide').trigger('click');
        	})
        	
        	$.each(namespace.domainModel.territory, function(key, val){
        		
        		bindChangeEvents(key, self.infoBox);
               
                $('#'+"territoryEditCopy_"+key).unbind().bind('change',function(e){
                    var property = $(this).attr('data-property');
                    if (this.checked) {
                        editCopy[property]= $('#territoryEdit_'+property).val();
                    }
                    else {
                        delete editCopy[property];
                    }
                });

                if (typeof editCopy[key]!= 'undefined'){
                	$('#'+"territoryEditCopy_"+key).attr('checked', true);
            		$('#'+"territoryEdit_"+key).val(editCopy[key]);
            		if (self.infoBox.region === "") {
            			namespace.territories.getNewRegion().getCollection()[$('#'+"territoryEdit_"+key).attr('data-id')][key]=(editCopy[key]);
            		} else {
            			namespace.territories.getExistingRegions()[self.infoBox.region][$('#'+"territoryEdit_"+key).attr('data-id')][key]=(editCopy[key]);
            		}
            		if (key == "status") {
            			if (self.infoBox.region === "") {
            				namespace.polygonPresenter.setProperties($('#'+"territoryEdit_"+key).attr('data-id'),"newRegion",
            						{status:namespace.territories.getNewRegion().getCollection()[$('#'+"territoryEdit_"+key).attr('data-id')].status}
            				);
            			} else {
            				namespace.polygonPresenter.setProperties($('#'+"territoryEdit_"+key).attr('data-id'),"existingRegion",
            						{region : self.infoBox.region,
            						status:namespace.territories.getExistingRegions()[self.infoBox.region][$('#'+"territoryEdit_"+key).attr('data-id')].status}
            				);
            			}
            		}
            		if (self.infoBox.region !== "") {
            		    var value = editCopy[key];
                        if (key == 'itFactor') value = parseInt(value);
            			self.dataModel['set'+key.ucfirst()](value);
               	 
               	 		self.dataModel.update(function(){
               	 		});
            		}
            	} else {
            		$('#'+"territoryEditCopy_"+key).attr('checked', false);
            	}
            });
        };
    }

    // public API

    /**
     * destroy view
     */
    TerritoryEditPresenter.prototype.destroyView = function() {
        this.infoBox.close();
    };

    /**
     * render view
     * @param position
     * @param data
     */
    TerritoryEditPresenter.prototype.renderView = function(position, data){
    	if (typeof position.latitude != 'undefined') {
    	    position = new google.maps.LatLng(position.latitude, position.longitude);
    	}
    	/*
    	var bounds = namespace.mapPresenter.getMap().getBounds();
    	var zoom = namespace.mapPresenter.getMap().getZoom();
    	var NE = bounds.getNorthEast();
    	var SW = bounds.getSouthWest();
    	var bounds = new google.maps.LatLngBounds();
    	namespace.mapPresenter.getMap().fitBounds(bounds);
    	*/
    	//$(document).trigger('setMapCenter', {latitude:position.lat(), longitude:position.lng()});
    	map_recenter(position, -155, 150);
    	
    	var displayContent = jQuery.extend({}, data);
        var infoBoxOptions={
            position : position,
            content	: _.template(this.template,{displayContent:displayContent})
        };
        this.infoBox.setOptions(infoBoxOptions);
        this.infoBox.zIndex_ = 1;
        this.infoBox.type = "territoryEdit";
        this.infoBox.region = displayContent.region;
        this.infoBox.territoryId = displayContent.id;
        this.infoBox.open(this.map);

        displayContent = null;
        
        
        function map_recenter(latlng,offsetx,offsety) {
            var point1 = namespace.mapPresenter.getMap().getProjection().fromLatLngToPoint(
                (latlng instanceof google.maps.LatLng) ? latlng : map.getCenter()
            );
            var point2 = new google.maps.Point(
                ( (typeof(offsetx) == 'number' ? offsetx : 0) / Math.pow(2, namespace.mapPresenter.getMap().getZoom()) ) || 0,
                ( (typeof(offsety) == 'number' ? offsety : 0) / Math.pow(2, namespace.mapPresenter.getMap().getZoom()) ) || 0
            );  
            namespace.mapPresenter.getMap().setCenter(namespace.mapPresenter.getMap().getProjection().fromPointToLatLng(new google.maps.Point(
                point1.x - point2.x,
                point1.y + point2.y
            )));
        }
        
        
    };

    return TerritoryEditPresenter;
})();
