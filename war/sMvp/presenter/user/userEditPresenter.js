/**
 * UserEditPresenter
 * (c) kuema 2017
 * 
 * @binding userFilterFocus
 * @binding user_info_event_propagate
 * @binding user_location_event_propagate
 * @binding user_edit_event_propagate
 * 
 */
var UserEditPresenter = (function(){

   	UserEditPresenter.prototype = new SMVP.Presenter(namespace.userEditViewModel);

   	/**
   	 * constructor
   	 */
    function UserEditPresenter(){
    	var self=this;
        var viewOffset = 30;
        var _userId = null;

        this.getUserId = function(){
            return _userId;
        };
        this.setUserId = function(userId){
            _userId=userId;
        };
        
        /**
         * bind userFilterFocus
         */
        $(document).bind("userFilterFocus", function(o,event){
			$('[id^=edit]').removeClass('active');
    		self.destroyView();
    	});

        /**
         * bind user_info_event_propagate
         */
        $(document).bind("user_info_event_propagate", function(o,data){
            self.destroyView();
        });
        
        /**
         * bind user_location_event_propagate
         */
        $(document).bind("user_location_event_propagate", function(o,data){
            self.destroyView();
        });

        /**
         * bind user_edit_event_propagate
         */
    	$(document).bind("user_edit_event_propagate", function(o,data){
    		console.log("dataModel: ", data);
            var id= data.event.element.target.id;
            var userId = id.split('_')[1];

            self.setUserId(userId);
            $('[id^=edit_]').removeClass('active');
            $('#'+id).addClass('active');
            data.model.fetch(function(response){
            	self.getModel().setData(response);
            	self.destroyView();
     	    	self.getView().setContainer("userEditContainer_"+data.model.getId());
     	    	self.renderView(self,function(){
     	    		self.animateView(data.event, $('#userListContainer'), viewOffset);
     	    		validateFormInput();
     	    	});
            });
    	});

        /**
         * validate form input
         */
        var validateFormInput = function(){
            var changeStack = {};
            $('#editUserForm :input').keyup(function(event) {
                var currentVal = $(this).val();
                var actualVal = self.getModel().getData()["get"+event.target.id.split('_')[1].ucfirst()]();
                if(currentVal !== "" && currentVal != actualVal && Utilities.isValid(this) === true) {
                    changeStack[this.id] = 'changed';
                    $(this).removeClass('missingValue');
                    $('#submitEditUser').removeClass('error');
                }else if (currentVal ==  actualVal) {
                    delete changeStack[this.id];
                    $('#submitEditUser').removeClass('error');
                } else if (currentVal === "" || Utilities.isValid(this) === false){
                    $(this).addClass('missingValue');
                    $('#submitEditUser').addClass('error');
                    delete changeStack[this.id];
                }
                if (!$.isEmptyObject(changeStack) && !$('#submitEditUser').hasClass('error')){
                    $('#submitEditUser').addClass('changed');
                } else {
                    $('#submitEditUser').removeClass('changed');
                }
            });

            $('#editUserForm :input').change(function(event) {
                var currentVal = $(this).val();
                var actualVal = self.getModel().getData()["get"+event.target.id.split('_')[1].ucfirst()]();
                if(currentVal !== "" && currentVal != actualVal && Utilities.isValid(this)) {
                    changeStack[this.id] = 'changed';
                    $(this).removeClass('missingValue');
                    $('#submitEditUser').removeClass('error');
                }else if (currentVal ==  actualVal) {
                    delete changeStack[this.id];
                    $('#submitEditUser').removeClass('error');
                } else if (currentVal === "" || Utilities.isValid(this) === false){
                    $(this).addClass('missingValue');
                    $('#submitEditUser').addClass('error');
                    delete changeStack[this.id];
                }

                if (!$.isEmptyObject(changeStack) && !$('#submitEditUser').hasClass('error')){
                    $('#submitEditUser').addClass('changed');
                } else {
                    $('#submitEditUser').removeClass('changed');
                }
               self.getModel().getData()["set"+event.target.id.split('_')[1].ucfirst()](currentVal,"noTrigger");
            });
        };
    }
    
    // public API

    /**
     * userEditView_event
     */
    UserEditPresenter.prototype.userEditView_event = function(event){
          event.element.stopPropagation();
        try {
            this["" + event.element.target.id](event);
          
        } catch(e){}
    };

    /**
     * submitEditUser
     * @param event
     */
    UserEditPresenter.prototype.submitEditUser = function(event){
    	var self = this;
        if ( $('#submitEditUser').hasClass('changed')) {
        	
            this.getModel().getData().update(function(model){
            	 self.destroyView();
            });
        }
    };

    /**
     * cancelEditUser
     * @paran event
     */
    UserEditPresenter.prototype.cancelEditUser = function(event){
        $('[id^=edit]').removeClass('active');
        $('#action_'+this.getUserId()).removeClass('changed');
        this.destroyView();
    };
    
    return UserEditPresenter;
})();