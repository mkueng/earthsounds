/**
 * UserCreatePresenter
 * (c) kuema 2017
 * 
 * @binding createUser_control_event_propagate
 * 
 */
var UserCreatePresenter = (function(){

    UserCreatePresenter.prototype = new SMVP.Presenter(namespace.userCreateViewModel);

    /**
     * constructor
     */
    function UserCreatePresenter(){
    	var self=this;
    	this.mandatory = [];
    
    	/**
    	 * bind createUser_control_event_propagate
    	 */
        $(document).bind("createUser_control_event_propagate", function(){
        	var newModel = {};
        	$.each(namespace.domainModel.user, function(key,val){
        		newModel[key] = val.value;
        		if (this.mandatory === true) self.mandatory.push(key);
        	});
        	console.log("newModel:", newModel);
            self.getModel().setData(new SMVP.Model(newModel));
        	
            self.renderView(self, function(){
            	$('#newUserForm :input').each(function() {
            		 if ($(this).is('select') !== true) $(this).addClass('missingValue');
     	        });
            	self.showView();
    		   
        		$('#newUserForm :input').keyup(function(event) {
                	validateFormInput(this);
                });
                
                $('#newUserForm :input').change(function(event) {
                	validateFormInput(this);
                });
        	});
        });
        
        /**
         * validate form input
         */
        var validateFormInput = function(event){
            var changeStack = {};
            var currentVal = $(event).val();
              
            if(Utilities.isValid(event) === true) {
                changeStack[event.id] = 'changed';
                $(event).removeClass('missingValue');
                $('#submitNewUser').removeClass('error');
            
            } else if (Utilities.isValid(event) === false){
                $(event).addClass('missingValue');
                $('#submitNewUser').addClass('error');
                delete changeStack[event.id];
            }
               
            self.getModel().getData()["set"+event.id.split('_')[1].ucfirst()](currentVal,"noTrigger");
        };
    }
    
    // public API
    
    /**
     * userCreateView_event
     * @param event
     */
    UserCreatePresenter.prototype.userCreateView_event = function(event){
        try {
            this["" + event.element.target.id](event);
        } catch(e){}
    };

    /**
     * cancel new user creation
     */
    UserCreatePresenter.prototype.cancelNewUser = function() {
        this.destroyView();
        this.hideView();
    };

    /**
     * submit new user
     */
    UserCreatePresenter.prototype.submitNewUser = function() {
        var self = this;
        console.log("user:", self.getModel().getData().getObjectRepresentation());
        if (!$('#newUserForm :input').hasClass('missingValue')) {
        	self.getModel().getData().post();
        } 
    };

    return UserCreatePresenter;
})();