/**
 * UserLocationPresenter
 * (c) kuema 2017
 * 
 * @binding userFilterFocus
 * @binding user_edit_event_propagate
 * @binding user_info_event_propagate
 * @binding markerUpdateEvent
 * @binding user_location_event_propagate
 * 
 * @trigger setMapCenter
 * @trigger updateMarker
 * 
 */
var UserLocationPresenter = (function(){

    UserLocationPresenter.prototype = new SMVP.Presenter(namespace.userLocationViewModel);
    
    playerLocationModel = null;

    /**
     * constructor
     */
    function UserLocationPresenter(){
    	var self=this;
        var viewOffset = 30;
        var domainModel = {};
        var isShowCurrentUserAgentPosition  = false;
        var isUpdateCurrentUserAgentPosition = false;
        var updateIntervalId = null;
        this.userAgentPositionModel = new SMVP.Model(namespace.domainModel.userAgentPosition)
        
    	$.each(namespace.domainModel.playerLocation, function(key,val){
    		domainModel[key] = val.value;
    	});
    	
    	self.getModel().setData(new SMVP.Model(domainModel));
        
    	/**
    	 * bind toggleCurrentAgentPosition click
    	 */
    	$('#toggleCurrentAgentPosition').on('click', function(){
    		
    		if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
    			toggleCurrentAgentPositionInterval();
    		} else {
    			toggleCurrentAgentPosition();
    		}
    	})
    	
    	/**
    	 * bind toggleCurrentAgentPosition double click
    	 */
    	$('#toggleCurrentAgentPosition').on('dblclick', function(){
    		toggleCurrentAgentPositionInterval();
    	})
    	
    	
    	/**
    	 * toggleCurrentAgentPosition
    	 */
    	var toggleCurrentAgentPosition = function(){
    		
	   		if (!isShowCurrentUserAgentPosition) {
	   			self.showCurrentUserAgentPosition();
	   		} else {
	   			self.hideCurrentUserAgentPosition();
	   		}
	   		$('#toggleCurrentAgentPosition').toggleClass('toggleCurrentAgentPositionOneClick');
	   		isShowCurrentUserAgentPosition = !isShowCurrentUserAgentPosition;
    	}
    	
    	/**
    	 * toggleCurrentAgentPositionInterval
    	 */
    	var toggleCurrentAgentPositionInterval = function(){
    		$('#toggleCurrentAgentPosition').toggleClass('toggleCurrentAgentPositionDoubleClick');
    		if (!isUpdateCurrentUserAgentPosition) {
    			updateIntervalId = setInterval(function(){self.updateCurrentUserAgentPosition(self);}, 1500);
    		} else {
    			clearInterval(updateIntervalId);
    		}
    		if (!isShowCurrentUserAgentPosition) {
    			self.showCurrentUserAgentPosition();
    		} 
    		$('#toggleCurrentAgentPosition').addClass('toggleCurrentAgentPositionOneClick');
    		isShowCurrentUserAgentPosition = true;
    		isUpdateCurrentUserAgentPosition = !isUpdateCurrentUserAgentPosition;
    	}
    	
    	  if (namespace.clearance != "administrator") {
    		  $('#toggleCurrentAgentPosition').dblclick();
		  }
    	
    	/**
    	 * bind userFilterFocus
    	 */
    	$(document).bind("userFilterFocus", function(o,event){
			$('[id^=location]').removeClass('active');
    		self.destroyView();
    	});

    	/**
    	 * bind user_edit_event_propagate
    	 */
        $(document).bind("user_edit_event_propagate", function(o,data){
            self.destroyView();
        });

        /**
         * bind user_info_event_propagate
         */
        $(document).bind("user_info_event_propagate", function(o,data){
            self.destroyView();
        });
        
        /**
         * bind markerUpdateEvent
         */
        $(document).bind("markerUpdateEvent", function(o,data){
        	self.getModel().getData().setId(data.id);
        	self.getModel().getData().setLatitude(parseFloat(data.lat));
        	self.getModel().getData().setLongitude(parseFloat(data.lng));
            
            //update player location
        	self.getModel().getData().update(function(model,response){
        		console.log("response:", response);
        		if (typeof response.actionContent !='undefined' && response.actionContent !="") {
        			$(document).trigger('territoryAction', {actionId: response.actionId, actionContent: response.actionContent, type: response.type});
        		}
            	self.getModel().getData().setId(data.id);
            	
            	self.getModel().getData().setEndpoint("/location?limit=10");
            	self.getModel().getData().fetch(function(response){
            		self.destroyView(function(){
            		      self.getView().setContainer("userLocationContainer_"+data.id);
                            self.renderView(self, function(){
                            	
                            	$('[id^=editPlayer]').keyup(function(event) {
                            		validateFormInput(this);
                            	}); 
                            });
                            //self.animateView(data.event, $('#userListContainer'), viewOffset);
                          self.getModel().getData().setEndpoint("/location");
        	         });
            	});
            });
        });
       
        /**
         * bind user_location_event_propagate
         */
    	$(document).bind("user_location_event_propagate", function(o,data){
    		
    		self.getModel().getData().setId(data.model.getId());
    		self.getModel().getData().setEndpoint("/location?limit=10");
    		self.getModel().getData().fetch(function(response){
    			if (namespace.playerPresenter.isPlayerShown(data.model.getId())) {
    				$(document).trigger("setMapCenter", {latitude:self.getModel().getData().getLatitude(), longitude:self.getModel().getData().getLongitude()});
    			}
    			self.getModel().getData().setEndpoint("/location");
    			$('#'+data.event.element.target.id).addClass('active');
    			self.destroyView();
    			self.getView().setContainer("userLocationContainer_"+data.model.getId());
    	        self.renderView(self, function(){
    	        	$('[id^=editPlayer]').keyup(function(event) {
    	        		validateFormInput(this);
    	            });
    	        	
    	        	$('[id^=editPlayer]').change(function(event) {
    	        		validateFormInput(this);
    	            });
    	        	self.animateView(data.event, $('#userListContainer'), viewOffset);
    	        });
    		});
    	});
    	
    	/**
    	 * validate form input
    	 */
    	var validateFormInput = function(event){
    		
    		var changeStack = {};
            var currentVal = $(event).val();
            var actualVal = self.getModel().getData()["get"+event.id.split('_')[1].ucfirst()]();
            
            if (currentVal !== actualVal && currentVal !== "") {
            	$('#submitEditPlayer').addClass('changed');
            } else {
            	$('#submitEditPlayer').removeClass('changed');
            }
            
            if(Utilities.isValid(event)) {
                changeStack[event.id] = 'changed';
                $(event).removeClass('missingValue');
                $('#submitEditPlayer').removeClass('error');
            } else {
                $(event).addClass('missingValue');
                $('#submitEditPlayer').addClass('error');
                delete changeStack[event.id];
            }
            
            self.getModel().getData()["set"+event.id.split('_')[1].ucfirst()](parseFloat(currentVal),"noTrigger");
        };	
        
        
    }
    
    // public API
    
    /**
     * userLocationView_event
     * @param event
     */
    UserLocationPresenter.prototype.userLocationView_event = function(event){
    	event.element.stopPropagation();
    	try {
    		this["" + event.element.target.id](event);
    	} catch(e){}
    };
    
    /**
     * cancel edit player
     * @param event
     */
    UserLocationPresenter.prototype.cancelEditPlayer = function(event){
		$('[id^=location]').removeClass('active');
	    this.destroyView();
	};
	
	/**
	 * show current user agent location
	 */
	UserLocationPresenter.prototype.showCurrentUserAgentPosition = function(){
		var self = this;
		if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position){
            	self.userAgentPositionModel.setLatitude(position.coords.latitude.toFixed(5));
            	self.userAgentPositionModel.setLongitude(position.coords.longitude.toFixed(5));
            	
            	self.userAgentPositionModel.update(function(model,response){
            		if (typeof response.actionContent !='undefined' && response.actionContent !="") {
            			$(document).trigger('territoryAction', {actionId: response.actionId, actionContent: response.actionContent});
            		}
            	});
         	    $(document).trigger("setMapCenter", position.coords);
         	    $(document).trigger("setMapZoomLevel", 14);
         	    $(document).trigger("createMarker", {type:"currentAgentLocation", latLng:position.coords})
            });
           
        } else {
        }
	}
	
	/**
	 * hide current user agent location
	 */
	UserLocationPresenter.prototype.hideCurrentUserAgentPosition = function(){
		  $(document).trigger("deleteMarker", {type:"currentAgentLocation", id:"currentLocationMarker"})
	}
	
	/**
	 * update current user agent location
	 */
	UserLocationPresenter.prototype.updateCurrentUserAgentPosition = function(self){
		
		if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position){
            	self.userAgentPositionModel.setLatitude(position.coords.latitude.toFixed(5));
            	self.userAgentPositionModel.setLongitude(position.coords.longitude.toFixed(5));
            	
            	self.userAgentPositionModel.update(function(model,response){
            		if (typeof response.actionContent !='undefined' && response.actionContent !="") {
            			$(document).trigger('territoryAction', {actionId: response.actionId, actionContent: response.actionContent});
            		}
            	});
         	    $(document).trigger("setMapCenter", position.coords);
         	    $(document).trigger("createMarker", {type:"currentAgentLocation", latLng:position.coords})
            });
        }
	}
	
	
	/**
	 * submit edit player
	 * @param event
	 */
	UserLocationPresenter.prototype.submitEditPlayer = function(event){
		var self = this;
		self.getModel().getData().update(function(){
			if (namespace.playerPresenter.isPlayerShown(self.getModel().getData().getId())) {
				$(document).trigger("updateMarker", {
			           type: 'player',
			           id : self.getModel().getData().getId(),
			           model: self.getModel().getData()
			       });
				$(document).trigger("setMapCenter", {latitude:self.getModel().getData().getLatitude(), longitude:self.getModel().getData().getLongitude()});
			}
		});
		$('[id^=location]').removeClass('active');
		this.destroyView();
	};
	
    return UserLocationPresenter;
})();