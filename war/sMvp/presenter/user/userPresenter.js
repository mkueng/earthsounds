/**
 * UserPrsenter
 * (c) kuema 2017
 * 
 * @binding collectionChanged_user
 * 
 * @trigger hideMarkers
 * @trigger userFilterFocus
 * @trigger createUser_control_event_propagate
 */
var UserPresenter = (function(){

    UserPresenter.prototype = new SMVP.Presenter(namespace.allAboutUserViewModel);

    var usersShown = {};
    var show = true;

    /**
     * @constructor
     */
    function UserPresenter(){
    	var self=this;

    	/**
    	 * bind collectionChanged_user
    	 */
        $(document).bind("collectionChanged_user", function(){
        	self.renderView(self,function(){
                self.setBindings();
           });
        });
     
        /**
         * bind standardDialogButtonYes click
         */
        $('#standardDialogButtonYes').bind('click',function(event){
            $(this).attr('modelType') == 'user'
                ? self[$(this).attr('modelAction')+'user'](  $(this).attr('modelId'))
                : false;
        });

        /**
         * set bindings
         */
        this.setBindings = function(){
            var showHide = $('#showHideUser');
            var userFilter =  $('#userFilter');

            for (var shown in usersShown) {
                $('#checkbox_'+shown).attr('checked', true).checkboxradio('refresh');
            }
            $('#_li').off('click');
            $(showHide).off("click");
            $(userFilter).off("click");

            $(showHide).bind('change',function(event){
                show = !show;
                show === true ? $(document).trigger("showMarkers","user") : $(document).trigger("hideMarkers","user");
                $("#allAboutUserContainer").toggleClass("allAboutUserContainer");
                $("#userListContainer").toggle();
                $("#userOptionsContainer").toggle();
                $("#userCreateContainer").hide();

            });

            $(userFilter).on("focus", function(){
                $(document).trigger("userFilterFocus");
            });
        };

        /**
         * render view
         */
        self.renderView(self,function(){
           self.setBindings();
           $('#showHideUser').change();
        });
    }
  
    // public API

    /**
     * user control (create)
     * @param event
     */
    UserPresenter.prototype.userControlView_event = function(event){
    	$(document).trigger("createUser_control_event_propagate", event);
    };

    /**
     * all about user (info, edit)
     * @param event
     */
    UserPresenter.prototype.allAboutUserView_event = function(event){
    	if (event.element.target.type === "button" ) {
		    $('[id^=edituser_]').removeClass('active');
		    $('[id^=infouser_]').removeClass('active');
		    $('[id^=locationuser_]').removeClass('active');
        	event.element.preventDefault();
        	//invoke method according to event
            this[""+event.element.target.id.split("_")[0]](event);
        
        } else {
        	if (typeof event.element.target.firstChild.id != 'undefined' && event.element.target.firstChild.id.split('_')[0] == 'actionToggleUser'){
        		event.element.preventDefault();
        	} else if (event.element.target.id.split('_')[1] !== undefined) {
        		event.element.preventDefault();
            	$('#'+event.element.target.id).parent().toggleClass('actionToggle');

        		this.showUser(event);
        		
	        }
        }
    };
    
    /**
     * action toggle
     * @param event
     */
    UserPresenter.prototype.actionToggleUser = function(event){
		$('#'+event.element.target.id).parent().toggleClass('ui-icon-carat-r ui-icon-carat-d');
		$('#actionToggleButton_'+event.element.target.id.split('_')[1]).toggle();
		$(document).trigger("userFilterFocus");
	};

    /**
     * show user
     * @param event
     */
    UserPresenter.prototype.showUser = function(event){
    	var id = event.element.target.id.split("_")[1];
		namespace.playerPresenter.togglePlayer(id);
    };
	
    /**
     * user info
     * @param event
     */
    UserPresenter.prototype.infouser = function(event){
        this.triggerPropagate(event, "info");
    };

    /**
     * user edit
     * @param event
     */
    UserPresenter.prototype.edituser = function(event){
        this.triggerPropagate(event, "edit");
    };
    
    /**
     * user location
     * @param event
     */
    UserPresenter.prototype.locationuser = function(event) {
    	this.triggerPropagate(event, "location");
    };

    /**
     * user delete request
     * @param event
     */
    UserPresenter.prototype.minususer = function(event){
        event.element.stopPropagation();
        var id = event.element.target.id.split("_")[1];
        var name = this.getModel().getData().readModel(id).getNickName();
        var dialogButton =  $('#standardDialogButtonYes');
        $('#standardDialogContentText').html('sure to delete user: '+name+'?');

        $(dialogButton).attr('modelId', id);
        $(dialogButton).attr('modelType', "user");
        $(dialogButton).attr('modelAction', "delete");
        $('#standardDialogLink').click();
    };

    /**
     * user delete
     * @param modelId
     */
    UserPresenter.prototype.deleteuser = function(modelId){
    	this.getModel().getData().readModel(modelId).destroy();
    };

    /**
     * trigger propagate
     * @param event
     * @param type
     */
    UserPresenter.prototype.triggerPropagate = function(event,type){
        var id = event.element.target.id.split("_")[1];
        event.element.stopPropagation();
        $(document).trigger("user_"+type+"_event_propagate",{
            event:event,
            model:this.getModel().getData().readModel(id)
        });
    };

    return UserPresenter;
})();