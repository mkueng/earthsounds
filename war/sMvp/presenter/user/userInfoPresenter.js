/**
 * UserInfoPresenter
 * (c) kuema 2017
 * 
 * @binding userFilterFocus
 * @binding user_edit_event_propagate
 * @binding user_location_event_propagate
 * @binding user_info_event_propagate
 * 
 */
var UserInfoPresenter = (function(){

    UserInfoPresenter.prototype = new SMVP.Presenter(namespace.userInfoViewModel);

    function UserInfoPresenter(){
    	var self=this;
        var viewOffset = 30;
    	
    	/**
    	 * bind userFilterFocus
    	 */
    	$(document).bind("userFilterFocus", function(o,event){
			$('[id^=info]').removeClass('active');
    		self.destroyView();
    	});

    	/**
    	 * bind user_edit_event_propagate
    	 */
        $(document).bind("user_edit_event_propagate", function(o,data){
            self.destroyView();
        });

        /**
         * bind user_location_event_propagate
         */
        $(document).bind("user_location_event_propagate", function(o,data){
            self.destroyView();
        });
        
        /**
         * bind user_info_event_propagate
         */
    	$(document).bind("user_info_event_propagate", function(o,data){
            $('[id^=info]').removeClass('active');
            $('#'+data.event.element.target.id).addClass('active');
           // data.model.fetch(function(response){
            	self.getModel().setData(data.model);
            	self.destroyView();
    	        self.getView().setContainer("userInfoContainer_"+data.model.getId());
    	        self.renderView(self);
    	        self.animateView(data.event, $('#userListContainer'), viewOffset);
            //});
            
	        
	        // $(document).trigger("setMapCenter", self.getModel().getData().getPosition())
    	});
    }
    
    return UserInfoPresenter;
})();