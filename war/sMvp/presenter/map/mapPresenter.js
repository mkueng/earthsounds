/**
 * MapPresenter
 * (c) kuema 2017
 * 
 * @binding setMapCenter
 * @binding windowResize
 * 
 * @listener map click
 * @listener map mousemove
 * @listener map dragend
 * @listener map zoom_changed
 * 
 * @trigger mapDragend
 * @trigger mapClicked
 * @trigger mapZoomChanged
 */
var MapPresenter = (function(){

	MapPresenter.prototype = new SMVP.Presenter(namespace.mapViewModel);

	var map = null;
	var mapZoomlevel = null;
	var mapZoomMax = 20;
	var mapZoomMin = 3;
    
	/**
     *
     * @constructor
     */
    function MapPresenter(){
    	var self=this;
    	
    	if (namespace.clearance != "administrator") {
    		mapZoomMin = 13;
    	}

        //application events

        $(document).bind("setMapCenter", function(o,data){
            self.setMapCenterPosition(data.latitude, data.longitude);
        });
        
        $(document).bind("setMapZoomLevel", function(o,data){
            self.setMapZoomLevel(data);
        });
        
        self.renderView(function(){
    	   
    	   /**
    	    * bind window resize
    	    */
    	   $(document).bind("windowResize", function(){
    		   $(document).trigger("mapDragend", {center: map.getCenter(), boundaries:map.getBounds(), zoomLevel: map.getZoom()});
           });
    	   
	       /**
	        * bind map click
	        */
           google.maps.event.addListener(map,"click",function(event){
        	   $(document).trigger("mapClicked",{latLng:event.latLng});
           });

    	   /**
    	    * bind mouse move 
    	    */
           google.maps.event.addListener(map,"mousemove",function(event){
           });

           /**
            * bind map drag
            */
           google.maps.event.addListener(map, 'dragend', function() {
               $(document).trigger("mapDragend", {center: map.getCenter(), boundaries:map.getBounds(), zoomLevel: map.getZoom()});
           });

           /**
            * bind map zoom changed
            */
           google.maps.event.addListener(map, 'zoom_changed', function () {
        	   mapZoomLevel=map.getZoom();
               $(document).trigger("mapZoomChanged", {center: map.getCenter(), boundaries:map.getBounds(), zoomLevel: map.getZoom()});
           });
       });
    }

    // public API
    
    MapPresenter.prototype = {

        /**
         * render view overwrite super
         * @param callback
         */
        renderView : function(callback){
            map= new google.maps.Map(document.getElementById("map-canvas"),{
                zoom: 9,
    	    	minZoom:mapZoomMin,
    	    	maxZoom:mapZoomMax,
    	    	scaleControl: false,
    	    	panControl : false,
    	    	scrollwheel: true,
    	    	disableDefaultUI: false,
    	    	zoomControl:false,
    	    	clickableIcons : false,
    	    	center: new google.maps.LatLng(47,7),
    	    	mapTypeId: google.maps.MapTypeId.ROADMAP,
    	    	streetViewControl: false
    	    });
            mapZoomLevel = map.getZoom();
            if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
            	map.set("disableDoubleClickZoom", true);
            };
            callback();
    	},

        /**
         * get map
         * @returns map
         */
        getMap : function(){
            return map;
        },

        /**
         * get map zoom level
         * @returns mapZoomLevel
         */
        getMapZoomLevel : function(){
        	return mapZoomLevel;
        },
        
        /**
         * set map zoom level
         * @param zoomLevel
         */
        setMapZoomLevel : function(zoomLevel){
        	map.setZoom(zoomLevel);
        },
        
        /**
         * add object
         * @param object
         */
        addObject : function(object){
            object.setMap(map);
        },

        /**
         * remove object
         * @param object
         */
        removeObject : function(object){
            object.setMap(null);
        },

        /**
         * set map center
         * @param latitude
         * @param longitude
         */
        setMapCenterPosition : function(latitude, longitude) {
            map.panTo(new google.maps.LatLng(latitude, longitude));
        }
    };
    
    return MapPresenter;
})();