/**
 * PlayerPresenter
 * (c) kuema 2017
 * 
 * @trigger setMapCenter
 * 
 */
var PlayerPresenter = (function(){

    PlayerPresenter.prototype = new SMVP.Presenter(namespace.allAboutPlayerViewModel);

    var playerModels = {};
    var playersShown = {};
    var show = true;

    /**
     * @constructor
     */
    function PlayerPresenter(){
    }
 
    //public API
    
    /**
     * isPlayerShown
     * @param playerId
     */
    PlayerPresenter.prototype.isPlayerShown = function(playerId){
    	return playersShown[playerId];
    };
    
    /**
     * player toggle
     * @param id
     */
    PlayerPresenter.prototype.togglePlayer = function(id){
    	var self = this;
    	if (!playersShown[id]) {
	    	if (playerModels[id]) {
	    		playerModels[id].fetch(function(response){
	    			self.triggerMarker(response, 'createMarker');
	    		});
	    		playersShown[id] = true;
	    	} else {
	    		self.instantiatePlayerModel(id, function(response){
	    			self.triggerMarker(response, 'createMarker');
	    		});
	    		playersShown[id] = true;
	    	}
    	} else {
			self.triggerMarker(playerModels[id], 'deleteMarker');
			delete playersShown[id];
    	}
    };

    /**
     * instantiate player model
     * @param playerId
     * @param callback
     */
    PlayerPresenter.prototype.instantiatePlayerModel = function(playerId, callback){
    	var model = {};
		$.each(namespace.domainModel.player, function(key,val){
    		model[key] = val.value;
    	});
		playerModels[playerId] = new SMVP.Model(model);
		playerModels[playerId].setId(playerId);
		playerModels[playerId].fetch(function(response){
			callback(response);
		});
    };
    
    /**
     * trigger marker
     * @param playerModel
     * @param action
     */
    PlayerPresenter.prototype.triggerMarker = function(playerModel, action){
    	$(document).trigger(action, {
    		type: 'player',
    		id : playerModel.getId(),
    		model: playerModel
    	});
    	if (action == 'createMarker') {
    		$(document).trigger("setMapCenter", {latitude:playerModel.getLatitude(), longitude:playerModel.getLongitude()});
    	}
    };
    
    return PlayerPresenter;
})();