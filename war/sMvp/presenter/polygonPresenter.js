/**
 * PolygonPresenter
 * (c) kuema 2017
 * 
 *  @binding cancelNewRegion
 *  
 *  @listener google.maps.event polygon click
 *  @listener google.maps.event polygon moouseover
 *  @listener google.maps.event polygon mouseout
 *	
 *	@trigger polygonClickNewRegionEvent
 *	@trigger polygonMouseOverNewRegionEvent
 *	@trigger polygonMouseOutNewRegionEvent
 *	@trigger polygonClickExistingRegionEvent
 *	@trigger polygonMouseOverExistingRegionEvent
 *	@trigger polygonMouseOutExistingRegionEvent
 */
var PolygonPresenter = (function(){

    PolygonPresenter.prototype = new SMVP.Presenter(namespace.polygon);

    var map=null;

    /**
     * @constructor
     */
    function PolygonPresenter(){
        var self = this;
        map = namespace.mapPresenter.getMap();

        /**
         * bind cancelNewRegion
         */
        $(document).bind("cancelNewRegion", function(){
            self.removePolygons("newRegion");
        });
    }

    //public API
    
    /**
     * set properties
     * @param id
     * @param type
     * @param properties
     */
    PolygonPresenter.prototype.setProperties = function(id, type, properties){
        var model = this.getModel();
        
        switch(type) {
            case "newRegion": {
                properties.fillColor = model.getStatusColors()[properties.status];
                model.getNewRegion().polygons[id].setOptions(properties);
                break;
            }
            case "existingRegion": {
            	properties.fillColor = model.getStatusColors()[properties.status];
            	model.getExistingRegions().polygons[properties.region][id].setOptions(properties);
            }
        }
    };

    /**
     * render polygon
     * @param polygon
     */
    PolygonPresenter.prototype.renderPolygon = function(polygon){
        polygon.setMap(map);
    };

    /**
     * render polygons
     * @param polygons
     */
    PolygonPresenter.prototype.renderPolygons = function(polygons){
        $(polygons).each(function(){
            this.setMap(map);
        });
    };
    
    /**
     * remove polygon from map
     * @param polygon
     */
    PolygonPresenter.prototype.remove = function(polygon) {
    	polygon.setMap(null);
    };
    
    /**
     * remove polygon from map
     * @param id
     * @param type
     * @param region
     */
    PolygonPresenter.prototype.removePolygon = function(id, type, region){
        var model = this.getModel();
        var polygons = null;
        
        switch(type) {
            case "newRegion" : {
                polygons = model.getNewRegion().polygons;
                this.unbindMouseEvents(polygons[id]);
                polygons[id].setMap(null);
                delete  model.getNewRegion().polygons[id];
                break;
            }
            case "existingRegion" : {
                polygons = model.getExistingRegions().polygons[region];
                this.unbindMouseEvents(polygons[id]);
                polygons[id].setMap(null);
                delete  model.getExistingRegions().polygons[region][id];
                break;
            }
        }
    };

    /**
     * remove polygons
     * @param type
     * @param region
     */
    PolygonPresenter.prototype.removePolygons = function(type, region){
        var model = this.getModel();
        var self = this;
        var polygons = null;
        
        switch(type) {
            case "newRegion" : {
                polygons = model.getNewRegion().polygons;
                $.each(polygons,function (key,value) {
                    self.unbindMouseEvents(value);
                    value.setMap(null);
                });
                model.getNewRegion().polygons = {};
                break;
            } 
            case "existingRegion" : {
            	polygons = model.getExistingRegions().polygons[region];
                if (typeof polygons != 'undefined') {
                    $.each(polygons,function (key,value) {
                        self.unbindMouseEvents(value);
                        value.setMap(null);
                        namespace.markerPresenter.deleteMarker({type:"territorySoundAction", id:value.id})
                    });
                }
                model.getExistingRegions().polygons[region] = {};
                break;
            }
        }
    };

    /**
     * creat polygon from geo points
     * @param geoPoints
     * @return google.maps.Polygon
     */
    PolygonPresenter.prototype.createPolygonFromGeoPoints = function(geoPoints, region){
    	
    	region = region || {};
   
    	  var  polygon = new google.maps.Polygon({
              paths: geoPoints,
              strokeColor : "#000000",
              fillColor : "#bccbff",
              fillOpacity: 0.5,
              strokeWeight: 0.5,
              strokeOpacity: 1.0,
              type: 'regionOutline',
              region: region.name || "",
              id : region.id || ""
              
          });
    	  this.bindMouseEvents(polygon);
    	  return polygon;
    };
   
    /**
     * create polygon
     * @param territory
     * @returns google.maps.Polygon
     */
    PolygonPresenter.prototype.createPolygon = function(territory){
    	
        var model = this.getModel();
        var type = null;

        type = territory.region === "" ? "newRegion" : territory.region;
        
           var  polygon = new google.maps.Polygon({
                paths: territory.geoPoints,
                id : territory.id,
                type : type,
                status : territory.status,
                strokeColor : "#000000",
                fillColor : model.getStatusColors()[territory.status],
                strokeWeight: 0.6,
                clickable: true
            });

            model.getNewRegion().polygons[territory.id] = polygon;
            this.bindMouseEvents(polygon);

        return polygon;
    };

    /**
     * create polygons from territory models
     * @param territories as SMVP models
     * @param type
     * @returns array of google.maps.Polygon
     */
    PolygonPresenter.prototype.createPolygonsFromTerritories = function(territories, type, region){
    	
    	var self = this;
        var model = this.getModel();
        var polygon = null;
       
        var latlng = {};
        var path = [];
        var polygons = [];

        switch (type){
            case "newRegion" : {
            	
            	$.each(territories, function(){
                    polygon = new google.maps.Polygon({
                        id : this.id,
                        type : "newRegion",
                        status : this.status,
                        paths: this.geoPoints,
                        strokeColor : "#000000",
                        fillColor : model.getStatusColors()["FREE"],
                        strokeWeight: 0.6,
                        clickable: true
                    });
                    model.getNewRegion().polygons[this.id]= polygon;
                    self.bindMouseEvents(polygon);
                    polygons.push(polygon);
                   
            	});
            	break;
            }
            
            case "existingRegion" : {
            	var existingRegionsPolygons = model.getExistingRegions().polygons;
            	if (typeof existingRegionsPolygons[region] == 'undefined') {
            		existingRegionsPolygons[region] = {};
            	}
            	
            	$.each(territories, function(){
            		$.each(this.geoPoints, function(){
            		    if (typeof this.latitude != 'undefined'){
                            latlng["lat"] = this.latitude;
                            latlng["lng"] = this.longitude;
                            path.push(latlng);
            		    } else {
            		        latlng["lat"] = this.lat();
            		        latlng["lng"] = this.lng();
            		        path.push(latlng);
            		    }
            			
            			latlng = {};
            		});
            		
                    polygon = new google.maps.Polygon({
                        id : this.id,
                        action : this.action,
                        actionContent : this.actionContent,
                        region : this.region,
                        type : "existingRegion",
                        status : this.status,
                        paths: path,
                        strokeColor : "#000000",
                        fillColor : model.getStatusColors()[this.status],
                        strokeWeight: 0.6,
                        clickable: true
                    });
            		namespace.territories.getExistingRegions()[this.region][this.id] = this;
            		if (polygon.action != "") {
            			
            			bla = {};
                		bla.latitude = parseFloat(this.id.split('_')[0]);
                		bla.longitude = parseFloat( this.id.split('_')[1]);
                		
                		namespace.markerPresenter.createMarker({type:"territorySoundAction", latLng: bla, id : this.id, actionContent: this.actionContent})
            		}
            		
                    model.getExistingRegions().polygons[this.region][this.id]= polygon;
                    self.bindMouseEvents(polygon);
                    polygons.push(polygon);
                    path = [];
            	});
            	break;
            }
        }
        return polygons;
    };

    /**
     * bind mouse events to polygon
     * @param polygon
     */
    PolygonPresenter.prototype.bindMouseEvents = function(polygon) {
        var model = this.getModel();
        
        switch (polygon.type) {
            case "newRegion" : {
            	//click
                google.maps.event.addListener(polygon, 'click', function (event) {
                    $(document).trigger("polygonClickNewRegionEvent", this);
                });
                //mouseover
                google.maps.event.addListener(polygon, 'mouseover', function (event) {
                    this.setOptions({
                        fillColor: model.getNewRegion().mouseInFillColor
                    });
                    $(document).trigger("polygonMouseOverNewRegionEvent", this);
                });
                //mouseout
                google.maps.event.addListener(polygon,'mouseout', function(event){
                    this.setOptions({
                        fillColor: model.getStatusColors()[polygon.status]
                    });
                    $(document).trigger("polygonMouseOutNewRegionEvent", this);
                });
                break;
            }
            
            case "existingRegion" : {
            	//click
            	if (namespace.clearance == 'administrator') {
	            	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
	            		  google.maps.event.addListener(polygon, 'dblclick', function (event) {
	                          $(document).trigger("polygonClickExistingRegionEvent", this);
	                      });
	            	} else {
	            		google.maps.event.addListener(polygon, 'click', function (event) {
	                        $(document).trigger("polygonClickExistingRegionEvent", this);
	                    });
	            	}
            	}
              
                //mouseover
            	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
            		 google.maps.event.addListener(polygon, 'click', function (event) {
 	                	this.setOptions({
 	                        fillColor: model.getNewRegion().mouseInFillColor
 	                    });
 	                    $(document).trigger("polygonMouseOverExistingRegionEvent", this);
 	                });
            	} else {
	                google.maps.event.addListener(polygon, 'mouseover', function (event) {
	                	this.setOptions({
	                        fillColor: model.getNewRegion().mouseInFillColor
	                    });
	                    $(document).trigger("polygonMouseOverExistingRegionEvent", this);
	                });
            	}
                //mouseout
                google.maps.event.addListener(polygon,'mouseout', function(event){
                    this.setOptions({
                        fillColor: model.getStatusColors()[polygon.status]
                    });
                    $(document).trigger("polygonMouseOutExistingRegionEvent", this);
                });
                break;
            }
            
            case "regionOutline" : {
            	  //mouseover
            	  google.maps.event.addListener(polygon, 'mouseover', function (event) {
            		  if (typeof (namespace.regionPresenter.getActiveShownRegions()[this.id]) == 'undefined') {
            			  this.setOptions({
            				  fillColor: model.getRegionOutline()["mouseInFillColor"]
            			  });
                      }
                  });
            	  
            	   //mouseout
                  google.maps.event.addListener(polygon,'mouseout', function(event){
                	  if (typeof (namespace.regionPresenter.getActiveShownRegions()[this.id]) == 'undefined') {
                		  this.setOptions({
                			  fillColor: model.getRegionOutline()["mouseOutFillColor"]
                		  });
                	  }
                  });
                  
                //click
                 
	                  google.maps.event.addListener(polygon, 'click', function (event) {
	                	  var element = {};
	                	  element.target= {};
	                	  element.target.id="region_"+this.id;
	                	  namespace.regionPresenter.toggleActiveRegion({element:element});
	                	  var fillcolors = typeof (namespace.regionPresenter.getActiveShownRegions()[this.id]) == 'undefined' 
	                		? "#cddc85"
	                	  	: model.getRegionOutline()["regionSelectedFillColor"]
	                	  this.setOptions({
	                		  fillColor: fillcolors
	   	                  });	
	                	  $(document).trigger("regionPolygonClicked", this);
	                	
	                	  
	                  });
                  
                  break;
            }
        }
    };
    
    /**
     * unbind mouse events from polygon
     * @param polygon
     */
    PolygonPresenter.prototype.unbindMouseEvents = function(polygon){
        google.maps.event.clearListeners( polygon,'mouseover');
        google.maps.event.clearListeners( polygon,'mouseout');
        google.maps.event.clearListeners( polygon,'click');
    };

    return PolygonPresenter;
})();