$(document).on('pageinit','#map-page',function(e,data){
	 $.getJSON('sMvp/model/domainModel.json', function(response){
		 namespace.domainModel = response;
		 var cookieValue = "";
			var nameEQ = "clearance" + "=";
		    var ca = document.cookie.split(';');
		    for(var i=0;i < ca.length;i++) {
		        var c = ca[i];
		        while (c.charAt(0)==' ') c = c.substring(1,c.length);
		        if (c.indexOf(nameEQ) == 0) cookieValue =  c.substring(nameEQ.length,c.length);
		    }
		    namespace.clearance = cookieValue;
		    
		    
		    var nickName = "";
			var nameEQ = "nickName" + "=";
		    var ca = document.cookie.split(';');
		    for(var i=0;i < ca.length;i++) {
		        var c = ca[i];
		        while (c.charAt(0)==' ') c = c.substring(1,c.length);
		        if (c.indexOf(nameEQ) == 0) nickName =  c.substring(nameEQ.length,c.length);
		    }
		    namespace.nickName = nickName;
		    
		    
		    $('#logedinasnickname').text(namespace.nickName);
		    $('#logout').bind('click', function(){
		    	document.cookie = "nickName=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
		    	document.cookie = "clearance=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
		    	document.cookie = "tokenString=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
		    	
		    	window.location="index.html";
		    })
		    if (namespace.clearance != "administrator") {
		    	$('#toggleSideBar').hide();
		    	
		    	//$('#toggleCurrentAgentPosition').click();
		    	$('#toggleCurrentAgentPosition').hide();
		    }
		
		 rodrigez.appStart();
	 })
	 .error(function(data) {
		  console.log("Error!");
		  console.log(data);
		});
});