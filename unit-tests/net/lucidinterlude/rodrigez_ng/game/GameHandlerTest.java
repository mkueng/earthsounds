package net.lucidinterlude.rodrigez_ng.game;


import javax.persistence.EntityManager;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

//import com.google.appengine.api.datastore.DatastoreService;
//import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;

import net.lucidinterlude.rodrigez_ng.dto.PlayerDto;
import net.lucidinterlude.rodrigez_ng.dto.ResponseDto;
import net.lucidinterlude.rodrigez_ng.model.EMF;
import net.lucidinterlude.rodrigez_ng.model.Player;
import net.lucidinterlude.rodrigez_ng.model.User;
import net.lucidinterlude.rodrigez_ng.persistence.UserDao;

public class GameHandlerTest {
	private final LocalServiceTestHelper helper =
			new LocalServiceTestHelper(new LocalDatastoreServiceTestConfig()
            .setDefaultHighRepJobPolicyUnappliedJobPercentage(100));
	
	private String user_unoId;
	private String user_dueId;
	private UserDao userDao;
	private ResponseDto<?> responseDto;
	private User user_uno;
	private User user_due;
	@SuppressWarnings("unused")
	private static GameHandler cut; 
	//private Player player;
	//private Player target;
	
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		helper.setUp();
		//DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
		userDao = new UserDao();
		user_uno = new User();
		user_due = new User();
		user_uno.setFirstName("TestUser_uno");
		user_uno.setNickName("macpro");
		user_uno.setDeviceToken("211D7807B398CFACCEF8972913652514BE54367D32EA8D8662C516738571AD0A");
		user_due.setFirstName("TestUser_due");
		user_due.setNickName("s�mim�ller");
		user_due.setDeviceToken("A0BF225901971A701F2ED68396FE02D9059034DAE500C513F84D0E064F866E97");
		responseDto = userDao.createUser(user_uno);
		user_unoId = (String)responseDto.getResponseContent("id");
		responseDto = userDao.createUser(user_due);
		user_dueId = (String)responseDto.getResponseContent("id");
		
	}

	@After
	public void tearDown() throws Exception {
		
		userDao.deleteUser(user_unoId);
		userDao.deleteUser(user_dueId);
		helper.tearDown();
	}

	@Test
	public void test_definePlayerAndTargetStatus() {
		EntityManager em = EMF.get().createEntityManager();
		User player = em.find(User.class, user_uno.getId());
		User target = em.find(User.class, user_due.getId());
		Player player_uno = player.getPlayer();
		player_uno.setDeviceToken(player.getDeviceToken());
		player_uno.setNickName(player.getNickName());
		Player player_due = target.getPlayer();
		player_due.setDeviceToken(target.getDeviceToken());
		player_due.setNickName(target.getNickName());
		PlayerDto playerDto = new PlayerDto(player_uno);
		PlayerDto targetDto = new PlayerDto(player_due);
		cut = new GameHandler(playerDto,targetDto);		
	
	}

}
