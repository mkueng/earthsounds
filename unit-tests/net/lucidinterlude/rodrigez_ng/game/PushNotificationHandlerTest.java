package net.lucidinterlude.rodrigez_ng.game;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import net.lucidinterlude.rodrigez_ng.application.AllTests;

public class PushNotificationHandlerTest {
	private static PushNotificationHandler cut = null;
	String tokenMarco = AllTests.user_01.getDeviceToken();
	String tokenDirk  = AllTests.user_02.getDeviceToken();

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		cut=new PushNotificationHandler();
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test_SendPushNotification() {
		Assert.assertTrue(cut.sendPushNotification(tokenMarco,"sent from UnitTest"));
	}

}
