package net.lucidinterlude.rodrigez_ng.persistence;

import java.beans.IntrospectionException;

import net.lucidinterlude.rodrigez_ng.model.User;

import org.json.JSONObject;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.gson.Gson;

public class EntityCreatorTest {
	private static JSONObject validJsonObj = null;
	private static JSONObject invalidJsonObj = null;
	private static EntityCreator cut = null;
	
	static class ValidEntity {
		@SuppressWarnings("unused")
		private String lastName = "ValidUser_lastName";
		@SuppressWarnings("unused")
		private String firstName ="ValidUser_firstName";
		
	};
	
	static class InvalidEntity {
		@SuppressWarnings("unused")
		private String invalidFieldLastName = "InvalidUser_lastName";
		@SuppressWarnings("unused")
		private String invalidFieldFirstName ="InvalidUser_firstName";
	}
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		cut = new EntityCreator();
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		cut = null;
	}

	@Before
	public void setUp() throws Exception {
		validJsonObj 	= new JSONObject (new Gson().toJson(new ValidEntity()));
		invalidJsonObj 	= new JSONObject (new Gson().toJson( new InvalidEntity()));
	}

	@After
	public void tearDown() throws Exception {
		validJsonObj 	= null;
		invalidJsonObj	= null;
	}

	@Test
	public void testCreateEntity_WithValidJsonObject_ShouldReturnEntity() throws IntrospectionException {
		User validUser = null;
		validUser = (User)cut.createEntity(validJsonObj, User.class);
		Assert.assertNotNull(validUser);
	}
	@Test
	public void testCreateEntity_WithInvalidJsonObject_ShouldThrowException() {
		User invalidUser = null;
		try {
			invalidUser = (User)cut.createEntity(invalidJsonObj, User.class);
		} catch (IntrospectionException e) {
		}
		Assert.assertNull(invalidUser);
	}
	
}
