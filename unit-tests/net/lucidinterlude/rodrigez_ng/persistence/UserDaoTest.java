package net.lucidinterlude.rodrigez_ng.persistence;

import java.util.Date;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import net.lucidinterlude.rodrigez_ng.application.AllTests;
import net.lucidinterlude.rodrigez_ng.dto.ResponseDto;
import net.lucidinterlude.rodrigez_ng.dto.UserDto;
import net.lucidinterlude.rodrigez_ng.model.User;

public class UserDaoTest {
	
	
	private static UserDao cut;
	private static User user_01;
	private static User user_02;
	
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		user_01 = new User();	
      	user_02 = new User();
      
      	user_01.setFirstName("user_01_firstName");
      	user_01.setLastName("user_01_lastName");
      	user_01.setNickName("user_01_nickName");
      	user_01.setGender("m");
      	user_01.setBirthdate("21.03.1971");
      	user_01.setDeviceToken("211D7807B398CFACCEF8972913652514BE54367D32EA8D8662C516738571AD0A");
      	
      	user_02.setFirstName("user_02_firstName");
      	user_02.setLastName("user_02_lastName");
      	user_02.setNickName("user_02_nickName");
      	user_02.setGender("m");
      	user_02.setBirthdate("21.03.1971");
      	user_02.setDeviceToken("A0BF225901971A701F2ED68396FE02D9059034DAE500C513F84D0E064F866E97");
	
      	
      	System.out.println("UserDaoTest");
		AllTests.helper.setUp();		
		cut = new UserDao();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		user_01=null;
    	user_02=null;
    	
    	cut = null;
		AllTests.helper.tearDown();
	}

	@Before
	public void setUp() throws Exception {
		
	}

	@After
	public void tearDown() throws Exception {
		
	}

	@Test
	public void testCreateUser_shouldReturnId() {		
		String id  = (String)cut.createUser(user_01).getResponseContent("id");
		Assert.assertNotNull(id);
		cut.deleteUser(id);
	}
	
	@Test
	public void testGetUser_shouldReturnUserDto() throws Exception{
		String id = (String) cut.createUser(user_02).getResponseContent("id");	
		User responseDto = cut.getUser(id);
		Assert.assertEquals(responseDto.getDeviceToken(),user_02.getDeviceToken());
		cut.deleteUser(id);
	}
}
