package net.lucidinterlude.rodrigez_ng.persistence;

import net.lucidinterlude.rodrigez_ng.model.Region;
import net.lucidinterlude.rodrigez_ng.application.AllTests;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class RegionDaoTest {

	private static RegionDao regionDao;
	private static String validId_01;
	private static String validId_02;
	private static String notValidId;
	private static Region region_01;
	private static Region region_02;
	
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		regionDao = new RegionDao();
		AllTests.helper.setUp();
		
		
		region_01 = new Region();
		region_01.setName("Tessin");
		region_01.setMainGrid(0.0512);
	
		region_02 = new Region();
		region_02.setName("Zurich");
		region_02.setMainGrid(0.0512);
		
		try {
			validId_01=regionDao.setRegion(region_01).getId();
			validId_02=regionDao.setRegion(region_02).getId();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		try {
			regionDao.deleteEntityWithNoRelation(validId_01, Region.class);
			regionDao.deleteEntityWithNoRelation(validId_02, Region.class);
		} catch (Exception e) {
		}
		
		
		AllTests.helper.tearDown();
		regionDao = null;
		region_01 = null;
		region_02 = null;
		validId_01 = null;
		validId_02 = null;
		notValidId = "-99.000_99.000";
		
	}

	@Before
	public void setUp(){
	
	}

	@After
	public void tearDown(){
		
	}
	
//getRegion
	
	@Test 
	public void testGetRegion_should_return_region(){
		String expexted =validId_01;
		String actual="";
		try {
			Region region =regionDao.getEntityById(validId_01, Region.class);
			actual = region.getId();
		} catch (Exception ex){
			System.out.println(ex);
		}
		Assert.assertEquals(actual,expexted);
	}
	
	
	
	@Test
	public void testGetRegion_should_return_null(){
		Boolean actual = false;
		Boolean expected = true;
		try {
			region_01 = regionDao.getEntityById(notValidId, Region.class);
		} catch (Exception e) {
			actual = true;
		}
		Assert.assertEquals(expected, actual);
	}
}
