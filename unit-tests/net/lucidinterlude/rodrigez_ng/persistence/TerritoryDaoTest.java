package net.lucidinterlude.rodrigez_ng.persistence;

import net.lucidinterlude.rodrigez_ng.model.Territory;
import net.lucidinterlude.rodrigez_ng.utility.FrameWorkHelper;
import net.lucidinterlude.rodrigez_ng.application.AllTests;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.restlet.ext.json.JsonRepresentation;
import org.json.JSONException;
import org.json.JSONObject;

 
public class TerritoryDaoTest {
	static TerritoryDao territoryDao;
	static Territory territory_01;
	static Territory territory_02;
	static String jsonEscapedTerritory_01 = "{\"name\":\"Bellevue\",\"owner\":\"macPro\",\"territoryType\":\"FREE\",\"itFactor\":1,\"region\":\"zurich\",\"gridInterval\":\"0.0512\"}";
	static JsonRepresentation jsonRepTerritory_01 = new JsonRepresentation(jsonEscapedTerritory_01); 
	static JSONObject jsonObject = new JSONObject();
	
	
	
	static String id_01 = "0.0256_-1.0552";
	static String id_02 = "-7.0156_-1.0552";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
		AllTests.helper.setUp();
		territoryDao = new TerritoryDao();
		territory_01 = new Territory(id_01);
		territory_01.setSpacing(0.0256);
		territory_01.setId(id_01);
		territory_01.setOwner("macPro");
		territory_01.setName("Bellevue");
		territory_01.setRegion("Zurich");
		territory_01.setStatus("OCCUPIED");
		
		territory_02 = new Territory(id_02);
		territory_02.setSpacing(0.0002);
		territory_02.setId(id_02);
		territory_02.setOwner("");
		territory_02.setName("Bahnhof");
		territory_02.setRegion("Zurich");
		territory_02.setStatus("SAVEPLACE");
		
		try {
			Territory territory = territoryDao.setTerritory(territory_01);
			System.out.println(territory.getId());
		} catch (Exception e) {
			e.printStackTrace();
		}
	
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		try {
			territoryDao.deleteEntityWithNoRelation(id_01, Territory.class);
			territoryDao.deleteEntityWithNoRelation(id_02, Territory.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		AllTests.helper.tearDown();
		territoryDao = null;
	}


	@Before
	public void setUp(){
		
	}
	
	@After
	public void tearDown(){
		
	}
	
	@Test
	public void testUpdateTerritory_should_not_throw_exception(){
		FrameWorkHelper.testcaseOut("testUpdateTerritory");
		Boolean result = false;
		try {
			jsonObject = jsonRepTerritory_01.getJsonObject();
			result = true;
		} catch (JSONException e) {
		}
		try {
			territoryDao.updateEntityWithNoRelation(id_01, jsonObject, Territory.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		Assert.assertTrue(result);
	}
	
	@Test
	public void testSetTerritory_should_not_throw_exception(){
		FrameWorkHelper.testcaseOut("testSetTerritory");
		Boolean result = false;
		try {
			territoryDao.setTerritory(territory_02);
			result = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		Assert.assertTrue(result);
	}
	
	@Test
	public void testDeleteTerritory_should_not_throw_exception() {
		FrameWorkHelper.testcaseOut("testDeleteTerritory");
		Boolean result = false;
		try {
			territoryDao.deleteEntityWithNoRelation(id_01, Territory.class);
			result = true;
		} catch (Exception e) {
		}
		Assert.assertTrue(result);
	}	
		
	
}
