package net.lucidinterlude.rodrigez_ng.application;

import net.lucidinterlude.rodrigez_ng.dto.ResponseDto;
import net.lucidinterlude.rodrigez_ng.dto.UserDto;
import net.lucidinterlude.rodrigez_ng.model.User;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.restlet.data.Status;
import org.restlet.ext.json.JsonRepresentation;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class UserBoTest {

	private static UserBO cut;
	private static JsonRepresentation jsonRepresentation_01;
	private static JsonRepresentation jsonRepresentation_02;
	private Gson gson; 
	private UserDto userDto_01;
	private UserDto userDto_02;
	private static String id = null;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		AllTests.helper.setUp();
		gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssz").create();
		cut  = new UserBO();
		userDto_01 = new UserDto();
		userDto_02 = new UserDto();
		userDto_01.setNickName("bla");
		userDto_02.setNickName("blu");
		jsonRepresentation_01 = new JsonRepresentation(gson.toJson(userDto_01));	
		jsonRepresentation_02 = new JsonRepresentation(gson.toJson(userDto_02));
	}

	@After
	public void tearDown() throws Exception {
		AllTests.helper.tearDown();
	}

	@Test
	public void testCreateUser_shouldReturnId() {
		ResponseDto<?> responseDto = cut.createUser(jsonRepresentation_01);
		id = (String) responseDto.getResponseContent("id");
		Assert.assertNotNull(id);
	}
	
	@Test
	public void testGetUser_shouldReturn_UserDto() throws Exception{
		User responseDtoUser =  cut.getUser(id);
		Assert.assertEquals(userDto_01.getNickName(),responseDtoUser.getNickName());
	}
	
	@Test
	public void testUpdateUser_shouldReturn_Status_Error() {
		ResponseDto<?> responseDto_02 = cut.createUser(jsonRepresentation_02);
		String id = (String) responseDto_02.getResponseContent("id");
		userDto_02.setNickName(userDto_01.getNickName());
		jsonRepresentation_02 = new JsonRepresentation(gson.toJson(userDto_02));
		ResponseDto<?> responseDto2 = cut.updateUser(id, jsonRepresentation_02);
		Assert.assertEquals(Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY, responseDto2.getResponse().getStatus());	
	}
	
	@Test
	public void testCheckNickName_shouldReturn_Status_Success() {
		ResponseDto<?>  responseDto = cut.checkNickname("bla");
		Assert.assertEquals(Status.SUCCESS_OK, responseDto.getResponse().getStatus());
	}
	
	@Test
	public void testCheckNickName_shouldReturn_Status_Error() {
		ResponseDto<?>  responseDto = cut.checkNickname("bli");
		Assert.assertEquals(Status.CLIENT_ERROR_NOT_FOUND, responseDto.getResponse().getStatus());
	}
	
}
