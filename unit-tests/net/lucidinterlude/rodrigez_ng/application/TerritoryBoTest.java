package net.lucidinterlude.rodrigez_ng.application;

import java.util.ArrayList;
import java.util.HashMap;

import net.lucidinterlude.rodrigez_ng.dto.TerritoryDto;
import net.lucidinterlude.rodrigez_ng.model.Territory;
import net.lucidinterlude.rodrigez_ng.utility.FrameWorkHelper;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.restlet.ext.json.JsonRepresentation;

public class TerritoryBoTest {
	
	private static String jsonEscapedTerritories = "{\"territories\":[{\"id\":\"0.1_0.1\",\"name\":\"Bellevue\",\"owner\":\"\",\"status\":\"OCCUPIED\",\"itFactor\":0.1,\"region\":\"c\",\"rasterWidth\":\"0.0112\",\"geoPoints\":[{\"latitude\":0.0,\"longitude\":0.0},{\"latitude\":0.0256,\"longitude\":0.0},{\"latitude\":0.0256,\"longitude\":0.0256},{\"latitude\":0.0,\"longitude\":0.0256}]},{\"id\":\"0.2_0.2\",\"name\":\"HB\",\"owner\":\"\",\"status\":\"FREE\",\"itFactor\":0.1,\"region\":\"c\",\"rasterWidth\":\"0.0512\",\"geoPoints\":[{\"latitude\":0.1,\"longitude\":0.1},{\"latitude\":0.612,\"longitude\":0.1},{\"latitude\":0.612,\"longitude\":0.612},{\"latitude\":0.1,\"longitude\":0.612}]},{\"id\":\"0.256_1.3824\",\"name\":\"\",\"owner\":\"\",\"status\":\"FREE\",\"itFactor\":0.1,\"region\":\"c\",\"rasterWidth\":\"0.0512\",\"geoPoints\":[{\"latitude\":0.256,\"longitude\":1.3824},{\"latitude\":0.3072,\"longitude\":1.3824},{\"latitude\":0.3072,\"longitude\":1.4592},{\"latitude\":0.256,\"longitude\":1.4592}]},{\"id\":\"0.3072_1.3824\",\"name\":\"\",\"owner\":\"\",\"status\":\"FREE\",\"itFactor\":0.1,\"region\":\"c\",\"rasterWidth\":\"0.0512\",\"geoPoints\":[{\"latitude\":0.3072,\"longitude\":1.3824},{\"latitude\":0.3584,\"longitude\":1.3824},{\"latitude\":0.3584,\"longitude\":1.4592},{\"latitude\":0.3072,\"longitude\":1.4592}]},{\"id\":\"0.3584_1.3824\",\"name\":\"\",\"owner\":\"\",\"status\":\"FREE\",\"itFactor\":0.1,\"region\":\"c\",\"rasterWidth\":\"0.0512\",\"geoPoints\":[{\"latitude\":0.3584,\"longitude\":1.3824},{\"latitude\":0.4096,\"longitude\":1.3824},{\"latitude\":0.4096,\"longitude\":1.4592},{\"latitude\":0.3584,\"longitude\":1.4592}]},{\"id\":\"0.256_1.4592\",\"name\":\"\",\"owner\":\"\",\"status\":\"FREE\",\"itFactor\":0.1,\"region\":\"c\",\"rasterWidth\":\"0.0512\",\"geoPoints\":[{\"latitude\":0.256,\"longitude\":1.4592},{\"latitude\":0.3072,\"longitude\":1.4592},{\"latitude\":0.3072,\"longitude\":1.536},{\"latitude\":0.256,\"longitude\":1.536}]},{\"id\":\"0.3072_1.4592\",\"name\":\"\",\"owner\":\"\",\"status\":\"FREE\",\"itFactor\":0.1,\"region\":\"c\",\"rasterWidth\":\"0.0512\",\"geoPoints\":[{\"latitude\":0.3072,\"longitude\":1.4592},{\"latitude\":0.3584,\"longitude\":1.4592},{\"latitude\":0.3584,\"longitude\":1.536},{\"latitude\":0.3072,\"longitude\":1.536}]},{\"id\":\"0.3584_1.4592\",\"name\":\"\",\"owner\":\"\",\"status\":\"FREE\",\"itFactor\":0.1,\"region\":\"c\",\"rasterWidth\":\"0.0512\",\"geoPoints\":[{\"latitude\":0.3584,\"longitude\":1.4592},{\"latitude\":0.4096,\"longitude\":1.4592},{\"latitude\":0.4096,\"longitude\":1.536},{\"latitude\":0.3584,\"longitude\":1.536}]}]}";
	//private static String jsonEscapedTerritory_01 = "{\"id\":\"0_0\",\"name\":\"Bellevue\",\"owner\":\"macPro\",\"territoryType\":\"FREE\",\"itFactor\":1,\"region\":\"zurich\",\"gridInterval\":\"0.0512\"}";
	
	private static JsonRepresentation jsonRepTerritories = new JsonRepresentation(jsonEscapedTerritories); 
	//private static JsonRepresentation jsonRepTerritory_01 = new JsonRepresentation(jsonEscapedTerritory_01); 
	
	private static TerritoryBO territoryBO;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		AllTests.helper.setUp();
		territoryBO = new TerritoryBO();
		try {
			territoryBO.setTerritory(jsonRepTerritories);
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		try {
			territoryBO.deleteTerritory("0.1_0.1");
			territoryBO.deleteTerritory("0.3584_1.4592");
			territoryBO.deleteTerritory("0.3072_1.4592");
			territoryBO.deleteTerritory("0.256_1.4592");
			territoryBO.deleteTerritory("0.3584_1.3824");
			territoryBO.deleteTerritory("0.3072_1.3824");
			territoryBO.deleteTerritory("0.256_1.3824");
			territoryBO.deleteTerritory("0.2_0.2");
		} catch (Exception e) {
			e.printStackTrace();
		}
		AllTests.helper.tearDown();
	}

	@Before
	public void setUpGetTerritory(){
		
	}
	
	@After
	public void tearDownTestSetTerritory(){
	
	}
	
//get territory
	

	@Test
	public void testGetTerritoryFromDataStore_shouldReturnTerritory() {
		FrameWorkHelper.testcaseOut("testGetTerritoryFromDatastore_shouldReturnTerritory");
		String expected ="HB";
		String actual="";
		
		try {
			Territory territory = territoryBO.getTerritoryFromDataStore("0.2_0.2");
			actual = territory.getName();
		} catch (Exception e) {
			e.printStackTrace();
		}
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	public void testGetTerritory_should_return_territory(){
		FrameWorkHelper.testcaseOut("testGetTerritory_should_return_territory");
		String expected ="Bellevue";
		String actual="";
		try {
			TerritoryDto territoryDto = territoryBO.getTerritoryFromIndex("0.1_0.1");
			actual = territoryDto.getName();
		} catch (Exception e) {
			e.printStackTrace();
		}
		Assert.assertEquals(expected, actual);
		
	}
	
	@Test
	public void testGetTerritoryByParams_should_return_territory() {
		FrameWorkHelper.testcaseOut("testGetTerritoryByParams_should_return_territory");
		HashMap<String, String> paramMap = new HashMap<String,String>();
		paramMap.put("name", "Bellevue");
		String expected ="0.1_0.1";
		String actual ="";
		try {
			ArrayList<TerritoryDto> results = territoryBO.getTerritoriesByParams(paramMap);
			actual = results.get(0).getId();
		} catch (Exception e) {
			e.printStackTrace();
		}
		Assert.assertEquals(expected,actual);
	}
	/*
	@Test
	public void testGetAllTerritories_should_return_territoryList() {
		FrameWorkHelper.testcaseOut("testGetAllTerritories_should_return_territoryList");
		int expected = 8;
		int actual = 0;
		
		try {
			ArrayList<TerritoryDto> results  = territoryBO.getAllTerritories();
			actual = results.size();
		} catch (Exception e) {
			e.printStackTrace();
		}
		Assert.assertEquals(expected, actual);
	}*/
	
	@Test
	public void testGetTerritory_should_return_null(){
		FrameWorkHelper.testcaseOut(" testGetTerritory_should_return_null");
		Territory territory = null;
		try {
			 territory = territoryBO.getTerritoryFromDataStore("1234_1234");
			
		} catch (Exception e) {
			
		}
		Assert.assertNull(territory);
	}
	
}
