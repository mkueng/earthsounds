package net.lucidinterlude.rodrigez_ng.application;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;

import net.lucidinterlude.rodrigez_ng.game.GameHandlerTest;
import net.lucidinterlude.rodrigez_ng.model.User;
import net.lucidinterlude.rodrigez_ng.persistence.EntityCreatorTest;
import net.lucidinterlude.rodrigez_ng.persistence.TerritoryDaoTest;
import net.lucidinterlude.rodrigez_ng.persistence.UserDaoTest;

@RunWith(Suite.class)
@SuiteClasses({
	EntityCreatorTest.class,
	UserDaoTest.class,
	TerritoryDaoTest.class,
	GameHandlerTest.class
})
 
public class AllTests {
	
	public static User user_01;
	public static User user_02;
	
	public static final LocalServiceTestHelper helper =
			new LocalServiceTestHelper(new LocalDatastoreServiceTestConfig()
            .setDefaultHighRepJobPolicyUnappliedJobPercentage(100));
	
	 @BeforeClass 
	    public static void setUpClass() {      
	      	
	    }

	    @AfterClass public static void tearDownClass() { 
	    
	    }

		public static User getUser_01() {
			return user_01;
		}

		public static void setUser_01(User user_01) {
			AllTests.user_01 = user_01;
		}

		public static User getUser_02() {
			return user_02;
		}

		public static void setUser_02(User user_02) {
			AllTests.user_02 = user_02;
		}
	    
	    
}
